* STM c8jpg properties

Property description for c8jpg class devices.

Required Properties:
- compatiable: should be one of the following.
    * "st,c8jpg": for Jpeg decoding IP.

- reg: physical memory area reserved for current IP.
    * <base_address size>
- reg-names: respective names for the physical memory.
    * "c8jpg-io"

- interrupts: interrupt numbers.
    * <0 X 0>
- interrupt-names: respective names for the interruptions.
    * "c8jpg-int"

Example:
/* C8JPG */
c8jpg@08c84000{
	compatible = "st,c8jpg";
	status = "disabled";

	/* Register map & Name */
	reg = <0x08c84000 0x1000>;
	reg-names = "c8jpg-io";

	/* Interruption & Name */
	interrupts = <0 56 0>;
	interrupt-names = "c8jpg-int";
};

