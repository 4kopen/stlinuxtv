/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef _STMCEC_H
#define _STMCEC_H

#include <linux/types.h>
/*
 * The following structure contents delivered on the cec bus using the read and write fops
 *
 */
struct stmcecio_msg {
	unsigned char data[16];
	unsigned char len;
};

/*
 * The following structure contents passed using STMCECIO_SET_LOGICAL_ADDRESS_ID will
 * change the Device's logical address used by the driver to recieve msgs intended for it.
 *
 */
struct stmcecio_logical_addr {
	unsigned char addr;
	unsigned char enable;	/* 1: enable/adds the given logical address,  0:disable the given logical address  */
};

/*
 * The following structure describes the event delivered by the device.
 *
 */

struct stmcec_event {
	__u32 type;
	union {
		__u32 id;
		__u32 reserved[8];
	};
};

#define STMCEC_EVENT_MSG_RECEIVED          	(1 << 0)
#define STMCEC_EVENT_MSG_ERROR_RECEIVED     (1 << 1)

/*
 * The following structure describes the event subscription for a given type.
 *
 */

struct stmcec_event_subscription {
	__u32 type;
	__u32 id;
};

#define STMCECIO_SET_LOGICAL_ADDRESS     _IOW ('C', 0x1, struct stmcecio_logical_addr)
#define STMCECIO_GET_LOGICAL_ADDRESS     _IOR ('C', 0x2, struct stmcecio_logical_addr)

#define STMCECIO_DQEVENT                 _IOR('C', 0x3, struct stmcec_event)
#define STMCECIO_SUBSCRIBE_EVENT         _IOW('C', 0x4, struct stmcec_event_subscription)
#define STMCECIO_UNSUBSCRIBE_EVENT       _IOW('C', 0x5, struct stmcec_event_subscription)

#endif /* _STMCEC_H */
