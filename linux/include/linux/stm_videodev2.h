/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Header containing the v4l2 definitions
************************************************************************/
#ifndef __STM_VIDEODEV2_H__
#define __STM_VIDEODEV2_H__
#include <linux/videodev2.h>
#include <stdbool.h>

/* ==================== Video event definitions ====================== */

/* enum v4l2_field STMicroElectronics 3D extensions  */
enum v4l2_field_sti {
	V4L2_FIELD_STI_EXTENSIONS_START = (V4L2_FIELD_INTERLACED_BT + 1),
	V4L2_FIELD_STI_3D_FRAME_PACK,		/* 3D progressive packed frame */
	V4L2_FIELD_STI_3D_FRAME_PACK_INT,	/* 3D interlaced packed frame with any field order */
	V4L2_FIELD_STI_3D_FRAME_PACK_TB,	/* 3D interlaced packed frame (Top/Odd first) */
	V4L2_FIELD_STI_3D_FRAME_PACK_BT,	/* 3D interlaced packed frame (Bottom first) */

	V4L2_FIELD_STI_3D_SBS_HALF,		/* 3D progressive Side by Side */
	V4L2_FIELD_STI_3D_SBS_HALF_INT,		/* 3D interlaced Side by Side with any field order */
	V4L2_FIELD_STI_3D_SBS_HALF_TB,		/* 3D interlaced Side by Side  (Top/Odd first) */
	V4L2_FIELD_STI_3D_SBS_HALF_BT,		/* 3D interlaced Side by Side  (Bottom/Even first) */

	V4L2_FIELD_STI_3D_TAB,			/* 3D progressive Top and bottom */
	V4L2_FIELD_STI_3D_TAB_INT,		/* 3D interlaced Top and bottom with any field order */
	V4L2_FIELD_STI_3D_TAB_TB,		/* 3D interlaced Top and bottom, Top/Odd lines first */
	V4L2_FIELD_STI_3D_TAB_BT,		/* 3D interlaced Top and bottom Bottom/Even lines first */
};

/*
 * struct v4l2_sti_st2086_metadata
 * @display_primaries_x_<num>      : specifies the normalized x chromaticity coordinate
 * 				     of the colour primary component c=<num> of the mastering
 * 				     display in increments of 0.00002, according to the
 * 				     CIE 1931 definition of x as specified in ISO 11664-1
 * 				     (see also ISO 11664-3 and CIE 15). The value is coded
 * 				     as unsigned 16-bit values in units of 0.00002, where
 * 				     0x0000 represents zero and 0xC350 represents 1.0000.
 * 				     When value is set to zero, then the Sink shall interpret
 * 				     these values as unknown.
 *
 * @display_primaries_y_<num>      : specifies the normalized y chromaticity coordinate of the
 * 				     colour primary component c=<num> of the mastering display in
 * 				     increments of 0.00002, according to the CIE 1931 definition
 * 				     of y as specified in ISO 11664-1 (see also ISO 11664-3 and
 * 				     CIE 15). The value is coded as unsigned 16-bit values in
 * 				     units of 0.00002, where 0x0000 represents zero and 0xC350
 * 				     represents 1.0000. When value is set to zero, then the
 * 				     Sink shall interpret these values as unknown.
 *
 * @white_point_x                  : specifies the normalized x chromaticity coordinate of the white
 * 				     point of the mastering display in normalized increments of 0.00002,
 * 				     according to the CIE 1931 definition of x as specified in ISO
 * 				     11664-1 (see also ISO 11664-3 and CIE 15). The value is coded as
 * 				     unsigned 16-bit values in units of 0.00002, where 0x0000 represents
 * 				     zero and 0xC350 represents 1.0000. When value is set to zero,
 * 				     then the Sink shall interpret these values as unknown.
 *
 * @white_point_y                  : specifies the normalized y chromaticity coordinate of the white
 * 				     point of the mastering display in normalized increments of 0.00002,
 * 				     according to the CIE 1931 definition of y as specified in ISO 11664-1
 * 				     (see also ISO 11664-3 and CIE 15). The value is coded as unsigned
 * 				     16-bit values in units of 0.00002, where 0x0000 represents zero and
 * 				     0xC350 represents 1.0000. When value is set to zero, then the Sink
 * 				     shall interpret these values as unknown.
 *
 * @max_display_mastering_luminance: specifies the nominal maximum display luminance of the mastering
 * 				     display. This value is coded as an unsigned 16-bit value in units
 * 				     of 1 cd/m2, where 0x0001 represents 1 cd/m2 and 0xFFFF represents
 * 				     65535 cd/m2.
 *
 * @min_display_mastering_luminance: specifies the nominal minimum display luminance of the mastering display.
 * 				     This value is coded as an unsigned 16-bit value in units of 0.0001 cd/m2,
 * 				     where 0x0001 represents 0.0001 cd/m2 and 0xFFFF represents 6.5535 cd/m2.
 */
struct v4l2_sti_st2086_metadata {
	__u16 display_primaries_x_0;
	__u16 display_primaries_y_0;
	__u16 display_primaries_x_1;
	__u16 display_primaries_y_1;
	__u16 display_primaries_x_2;
	__u16 display_primaries_y_2;
	__u16 white_point_x;
	__u16 white_point_y;
	__u16 max_display_mastering_luminance;
	__u16 min_display_mastering_luminance;
};

/*
 * struct v4l2_sti_hdr_metadata
 * @maxCLL : Maximum Content Light Level, this value is coded as an unsigned 16-bit
 * 	     value in units of 1 cd/m2, where 0x0001 represents 1 cd/m2 and 0xFFFF
 * 	     represents 65535 cd/m2. When MaxCLL is set to zero, then the Sink shall
 * 	     interpret this value as unknown.
 *
 * @maxFALL: Maximum Frame-Average Light Level, this value is coded as an unsigned
 * 	     16-bit value in units of 1 cd/m2, where 0x0001 represents 1 cd/m2 and
 * 	     0xFFFF represents 65535 cd/m2. When MaxCLL is set to zero, then the
 * 	     Sink shall interpret this value as unknown.
 */
struct v4l2_sti_hdr_metadata {
	__u16 maxCLL;
	__u16 maxFALL;
};

/* enum v4l2_sti_xfer_func STMicroElectronics extensions */
enum v4l2_sti_xfer_func {
	V4L2_STI_XFER_FUNC_GAMMA_HDR = V4L2_XFER_FUNC_NONE + 1, /* Traditional gamma - HDR Luminance Range */
};

/*
 * struct v4l2_sti_hdr_format
 * @eotf_type                 : EOTF type
 * @is_st2086_metadata_present: If set to true, st2086_metadata field info is valid.
 * @st2086_metadata           : Display Mastering metadata defined for SMPTE ST 2086 (relevant for any EOTF type)
 * @is_hdr_metadata_present   : If set to true, hdr_metadata field info is valid
 * @hdr_metadata              : metadata only relevant for HDR EOTF type only (SMPTE 2084 or HDR gamma)
 */
struct v4l2_sti_hdr_format {
	enum v4l2_xfer_func eotf_type;
	bool is_st2086_metadata_present;
	struct v4l2_sti_st2086_metadata st2086_metadata;
	bool is_hdr_metadata_present;
	struct v4l2_sti_hdr_metadata hdr_metadata;
};

/*
 * ============= Audio uncompressed data declarations ==============
 */
/*
 * enum v4l2_sti_emphasis_type
 * This type describes the level of emphasis present in an audio stream.
 */
enum v4l2_sti_emphasis_type {
       V4L2_STI_NO_EMPHASIS,
       V4L2_STI_EMPHASIS_50_15us,
       V4L2_STI_EMPHASIS_CCITT_J_17
};

/*
 * enum v4l2_sti_audio_channel_id
 * Gives info about "struct v4l2_audio_uncompressed_metadata@channel"
 */
enum v4l2_sti_audio_channel_id {
       V4L2_STI_AUDIO_CHANNEL_L,               /* Left */
       V4L2_STI_AUDIO_CHANNEL_R,               /* Right */
       V4L2_STI_AUDIO_CHANNEL_LFE,             /* Low Frequency Effect */
       V4L2_STI_AUDIO_CHANNEL_C,               /* Centre */
       V4L2_STI_AUDIO_CHANNEL_LS,              /* Left Surround */
       V4L2_STI_AUDIO_CHANNEL_RS,              /* Right Surround */

       V4L2_STI_AUDIO_CHANNEL_LT,              /* Surround Matrixed Left */
       V4L2_STI_AUDIO_CHANNEL_RT,              /* Surround Matrixed Right */
       V4L2_STI_AUDIO_CHANNEL_LPLII,           /* DPLII Matrixed Left */
       V4L2_STI_AUDIO_CHANNEL_RPLII,           /* DPLII Matrixed Right */

       V4L2_STI_AUDIO_CHANNEL_CREAR,           /* Rear Centre */

       V4L2_STI_AUDIO_CHANNEL_CL,              /* Centre Left */
       V4L2_STI_AUDIO_CHANNEL_CR,              /* Centre Right */

       V4L2_STI_AUDIO_CHANNEL_LFEB,            /* Second LFE */

       V4L2_STI_AUDIO_CHANNEL_L_DUALMONO,      /* Dual-Mono Left */
       V4L2_STI_AUDIO_CHANNEL_R_DUALMONO,      /* Dual-Mono Right */

       V4L2_STI_AUDIO_CHANNEL_LWIDE,           /* Wide Left */
       V4L2_STI_AUDIO_CHANNEL_RWIDE,           /* Wide Right */

       V4L2_STI_AUDIO_CHANNEL_LDIRS,           /* Directional Surround left */
       V4L2_STI_AUDIO_CHANNEL_RDIRS,           /* Directional Surround Right */

       V4L2_STI_AUDIO_CHANNEL_LSIDES,          /* Side Surround Left */
       V4L2_STI_AUDIO_CHANNEL_RSIDES,          /* Side Surround Right */

       V4L2_STI_AUDIO_CHANNEL_LREARS,          /* Rear Surround Left */
       V4L2_STI_AUDIO_CHANNEL_RREARS,          /* Rear Surround Right */

       V4L2_STI_AUDIO_CHANNEL_CHIGH,           /* High Centre */
       V4L2_STI_AUDIO_CHANNEL_LHIGH,           /* High Left */
       V4L2_STI_AUDIO_CHANNEL_RHIGH,           /* High Right */
       V4L2_STI_AUDIO_CHANNEL_LHIGHSIDE,       /* High Side Left */
       V4L2_STI_AUDIO_CHANNEL_RHIGHSIDE,       /* High Side Right */
       V4L2_STI_AUDIO_CHANNEL_CHIGHREAR,       /* High Rear Centre */
       V4L2_STI_AUDIO_CHANNEL_LHIGHREAR,       /* High Rear Left */
       V4L2_STI_AUDIO_CHANNEL_RHIGHREAR,       /* High Rear Right */

       V4L2_STI_AUDIO_CHANNEL_CLOWFRONT,       /* Low Front Centre */
       V4L2_STI_AUDIO_CHANNEL_TOPSUR,          /* Surround Top */

       V4L2_STI_AUDIO_CHANNEL_DYNSTEREO_LS,    /* Dynamic Stereo Left Surround */
       V4L2_STI_AUDIO_CHANNEL_DYNSTEREO_RS,    /* Dynamic Stereo Right Surround */

       /*
        * Last channel id with a defined positioning
        */
       V4L2_STI_AUDIO_CHANNEL_LAST_NAMED = V4L2_STI_AUDIO_CHANNEL_DYNSTEREO_RS,

       /*
        * Id for a channel with valid content but no defined positioning
        */
       V4L2_STI_AUDIO_CHANNEL_UNKNOWN,

       /*
        * Id for the last channel id with valid content
        */
       V4L2_STI_AUDIO_CHANNEL_LAST_VALID = V4L2_STI_AUDIO_CHANNEL_UNKNOWN,

       /*
        * This channel id is to indicate that although it is allocated in the
        * buffer it is not valid content. There is no guarantee that the content
        * of a STUFFING channel be set to 0.
        *
        * Channels identified as V4L2_STI_AUDIO_CHANNEL_STUFFING at the input of
        * the process should be ignored. If a process intends to use a STUFFING
        * channel (e.g. to send a buffer "as is" to a FDMA playback) it should
        * mute them.
        * For example if the buffer is allocated for 5.1 channels but the content
        * is stereo, there will be 4 V4L2_STI_AUDIO_CHANNEL_STUFFING ids.
        */
       V4L2_STI_AUDIO_CHANNEL_STUFFING = 254,

       /*
        * All values above V4L2_STI_AUDIO_CHANNEL_STUFFING are reserved, because we will
        * case to uint8_t
        */
       V4L2_STI_AUDIO_CHANNEL_RESERVED = 255
};

#define V4L2_STM_AUDIO_MAX_CHANNELS 32
#define V4L2_STM_AUDENC_MAX_CHANNELS V4L2_STM_AUDIO_MAX_CHANNELS

/*
 * struct v4l2_audio_uncompressed_metadata
 * @sample_rate  : samples per second (Hz)
 * @channel_count: Number of channels (8 by default)
 * @channel      : To be parsed till @channel_count
 * @sample_format: PCM sample format. In the current release it is 0 indicating
 *                 PCM Signed 32bit LE. Any future extension will be documented
 *                 further
 * @program_level: Expressed in millibels, describes the average loudness of the
 *                 data this metadata applies to. Typical values are -3100 (-31dB),
 *                 -2000 (-21dB), -1800 (-18dB).
 * @emphasis     : Describes whether any type of pre-emphasis has been applied on
 *                 this buffer. See enum v4l2_sti_emphasis_type
 */
struct v4l2_audio_uncompressed_metadata {
       unsigned int  sample_rate;
       int           channel_count;
       unsigned char channel[V4L2_STM_AUDIO_MAX_CHANNELS];
       unsigned int  sample_format;
       int           program_level;
       unsigned int  emphasis;
};

/*
 * ============= Video uncompressed data declarations ==============
 */
/*
 * struct v4l2_video_uncompressed_metadata
 * @framerate_num: framerate numerator
 * @framerate_den: framerate denominator
 * @framerate_num/@framerate_den gives fps
 */
struct v4l2_video_uncompressed_metadata {
       unsigned int framerate_num;
       unsigned int framerate_den;
       unsigned int reserved[8];

       /* unsigned int width;
          unsigned int height;
          unsigned int pitch;
          unsigned int vertical_alignment;
          unsigned int noise_filter;
          unsigned int colorspace;
          unsigned int pixelformat */
};

/*
 * ============= User-data uncompressed data declarations ==============
 */
/*
 * struct v4l2_sti_raw_user_data
 * @id                 : 'STUD'
 * @block_length       : Value of the whole block length
 * @header_length      : Value of the whole header length
 * @reserved_1         : 1-> if this packet may is collected from a spoiled or
 *                       uncontiguous bitstream, 0 otherwise.
 * @num_padding_bytes  : number of padding bytes before actual payload
 * @stream_abridgement :
 * @overflow           : 1 -> if this packet id is the first one after a discontinuity
 *                      resulting from a too low providing of buffers through the memsink
 * @codec_id           : 0 -> if MPEG2, 1 -> if H264/HEVC
 * @reserved_2         :
 * @is_there_a_pts     : 1 -> if pts_msb and pts reflect a PTS belonging to the picture
 *                      the  user data is collected from.
 * @is_pts_interpolated: 1 -> if there is missing pts from the picture the user data is
 *                      collected from. It is then internally calculated
 * @reserved_3         :
 * @pts_msb            : pts 33 bits most significant bit
 * @pts                : pts 32 bits less significant bits
 */
struct v4l2_sti_raw_user_data {
       char id[4];
       unsigned block_length:16;
       unsigned header_length:16;
       unsigned reserved_1:1;
       unsigned num_padding_bytes:5;
       unsigned stream_abridgement:1;
       unsigned overflow:1;
       unsigned codec_id:8;
       unsigned reserved_2:6;
       unsigned is_there_a_pts:1;
       unsigned is_pts_interpolated:1;
       unsigned reserved_3:7;
       unsigned pts_msb:1;
       unsigned pts:32;
};

/*
 * struct v4l2_sti_raw_user_data_mpeg2
 * If struct v4l2_sti_raw_user_data.codec_id == MPEG2 (0), then map this
 * structure over the user data post the sizeof(struct v4l2_sti_raw_user_data)
 */
struct v4l2_sti_user_data_mpeg2 {
       unsigned reserved:31;
       unsigned top_field_first:1;
};

/*
 * struct v4l2_sti_raw_user_data_h264 (h264/hevc)
 * If struct v4l2_sti_raw_user_data.codec_id == H264 (1), then map this
 * structure over the user data post the sizeof(struct v4l2_sti_raw_user_data)
 */
struct v4l2_sti_raw_user_data_h264 {
       unsigned reserved:31;
       unsigned is_registered:1;
       unsigned itu_t_t35_country_code:8;
       unsigned itu_t_t35_country_code_extension_byte:8;
       unsigned itu_t_t35_provider_code:16;
       char uuid_iso_iec_11578[16];
};
#endif
