/************************************************************************
Copyright (C) 2011 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.

Source file name : stm-dmx.h
Author :           Nick <nick.haydock@st.com>

Header for dmx ST proprietary ioctls

Date        Modification                                    Name
----        ------------                                    --------

 ************************************************************************/

#ifndef _STMDVBDMX_H_
#define _STMDVBDMX_H_
#include <linux/dvb/dmx.h>

struct dmx_pcr {
	unsigned long long pcr;	/* Returned in pts format */
	unsigned long long system_time;
};

typedef enum {
        DMX_INDEX_NONE                  = 0x0000, /* No event */
        DMX_INDEX_PUSI                  = 0x0001, /* Payload Unit Start Indicator Flag */
        DMX_INDEX_SCRAM_TO_CLEAR        = 0x0002, /* Scrambling Change To Clear Flag */
        DMX_INDEX_TO_EVEN_SCRAM         = 0x0004, /* Scrambling Change To Even Flag */
        DMX_INDEX_TO_ODD_SCRAM          = 0x0008, /* Scrambling Change To Odd Flag */
        DMX_INDEX_DISCONTINUITY         = 0x0010, /* Discontinuity Flag */
        DMX_INDEX_RANDOM_ACCESS         = 0x0020, /* Random Access Flag */
        DMX_INDEX_ES_PRIORITY		= 0x0040, /* ES Priority Flag */
        DMX_INDEX_PCR                   = 0x0080, /* PCR Flag*/
        DMX_INDEX_OPCR                  = 0x0100, /* OPCR Flag */
        DMX_INDEX_SPLICING_POINT        = 0x0200, /* Splicing Point Flag */
        DMX_INDEX_TS_PRIVATE_DATA       = 0x0400, /* Transport Private Data Flag */
        DMX_INDEX_ADAPTATION_EXT        = 0x0800, /* Adaptation Field Extension Flag */
        DMX_INDEX_FIRST_REC_PACKET      = 0x1000, /* First Recorded Packet */
        DMX_INDEX_START_CODE            = 0x2000, /* Index Start Code Flag */
        DMX_INDEX_PTS                   = 0x4000  /* PTS Flag */
} dmx_index_event;

struct dmx_index {
        __u16 pid;
        dmx_index_event event;
        unsigned int packet_count;
        unsigned long long pcr;
        unsigned long long system_time;
        unsigned char mpeg_start_code;
        unsigned char mpeg_start_code_offset;
        __u16 extra_bytes;
        unsigned char extra[5];
};

struct dmx_index_pid {
        __u16 pid;
       __u8 number_of_start_codes;
       __u8 *start_codes;
       dmx_index_event flags;
};

struct dmx_index_filter_params {
        unsigned int n_pids;
        struct dmx_index_pid *pids;
};

struct dmx_insert_filter_params {
	unsigned int pid;
	unsigned int freq_ms;
	unsigned char *data;
	unsigned int data_size;
};

struct dmx_replace_filter_params {
	unsigned int pid;
	unsigned char *data;
	unsigned int data_size;
};

#define DMX_PES_MARKER_SIZE	10

struct dmx_pes_marker_mdata {
	__u8 marker_type;
	__u8 size;
	__u8 user_data[DMX_PES_MARKER_SIZE];
};

enum dmx_marker_type {
	DMX_MARKER_CMD_TIMING_REQUEST,
	DMX_MARKER_BREAK_FORWARD,
	DMX_MARKER_BREAK_BACKWARD,
	DMX_MARKER_BREAK_BACKWARD_SMOOTH_REVERSE,
	DMX_MARKER_BREAK_END_OF_STREAM,
	DMX_MARKER_BREAK_SPLICING,
	DMX_MARKER_PTS_INTERVAL,
};


typedef enum {
	DMX_TS_AUTO		= 0x0000, /* Auto-detected by demux */
	DMX_TS_TYPE_DVB		= 0x0001, /* DVB type, 188 bytes MPEG TS packet */
	DMX_TS_TYPE_TTS		= 0x0002, /* 192 bytes TTS (time stamped transport stream) */
	DMX_TS_TYPE_SYS_B	= 0x0003, /* 130 bytes TS packet */
	DMX_TS_TYPE_SYS_B_TTS	= 0x0004, /* 134 bytes TS packet (4 bytes timestamp, 130 bytes payload) */
} dmx_ts_format_t;

typedef enum {
	DMX_SCRAMBLED,	/* Non descrambled data generated */
	DMX_DESCRAMBLED	/* Descrambled data generated */
} dmx_scrambling_t;

/*
 * Add an extension flag to avoid re-use of PID filter
 *
 */
#define DMX_GET_NEW_RESOURCE 0x800


/*
 * dmx_output_sti is an extension to dmx_output_t.
 * The usage of this output type comes for Fast Channel Change,
 * where, we want to connect PES cache'r to the demux filters.
 * This will allow to have PES cached as input to decoder.
 */
enum dmx_output_sti {
	DMX_OUT_SELECTOR = (DMX_OUT_TSDEMUX_TAP + 1),
};

/*
 * dmx ctrl structure is to mangage TE controls which cannot be
 * intrinsically decided upon filter creation or any other activity
 * @id        : ctrl id
 * @size      : normally 0 or don't care unless ctrl requires @data
 * @value     : integer value for control
 * @start_code: start codes to be given for GOP detection
 * @data      : compound control data(may be array of int, .. or a struct)
 * @output    : dmx_output_t, dmx_output_sti
 * @pes_type  : dmx_pes_type_t (audio/video/pcr)
 */
#define MAX_START_CODE		6

struct dmx_ctrl {
	__u32 id;
	__u32 size;
	union {
		__u32 value;
		__u8 start_code[MAX_START_CODE];
		void *data;
	};
	__u32 output;
	dmx_pes_type_t pes_type;
};

/*
 * TE controls exposed using the 'struct dmx_ctrl.id'
 */

/*
 * Following are the control ID's
 * @DMX_CTRL_PES_SELECTOR_MODE             : Selector mode
 * @DMX_CTRL_PES_READ_IN_QUANTIZATION_UNITS: PES output at PES boundaries or not
 * @DMX_CTRL_TS_INPUT_TYPE                 : Inform the TE about the incoming data
 * @DMX_CTRL_PS_INPUT_SUB_STREAM_ID        : Select the ProgramStream subs stream ID
 */
#define DMX_CTRL_PES_SELECTOR_MODE		0x1
/*
 * Above control takes the following values:
 * @DMX_CTRL_VALUE_PES_PASS_THROUGH_MODE: Demux'ed PES being injected to SE for decode, if
 * 					  selector is able to Fast Channel Change.
 * @DMX_CTRL_VALUE_PES_CACHED_MODE      : Demux'ed PES being cached. Set the control value to
 *                                        this for audio/pcr pes. For video, fill in
 *                                        the start codes.
 * @DMX_CTRL_VALUE_PES_FORCE_PASS_THROUGH_MODE: Demux'ed PES being injected to SE for decode,
 * 					        even if selector in unable to Fast Channel Change.
 */
enum dmx_ctrl_value_pes_mode {
	DMX_CTRL_VALUE_PES_PASS_THROUGH_MODE,
	DMX_CTRL_VALUE_PES_CACHED_MODE,
	DMX_CTRL_VALUE_PES_FORCE_PASS_THROUGH_MODE,
};

#define DMX_CTRL_PES_READ_IN_QUANTIZATION_UNITS	0x2
/*
 * Above control can take following values. This enum
 * is valid for all boolean controls available
 * @DMX_CTRL_VALUE_DISABLE: Disable the control
 * @DMX_CTRL_VALUE_ENABLE : Enable the control
 */
enum dmx_ctrl_value {
	DMX_CTRL_VALUE_DISABLE,
	DMX_CTRL_VALUE_ENABLE,
};

#define DMX_CTRL_TS_INPUT_TYPE			0x3
/*
 * Above control can take on following values
 * @DMX_CTRL_VALUE_TS_AUTO     : Auto-detected by demux
 * @DMX_CTRL_VALUE_TS_DVB      : DVB type, 188 bytes MPEG TS packet
 * @DMX_CTRL_VALUE_TS_TTS      : 192 bytes TTS (time stamped transport stream)
 * @DMX_CTRL_VALUE_TS_SYS_B    : 130 bytes TS packet
 * @DMX_CTRL_VALUE_TS_SYS_B_TTS: 134 bytes TS packet (4 bytes timestamp, 130 bytes payload)
 */
enum dmx_ctrl_value_ts_input_type {
	DMX_CTRL_VALUE_TS_AUTO,
	DMX_CTRL_VALUE_TS_DVB,
	DMX_CTRL_VALUE_TS_TTS,
	DMX_CTRL_VALUE_TS_SYS_B,
	DMX_CTRL_VALUE_TS_SYS_B_TTS,
};

/*
 * Control to Set Substream Id for PS demux
 */
#define DMX_CTRL_PS_INPUT_SUB_STREAM_ID		0x4

/*
 * This control is read only and will get the buffer
 * level of output buffer. @Usage:
 * id       = DMX_CTRL_OUTPUT_BUFFER_STATUS
 * pes_type = DMX_PES_AUDIO/DMX_PES_VIDEO/DMX_PES_PCR
 * value field will return bytes in buffer
 */
#define DMX_CTRL_OUTPUT_BUFFER_STATUS           0x5

/*
 * specify a PTS interval that is propagated inband with the data
 */
#define DMX_CTRL_SET_PTS_INTERVAL		0x06


/*
 * This control is used to enable dvb packet filtering based on transport priority bit
 */
#define DMX_CTRL_TSPRIORITY_BIT_FILTERING	0x7

/*
 * Above control can take on following values
 * @DMX_CTRL_VALUE_TS_PRIORITY_0_FILTERING         : Filter all packets with priority bit set to 0
 * @DMX_CTRL_VALUE_TS_PRIORITY_1_FILTERING         : Filter all packets with priority bit set to 1
 * @DMX_CTRL_VALUE_TS_PRIORITY_IGNORE_FILTERING    : Filter all packets without priority filtering
 */
enum dmx_ctrl_value_priority_bit_filtering_mode {
	DMX_CTRL_VALUE_TS_PRIORITY_0_FILTERING = 0,
	DMX_CTRL_VALUE_TS_PRIORITY_1_FILTERING = 1,
	DMX_CTRL_VALUE_TS_PRIORITY_IGNORE_FILTERING = 2
};

/*
 * ST defined IOCTL's on demux/dvr
 */
#define DMX_FLUSH_CHANNEL		_IO('o',   160)
#define DMX_SET_INDEX_FILTER		_IOW('o',  162, struct dmx_index_filter_params)
#define DMX_SET_INS_FILTER		_IOW('o',  163, struct dmx_insert_filter_params)
#define DMX_SET_REP_FILTER		_IOW('o',  164, struct dmx_replace_filter_params)
#define DMX_SET_TS_FORMAT		_IOW('o',  165, dmx_ts_format_t)
#define DMX_SET_SCRAMBLING		_IOW('o',  166, dmx_scrambling_t)
#define DMX_SET_CTRL			_IOW('o',  167, struct dmx_ctrl)
#define DMX_GET_CTRL			_IOW('o',  168, struct dmx_ctrl)

/*
 * When we disconnect one of the PES filters associated with the TS read, then the
 * default flushing behavior can be overridden by the flag DMX_TS_NO_FLUSH_ON_DETACH.
 * This will not flush demux filters when one of the PID filters is detached. This
 * flag is only applicable for TS buffer.
 */
#define DMX_TS_NO_FLUSH_ON_DETACH 0x8

#define DMX_PES_SCR0	DMX_PES_PCR0
#define DMX_PES_SCR1	DMX_PES_PCR1
#define DMX_PES_SCR2	DMX_PES_PCR2
#define DMX_PES_SCR3	DMX_PES_PCR3
#define DMX_PES_SCR4	DMX_PES_PCR4
#define DMX_PES_SCR5	DMX_PES_PCR5
#define DMX_PES_SCR6	DMX_PES_PCR6
#define DMX_PES_SCR7	DMX_PES_PCR7
#define DMX_PES_SCR8	DMX_PES_PCR8
#define DMX_PES_SCR9	DMX_PES_PCR9
#define DMX_PES_SCR10	DMX_PES_PCR10
#define DMX_PES_SCR11	DMX_PES_PCR11
#define DMX_PES_SCR12	DMX_PES_PCR12
#define DMX_PES_SCR13	DMX_PES_PCR13
#define DMX_PES_SCR14	DMX_PES_PCR14
#define DMX_PES_SCR15	DMX_PES_PCR15
#define DMX_PES_SCR16	DMX_PES_PCR16
#define DMX_PES_SCR17	DMX_PES_PCR17
#define DMX_PES_SCR18	DMX_PES_PCR18
#define DMX_PES_SCR19	DMX_PES_PCR19
#define DMX_PES_SCR20	DMX_PES_PCR20
#define DMX_PES_SCR21	DMX_PES_PCR21
#define DMX_PES_SCR22	DMX_PES_PCR22
#define DMX_PES_SCR23	DMX_PES_PCR23
#define DMX_PES_SCR24	DMX_PES_PCR24
#define DMX_PES_SCR25	DMX_PES_PCR25
#define DMX_PES_SCR26	DMX_PES_PCR26
#define DMX_PES_SCR27	DMX_PES_PCR27
#define DMX_PES_SCR28	DMX_PES_PCR28
#define DMX_PES_SCR29	DMX_PES_PCR29
#define DMX_PES_SCR30	DMX_PES_PCR30
#define DMX_PES_SCR31	DMX_PES_PCR31

#define DMX_PS_FILTERING DMX_TS_FILTERING

#endif
