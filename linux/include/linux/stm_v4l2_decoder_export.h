/************************************************************************
 * Copyright (C) 2014 STMicroelectronics. All Rights Reserved.
 *
 * This file is part of the STLinuxTV Library.
 *
 * STLinuxTV is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation.
 *
 * STLinuxTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with player2; see the file COPYING.  If not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * The STLinuxTV Library may alternatively be licensed under a proprietary
 * license from ST.
 * Definitions for STM V4L2 Decoder controls
 *************************************************************************/
#ifndef __STM_V4L2_DECODER_EXPORT_H__
#define __STM_V4L2_DECODER_EXPORT_H__

/*
 * ST specific decoder controls
 * @VV4L2_CID_MPEG_STI_CAPTURE_PROFILE: This control is to configure the decoding
 *                                      modes, when the input is coming from capture
 * @V4L2_CID_MPEG_STI_VIDEO_LATENCY   : This control is to get capture to render latency
 */
#define V4L2_CID_MPEG_STI_BASE			(V4L2_CTRL_CLASS_MPEG | 0x3000)
#define V4L2_CID_MPEG_STI_CAPTURE_PROFILE	(V4L2_CID_MPEG_STI_BASE + 1)
#define V4L2_CID_MPEG_STI_VIDEO_LATENCY		(V4L2_CID_MPEG_STI_BASE + 2)

#define V4L2_EVENT_CLASS_MPEG				(V4L2_EVENT_PRIVATE_START + 2 * 1000)
#define V4L2_EVENT_STI_PARAMETERS_CHANGED		(V4L2_EVENT_CLASS_MPEG + 1)
#define V4L2_EVENT_STI_FRAME_RATE_CHANGED		(V4L2_EVENT_CLASS_MPEG + 2)
#define V4L2_EVENT_STI_DECODER_STOPPED			(V4L2_EVENT_CLASS_MPEG + 3)
#define V4L2_EVENT_STI_VSYNC				(V4L2_EVENT_CLASS_MPEG + 4)
#define V4L2_EVENT_STI_FIRST_FRAME_ON_DISPLAY		(V4L2_EVENT_CLASS_MPEG + 5)
#define V4L2_EVENT_STI_FRAME_DECODED_LATE		(V4L2_EVENT_CLASS_MPEG + 6)
#define V4L2_EVENT_STI_DATA_DELIVERED_LATE		(V4L2_EVENT_CLASS_MPEG + 7)
#define V4L2_EVENT_STI_STREAM_UNPLAYABLE		(V4L2_EVENT_CLASS_MPEG + 8)
#define V4L2_EVENT_STI_TRICK_MODE_CHANGE		(V4L2_EVENT_CLASS_MPEG + 9)
#define V4L2_EVENT_STI_VSYNC_OFFSET_MEASURED		(V4L2_EVENT_CLASS_MPEG + 10)
#define V4L2_EVENT_STI_FATAL_ERROR			(V4L2_EVENT_CLASS_MPEG + 11)
#define V4L2_EVENT_STI_OUTPUT_SIZE_CHANGED		(V4L2_EVENT_CLASS_MPEG + 12)
#define V4L2_EVENT_STI_FATAL_HARDWARE_FAILURE		(V4L2_EVENT_CLASS_MPEG + 13)
#define V4L2_EVENT_STI_FRAME_DECODED			(V4L2_EVENT_CLASS_MPEG + 14)
#define V4L2_EVENT_STI_FRAME_RENDERED			(V4L2_EVENT_CLASS_MPEG + 15)
#define V4L2_EVENT_STI_STREAM_IN_SYNC			(V4L2_EVENT_CLASS_MPEG + 16)
#define V4L2_EVENT_STI_LOST				(V4L2_EVENT_CLASS_MPEG + 17)
#define V4L2_EVENT_STI_END_OF_STREAM			(V4L2_EVENT_CLASS_MPEG + 18)
#define V4L2_EVENT_STI_FRAME_STARVATION			(V4L2_EVENT_CLASS_MPEG + 19)
#define V4L2_EVENT_STI_FRAME_SUPPLIED			(V4L2_EVENT_CLASS_MPEG + 20)
#define V4L2_EVENT_STI_PIXEL_ASPECT_RATIO_CHANGED	(V4L2_EVENT_CLASS_MPEG + 21)
#define V4L2_EVENT_STI_HDR_FORMAT_CHANGED		(V4L2_EVENT_CLASS_MPEG + 22)
#define V4L2_EVENT_STI_COLORIMETRY_CHANGED		(V4L2_EVENT_CLASS_MPEG + 23)

/*
 * V4L2_EVENT_STI_PIXEL_ASPECT_RATIO_CHANGED: To be read from v4l2_event.u.data
 */
struct v4l2_sti_ratio {
	__u32 numerator;
	__u32 denominator;
};

/*
 * Audio encoding
 */
enum v4l2_sti_audio_encoding {
	V4L2_STI_AUDIO_ENCODING_AUTO,
	V4L2_STI_AUDIO_ENCODING_PCM,
	V4L2_STI_AUDIO_ENCODING_LPCM,
	V4L2_STI_AUDIO_ENCODING_MPEG1,
	V4L2_STI_AUDIO_ENCODING_MPEG2,
	V4L2_STI_AUDIO_ENCODING_MP3,
	V4L2_STI_AUDIO_ENCODING_AC3,
	V4L2_STI_AUDIO_ENCODING_DTS,
	V4L2_STI_AUDIO_ENCODING_AAC,
	V4L2_STI_AUDIO_ENCODING_WMA,
	V4L2_STI_AUDIO_ENCODING_RAW,
	V4L2_STI_AUDIO_ENCODING_LPCMA,
	V4L2_STI_AUDIO_ENCODING_LPCMH,
	V4L2_STI_AUDIO_ENCODING_LPCMB,
	V4L2_STI_AUDIO_ENCODING_SPDIF,
	V4L2_STI_AUDIO_ENCODING_DTS_LBR,
	V4L2_STI_AUDIO_ENCODING_MLP,
	V4L2_STI_AUDIO_ENCODING_RMA,
	V4L2_STI_AUDIO_ENCODING_AVS,
	V4L2_STI_AUDIO_ENCODING_VORBIS,
	V4L2_STI_AUDIO_ENCODING_FLAC,
	V4L2_STI_AUDIO_ENCODING_DRA,
	V4L2_STI_AUDIO_ENCODING_NONE,
	V4L2_STI_AUDIO_ENCODING_MS_ADPCM,
	V4L2_STI_AUDIO_ENCODING_IMA_ADPCM,
	V4L2_STI_AUDIO_ENCODING_PRIVATE,
};

struct v4l2_sti_audio_channel_placement {
	unsigned int pair0:6; /* channels 0 and 1 */
	unsigned int pair1:6; /* channels 2 and 3 */
	unsigned int pair2:6; /* channels 4 and 5 */
	unsigned int pair3:6; /* channels 6 and 7 */
	unsigned int pair4:6; /* channels 8 and 9 */
	unsigned int reserved0:1;
	unsigned int malleable:1;
};

/*
 * Audio parameters for PARAMETERS_CHANGED event
 */
struct v4l2_sti_audio_params {
	enum v4l2_sti_audio_encoding encoding;
	struct v4l2_sti_audio_channel_placement channel_placement;
	int bitrate;
	unsigned int sampling_freq;
	unsigned char channels;
	int dual_mono;
};

/*
 * Aspect ration identification
 */
enum v4l2_sti_aspect_ratio {
	V4L2_STI_ASPECT_RATIO_4_BY_3,
	V4L2_STI_ASPECT_RATIO_16_BY_9,
	V4L2_STI_ASPECT_RATIO_221_1,
};

/*
 * video params returned for PARAMETERS_CHANGED event
 */
struct v4l2_sti_video_size
{
	__u32 width;
	__u32 height;
	enum v4l2_sti_aspect_ratio aspect_ratio;
};

/*
 * Capture profile controls over v4l2 uncompressed video decoder and audio decoder
 */
enum v4l2_sti_capture_profile {
	V4L2_CID_MPEG_STI_CAPTURE_PROFILE_DISABLED,
	V4L2_CID_MPEG_STI_CAPTURE_PROFILE_HDMIRX_NO_AUD_DEC_NO_VID_FRC,
	V4L2_CID_MPEG_STI_CAPTURE_PROFILE_HDMIRX_AUD_DEC_VID_FRC,
	V4L2_CID_MPEG_STI_CAPTURE_PROFILE_HDMIRX_NO_AUD_DEC_VID_FRC,
	V4L2_CID_MPEG_STI_CAPTURE_PROFILE_HDMIRX_AUD_DEC_NO_VID_FRC,
	V4L2_CID_MPEG_STI_CAPTURE_PROFILE_COMPO_NO_AUD_VID_FRC,
	V4L2_CID_MPEG_STI_CAPTURE_PROFILE_COMPO_NO_AUD_NO_VID_FRC
};

#endif
