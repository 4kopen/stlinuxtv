/************************************************************************
Copyright (C) 2015 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Implementation of dvp capture video device
************************************************************************/
#include <media/v4l2-ioctl.h>
#include <media/v4l2-dev.h>
#include <media/videobuf2-bpa2-contig.h>

#include "linux/stm/stmedia_export.h"
#include "stm_v4l2_pixel_capture.h"
#include "stv_debug.h"

/**
 * dvp_cap_buffer_callback() - callback when buffer is filled
 */
static void dvp_cap_buffer_callback(void *data,
			const struct stm_i_capture_filled_buffer *filled_buf_desc)
{
	uint64_t capture_time;
	struct vb2_buffer *vb2 = filled_buf_desc->buffer_desc->user_data;
	struct v4l2_buffer *v4l2_buf = &vb2->v4l2_buf;

	/*
	 * Fill in the metadata
	 */
	capture_time = filled_buf_desc->captured_time;
	v4l2_buf->timestamp.tv_usec = do_div(capture_time, USEC_PER_SEC);
	v4l2_buf->timestamp.tv_sec = capture_time;
	vb2_set_plane_payload(vb2, 0, vb2_plane_size(vb2, 0));

	if (filled_buf_desc->content_is_valid)
		vb2_buffer_done(vb2, VB2_BUF_STATE_DONE);
	else
		vb2_buffer_done(vb2, VB2_BUF_STATE_ERROR);

}

/**
 * dvp_vb2_buf_init() - buf init for dvp capture
 */
static int dvp_vb2_buf_init(struct vb2_buffer *vb2)
{
	/*
	 * For the moment, nothing to be done here
	 */
	return 0;
}

static void dvp_vb2_buf_cleanup(struct vb2_buffer *vb2)
{
	/*
	 * For the moment, nothing to be done here
	 */
    return;
}

/**
 * dvp_vb2_buf_queue() - buf queue for dvp
 */
static void dvp_vb2_buf_queue(struct vb2_buffer *vb2)
{
	struct vb2_queue *q = vb2->vb2_queue;
	struct capture_context *cap_ctx = vb2_get_drv_priv(q);
	struct stm_i_capture_buffer_desc buf_desc;

	memset(&buf_desc, 0, sizeof(buf_desc));

	/*
	 * Compo-capture IP works on physical address, so, we
	 * get the physical address of the buffer. In a BT2020
	 * use-case, we need to send a local buffer for capture
	 * and then translate to 8 bit, BT709
	 */
	buf_desc.plane_cnt = 1;
	buf_desc.plane_addr[0] = (void *)(*(unsigned long *)vb2_plane_cookie(vb2, 0));
	if (unlikely(!buf_desc.plane_addr[0])) {
		stv_err("Failed to get the buffer physical address\n");
		goto queue_buf_failed;
	}

	buf_desc.buffer_filled = dvp_cap_buffer_callback;
	buf_desc.user_data = vb2;

	/*
	 * Queue buffer to pixel capture
	 */
	if (cap_ctx->capture_intf.queue_buffer(cap_ctx->pixel_capture, &buf_desc))
		stv_err("Failed to queue buffer to pixel capture\n");


queue_buf_failed:
	return;
}

/*
 * vb2 ops for dvp capture
 */
static struct vb2_ops dvp_v4l2_vb2_ops = {
	.queue_setup = stm_vb2_pixel_cap_queue_setup,
	.wait_prepare = stm_vb2_pixel_cap_wait_prepare,
	.wait_finish = stm_vb2_pixel_cap_wait_finish,
	.buf_init = dvp_vb2_buf_init,
	.buf_cleanup = dvp_vb2_buf_cleanup,
	.start_streaming = stm_vb2_pixel_cap_start_streaming,
	.stop_streaming = stm_vb2_pixel_cap_stop_streaming,
	.buf_queue = dvp_vb2_buf_queue,
};

/**
 * dvp_v4l2_querycap() - dvp capture query cap
 */
static int dvp_v4l2_querycap(struct file *file, void *fh,
				struct v4l2_capability *cap)
{
	strlcpy(cap->driver, "DVP", sizeof(cap->driver));
	strlcpy(cap->card, "STMicroelectronics", sizeof(cap->card));
	strlcpy(cap->bus_info, "platform: ST SoC", sizeof(cap->bus_info));
	cap->version = LINUX_VERSION_CODE;
	cap->device_caps = V4L2_CAP_VIDEO_CAPTURE | V4L2_CAP_STREAMING;
	cap->capabilities = cap->device_caps | V4L2_CAP_DEVICE_CAPS;
	return 0;
}

/**
 * dvp_v4l2_s_input() - set pixel capture input
 */
static int dvp_v4l2_s_input(struct file *file, void *fh, unsigned int i)
{
	int ret;
	struct output_fh *cap_fh = fh;

	ret = stm_v4l2_pixel_cap_s_input(cap_fh->cap_ctx, i, &dvp_v4l2_vb2_ops, 0);
	if (ret)
		stv_err("Failed to set DVP input\n");

	return ret;
}

/*
 * dvp capture ioctl ops
 */
static const struct v4l2_ioctl_ops dvp_v4l2_ioctl_ops = {
	.vidioc_querycap = dvp_v4l2_querycap,
	.vidioc_enum_fmt_vid_cap = stm_v4l2_pixel_cap_enum_fmt_vid_cap,
	.vidioc_g_fmt_vid_cap = stm_v4l2_pixel_cap_g_fmt_vid_cap,
	.vidioc_s_fmt_vid_cap = stm_v4l2_pixel_cap_s_fmt_vid_cap,
	.vidioc_try_fmt_vid_cap = stm_v4l2_pixel_cap_try_fmt_vid_cap,
	.vidioc_reqbufs = stm_v4l2_pixel_cap_reqbufs,
	.vidioc_querybuf = stm_v4l2_pixel_cap_querybuf,
	.vidioc_qbuf = stm_v4l2_pixel_cap_qbuf,
	.vidioc_dqbuf = stm_v4l2_pixel_cap_dqbuf,
	.vidioc_streamon = stm_v4l2_pixel_cap_streamon,
	.vidioc_streamoff = stm_v4l2_pixel_cap_streamoff,
	.vidioc_enum_input = stm_v4l2_pixel_cap_enum_input,
	.vidioc_g_input = stm_v4l2_pixel_cap_g_input,
	.vidioc_s_input = dvp_v4l2_s_input,
};

/*
 * DVP video device file operations
 */
static struct v4l2_file_operations dvp_v4l2_fops = {
	.owner = THIS_MODULE,
	.unlocked_ioctl = stm_v4l2_pixel_cap_unlocked_ioctl,
	.mmap = stm_v4l2_pixel_cap_mmap,
	.open = stm_v4l2_pixel_cap_open,
	.release = stm_v4l2_pixel_cap_release,
};

/*
 * media entity operations
 */
static struct media_entity_operations dvp_v4l2_media_entity_ops = {
	.link_setup = stm_v4l2_pixel_cap_link_setup,
};

/*
 * stm_v4l2_dvp_init() - register dvp subdev and video device
 */
int stm_v4l2_dvp_init(struct stm_v4l2_pixel_capture_dev *pixel_dev,
					struct capture_context *cap_ctx)
{
	int ret;
	struct video_device *vdev = &pixel_dev->viddev;

	/*
	 * Register dvp subdev
	 */
	ret = stm_v4l2_dvp_subdev_init(cap_ctx);
	if (ret) {
		stv_err("dvp subdev init failed\n");
		goto dvp_subdev_init_failed;
	}

	/*
	 * Register dvp video capture device
	 */
	strlcpy(vdev->name, DVP_CAPTURE_VDEV_NAME, sizeof(vdev->name));
	vdev->fops = &dvp_v4l2_fops;
	vdev->lock = &cap_ctx->lock;
	vdev->ioctl_ops = &dvp_v4l2_ioctl_ops;
	vdev->release = stm_v4l2_pixel_cap_vdev_release;
        vdev->vfl_dir = VFL_DIR_RX;
	vdev->entity.ops = &dvp_v4l2_media_entity_ops;
	video_set_drvdata(vdev, cap_ctx);
	ret = stm_v4l2_pixel_cap_register_vdev(pixel_dev, cap_ctx, "vid-raw-input");
	if (ret) {
		stv_err("Failed to register dvp video device\n");
		goto vdev_register_failed;
	}

	/*
	 * Create disabled links to dvp subdev
	 */
	ret = stm_v4l2_pixel_cap_create_disabled_links(MEDIA_ENT_T_V4L2_SUBDEV_DVP, 1,
							&vdev->entity, 0);
	if (ret) {
		stv_err("Failed to link dvp vdev to dvp subdev\n");
		goto link_setup_failed;
	}

	return 0;

link_setup_failed:
	stm_v4l2_pixel_cap_unregister_vdev(pixel_dev, cap_ctx);
vdev_register_failed:
	stm_v4l2_dvp_subdev_exit(cap_ctx);
dvp_subdev_init_failed:
	return ret;
}

/**
 * stm_v4l2_dvp_exit() - unregister dvp subdev and video device
 */
void stm_v4l2_dvp_exit(struct stm_v4l2_pixel_capture_dev *pixel_dev,
					struct capture_context *cap_ctx)
{
	stm_v4l2_pixel_cap_unregister_vdev(pixel_dev, cap_ctx);
	stm_v4l2_dvp_subdev_exit(cap_ctx);
}
