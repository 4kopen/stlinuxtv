/************************************************************************
Copyright (C) 2012 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Implementation of V4L2 output control video device
 * First initial driver by Akram Ben Belgacem
 * Updated for V4L2 vb2/video_ioctl2 and cleanup by Alain Volmat
************************************************************************/

#include <linux/module.h>
#include <linux/slab.h>

#include <media/v4l2-ioctl.h>
#include <media/videobuf2-core.h>
#include <media/videobuf2-bpa2-contig.h>
#include <asm/tlb.h>

#include <linux/stm/blitter.h>
#include <stm_display.h>
#include <stm_pixel_capture.h>

#include "linux/stmvout.h"
#include "stmedia.h"
#include "stm_v4l2_pixel_capture.h"
#include "linux/dvb/dvb_v4l2_export.h"
#include "linux/stm/stmedia_export.h"
#include "stv_debug.h"

static struct stm_v4l2_pixel_capture_dev stm_v4l2_pixel_dev[MAX_CAPTURE_DEVICES];
static struct capture_context stm_v4l2_pixel_cap_ctx[MAX_CAPTURE_DEVICES];

/**
 * stm_v4l2_pixel_cap_create_disabled_links() - create dvp/compo-cap disabled links
 * @src_entity_type: source entity type
 * @src_pad        : source pad number
 * @sink           : dvp entity
 * @sink_pad       : sink pad number
 * Create disabled links from all dvp sinks to all possible dvp sources.
 * The source is identified by first parameter @src_entity_type
 */
int stm_v4l2_pixel_cap_create_disabled_links(u32 src_entity_type, u16 src_pad,
					struct media_entity *sink, u16 sink_pad)
{
	struct media_entity *src, *next_sink = sink;
	int ret = 0;

	/*
	 * Create all possible disabled links from all sources to all sinks.
	 * a. Find the source and connect to all sinks
	 * b. Do this for all sources
	 */
	src = stm_media_find_entity_with_type_first(src_entity_type);
	while (src) {

		while(next_sink) {

			ret = media_entity_create_link(src, src_pad,
					next_sink, sink_pad, !MEDIA_LNK_FL_ENABLED);
			if (ret) {
				pr_err("%s(): failed to create hdmirx->dvp\n", __func__);
				goto link_setup_failed;
			}

			next_sink = stm_media_find_entity_with_type_next(next_sink,
								next_sink->type);
		}

		src = stm_media_find_entity_with_type_next(src, src_entity_type);
	}

link_setup_failed:
	return ret;
}

/**
 * stm_v4l2_pixel_cap_subdev_link_status() - check if dvp subdev has a valid source
 */
int stm_v4l2_pixel_cap_subdev_link_status(const struct media_pad *local,
					const struct media_pad *remote)
{
	int ret = 0;
	__u32 search_type = 0, id = 0;
	struct media_pad *sink_pad;

	if (remote->entity->type == MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_DECODER)
		search_type = MEDIA_ENT_T_DEVNODE_V4L;
	else if (remote->entity->type == MEDIA_ENT_T_DEVNODE_V4L)
		search_type = MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_DECODER;

	sink_pad = stm_media_find_remote_pad_with_type(local,
						MEDIA_LNK_FL_ENABLED,
						search_type, &id);
	if (sink_pad) {
		stv_err("%s already connected to %s\n", local->entity->name,
						sink_pad->entity->name);
		ret = -EBUSY;
	}

	return ret;
}

/**
 * stm_v4l2_pixel_cap_link_setup() - link setup for dvp video device
 */
int stm_v4l2_pixel_cap_link_setup(struct media_entity *entity,
		const struct media_pad *local,
		const struct media_pad *remote, u32 flags)
{
	int ret = 0;
	struct video_device *vdev = media_entity_to_video_device(entity);
	struct capture_context *cap_ctx = video_get_drvdata(vdev);

	MUTEX_INTERRUPTIBLE(&cap_ctx->lock);

	switch (remote->entity->type) {
	case MEDIA_ENT_T_V4L2_SUBDEV_DVP:
	case MEDIA_ENT_T_V4L2_SUBDEV_COMPO:
		if (flags & MEDIA_LNK_FL_ENABLED)
			cap_ctx->sd_pad = (struct media_pad *)remote;
		else
			cap_ctx->sd_pad = NULL;
		break;

	default:
		ret = -EINVAL;
	}

	mutex_unlock(&cap_ctx->lock);

	return ret;
}




/* VideoBuf2 handlers */
int stm_vb2_pixel_cap_queue_setup(struct vb2_queue *vq,
				const struct v4l2_format *f,
				unsigned int *nbuffers,
				unsigned int *nplanes,
				unsigned int sizes[], void *alloc_ctxs[])
{
	struct capture_context *cap_ctx = vb2_get_drv_priv(vq);

	*nplanes = 1;
	sizes[0] = cap_ctx->buffer_size;
	alloc_ctxs[0] = cap_ctx->mem_bpa2_ctx;
	return 0;
}

void stm_vb2_pixel_cap_wait_prepare(struct vb2_queue *q)
{
	struct capture_context *cap_ctx = vb2_get_drv_priv(q);
	mutex_unlock(&cap_ctx->lock);
}

void stm_vb2_pixel_cap_wait_finish(struct vb2_queue *q)
{
	struct capture_context *cap_ctx = vb2_get_drv_priv(q);
	mutex_lock(&cap_ctx->lock);
}


/**
 * stm_vb2_pixel_cap_start_streaming() - start streaming the pixel capture
 */
int stm_vb2_pixel_cap_start_streaming(struct vb2_queue *q,
						unsigned int count)
{
	int ret;
	struct capture_context *cap_ctx = vb2_get_drv_priv(q);

	/*
	 *  Start pixel capture
	 */
	ret = stm_pixel_capture_start(cap_ctx->pixel_capture);
	if (ret)
		pr_err("%s(): failed to start pixel capture\n", __func__);

	return ret;
}

/**
 * stm_vb2_pixel_cap_stop_streaming() - stop streaming the pixel capture
 */
int stm_vb2_pixel_cap_stop_streaming(struct vb2_queue *q)
{
	int ret;
	struct capture_context *cap_ctx = vb2_get_drv_priv(q);

	/*
	 * Stop pixel capture
	 */
	ret = stm_pixel_capture_stop(cap_ctx->pixel_capture);
	if (ret) {
		pr_err("%s(): failed to stop pixel capture\n", __func__);
		goto streamoff_done;
	}

	memset(&cap_ctx->capture_intf, 0, sizeof(cap_ctx->capture_intf));

streamoff_done:
	return ret;
}


/**
 * stm_v4l2_pixel_cap_reqbufs() - pixel capture reqbufs
 */
int stm_v4l2_pixel_cap_reqbufs(struct file *file,
			void *priv, struct v4l2_requestbuffers *p)
{
	struct output_fh *fh = file->private_data;
	struct capture_context *cap_ctx = fh->cap_ctx;

	return vb2_reqbufs(&cap_ctx->vq, p);
}

/**
 * stm_v4l2_pixel_cap_querybuf() - querybuf for pixel capture
 */
int stm_v4l2_pixel_cap_querybuf(struct file *file,
				void *priv, struct v4l2_buffer *p)
{
	struct output_fh *fh = file->private_data;
	struct capture_context *cap_ctx = fh->cap_ctx;

	return vb2_querybuf(&cap_ctx->vq, p);
}

/**
 * stm_v4l2_pixel_cap_qbuf() - querybuf the pixel capture
 */
int stm_v4l2_pixel_cap_qbuf(struct file *file,
				void *fh, struct v4l2_buffer *p)
{
	struct output_fh *cap_fh = fh;
	struct capture_context *cap_ctx = cap_fh->cap_ctx;

	return vb2_qbuf(&cap_ctx->vq, p);
}

/**
 * stm_v4l2_pixel_cap_dqbuf() - dqbuf from pixel capture
 */
int stm_v4l2_pixel_cap_dqbuf(struct file *file,
				void *priv, struct v4l2_buffer *p)
{
	struct output_fh *fh = file->private_data;
	struct capture_context *cap_ctx = fh->cap_ctx;

	return vb2_dqbuf(&cap_ctx->vq, p, file->f_flags & O_NONBLOCK);
}

/**
 * stm_v4l2_pixel_cap_streamon() - streamon pixel capture
 */
int stm_v4l2_pixel_cap_streamon(struct file *file,
			void *priv, enum v4l2_buf_type i)
{
	int ret, size;
	stm_object_h obj_type;
	struct output_fh *fh = file->private_data;
	struct capture_context *cap_ctx = fh->cap_ctx;

	/**
	 * Get the push interface for queueing buffers into pixel capture
	 */
	ret = stm_registry_get_object_type(cap_ctx->pixel_capture, &obj_type);
	if (ret) {
		pr_err("%s(): failed to get pixel capture object type\n", __func__);
		goto streamon_done;
	}

	ret = stm_registry_get_attribute(obj_type,
				STM_DATA_INTERFACE_CAPTURE,
				STM_REGISTRY_ADDRESS,
				sizeof(stm_data_interface_capture_t),
				&cap_ctx->capture_intf,
				&size);

	if (ret) {
		pr_err("%s(): failed to get capture interface of pixel driver\n", __func__);
		goto streamon_done;
	}

	ret = vb2_streamon(&cap_ctx->vq, i);

streamon_done:
	return ret;
}

/**
 * stm_v4l2_pixel_cap_streamoff() - stream off pixel capture
 */
int stm_v4l2_pixel_cap_streamoff(struct file *file,
				void *priv, enum v4l2_buf_type i)
{
	struct output_fh *fh = file->private_data;
	struct capture_context *cap_ctx = fh->cap_ctx;

	return vb2_streamoff(&cap_ctx->vq, i);
}

/**
 * stm_v4l2_pixel_cap_g_input() - get currently configured input
 */
int stm_v4l2_pixel_cap_g_input(struct file *file,
				void *priv, unsigned int *i)
{
	int ret;
	struct output_fh *fh = file->private_data;
	struct capture_context *cap_ctx = fh->cap_ctx;

	ret = stm_pixel_capture_get_input(cap_ctx->pixel_capture, i);
	if (ret)
		stv_err("Failed to get input (%d)\n", ret);

	return ret;
}

/**
 * stm_v4l2_pixel_cap_enum_fmt_vid_cap() - enumerate the supported formats
 */
int stm_v4l2_pixel_cap_enum_fmt_vid_cap(struct file *file, void *fh,
					struct v4l2_fmtdesc *f)
{
	int ret, max_fmts;
	struct pixel_info info;
	struct output_fh *cap_fh = fh;
	struct capture_context *cap_ctx = cap_fh->cap_ctx;
	stm_pixel_capture_format_t pix_fmt[STM_PIXEL_FORMAT_COUNT];

	/*
	 * Find out the number of supported formats
	 */
	memset(&pix_fmt, 0, sizeof(pix_fmt));
	max_fmts = stm_pixel_capture_enum_image_formats(cap_ctx->pixel_capture,
							pix_fmt, sizeof(pix_fmt));
	if (max_fmts < 0) {
		ret = max_fmts;
		stv_err("Failed to enumerate supported formats (%d)\n", ret);
		goto enum_fmt_done;
	}

	if (f->index >= max_fmts) {
		stv_debug("Reached the end of supported formats\n");
		ret = -EINVAL;
		goto enum_fmt_done;
	}

	/*
	 * Map supported pixel capture format to v4l2 pixel format
	 */
	ret = map_capture_to_v4l2_pixelfmt(pix_fmt[f->index], &info);
	if (ret) {
		stv_err("No V4L2 mapping exist of pixfmt 0x%x\n", pix_fmt[f->index]);
		ret = -EINVAL;
		goto enum_fmt_done;
	}

	f->pixelformat = info.pixelfmt;

enum_fmt_done:
	return ret;
}

/**
 * stm_v4l2_pixel_cap_g_fmt_vid_cap() - get the currently set capture format
 */
int stm_v4l2_pixel_cap_g_fmt_vid_cap(struct file *file, void *fh,
					struct v4l2_format *f)
{
	int ret;
	struct pixel_info info;
	struct output_fh *cap_fh = fh;
	struct capture_context *cap_ctx = cap_fh->cap_ctx;
	stm_pixel_capture_buffer_format_t *fmt = &cap_ctx->format;

	/*
	 * Fill in the structure
	 */
	f->fmt.pix.width = fmt->width;
	f->fmt.pix.height = fmt->height;

	memset(&info, 0, sizeof(info));
	ret = map_capture_to_v4l2_pixelfmt(fmt->format, &info);
	if (ret) {
		stv_err("No fmt set for the moment\n");
		goto g_fmt_done;
	}
	f->fmt.pix.pixelformat = info.pixelfmt;
	f->fmt.pix.bytesperline = (f->fmt.pix.width * info.bitsperpixel) / 8;
	f->fmt.pix.sizeimage = f->fmt.pix.bytesperline * f->fmt.pix.height;
	f->fmt.pix.colorspace = map_capture_to_v4l2_colorspace(fmt->color_space);
	f->fmt.pix.field = map_capture_to_v4l2_field(fmt->flags);

g_fmt_done:
	return ret;
}

/**
 * pixel_cap_get_capture_fmt() - get the supported capture format
 */
static int pixel_cap_get_capture_fmt(struct capture_context *cap_ctx,
				__u32 usr_fmt, struct pixel_info *info)
{
	int i, max_fmts, ret;
	stm_pixel_capture_format_t pix_fmt[STM_PIXEL_FORMAT_COUNT];

	/*
	 * Find out if the requested fmt is supported or not
	 */
	memset(&pix_fmt, 0, sizeof(pix_fmt));
	max_fmts = stm_pixel_capture_enum_image_formats(cap_ctx->pixel_capture,
							pix_fmt, sizeof(pix_fmt));
	if (max_fmts < 0) {
		ret = max_fmts;
		stv_err("Failed to enumerate supported formats (%d)\n", ret);
		goto get_fmt_done;
	}

	/*
	 * Find out the pixel capture mapping for the v4l2 format sent
	 */
	for (i = 0; i < max_fmts; i++) {
		memset(info, 0, sizeof(*info));
		ret = map_capture_to_v4l2_pixelfmt(pix_fmt[i], info);
		if (ret) {
			stv_info("Setting pixel capture to default\n");
			break;
		}

		if (info->pixelfmt == usr_fmt)
			break;
	}

	if (ret || i == max_fmts) {
		map_capture_to_v4l2_pixelfmt(pix_fmt[0], info);
		ret = 0;
	}

get_fmt_done:
	return ret;
}

/**
 * stm_v4l2_pixel_cap_s_fmt_vid_cap() - set format on dvp
 */
int stm_v4l2_pixel_cap_s_fmt_vid_cap(struct file *file,
				void *fh, struct v4l2_format *f)
{
	int ret;
	struct pixel_info info;
	struct output_fh *cap_fh = fh;
	struct capture_context *cap_ctx = cap_fh->cap_ctx;
	stm_pixel_capture_buffer_format_t buf_fmt;

	/*
	 * Cannot reset format if it's streaming
	 */
	if (vb2_is_streaming(&cap_ctx->vq)) {
		ret = -EBUSY;
		goto s_fmt_cap_done;
	}

	/*
	 * Get the capture format
	 */
	ret = pixel_cap_get_capture_fmt(cap_ctx, f->fmt.pix.pixelformat, &info);
	if (ret) {
		stv_err("Failed to get the supported fmt\n");
		goto s_fmt_cap_done;
	}

	/*
	 * Prepare the capture buffer descriptor and set format
	 */
	memset(&buf_fmt, 0, sizeof(buf_fmt));
	if (!f->fmt.pix.bytesperline)
		f->fmt.pix.bytesperline = (info.bitsperpixel * f->fmt.pix.width / 8);
	buf_fmt.format = map_v4l2_to_capture_pixelfmt(info.pixelfmt);
	buf_fmt.color_space = map_v4l2_to_capture_colorspace(f->fmt.pix.colorspace);
	buf_fmt.width = f->fmt.pix.width;
	buf_fmt.height = f->fmt.pix.height;
	buf_fmt.stride = f->fmt.pix.bytesperline;
	buf_fmt.flags = map_v4l2_to_capture_field(f->fmt.pix.field);
	ret = stm_pixel_capture_set_format(cap_ctx->pixel_capture, buf_fmt);
	if (ret) {
		stv_err("Failed to set the pixel capture format (%d)\n", ret);
		goto s_fmt_cap_done;
	}
	cap_ctx->buffer_size = f->fmt.pix.bytesperline * f->fmt.pix.height;

	/*
	 * Update the user format structure
	 */
	f->fmt.pix.pixelformat = info.pixelfmt;
	f->fmt.pix.colorspace = map_capture_to_v4l2_colorspace(buf_fmt.color_space);
	f->fmt.pix.sizeimage = f->fmt.pix.bytesperline * f->fmt.pix.height;

	/*
	 * Get the set format into the local buffer
	 */
	ret = stm_pixel_capture_get_format(cap_ctx->pixel_capture,
							&cap_ctx->format);
	if (ret) {
		stv_err("Failed to get the format (%d)\n", ret);
		goto s_fmt_cap_done;
	}

s_fmt_cap_done:
	return ret;
}

/**
 * stm_v4l2_pixel_cap_try_fmt_vid_cap() - try format
 */
int stm_v4l2_pixel_cap_try_fmt_vid_cap(struct file *file, void *fh,
					struct v4l2_format *f)
{
	int ret;
	struct pixel_info info;
	struct output_fh *cap_fh = fh;
	struct capture_context *cap_ctx = cap_fh->cap_ctx;

	/*
	 * Get the capture format
	 */
	ret = pixel_cap_get_capture_fmt(cap_ctx, f->fmt.pix.pixelformat, &info);
	if (ret) {
		stv_err("Failed to get the supported fmt\n");
		goto try_fmt_cap_done;
	}
	f->fmt.pix.pixelformat = info.pixelfmt;

try_fmt_cap_done:
	return ret;
}

/**
 * stm_v4l2_pixel_cap_enum_input() - enumerate the dvp inputs
 */
int stm_v4l2_pixel_cap_enum_input(struct file *file,
			void *fh, struct v4l2_input *inp)
{
	int ret;
	const char *name;
	struct output_fh *cap_fh = fh;
	struct capture_context *cap_ctx = cap_fh->cap_ctx;

	inp->type = V4L2_INPUT_TYPE_CAMERA;

	ret = stm_pixel_capture_enum_inputs(cap_ctx->pixel_capture,
							inp->index, &name);
	if (ret) {
		stv_debug("Possible ending of all the inputs\n");
		goto enum_input_done;
	}
	strlcpy(inp->name, name, sizeof(inp->name));

enum_input_done:
	return ret;
}

/**
 * stm_v4l2_pixel_cap_s_input() - set the dvp input
 */
int stm_v4l2_pixel_cap_s_input(struct capture_context *cap_ctx,
			unsigned int i, struct vb2_ops *vb2_ops,
			unsigned int buffer_size)
{
	int ret;

	/*
	 * Set input and lock for usage
	 */
	ret = stm_pixel_capture_set_input(cap_ctx->pixel_capture, i);
	if (ret) {
		stv_err("Failed to set input (%d)\n", ret);
		goto s_input_failed;
	}

	ret = stm_pixel_capture_lock(cap_ctx->pixel_capture);
	if (ret) {
		stv_err("Failed to lock pixel capture (%d)\n", ret);
		goto s_input_failed;
	}

	/*
	 * Initialize vb2 queue
	 */
	if (cap_ctx->vq_set)
		goto s_input_failed;

	memset(&cap_ctx->vq, 0, sizeof(cap_ctx->vq));
	cap_ctx->vq.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	cap_ctx->vq.io_modes = VB2_USERPTR | VB2_MMAP;
	cap_ctx->vq.drv_priv = cap_ctx;
	cap_ctx->vq.buf_struct_size = buffer_size;
	cap_ctx->vq.ops = vb2_ops;
	cap_ctx->vq.mem_ops = &vb2_bpa2_contig_memops;
	cap_ctx->vq.timestamp_type = V4L2_BUF_FLAG_TIMESTAMP_MONOTONIC;
	ret = vb2_queue_init(&cap_ctx->vq);
	if (ret) {
		stv_err("Failed to init queue (%d)\n", ret);
		goto q_init_failed;
	}
	cap_ctx->vq_set = 1;

	return 0;

q_init_failed:
	stm_pixel_capture_unlock(cap_ctx->pixel_capture);
s_input_failed:
	return ret;
}

/**
 * stm_v4l2_pixel_cap_s_crop() - crop the input of pixel capture
 */
int stm_v4l2_pixel_cap_s_crop(struct file *file,
			void *fh, const struct v4l2_crop *c)
{
	int ret;
	struct output_fh *cap_fh = fh;
	struct capture_context *cap_ctx = cap_fh->cap_ctx;
	stm_pixel_capture_rect_t rect;

	memset(&rect, 0, sizeof(rect));

	switch (c->type) {
	case V4L2_BUF_TYPE_VIDEO_CAPTURE:

		rect.x = c->c.left;
		rect.y = c->c.top;
		rect.width = c->c.width;
		rect.height = c->c.height;

		ret = stm_pixel_capture_set_input_window(cap_ctx->pixel_capture,
								rect);
		if (ret)
			stv_err("Failed to set the input window rect (%d)\n", ret);
		break;

	default:
		ret = -EINVAL;
		break;
	}

	return ret;
}

/**
 * stm_v4l2_pixel_cap_g_crop() - get the cropping rectangle
 */
int stm_v4l2_pixel_cap_g_crop(struct file *file, void *fh, struct v4l2_crop *c)
{
	int ret;
	struct output_fh *cap_fh = fh;
	struct capture_context *cap_ctx = cap_fh->cap_ctx;
	stm_pixel_capture_rect_t rect = { 0 };

	switch (c->type) {
	case V4L2_BUF_TYPE_VIDEO_CAPTURE:

		ret = stm_pixel_capture_get_input_window(cap_ctx->pixel_capture,
								&rect);
		if (ret) {
			stv_err("Failed to get input rect (%d)\n", ret);
			break;
		}

		c->c.left = rect.x;
		c->c.top = rect.y;
		c->c.width = rect.width;
		c->c.height = rect.height;
		break;

	default:
		ret = -EINVAL;
		break;
	}

	return ret;
}

/**
 * stm_v4l2_pixel_cap_cropcap() - get the cropping caps of pixel capture
 */
int stm_v4l2_pixel_cap_cropcap(struct file *file,
			void *fh, struct v4l2_cropcap *c)
{
	int ret = 0;
	struct output_fh *cap_fh = fh;
	struct capture_context *cap_ctx = cap_fh->cap_ctx;
	stm_pixel_capture_rect_t rect;

	switch (c->type) {
	case V4L2_BUF_TYPE_VIDEO_CAPTURE:

		memset(&rect, 0, sizeof(rect));
		ret = stm_pixel_capture_get_input_window(cap_ctx->pixel_capture,
								&rect);
		if (ret) {
			stv_err("Failed to get input window params (%d)\n", ret);
			break;
		}

		c->bounds.left = rect.x;
		c->bounds.top = rect.y;
		c->bounds.width = rect.width;
		c->bounds.height = rect.height;

		c->defrect = c->bounds;

		c->pixelaspect.numerator = 1;
		c->pixelaspect.denominator = 1;

		break;

	default:
		ret = -EINVAL;
		break;
	}

	return ret;
}

/**
 * pixel_cap_check_ops() - check the basic condition for ioctls
 */
static int pixel_cap_check_ops(struct file *file, unsigned long cmd)
{
	int ret = 0;
	bool link_status = false;
	struct output_fh *cap_fh = file->private_data;
	struct capture_context *cap_ctx = cap_fh->cap_ctx;

	if (cap_ctx->sd_pad)
		link_status = true;

	switch (cmd) {
	case VIDIOC_ENUM_FMT:
	case VIDIOC_G_FMT:
	case VIDIOC_S_FMT:
	case VIDIOC_TRY_FMT:
	case VIDIOC_REQBUFS:
	case VIDIOC_QUERYBUF:
	case VIDIOC_QBUF:
	case VIDIOC_DQBUF:
	case VIDIOC_STREAMON:
	case VIDIOC_STREAMOFF:
	case VIDIOC_S_INPUT:
		if (!link_status)
			ret = -EBUSY;
		break;
	default:
		break;
	}

	return ret;
}

/**
 * stm_v4l2_pixel_cap_unlocked_ioctl() - pixel capture v4l2 ioctl ops
 */
long stm_v4l2_pixel_cap_unlocked_ioctl(struct file *file,
			unsigned int cmd, unsigned long arg)
{
	long ret;

	/*
	 * Check the working condition for streaming ioctls
	 */
	ret = pixel_cap_check_ops(file, cmd);
	if (ret) {
		stv_err("Streaming condition not satisfied\n");
		goto exit;
	}

	ret = video_ioctl2(file, cmd, arg);

exit:
	return ret;
}

/**
 * stm_v4l2_pixel_cap_mmap() - mmap for pixel capture video devices
 */
int stm_v4l2_pixel_cap_mmap(struct file *file, struct vm_area_struct *vma)
{
	int ret;
	struct output_fh *compo_fh = file->private_data;
	struct capture_context *cap_ctx = compo_fh->cap_ctx;

	if (mutex_lock_interruptible(&cap_ctx->lock)) {
		ret = -ERESTARTSYS;
		goto mmap_failed;
	}

	ret = vb2_mmap(&cap_ctx->vq, vma);

mmap_failed:
	mutex_unlock(&cap_ctx->lock);
	return ret;
}

/**
 * stm_v4l2_pixel_cap_open() - open for pixel capture video device
 */
int stm_v4l2_pixel_cap_open(struct file *file)
{
	struct output_fh *fh;
	struct video_device *viddev = video_devdata(file);

	/* Allocate memory */
	fh = kzalloc(sizeof(struct output_fh), GFP_KERNEL);
	if (!fh) {
		printk(KERN_ERR "%s: nomem on v4l2 open\n", __func__);
		return -ENOMEM;
	}
	if (!strncmp(viddev->name, COMPO_CAPTURE_VDEV_NAME,
					strlen(COMPO_CAPTURE_VDEV_NAME)))
		fh->cap_ctx = &stm_v4l2_pixel_cap_ctx[COMPO_INDEX];
	else
		fh->cap_ctx = &stm_v4l2_pixel_cap_ctx[DVP_INDEX];

	v4l2_fh_init(&fh->fh, viddev);
	file->private_data = fh;
	v4l2_fh_add(&fh->fh);

	return 0;
}

/**
 * stm_v4l2_pixel_cap_release() - close for pixel capture video device
 */
int stm_v4l2_pixel_cap_release(struct file *file)
{
	struct output_fh *fh = file->private_data;
	struct capture_context *cap_ctx = fh->cap_ctx;
	int ret;

	if (!v4l2_fh_is_singular_file(file) || !cap_ctx)
		goto exit;

	if (cap_ctx->vq_set) {
		if (vb2_is_streaming(&cap_ctx->vq))
			vb2_streamoff(&cap_ctx->vq, cap_ctx->vq.type);

		vb2_queue_release(&cap_ctx->vq);
		cap_ctx->vq_set = 0;
	}

	/* Unlock Capture before exiting */
	ret = stm_pixel_capture_unlock(cap_ctx->pixel_capture);
	if (ret)
		printk(KERN_ERR "%s: pixel capture unlock failed(%d)\n",
					__func__, ret);

exit:
	return v4l2_fh_release(file);
}

/**
 * stm_v4l2_pixel_cap_vdev_release() - mandatory release function for vdev
 */
void stm_v4l2_pixel_cap_vdev_release(struct video_device *vdev)
{
	/* Nothing to do, but need by V4L2 stack */
}

/**
 * stm_v4l2_pixel_cap_register_vdev() - register the pixel capture video device
 */
int stm_v4l2_pixel_cap_register_vdev(struct stm_v4l2_pixel_capture_dev *pixel_dev,
				struct capture_context *cap_ctx, char *bpa2_name)
{
	int ret;
	void *mem_bpa2_ctx;
	struct media_pad *pads;
	struct bpa2_part *bpa2_part;
	struct video_device *viddev = &pixel_dev->viddev;

	/*
	 * Initialize the media entity for the video capture video device
	 */
	pads = (struct media_pad *)kzalloc(sizeof(*pads), GFP_KERNEL);
	if (!pads) {
		stv_err("Out of memory for video capture pads\n");
		ret = -ENOMEM;
		goto pad_alloc_failed;
	}
	pixel_dev->pads = pads;
	pads[0].flags = MEDIA_PAD_FL_SINK;

	ret = media_entity_init(&viddev->entity, 1, pads, 0);
	if (ret) {
		stv_err("entity init failed for %s\n", viddev->name);
		goto entity_init_failed;
	}

	/*
	 * Register the pixel capture video device
	 */
	ret = stm_media_register_v4l2_video_device(viddev, VFL_TYPE_GRABBER, -1);
	if (ret) {
		stv_err("failed to register the %s video device\n", viddev->name);
		goto viddev_regsiter_failed;
	}

	/*
	 * Initialize the bpa2 contig allocator context
	 */
	bpa2_part = bpa2_find_part(bpa2_name);
	if (!bpa2_part) {
		stv_err("failed to find bpa2 part %s\n", VB2_COMPO_CAPTURE_BPA2);
		goto find_bpa2_failed;
	}

	/*
	 * Initialize the memory allocator context
	 */
	mem_bpa2_ctx = vb2_bpa2_contig_init_ctx(&viddev->dev, bpa2_part);
	if (!mem_bpa2_ctx) {
		stv_err("failed to init contig bpa2 alloctor ctx\n");
		goto find_bpa2_failed;
	}
	cap_ctx->mem_bpa2_ctx = mem_bpa2_ctx;

	return 0;

find_bpa2_failed:
	stm_media_unregister_v4l2_video_device(viddev);
viddev_regsiter_failed:
	media_entity_cleanup(&viddev->entity);
entity_init_failed:
	kfree(pads);
pad_alloc_failed:
	return ret;
}

/**
 * stm_v4l2_pixel_cap_unregister_vdev() - unregister the pixel capture video device
 */
void stm_v4l2_pixel_cap_unregister_vdev(struct stm_v4l2_pixel_capture_dev *pixel_dev,
						struct capture_context *cap_ctx)
{
	vb2_bpa2_contig_cleanup_ctx(cap_ctx->mem_bpa2_ctx);
	stm_media_unregister_v4l2_video_device(&pixel_dev->viddev);
	media_entity_cleanup(&pixel_dev->viddev.entity);
	kfree(pixel_dev->pads);
}

/**
 * stm_v4l2_pixel_cap_init() - initialize video capture device and subdevs(compo/dvp)
 */
static int __init stm_v4l2_pixel_cap_init(void)
{
	int ret;
	struct capture_context *cap_ctx = stm_v4l2_pixel_cap_ctx;

	/*
	 * Initialize compo capture device
	 */
	ret = stm_v4l2_compo_cap_init(&stm_v4l2_pixel_dev[COMPO_INDEX],
							&cap_ctx[COMPO_INDEX]);
	if (ret) {
		stv_err("Failed to init Compo-capture\n");
		goto compo_cap_init_failed;
	}

	/*
	 * Initialize dvp capture device
	 */
	ret = stm_v4l2_dvp_init(&stm_v4l2_pixel_dev[DVP_INDEX],
						&cap_ctx[DVP_INDEX]);
	if (ret) {
		stv_err("Failed to init dvp capture\n");
		goto dvp_init_failed;
	}

	return 0;

dvp_init_failed:
	stm_v4l2_compo_cap_exit(&stm_v4l2_pixel_dev[COMPO_INDEX],
						&cap_ctx[COMPO_INDEX]);
compo_cap_init_failed:
	return ret;
}

/**
 * stm_v4l2_pixel_cap_exit() - exits the video capture device
 */
static void __exit stm_v4l2_pixel_cap_exit(void)
{

	stm_v4l2_compo_cap_exit(&stm_v4l2_pixel_dev[COMPO_INDEX],
					&stm_v4l2_pixel_cap_ctx[COMPO_INDEX]);

	stm_v4l2_dvp_exit(&stm_v4l2_pixel_dev[DVP_INDEX],
					&stm_v4l2_pixel_cap_ctx[DVP_INDEX]);
}

module_init(stm_v4l2_pixel_cap_init);
module_exit(stm_v4l2_pixel_cap_exit);
MODULE_DESCRIPTION("V4L2 Video Capture devices - Compositor/Planes/DVP");
MODULE_AUTHOR("ST Microelectronics");
MODULE_LICENSE("GPL");
