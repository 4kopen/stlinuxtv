/************************************************************************
Copyright (C) 2015 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Implementation of compo capture video device
************************************************************************/
#include <media/v4l2-ioctl.h>
#include <media/v4l2-dev.h>
#include <media/videobuf2-bpa2-contig.h>

#include "linux/stm/stmedia_export.h"
#include "stm_v4l2_pixel_capture.h"
#include "stv_debug.h"

/*
 * @src_phys_addr: physical address of locally allocated buffer
 * @src_surface  : create a source surface from locally allocated buffer
 * @dest_surface : create a destination surface from user-space buffer
 */
struct compo_cap_buf {
	struct vb2_buffer vb2;

	void *src_mem_priv;
	unsigned long src_phys_addr;

	stm_blitter_surface_t *src_surface, *dest_surface;
};

#define to_compo_cap_buf(vb2)	\
	container_of(vb2, struct compo_cap_buf, vb2)

/*
 * This macro is copied from videobuf2-core.c, to call queue memops
 */
#define call_memop(q, op, args...)			\
	(((q)->mem_ops->op) ? ((q)->mem_ops->op(args)) : 0)

/**
 * stm_v4l2_compo_cap_subdev_notify() - subdev notification mechanism
 * Notification to pixel capture video device from connected subdev
 */
void stm_v4l2_compo_cap_subdev_notify(struct media_pad *vdev_pad,
						__u32 cmd, void *val)
{
	struct video_device *vdev;
	struct capture_context *cap_ctx;

	/*
	 * If there's no vdev connected, cannot process anything
	 */
	if (!vdev_pad)
		goto notify_done;

	vdev = media_entity_to_video_device(vdev_pad->entity);
	cap_ctx = video_get_drvdata(vdev);

	/*
	 * Process the commands
	 */
	switch (cmd) {
	case VIDIOC_STI_S_VIDEO_INPUT_STABLE:

		/*
		 * For the moment, this is only used for indicating
		 * instability, so, stopping the capture
		 */
		if (cap_ctx->vq_set)
			vb2_streamoff(&cap_ctx->vq, cap_ctx->vq.type);

		/*
		 * Release blitter
		 */
		if (cap_ctx->blitter) {
			stm_blitter_put(cap_ctx->blitter);
			cap_ctx->blitter = NULL;
		}

		break;

	case V4L2_STI_FMT_CHANGE:

		/*
		 * Check if colorspace is BT2020 and input format is not the
		 * special validation format of 32 bits. If it is we need to open
		 * blitter and allocate intermediate buffers for compo-capture
		 */
		if ((cap_ctx->format.color_space != STM_PIXEL_CAPTURE_BT2020)
			|| (cap_ctx->format.format == STM_PIXEL_FORMAT_RGB_10B10B10B_SP))
			break;

		cap_ctx->blitter = stm_blitter_get(0);
		if (IS_ERR(cap_ctx->blitter))
			stv_err("Failed to get blitter0\n");

		break;

	default:
		break;
	}

notify_done:
	return;
}

/**
 * compo_cap_downscale_bt2020() - process for bt2020 buffer
 */
static int compo_cap_downscale_bt2020(struct capture_context *cap_ctx,
						struct compo_cap_buf *buf)
{
	int ret = 0;
	stm_blitter_serial_t serial;
	stm_blitter_rect_t src_rect, dest_rect;

	/*
	 * If there's no blitter, then, there's no BT2020 input
	 */
	if (!cap_ctx->blitter)
		goto bt2020_done;

	/*
	 * Do the conversion from BT2020 to BT709
	 */
	memset(&src_rect, 0, sizeof(src_rect));
	src_rect.size.w = cap_ctx->format.width;
	src_rect.size.h = cap_ctx->format.height;
	memcpy(&dest_rect, &src_rect, sizeof(dest_rect));
	ret = stm_blitter_surface_stretchblit(cap_ctx->blitter,
					buf->src_surface, &src_rect,
					buf->dest_surface, &dest_rect,
					1);
	if (ret) {
		stv_err("Failed to perform BT2020->BT709 conversion (%d)\n", ret);
		goto blit_done;
	}

	/*
	 * Wait for blit to complete
	 */
	ret = stm_blitter_surface_get_serial(buf->dest_surface, &serial);
	if (ret) {
		stv_err("Failed to get serial handle on blitter (%d)\n", ret);
		goto blit_done;
	}
	ret = stm_blitter_wait(cap_ctx->blitter, STM_BLITTER_WAIT_FENCE, serial);
	if (ret)
		stv_err("Blit wait failed (%d)\n", ret);

	/*
	 * Put the destination blitter surface. We create a new surface
	 * on every QBUF, as, any virtual address can be sent in.
	 */
blit_done:
	stm_blitter_surface_put(buf->dest_surface);
	buf->dest_surface = NULL;

bt2020_done:
	return ret;
}

/**
 * compo_cap_buffer_callback() - callback when buffer is filled
 */
static void compo_cap_buffer_callback(void *data,
			const struct stm_i_capture_filled_buffer *filled_buf_desc)
{
	uint64_t capture_time;
	struct vb2_buffer *vb2 = filled_buf_desc->buffer_desc->user_data;
	struct v4l2_buffer *v4l2_buf = &vb2->v4l2_buf;
	struct compo_cap_buf *buf = to_compo_cap_buf(vb2);
	struct vb2_queue *q = vb2->vb2_queue;
	struct capture_context *cap_ctx = vb2_get_drv_priv(q);

	/*
	 * Fill in the metadata
	 */
	capture_time = filled_buf_desc->captured_time;
	v4l2_buf->timestamp.tv_usec = do_div(capture_time, USEC_PER_SEC);
	v4l2_buf->timestamp.tv_sec = capture_time;
	vb2_set_plane_payload(vb2, 0, vb2_plane_size(vb2, 0));

	if (filled_buf_desc->content_is_valid) {
		/*
		 * Process for BT2020 buffer
		 */
		if (compo_cap_downscale_bt2020(cap_ctx, buf)) {
			stv_err("Failed to process for BT2020 input\n");
			vb2_buffer_done(vb2, VB2_BUF_STATE_ERROR);
            return;
		}

		vb2_buffer_done(vb2, VB2_BUF_STATE_DONE);
	} else
		vb2_buffer_done(vb2, VB2_BUF_STATE_ERROR);

	return;
}


/**
 * compo_cap_blit_buf_init() - allocate buffers for blitting if BT2020
 */
static int compo_cap_blit_buf_init(struct vb2_buffer *vb2)
{
	int ret = 0;
	struct pixel_info info;
	unsigned long size;
	struct vb2_queue *q = vb2->vb2_queue;
	struct compo_cap_buf *buf = to_compo_cap_buf(vb2);
	struct capture_context *cap_ctx = vb2_get_drv_priv(q);
	stm_blitter_surface_address_t blit_addr;
	stm_blitter_dimension_t dim;

	/*
	 * If there's no blitter activated, then input is not BT2020, so,
	 * no need to allocate buffers. Local buffers are allocated just once.
	 */
	if (!cap_ctx->blitter || buf->src_mem_priv)
		goto init_done;

	buf->src_mem_priv = call_memop(q, alloc, q->alloc_ctx[0],
					cap_ctx->buffer_size, q->gfp_flags);
	if (IS_ERR_OR_NULL(buf->src_mem_priv)) {
		ret = PTR_ERR(buf->src_mem_priv);
		stv_err("Failed to allocate buffer for BT2020 input (%d)\n", ret);
		goto init_done;
	}
	buf->src_phys_addr = *(unsigned long *)call_memop(q,cookie, buf->src_mem_priv);

	/*
	 * Get the blit pix fmt corresponding to pixel capture
	 */
	memset(&info, 0, sizeof(info));
	ret = map_pixel_cap_to_blitter_pixfmt(cap_ctx->format.format, &info);
	if (ret) {
		stv_err("Failed to get supported blit fmt from pixel capture\n");
		goto map_failed;
	}

	/*
	 * Prepare a blitter surface out of the local buffer
	 */
	memset(&blit_addr, 0, sizeof(blit_addr));
	memset(&dim, 0, sizeof(dim));
	blit_addr.base = buf->src_phys_addr;
	size = cap_ctx->format.height * cap_ctx->format.stride;
	dim.w = cap_ctx->format.width;
	dim.h = cap_ctx->format.height;
	buf->src_surface = stm_blitter_surface_new_preallocated(info.pixelfmt,
						STM_BLITTER_SCS_BT2020_FULLRANGE,
						&blit_addr, size, &dim,
						cap_ctx->format.stride);
	if (IS_ERR(buf->src_surface)) {
		stv_err("Failed to create the surface for BT2020 buffer\n");
		goto map_failed;
	}

	return 0;
map_failed:
	call_memop(q, put, buf->src_mem_priv);
	buf->src_mem_priv = NULL;
	buf->src_phys_addr = 0;
init_done:
	return ret;
}

/**
 * compo_cap_blit_buf_cleanup() - cleanup blit buffer
 */
static void compo_cap_blit_buf_cleanup(struct vb2_buffer *vb2)
{
	struct vb2_queue *q = vb2->vb2_queue;
	struct compo_cap_buf *buf = to_compo_cap_buf(vb2);
	struct capture_context *cap_ctx = vb2_get_drv_priv(q);

	/*
	 * If there's no blitter, then no memory has been allocated for converison
	 */
	if (!cap_ctx->blitter)
		goto cleanup_done;

	stm_blitter_surface_put(buf->src_surface);
	call_memop(q, put, buf->src_mem_priv);
	buf->src_mem_priv = NULL;
	buf->src_phys_addr = 0;

cleanup_done:
	return;
}

/**
 * compo_cap_buf_init() - inititalize the buffer
 */
static int compo_cap_buf_init(struct vb2_buffer *vb2)
{
	int ret = 0;

	/*
	 * Init the compo capture blit buffer
	 */
	ret = compo_cap_blit_buf_init(vb2);
	if (ret) {
		stv_err("Failed to init the blit buffer\n");
		goto init_done;
	}


	return 0;

init_done:
	return ret;
}

/**
 * compo_cap_buf_prepare() - prepare compo capture buffer
 */
static int compo_cap_buf_prepare(struct vb2_buffer *vb2)
{
	int ret = 0;
	unsigned long buf_size;
	struct pixel_info info;
	struct compo_cap_buf *buf = to_compo_cap_buf(vb2);
	struct vb2_queue *q = vb2->vb2_queue;
	struct capture_context *cap_ctx = vb2_get_drv_priv(q);
	stm_blitter_dimension_t dim = {0};
	stm_blitter_surface_address_t src_blit_addr = {0};

	if (!cap_ctx->blitter)
		goto prepare_done;

	/*
	 * Get the pixel capture format and prepare the destination buffer
	 */
	memset(&info, 0, sizeof(info));
	ret = map_pixel_cap_to_blitter_pixfmt(cap_ctx->format.format, &info);
	if (ret) {
		stv_err("failed to map pixel cap fmt to blitter fmt\n");
		goto prepare_done;
	}

	/*
	 * Create the destination surface of 8bit/BT2020
	 */
	src_blit_addr.base = *(unsigned long *)vb2_plane_cookie(vb2, 0);
	buf_size = cap_ctx->format.height * cap_ctx->format.stride;
	dim.w = cap_ctx->format.width;
	dim.h = cap_ctx->format.height;
	buf->dest_surface = stm_blitter_surface_new_preallocated(info.pixelfmt,
						STM_BLITTER_SCS_BT709_FULLRANGE,
						&src_blit_addr, buf_size,
						&dim, cap_ctx->format.stride);
	if (IS_ERR(buf->dest_surface))
		stv_err("Failed to create the surface for BT2020 buffer\n");

prepare_done:
	return ret;
}

/**
 * compo_cap_buf_cleanup() - cleanup the vb2 buffer
 */
static void compo_cap_buf_cleanup(struct vb2_buffer *vb2)
{
	/*
	 * Cleanup the blit buffer
	 */
	compo_cap_blit_buf_cleanup(vb2);
}

/**
 * compo_cap_buf_queue() - queue the buffer
 */
static void compo_cap_buf_queue(struct vb2_buffer *vb2)
{
	struct compo_cap_buf *buf = to_compo_cap_buf(vb2);
	struct vb2_queue *q = vb2->vb2_queue;
	struct capture_context *cap_ctx = vb2_get_drv_priv(q);
	struct stm_i_capture_buffer_desc buf_desc;

	memset(&buf_desc, 0, sizeof(buf_desc));

	/*
	 * Compo-capture IP works on physical address, so, we
	 * get the physical address of the buffer. In a BT2020
	 * use-case, we need to send a local buffer for capture
	 * and then translate to 8 bit, BT709
	 */
	buf_desc.plane_cnt = 1;
	if (!cap_ctx->blitter)
		buf_desc.plane_addr[0] = (void *)(*(unsigned long *)vb2_plane_cookie(vb2, 0));
	else
		buf_desc.plane_addr[0] = (void *)buf->src_phys_addr;
	if (unlikely(!buf_desc.plane_addr[0])) {
		stv_err("Failed to get the buffer physical address\n");
		goto queue_buf_failed;
	}

	buf_desc.buffer_filled = compo_cap_buffer_callback;
	buf_desc.user_data = vb2;

	/*
	 * Queue buffer to pixel capture
	 */
	if (cap_ctx->capture_intf.queue_buffer(cap_ctx->pixel_capture, &buf_desc))
		stv_err("Failed to queue buffer to pixel capture\n");


queue_buf_failed:
	return;
}

/*
 * compo-capture vb2 ops
 */
static struct vb2_ops compo_cap_vb2_ops = {
	.queue_setup = stm_vb2_pixel_cap_queue_setup,
	.wait_prepare = stm_vb2_pixel_cap_wait_prepare,
	.wait_finish = stm_vb2_pixel_cap_wait_finish,
	.buf_init = compo_cap_buf_init,
	.buf_prepare = compo_cap_buf_prepare,
	.buf_cleanup = compo_cap_buf_cleanup,
	.start_streaming = stm_vb2_pixel_cap_start_streaming,
	.stop_streaming = stm_vb2_pixel_cap_stop_streaming,
	.buf_queue = compo_cap_buf_queue,
};

/**
 * compo_cap_querycap() - compo capture query cap
 */
static int compo_cap_querycap(struct file *file, void *priv,
			   struct v4l2_capability *cap)
{
	strlcpy(cap->driver, "Compo", sizeof(cap->driver));
	strlcpy(cap->card, "STMicroelectronics", sizeof(cap->card));
	strlcpy(cap->bus_info, "platform: ST SoC", sizeof(cap->bus_info));

	cap->version = LINUX_VERSION_CODE;
	cap->device_caps = V4L2_CAP_VIDEO_CAPTURE | V4L2_CAP_STREAMING;
	cap->capabilities = cap->device_caps | V4L2_CAP_DEVICE_CAPS;

	return 0;
}

/**
 * compo_cap_v4l2_s_input() - set pixel capture input
 */
static int compo_cap_v4l2_s_input(struct file *file, void *fh, unsigned int i)
{
	int ret;
	struct output_fh *cap_fh = fh;

	ret = stm_v4l2_pixel_cap_s_input(cap_fh->cap_ctx,
				i, &compo_cap_vb2_ops,
				sizeof(struct compo_cap_buf));
	if (ret)
		stv_err("Failed to set DVP input\n");

	return ret;
}

/*
 * v4l2 ioctl ops
 */
static const struct v4l2_ioctl_ops compo_cap_ioctl_ops = {
	.vidioc_querycap = compo_cap_querycap,
	.vidioc_enum_fmt_vid_cap = stm_v4l2_pixel_cap_enum_fmt_vid_cap,
	.vidioc_g_fmt_vid_cap = stm_v4l2_pixel_cap_g_fmt_vid_cap,
	.vidioc_s_fmt_vid_cap = stm_v4l2_pixel_cap_s_fmt_vid_cap,
	.vidioc_try_fmt_vid_cap = stm_v4l2_pixel_cap_try_fmt_vid_cap,
	.vidioc_reqbufs = stm_v4l2_pixel_cap_reqbufs,
	.vidioc_querybuf = stm_v4l2_pixel_cap_querybuf,
	.vidioc_qbuf = stm_v4l2_pixel_cap_qbuf,
	.vidioc_dqbuf = stm_v4l2_pixel_cap_dqbuf,
	.vidioc_streamon = stm_v4l2_pixel_cap_streamon,
	.vidioc_streamoff = stm_v4l2_pixel_cap_streamoff,
	.vidioc_enum_input = stm_v4l2_pixel_cap_enum_input,
	.vidioc_g_input = stm_v4l2_pixel_cap_g_input,
	.vidioc_s_input = compo_cap_v4l2_s_input,
	.vidioc_s_crop = stm_v4l2_pixel_cap_s_crop,
	.vidioc_g_crop = stm_v4l2_pixel_cap_g_crop,
	.vidioc_cropcap = stm_v4l2_pixel_cap_cropcap,
};

/*
 * compo capture v4l2 ops
 */
static struct v4l2_file_operations compo_cap_v4l2_fops = {
	.owner = THIS_MODULE,
	.unlocked_ioctl = stm_v4l2_pixel_cap_unlocked_ioctl,
	.mmap = stm_v4l2_pixel_cap_mmap,
	.open = stm_v4l2_pixel_cap_open,
	.release = stm_v4l2_pixel_cap_release,
};

/*
 * media entity operations
 */
static struct media_entity_operations compo_cap_v4l2_media_entity_ops = {
	.link_setup = stm_v4l2_pixel_cap_link_setup,
};

/**
 * stm_v4l2_compo_cap_init() - init compo capture subdev and vdev
 */
int stm_v4l2_compo_cap_init(struct stm_v4l2_pixel_capture_dev *pixel_dev,
						struct capture_context *cap_ctx)
{
	int ret;
	struct video_device *vdev = &pixel_dev->viddev;

	/*
	 * Initialize compo capture device
	 */

	ret = stm_v4l2_compo_capture_subdev_init(cap_ctx);
	if (ret) {
		stv_err("Compo subdev init failed\n");
		goto compo_subdev_init_failed;
	}

	/*
	 * Register video devices for compo capture
	 */
	strlcpy(vdev->name, COMPO_CAPTURE_VDEV_NAME, sizeof(vdev->name));
	vdev->fops = &compo_cap_v4l2_fops;
	vdev->lock = &cap_ctx->lock;
	vdev->ioctl_ops = &compo_cap_ioctl_ops;
	vdev->release = stm_v4l2_pixel_cap_vdev_release;
        vdev->vfl_dir = VFL_DIR_RX;
	vdev->entity.ops = &compo_cap_v4l2_media_entity_ops;
	video_set_drvdata(vdev, cap_ctx);
	vdev->lock = &cap_ctx->lock;
	ret = stm_v4l2_pixel_cap_register_vdev(pixel_dev,
					cap_ctx, VB2_COMPO_CAPTURE_BPA2);
	if (ret) {
		stv_err("failed to register %s\n", VB2_COMPO_CAPTURE_BPA2);
		goto vdev_register_failed;
	}

	/*
	 * Create disabled links to compo-cap subdev
	 */
	ret = stm_v4l2_pixel_cap_create_disabled_links(MEDIA_ENT_T_V4L2_SUBDEV_COMPO, 1,
							&vdev->entity, 0);
	if (ret) {
		stv_err("Failed to link compo-cap vdev to compo-cap subdev\n");
		goto link_setup_failed;
	}

	return 0;

link_setup_failed:
	stm_v4l2_pixel_cap_unregister_vdev(pixel_dev, cap_ctx);
vdev_register_failed:
	stm_v4l2_compo_capture_subdev_exit(cap_ctx);
compo_subdev_init_failed:
	return ret;
}

/**
 * stm_v4l2_compo_cap_cleanup() - unregister vdev and subdev of compo capture
 */
void stm_v4l2_compo_cap_exit(struct stm_v4l2_pixel_capture_dev *pixel_dev,
						struct capture_context *cap_ctx)
{
	stm_v4l2_pixel_cap_unregister_vdev(pixel_dev, cap_ctx);
	stm_v4l2_compo_capture_subdev_exit(cap_ctx);
}
