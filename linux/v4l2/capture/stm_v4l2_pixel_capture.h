/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Header containing the structure definitions
************************************************************************/
#ifndef __STM_V4L2_PIXEL_CAPTURE_H__
#define __STM_V4L2_PIXEL_CAPTURE_H__

#include <media/media-entity.h>
#include <media/videobuf2-core.h>

#include <linux/stm/blitter.h>
#include "stm_data_interface.h"
#include "stm_pixel_capture.h"

#include "stmedia.h"

#define COMPO_CAPTURE_VDEV_NAME	"STM Compo Capture"
#define DVP_CAPTURE_VDEV_NAME	"STM DVP Capture"
#define VB2_COMPO_CAPTURE_BPA2	"v4l2-compo"

#define MUTEX_INTERRUPTIBLE(mutex)		\
	if (mutex_lock_interruptible(mutex))	\
		return -ERESTARTSYS;

struct stm_v4l2_pixel_capture_dev {
	struct video_device viddev;
	struct media_pad *pads;
};

/*
 * struct capture_context
 *
 * For the moment, the capture context is shared between dvp video device
 * and dvp subdevice. This capture context needs to be separate for both.
 *
 * @sd_pad   : this is for dvp/compo-cap video device to see if its connected to
 *             dvp/compo-cap subdevice as it's source
 * @vdev_pad : this is to save video device pad to be used by dvp/compo-capture subdev
 *             to identify, if it's connected to it.
 *             if it's connected to vdev or not.
 * @dec_pad  : this is for dvp/compo-cap subdev to see it's connected to it's sink
 * 	       i.e. v4l2 raw video decoder subdev
 */
struct capture_context {
	struct mutex lock;
	unsigned int id;
	struct media_pad *sd_pad;
	struct media_pad *vdev_pad;

	struct v4l2_subdev subdev;
	struct media_pad *pads, *dec_pad;

	int mapping_index;
	unsigned int flags;

	unsigned int vq_set;
	struct vb2_queue vq;

	stm_pixel_capture_h pixel_capture;
	stm_pixel_capture_buffer_format_t format;
	stm_pixel_capture_input_params_t params;

	unsigned long chroma_offset;
	unsigned long buffer_size;

	/*
	 * @blitter : blitter handle in case of BT2020 grab
	 */
	stm_blitter_t *blitter;
	void *mem_bpa2_ctx;
	stm_data_interface_capture_t capture_intf;
};

struct output_fh {
	struct v4l2_fh fh;
	struct capture_context *cap_ctx;
};

/*
 * struct pixel_info: pixel info containing conversion information
 * @pixelcode   : this contains the converted pixel code from one sub-system
 *                to another. Mainly used to represent v4l2_mbus_pixelcode.
 * @pixelfmt    : this contains converted pixel format from one sub-system
 *                to another. This represents pixel format of any subsystem.
 * @bitsperpixel: number of bits per pixel
 */
struct pixel_info {
	union {
		__u32 pixelcode;
		__u32 pixelfmt;
	};
	short int bitsperpixel;
};

/*
 * Indexes to find out the input context
 */
#define COMPO_INDEX			0
#define DVP_INDEX			1
#define MAX_CAPTURE_DEVICES		(DVP_INDEX + 1)

/*
 * Compo Pads indexing
 */
#define COMPO_SNK_PAD			0
#define COMPO_SRC_PAD			1

/*
 * DVP pads indexing
 */
#define DVP_SNK_PAD			0
#define DVP_SRC_PAD			1

#define VIDIOC_STI_S_VIDEO_INPUT_STABLE		\
	_IOW('V', BASE_VIDIOC_PRIVATE + 1, int )

#define V4L2_STI_FMT_CHANGE		\
	_IOW('V', BASE_VIDIOC_PRIVATE + 2, void * )

/*
 * Function declaration/definitions
 */
int stm_v4l2_compo_capture_subdev_init(struct capture_context *cap_ctx);
void stm_v4l2_compo_capture_subdev_exit(struct capture_context *cap_ctx);

#ifdef CONFIG_STLINUXTV_DVP
int stm_v4l2_dvp_subdev_init(struct capture_context *cap_ctx);
void stm_v4l2_dvp_subdev_exit(struct capture_context *cap_ctx);
#else
static inline int stm_v4l2_dvp_subdev_init(struct capture_context *cap_ctx)
{
	return 0;
}

static inline void stm_v4l2_dvp_subdev_exit(struct capture_context *cap_ctx)
{
	return;
}
#endif

/*
 * Common function declarations for compo capture and dvp
 */
int stm_v4l2_pixel_cap_create_disabled_links(u32 src_entity_type, u16 src_pad,
					struct media_entity *sink, u16 sink_pad);
int stm_v4l2_pixel_cap_link_setup(struct media_entity *entity,
		const struct media_pad *local,
		const struct media_pad *remote, u32 flags);
int stm_v4l2_pixel_cap_subdev_link_status(const struct media_pad *local,
					const struct media_pad *remote);
void stm_v4l2_compo_cap_subdev_notify(struct media_pad *pad, __u32 cmd, void *val);

/*
 * V4L2 file operations
 */
int stm_v4l2_pixel_cap_open(struct file *file);
int stm_v4l2_pixel_cap_release(struct file *file);
int stm_v4l2_pixel_cap_mmap(struct file *file, struct vm_area_struct *vma);

/*
 * V4L2 video device ioctls
 */
long stm_v4l2_pixel_cap_unlocked_ioctl(struct file *file,
			unsigned int cmd, unsigned long arg);
int stm_v4l2_pixel_cap_enum_fmt_vid_cap(struct file *file, void *fh,
					struct v4l2_fmtdesc *f);
int stm_v4l2_pixel_cap_g_fmt_vid_cap(struct file *file, void *fh,
					struct v4l2_format *f);
int stm_v4l2_pixel_cap_s_fmt_vid_cap(struct file *file,
				void *fh, struct v4l2_format *f);
int stm_v4l2_pixel_cap_try_fmt_vid_cap(struct file *file, void *fh,
					struct v4l2_format *f);
int stm_v4l2_pixel_cap_reqbufs(struct file *file,
			void *priv, struct v4l2_requestbuffers *p);
int stm_v4l2_pixel_cap_querybuf(struct file *file,
				void *priv, struct v4l2_buffer *p);
int stm_v4l2_pixel_cap_qbuf(struct file *file,
				void *priv, struct v4l2_buffer *p);
int stm_v4l2_pixel_cap_dqbuf(struct file *file,
				void *priv, struct v4l2_buffer *p);
int stm_v4l2_pixel_cap_streamon(struct file *file,
			void *priv, enum v4l2_buf_type i);
int stm_v4l2_pixel_cap_streamoff(struct file *file,
				void *priv, enum v4l2_buf_type i);
int stm_v4l2_pixel_cap_g_input(struct file *file,
				void *priv, unsigned int *i);
int stm_v4l2_pixel_cap_enum_input(struct file *file,
			void *fh, struct v4l2_input *inp);
int stm_v4l2_pixel_cap_s_input(struct capture_context *cap_ctx,
				unsigned int i, struct vb2_ops *vb2_ops,
				unsigned int buffer_size);
int stm_v4l2_pixel_cap_s_crop(struct file *file,
			void *fh, const struct v4l2_crop *c);
int stm_v4l2_pixel_cap_g_crop(struct file *file,
			void *fh, struct v4l2_crop *c);
int stm_v4l2_pixel_cap_cropcap(struct file *file,
			void *fh, struct v4l2_cropcap *c);
#endif

/*
 * vb2 queue operations
 */
int stm_vb2_pixel_cap_queue_setup(struct vb2_queue *vq,
				const struct v4l2_format *f,
				unsigned int *nbuffers,
				unsigned int *nplanes,
				unsigned int sizes[], void *alloc_ctxs[]);
void stm_vb2_pixel_cap_wait_prepare(struct vb2_queue *q);
void stm_vb2_pixel_cap_wait_finish(struct vb2_queue *q);
int stm_vb2_pixel_cap_buf_prepare(struct vb2_buffer *vb2);
int stm_vb2_pixel_cap_buf_init(struct vb2_buffer *vb2);
void stm_vb2_pixel_cap_buf_cleanup(struct vb2_buffer *vb2);
void stm_vb2_pixel_cap_buf_queue(struct vb2_buffer *vb2);
int stm_vb2_pixel_cap_start_streaming(struct vb2_queue *q,
						unsigned int count);
int stm_vb2_pixel_cap_stop_streaming(struct vb2_queue *q);

/*
 * V4L2 device registration API's
 */
int stm_v4l2_compo_cap_init(struct stm_v4l2_pixel_capture_dev *pixel_cap,
					struct capture_context *cap_ctx);
void stm_v4l2_compo_cap_exit(struct stm_v4l2_pixel_capture_dev *pixel_cap,
					struct capture_context *cap_ctx);
#ifdef CONFIG_STLINUXTV_DVP
int stm_v4l2_dvp_init(struct stm_v4l2_pixel_capture_dev *pixel_cap,
					struct capture_context *cap_ctx);
void stm_v4l2_dvp_exit(struct stm_v4l2_pixel_capture_dev *pixel_cap,
					struct capture_context *cap_ctx);
#else
static inline int stm_v4l2_dvp_init(struct stm_v4l2_pixel_capture_dev *pixel_cap,
						struct capture_context *cap_ctx)
{
	return 0;
}
static inline void stm_v4l2_dvp_exit(struct stm_v4l2_pixel_capture_dev *pixel_cap,
					struct capture_context *cap_ctx)
{
}
#endif
int stm_v4l2_pixel_cap_register_vdev(struct stm_v4l2_pixel_capture_dev *pixel_cap,
				struct capture_context *cap_ctx, char *bpa2_name);
void stm_v4l2_pixel_cap_unregister_vdev(struct stm_v4l2_pixel_capture_dev *pixel_cap,
					struct capture_context *cap_ctx);
void stm_v4l2_pixel_cap_vdev_release(struct video_device *vdev);

/*
 * V4L2 to pixel capture conversion definitions
 */
stm_pixel_capture_color_space_t
	map_v4l2_to_capture_colorspace(enum v4l2_colorspace v4l2_colorspace);
stm_pixel_capture_format_t
	map_v4l2_to_capture_pixelcode(enum v4l2_mbus_pixelcode pixel_code);
stm_pixel_capture_format_t map_v4l2_to_capture_pixelfmt(__u32 pixfmt);
stm_pixel_capture_flags_t map_v4l2_to_capture_field(enum v4l2_field field);
enum v4l2_mbus_pixelcode
	map_capture_to_v4l2_pixelcode(stm_pixel_capture_format_t pixel_fmt);
int map_capture_to_v4l2_pixelfmt(stm_pixel_capture_format_t stm_pixfmt,
							struct pixel_info *info);
enum v4l2_field
	map_capture_to_v4l2_field(stm_pixel_capture_flags_t flags);
enum v4l2_colorspace
	map_capture_to_v4l2_colorspace(stm_pixel_capture_color_space_t stm_color_space);
int map_pixel_cap_to_blitter_pixfmt(stm_pixel_capture_format_t cap_pixfmt,
							struct pixel_info *info);

/*
 * Pixel capture control definitions
 */
bool stm_capture_streaming(struct capture_context *cap_ctx);
int stm_capture_streamoff(struct capture_context *cap_ctx,
				struct stmedia_v4l2_subdev *dec_stm_sd);
int stm_capture_streamon(struct capture_context *cap_ctx,
				struct stmedia_v4l2_subdev *dec_stm_sd);
int stm_capture_enum_frame_size(struct capture_context *cap_ctx,
				struct v4l2_subdev_frame_size_enum *fse);
