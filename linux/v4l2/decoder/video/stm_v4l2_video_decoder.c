/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Implementation of v4l2 video decoder subdev
************************************************************************/
#include <linux/videodev2.h>

#include "stm_se.h"
#include "stmedia.h"
#include "linux/stm/stmedia_export.h"
#include "linux/stm_v4l2_export.h"
#include "stm_v4l2_decoder.h"
#include "stm_v4l2_video_decoder.h"
#include "linux/stm_v4l2_decoder_export.h"
#include "backend.h"
#include "stmvout_driver.h"
#include "stv_debug.h"

#define MUTEX_INTERRUPTIBLE(mutex)	\
	if (mutex_lock_interruptible(mutex))	\
		return -ERESTARTSYS;

#define SYNC_MODE	1

struct stm_video_data {
	u8 num_users;
	u32 grab_type, src_count, evt_err, ctrl_id;
	struct stmvout_src_info src_info;
	struct semaphore listener_sem;
};

static struct stm_v4l2_decoder_context *stm_v4l2_rawvid_ctx;
static struct stm_video_data *stm_video_data;

/**
 * rawvid_connect_callback() - connect callback for src to plane
 */
void rawvid_connect_callback(void *priv, const stm_time64_t vsync_time,
			const stm_asynch_ctrl_status_t *status, int count)
{
	int i;
	struct stm_v4l2_decoder_context *rawvid_ctx = priv;
	struct stm_video_data *vid_data = rawvid_ctx->priv;

	for (i = 0; i < count; i++) {
		if (vid_data->ctrl_id == status[i].ctrl_id) {
			vid_data->evt_err = status[i].error_code;
			up(&vid_data->listener_sem);
		}
	}
}

/**
 * rawvid_connect_planes() - connect decoder to plane
 */
static int
rawvid_connect_planes(struct stm_v4l2_decoder_context *rawvid_ctx,
					const struct media_pad *disp_pad)
{
	int ret = 0;
	stm_object_h plane;
	struct stmvout_ctrl vout_ctrl;
	struct stm_video_data *vid_data = rawvid_ctx->priv;

	/*
	 * With the new attachment model, the src is to be created by decoder
	 * and attached to plane. Later when play stream is created, we
	 * connect play stream to src.
	 * In case of multi plane connection, we will not create a new source,
	 * in fact use an earlier one, and connect the same to plane
	 */
	plane = (entity_to_stmedia_v4l2_subdev(disp_pad->entity))->stm_obj;
	if (!stmvout_source_get_status(&vid_data->src_info)) {
		ret = stmvout_plane_get_src(plane, &vid_data->src_info);
		if (ret) {
			pr_err("%s(): all sources exhausted cannot connect\n", __func__);
			ret = -EBUSY;
			goto connect_busy;
		}
		vid_data->src_count = 1;
	} else
		vid_data->src_count++;

	/*
	 * Now we have plane source, so, connect it to
	 */
	memset(&vout_ctrl, 0, sizeof(vout_ctrl));
	vout_ctrl.id = PLANE_CTRL_CONNECT_TO_SOURCE;
	vout_ctrl.data = vid_data->src_info.src;
	vout_ctrl.callback = rawvid_connect_callback;
	vout_ctrl.priv = (void *)rawvid_ctx;
	vout_ctrl.sem = &vid_data->listener_sem;
	vid_data->ctrl_id = vout_ctrl.id;
	ret = stmvout_plane_set_ctrl(SYNC_MODE, plane, &vout_ctrl);
	if (ret) {
		pr_err("%s(): failed to connect to plane\n", __func__);
		goto connect_failed;
	}
	if (SYNC_MODE && vid_data->evt_err) {
		pr_err("%s(): failed to connect src with plance\n", __func__);
		ret = vid_data->evt_err;
		goto connect_failed;
	}

	/*
	 * See if it's possible to connect src to play stream
	 */
	if (rawvid_ctx->play_stream) {
		ret = stm_se_play_stream_attach(rawvid_ctx->play_stream,
					vid_data->src_info.src,
					STM_SE_PLAY_STREAM_OUTPUT_PORT_DEFAULT);
		if (ret && (ret != -EALREADY)) {
			pr_err("%s(): failed to attach play stream to source\n", __func__);
			goto attach_failed;
		} else
			ret = 0;
	}

	vid_data->ctrl_id = 0;
	return ret;

attach_failed:
	vout_ctrl.id = PLANE_CTRL_DISCONNECT_FROM_SOURCE;
	ret = stmvout_plane_set_ctrl(SYNC_MODE, plane, &vout_ctrl);
	if (ret)
		stv_err("failed to disconnect to plane\n");
connect_failed:
	if (!(--vid_data->src_count)) {
		stmvout_plane_put_src(&vid_data->src_info);
		vid_data->src_info.src = NULL;
		vid_data->src_info.display = NULL;
	}
connect_busy:
	return ret;
}

/**
 * rawvid_disconnect_planes() - disconnect decoder from plane
 */
static int
rawvid_disconnect_planes(struct stm_v4l2_decoder_context *rawvid_ctx,
					const struct media_pad *disp_pad)
{
	int display_id,ret = 0;
	stm_object_h plane;
	struct stmvout_ctrl vout_ctrl;
	struct stm_video_data *vid_data = rawvid_ctx->priv;

	plane = (entity_to_stmedia_v4l2_subdev(disp_pad->entity))->stm_obj;

	ret = stm_display_plane_get_device_id(plane, &display_id);
	if (ret) {
		pr_err("%s: failed to get display id\n", __func__);
		goto get_device_id_failed;
	}

	/*
	 * Get display from the display id
	 */
	ret = stm_display_open_device(display_id, &vid_data->src_info.display);
	if (ret) {
		pr_err("%s: failed to get display handle\n", __func__);
		goto get_device_id_failed;
	}
	/*
	 * Detach decoder from source
	 */
	if (rawvid_ctx->play_stream && (vid_data->src_count))
		stm_se_play_stream_detach(rawvid_ctx->play_stream,
						vid_data->src_info.src);

	/*
	 * Now we have plane source, so, disconnect it
	 */
	memset(&vout_ctrl, 0, sizeof(vout_ctrl));
	vout_ctrl.id = PLANE_CTRL_DISCONNECT_FROM_SOURCE;
	vout_ctrl.data = vid_data->src_info.src;
	vout_ctrl.callback = rawvid_connect_callback;
	vout_ctrl.priv = rawvid_ctx;
	vout_ctrl.sem = &vid_data->listener_sem;
	vid_data->ctrl_id = vout_ctrl.id;
	ret = stmvout_plane_set_ctrl(SYNC_MODE, plane, &vout_ctrl);
	if (ret)
		pr_err("%s(): failed to disconnect to plane\n", __func__);
	if (SYNC_MODE && vid_data->evt_err) {
		pr_err("%s(): failed to disconnect src from plane\n", __func__);
		ret = vid_data->evt_err;
	}
	if (!(--vid_data->src_count)) {
		stmvout_plane_put_src(&vid_data->src_info);
		vid_data->src_info.src = NULL;
	}

	stm_display_device_close(vid_data->src_info.display);
	vid_data->src_info.display = NULL;
	vid_data->ctrl_id = 0;

get_device_id_failed:
	return ret;
}

/**
 * rawvid_connect_grabber() - create a playback and play_stream
 * @rawvid_ctx : Uncompressed video decoder context
 * The connect is called from link_setup twice, once explicitly
 * from source (dvp/compo) and then by media controller.
 */
static int rawvid_connect_grabber(struct stm_v4l2_decoder_context *rawvid_ctx,
						struct stmedia_v4l2_subdev *dec_stm_sd)
{
	int ret = 0;
	char name[16];

	/*
	 * No-go ahead if already created
	 */
	if (rawvid_ctx->play_stream) {
		pr_debug("%s(): raw video play stream already created\n", __func__);
		goto connect_done;
	}

	/*
	 * Check if the playback is already created
	 */
	if (!rawvid_ctx->playback_ctx) {
		pr_err("%s(): decoder not configured for streamon\n",  __func__);
		ret = -EINVAL;
		goto connect_done;
	}

	/*
	 * Create a new raw video playstream
	 */
	snprintf(name, sizeof(name), "v4l2.video%d", rawvid_ctx->id);
	ret = stm_se_play_stream_new(name, rawvid_ctx->playback_ctx->playback,
				STM_SE_STREAM_ENCODING_VIDEO_UNCOMPRESSED,
				&rawvid_ctx->play_stream);
	if (ret) {
		pr_err("%s(): failed to create video play stream\n", __func__);
		goto connect_done;
	}
	dec_stm_sd->stm_obj = rawvid_ctx->play_stream;

	/*
	 * Connect to encoder now. This is a deferred connect
	 */
	ret = stm_decoder_setup_encoder(&rawvid_ctx->pads[RAW_VID_SRC_PAD], NULL,
					dec_stm_sd->stm_obj,
					MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_ENCODER,
					MEDIA_LNK_FL_ENABLED);
	if (ret) {
		pr_err("%s(): failed to attach decoder to encoder\n", __func__);
		goto connect_encoder_failed;
	}

	return 0;

connect_encoder_failed:
	stm_se_play_stream_delete(rawvid_ctx->play_stream);
	rawvid_ctx->play_stream = NULL;
	dec_stm_sd->stm_obj = NULL;
connect_done:
	return ret;
}

/**
 * rawvid_disconnect_grabber() - disconnect from hdmirx
 */
static void rawvid_disconnect_grabber(struct stm_v4l2_decoder_context *rawvid_ctx,
						struct stmedia_v4l2_subdev *dec_stm_sd)
{

	if (stm_decoder_setup_encoder(&rawvid_ctx->pads[RAW_VID_SRC_PAD], NULL,
					dec_stm_sd->stm_obj,
					MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_ENCODER,
					!MEDIA_LNK_FL_ENABLED))
		pr_err("%s(): failed to disconnect with encoder\n", __func__);

	/*
	 * Called again, so, no play_stream, we go out
	 */
	if (!rawvid_ctx->play_stream)
		goto done;

	/*
	 * Delete play stream and reset the context
	 */
	if (stm_se_play_stream_delete(rawvid_ctx->play_stream))
		pr_err("%s(): failed to delete play stream\n", __func__);
	rawvid_ctx->play_stream = NULL;
	dec_stm_sd->stm_obj = NULL;

done:
	return;
}

/**
 * rawvid_capture_start() - start video capturing
 */
static int rawvid_capture_start(struct stm_v4l2_decoder_context *rawvid_ctx)
{
	bool status;
	int ret = 0, id = 0;
	struct media_pad *grab_pad;
	struct v4l2_subdev *grab_sd;
	struct stm_video_data *vid_data = rawvid_ctx->priv;

	/*
	 * Find the remote capture subdev
	 */
	grab_pad = stm_media_find_remote_pad_with_type
					(&rawvid_ctx->pads[RAW_VID_SNK_PAD],
					MEDIA_LNK_FL_ENABLED,
					vid_data->grab_type,
					&id);
	if (!grab_pad) {
		pr_err("%s(): connect capture first, capture not started\n", __func__);
		ret = -ENOTCONN;
		goto start_failed;
	}

	/*
	 * Connect play stream with display source
	 */
	status = stmvout_source_get_status(&vid_data->src_info);
	if (status) {
		ret = stm_se_play_stream_attach(rawvid_ctx->play_stream,
					vid_data->src_info.src,
					STM_SE_PLAY_STREAM_OUTPUT_PORT_DEFAULT);
		if (ret && (ret != -EALREADY)) {
			pr_err("%s(): failed to attach play stream to source\n", __func__);
			goto start_failed;
		} else
			ret = 0;
	}

	/*
	 * Start streaming
	 */
	grab_sd = media_entity_to_v4l2_subdev(grab_pad->entity);
	ret = v4l2_subdev_call(grab_sd, video, s_stream, 1);
	if (ret) {
		pr_err("%s(): failed to streamon capture\n", __func__);
		goto streaming_failed;
	}

	return 0;

streaming_failed:
	if (vid_data->src_info.src)
		stm_se_play_stream_detach(rawvid_ctx->play_stream,
						vid_data->src_info.src);
start_failed:
	return ret;
}

/**
 * rawvid_capture_stop() - stop video capturing
 */
static int rawvid_capture_stop(struct stm_v4l2_decoder_context *rawvid_ctx)
{
	int ret = 0, id = 0;
	struct media_pad *grab_pad;
	struct v4l2_subdev *grab_sd;
	struct stm_video_data *vid_data = rawvid_ctx->priv;

	/*
	 * Find the remote capture subdev
	 */
	grab_pad = stm_media_find_remote_pad_with_type
					(&rawvid_ctx->pads[RAW_VID_SNK_PAD],
					MEDIA_LNK_FL_ENABLED,
					vid_data->grab_type,
					&id);
	if (!grab_pad) {
		pr_err("%s(): connect capture first, capture not started\n", __func__);
		ret = -ENOTCONN;
		goto stop_failed;
	}

	/*
	 * Stop streaming
	 */
	grab_sd = media_entity_to_v4l2_subdev(grab_pad->entity);
	ret = v4l2_subdev_call(grab_sd, video, s_stream, 0);
	if (ret)
		pr_err("%s(): failed to streamon capture\n", __func__);

	/*
	 * Detach play stream from source
	 */
	if (vid_data->src_info.src) {
		if (stm_se_play_stream_detach(rawvid_ctx->play_stream,
						vid_data->src_info.src))
			pr_err("%s(): failed to detach play stream from src\n", __func__);
	}

stop_failed:
	return ret;
}

/**
 * rawvid_subdev_core_ioctl() - subdev core ioctl op
 */
static long rawvid_subdev_core_ioctl(struct v4l2_subdev *sd,
					unsigned int cmd, void *arg)
{
	long ret = 0;
	struct stmedia_v4l2_subdev *dec_stm_sd;
	struct stm_v4l2_decoder_context *rawvid_ctx = v4l2_get_subdevdata(sd);

	dec_stm_sd = container_of(sd, struct stmedia_v4l2_subdev, sdev);

	/*
	 * FIXME: Bypass for now, but, we need to do sanity on ioctl
	 */

	MUTEX_INTERRUPTIBLE(&rawvid_ctx->mutex);

	switch(cmd) {
	case VIDIOC_SUBDEV_STI_STREAMON:

		/*
		 * Configure video capture
		 */
		ret = rawvid_connect_grabber(rawvid_ctx, dec_stm_sd);
		if (ret) {
			pr_err("%s(): failed to setup the capture\n", __func__);
			break;
		}

		/*
		 * Start video capture
		 */
		ret = rawvid_capture_start(rawvid_ctx);
		if (ret) {
			rawvid_disconnect_grabber(rawvid_ctx, dec_stm_sd);
			pr_err("%s(): failed to start video capture\n", __func__);
		}

		break;

	case VIDIOC_SUBDEV_STI_STREAMOFF:

		/*
		 * Stop capturing
		 */
		ret = rawvid_capture_stop(rawvid_ctx);
		if (ret)
			pr_err("%s(): failed to stop capture\n", __func__);

		/*
		 * Destroy the playback and the play stream
		 */
		rawvid_disconnect_grabber(rawvid_ctx, dec_stm_sd);

		break;

	default:
		ret = -EINVAL;
		break;
	}

	mutex_unlock(&rawvid_ctx->mutex);

	return ret;
}

/*
 * raw video decoder subdev core ops
 */
static struct v4l2_subdev_core_ops rawvid_subdev_core_ops = {
	.g_ctrl = v4l2_subdev_g_ctrl,
	.s_ctrl = v4l2_subdev_s_ctrl,
	.g_ext_ctrls = v4l2_subdev_g_ext_ctrls,
	.s_ext_ctrls = v4l2_subdev_s_ext_ctrls,
	.ioctl = rawvid_subdev_core_ioctl,
};

/*
 * raw video decoder subdev ops
 */
static struct v4l2_subdev_ops rawvid_subdev_ops = {
	.core = &rawvid_subdev_core_ops,
};

/**
 * rawvid_link_setup() - link setup function
 */
static int rawvid_link_setup(struct media_entity *entity,
				const struct media_pad *local,
				const struct media_pad *remote, u32 flags)
{
	int ret = 0, capture_policy = 0;
	struct stmedia_v4l2_subdev *dec_stm_sd;
	struct stm_v4l2_decoder_context *rawvid_ctx;
	struct stm_video_data *vid_data;

	dec_stm_sd = entity_to_stmedia_v4l2_subdev(entity);
	rawvid_ctx = v4l2_get_subdevdata(&dec_stm_sd->sdev);
	vid_data = rawvid_ctx->priv;

	switch (remote->entity->type) {
	case MEDIA_ENT_T_V4L2_SUBDEV_DVP:
	case MEDIA_ENT_T_V4L2_SUBDEV_COMPO:
	{
		struct v4l2_ctrl *capture_profile;

		pr_debug("%s(): <<IN process link Setup to capture\n", __func__);

		capture_profile = v4l2_ctrl_find(dec_stm_sd->sdev.ctrl_handler,
					V4L2_CID_MPEG_STI_CAPTURE_PROFILE);
		if (!capture_profile) {
			ret = -EINVAL;
			pr_err("%s(): failed to find the control for initial setup\n", __func__);
			break;
		}

		if (flags & MEDIA_LNK_FL_ENABLED) {

			MUTEX_INTERRUPTIBLE(&rawvid_ctx->mutex);

			/*
			 * Create playback in the link setup, so, that we
			 * can apply the capture profile policy.
			 */
			rawvid_ctx->playback_ctx = stm_v4l2_decoder_create_playback(rawvid_ctx->id);
			if (!rawvid_ctx->playback_ctx) {
				pr_err("%s(): failed to create playback\n", __func__);
				ret = -ENOMEM;
				mutex_unlock(&rawvid_ctx->mutex);
				break;
			}

			/*
			 * Change the SE master clock to system clock
			 */
			ret = stm_se_playback_set_control(rawvid_ctx->playback_ctx->playback,
						STM_SE_CTRL_MASTER_CLOCK,
						STM_SE_CTRL_VALUE_SYSTEM_CLOCK_MASTER);
			if(ret) {
				pr_err("%s(): failed to set system clock as master clock of SE\n", __func__);
				mutex_unlock(&rawvid_ctx->mutex);
				break;
			}

			vid_data->grab_type = remote->entity->type;
			mutex_unlock(&rawvid_ctx->mutex);

			/*
			 * Set up the initial dvp capture profile of playback to AudioDecode VideoFrc
			 */
			switch (remote->entity->type) {
			case MEDIA_ENT_T_V4L2_SUBDEV_DVP:
				 capture_policy = STM_SE_CTRL_VALUE_CAPTURE_PROFILE_HDMIRX_AUD_DEC_VID_FRC;
				 break;
			case MEDIA_ENT_T_V4L2_SUBDEV_COMPO:
				 capture_policy = STM_SE_CTRL_VALUE_CAPTURE_PROFILE_COMPO_NO_AUD_VID_FRC;
				 break;
			}
			ret = v4l2_ctrl_s_ctrl(capture_profile, capture_policy);
			if (ret)
				pr_err("%s(): failed to set the dvp capture profile policy\n", __func__);

		} else {

			/*
			 * Reset the control handler to disable dvp capture
			 */
			ret = v4l2_ctrl_s_ctrl(capture_profile, STM_SE_CTRL_VALUE_CAPTURE_PROFILE_DISABLED);
			if (ret)
				pr_err("%s(): failed to set the capture profile policy\n", __func__);

			MUTEX_INTERRUPTIBLE(&rawvid_ctx->mutex);

			/*
			 * Delete playback
			 */
			stm_v4l2_decoder_delete_playback(rawvid_ctx->id);
			vid_data->grab_type = 0;
			mutex_unlock(&rawvid_ctx->mutex);
		}

		pr_debug("%s(): <<OUT process link Setup to capture\n", __func__);

		break;
	}

	case MEDIA_ENT_T_V4L2_SUBDEV_PLANE_VIDEO:
	case MEDIA_ENT_T_V4L2_SUBDEV_PLANE_GRAPHIC:

		/*
		 * Check from the sink if it's ready for connection
		 */
		pr_debug("%s(): <<IN process link Setup to Planes\n", __func__);

		MUTEX_INTERRUPTIBLE(&rawvid_ctx->mutex);

		/*
		 * Now, the actual connect/disconnect
		 */
		if (flags & MEDIA_LNK_FL_ENABLED)

			ret = rawvid_connect_planes(rawvid_ctx, remote);
		else
			ret = rawvid_disconnect_planes(rawvid_ctx, remote);

		mutex_unlock(&rawvid_ctx->mutex);

		pr_debug("%s(): <<OUT process link Setup to Planes\n", __func__);

		break;

	case MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_ENCODER:

		MUTEX_INTERRUPTIBLE(&rawvid_ctx->mutex);

		/*
		 * Check from the sink if it's ready for connection.
		 * An encoder can connect to a single decoder, so,
		 * let's give encoder a chance to refuse
		 */
		if (flags & MEDIA_LNK_FL_ENABLED) {

			ret = media_entity_call(remote->entity,
					link_setup, remote, local, flags);
			if (ret) {
				mutex_unlock(&rawvid_ctx->mutex);
				break;
			}
		}

		/*
		 * Without any play stream we cannot connect
		 * to encoder, so, the connect is deferred
		 */
		if (!dec_stm_sd->stm_obj) {
			mutex_unlock(&rawvid_ctx->mutex);
			break;
		}

		ret = stm_decoder_setup_encoder(local, remote,
					dec_stm_sd->stm_obj,
					remote->entity->type, flags);

		mutex_unlock(&rawvid_ctx->mutex);

		break;

	default:
		ret = -EINVAL;
		break;
	}

	return ret;
}

static struct media_entity_operations rawvid_media_ops = {
	.link_setup = rawvid_link_setup,
};

/**
 * rawvid_internal_open() - subdev internal open
 */
static int rawvid_internal_open(struct v4l2_subdev *sd,
					struct v4l2_subdev_fh *fh)
{
	struct stm_v4l2_decoder_context *rawvid_ctx = v4l2_get_subdevdata(sd);
	struct stm_video_data *vid_data = (struct stm_video_data *)rawvid_ctx->priv;

	vid_data->num_users++;

	return 0;
}

/**
 * rawvid_internal_close() - subdev internal close
 */
static int rawvid_internal_close(struct v4l2_subdev *sd,
					struct v4l2_subdev_fh *fh)
{
	struct stm_v4l2_decoder_context *rawvid_ctx = v4l2_get_subdevdata(sd);
	struct stm_video_data *vid_data = (struct stm_video_data *)rawvid_ctx->priv;
	struct stmedia_v4l2_subdev *dec_stm_sd = container_of(sd, struct stmedia_v4l2_subdev, sdev);

	/*
	 * Subdevice ioctl is not giving in any file handle in the
	 * callback to decide the ownership of streaming. So, an open
	 * count is maintained to decide when to stop streaming in close
	 */
	vid_data->num_users--;
	if (!vid_data->num_users) {
		rawvid_capture_stop(rawvid_ctx);
		rawvid_disconnect_grabber(rawvid_ctx, dec_stm_sd);
	}

	return 0;
}

/*
 * rawvid subdev internal ops
 */
static struct v4l2_subdev_internal_ops rawvid_internal_ops = {
	.open = rawvid_internal_open,
	.close = rawvid_internal_close,
};

/**
 * rawvid_subdev_init() - subdev init for video decoder
 */
static int rawvid_subdev_init(struct stm_v4l2_decoder_context *rawvid_ctx)
{
	int ret = 0;
	struct v4l2_subdev *sd;
	struct media_entity *me;
	struct media_pad *pads;

	/*
	 * Init raw video decoder subdev with ops
	 */
	sd = &rawvid_ctx->stm_sd.sdev;
	v4l2_subdev_init(sd, &rawvid_subdev_ops);
	snprintf(sd->name, sizeof(sd->name), "v4l2.video%d", rawvid_ctx->id);
	v4l2_set_subdevdata(sd, rawvid_ctx);
	sd->flags = V4L2_SUBDEV_FL_HAS_DEVNODE;
	sd->internal_ops = &rawvid_internal_ops;

	pads = (struct media_pad *)kzalloc(sizeof(struct media_pad) *
					(RAW_VID_SRC_PAD + 1), GFP_KERNEL);
	if (!pads) {
		pr_err("%s(): out of memory for rawvid pads\n", __func__);
		ret = -ENOMEM;
		goto pad_alloc_failed;
	}
	rawvid_ctx->pads = pads;

	pads[RAW_VID_SNK_PAD].flags = MEDIA_PAD_FL_SINK;
	pads[RAW_VID_SRC_PAD].flags = MEDIA_PAD_FL_SOURCE;

	/*
	 * Register with media controller
	 */
	me = &sd->entity;
	ret = media_entity_init(me, RAW_VID_SRC_PAD + 1, pads, 0);
	if (ret) {
		pr_err("%s(): rawvid entity init failed\n", __func__);
		goto entity_init_failed;
	}
	me->ops = &rawvid_media_ops;
	me->type = MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_DECODER;

	/*
	 * Init control handler for raw video decoder
	 */
	sd->ctrl_handler = &rawvid_ctx->ctrl_handler;
	ret = stm_v4l2_rawvid_ctrl_init(sd->ctrl_handler, rawvid_ctx);
	if (ret) {
		pr_err("%s(): failed to init rawvid ctrl handler\n", __func__);
		goto ctrl_init_failed;
	}

	/*
	 * Register v4l2 subdev
	 */
	ret = stm_media_register_v4l2_subdev(sd);
	if (ret) {
		pr_err("%s() : failed to register raw viddec subdev\n", __func__);
		goto subdev_register_failed;
	}

	return 0;

subdev_register_failed:
	stm_v4l2_viddec_ctrl_exit(sd->ctrl_handler);
ctrl_init_failed:
	media_entity_cleanup(me);
entity_init_failed:
	kfree(rawvid_ctx->pads);
pad_alloc_failed:
	return ret;
}

/**
 * rawvid_release_display_source() - release the display sources
 */
static int
rawvid_release_display_source(struct stm_v4l2_decoder_context *rawvid_ctx,
							u32 plane_type)
{
	int id = 0, ret = 0;
	struct media_pad *disp_pad;
	struct stm_video_data *vid_data = rawvid_ctx->priv;

	/*
	 * Take the mutex to avoid the race between media-ctl and module exit.
	 */
	MUTEX_INTERRUPTIBLE(&rawvid_ctx->mutex);

	if (!vid_data->src_info.src)
		goto release_done;

	/*
	 * Someone, still using the source, refuse unload
	 */
	if(vid_data->src_count) {
		ret = -EBUSY;
		goto release_done;
	}

	do {
		disp_pad = stm_media_find_remote_pad_with_type
					(&rawvid_ctx->pads[RAW_VID_SRC_PAD],
					MEDIA_LNK_FL_ENABLED, plane_type,
					&id);
		if (!disp_pad)
			break;

		/*
		 * Put the source
		 */
		stmvout_plane_put_src(&vid_data->src_info);
		vid_data->src_info.src = NULL;
		vid_data->src_info.display = NULL;
	} while(1);

release_done:
	mutex_unlock(&rawvid_ctx->mutex);
	return ret;
}

/*
 * rawvid_subdev_exit() - cleans up the raw video subdevice
 */
static void rawvid_subdev_exit(struct stm_v4l2_decoder_context *rawvid_ctx)
{
	rawvid_release_display_source(rawvid_ctx,
				MEDIA_ENT_T_V4L2_SUBDEV_PLANE_VIDEO);
	rawvid_release_display_source(rawvid_ctx,
				MEDIA_ENT_T_V4L2_SUBDEV_PLANE_GRAPHIC);

	stm_media_unregister_v4l2_subdev(&rawvid_ctx->stm_sd.sdev);
	media_entity_cleanup(&rawvid_ctx->stm_sd.sdev.entity);
	kfree(rawvid_ctx->pads);
}

/**
 * stm_v4l2_rawvid_subdev_init() - initialize the raw video decoder subdev
 */
static int __init stm_v4l2_rawvid_subdev_init(void)
{
	int ret = 0, i;
	struct stm_v4l2_decoder_context *rawvid_ctx;

	pr_debug("%s(): <<IN create subdev for raw video decoder\n", __func__);

	/*
	 * Allocate the raw video decoder context
	 */
	rawvid_ctx = kzalloc(sizeof(*rawvid_ctx) * V4L2_MAX_PLAYBACKS,
					GFP_KERNEL);
	if (!rawvid_ctx) {
		pr_err("%s(): out of memory for video context\n", __func__);
		ret = -ENOMEM;
		goto ctx_alloc_failed;
	}
	stm_v4l2_rawvid_ctx = rawvid_ctx;

	/*
	 * Allocate stm video private data
	 */
	stm_video_data = kzalloc(sizeof(*stm_video_data)
				* V4L2_MAX_PLAYBACKS, GFP_KERNEL);
	if (!stm_video_data) {
		pr_err("%s(): out of memory for video data\n", __func__);
		ret = -ENOMEM;
		goto data_alloc_failed;
	}

	/*
	 * Set the ids for the video contexts
	 */
	for (i = 0; i < V4L2_MAX_PLAYBACKS; i++) {
		mutex_init(&rawvid_ctx[i].mutex);
		rawvid_ctx[i].id = i;
		rawvid_ctx[i].priv = (void *)&stm_video_data[i];
		sema_init(&stm_video_data[i].listener_sem, 0);
		ret = rawvid_subdev_init(&rawvid_ctx[i]);
		if (ret) {
			pr_err("%s(): subdev(%d) init failed\n", __func__, i);
			goto subdev_init_failed;
		}
	}

	/*
	 * Create disabled links with dvp capture as source
	 * TODO: src_pad = 1
	 */
	ret = stm_v4l2_create_links(MEDIA_ENT_T_V4L2_SUBDEV_DVP, 1, NULL,
			MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_DECODER, RAW_VID_SNK_PAD,
			"v4l2.video", !MEDIA_LNK_FL_ENABLED);
	if (ret) {
		pr_err("%s(): failed to create links from dvp\n", __func__);
		goto subdev_init_failed;
	}

	/*
	 * Create disabled links with compo capture as source
	 * TODO: src_pad = 1
	 */
	ret = stm_v4l2_create_links(MEDIA_ENT_T_V4L2_SUBDEV_COMPO, 1, NULL,
			MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_DECODER, RAW_VID_SNK_PAD,
			"v4l2.video", !MEDIA_LNK_FL_ENABLED);
	if (ret) {
		pr_err("%s(): failed to create links from compo\n", __func__);
		goto subdev_init_failed;
	}

	/*
	 * Create Disabled links with video planes as sink
	 */
	ret = stm_v4l2_create_links(MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_DECODER,
			RAW_VID_SRC_PAD, "v4l2.video",
			MEDIA_ENT_T_V4L2_SUBDEV_PLANE_VIDEO,
			0, NULL, !MEDIA_LNK_FL_ENABLED);
	if (ret) {
		pr_err("%s(): failed to create links to vdp planes\n", __func__);
		goto subdev_init_failed;
	}

	/*
	 * Create Disabled links with video planes as sink
	 */
	ret = stm_v4l2_create_links(MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_DECODER,
			RAW_VID_SRC_PAD, "v4l2.video",
			MEDIA_ENT_T_V4L2_SUBDEV_PLANE_GRAPHIC,
			0, NULL, !MEDIA_LNK_FL_ENABLED);
	if (ret) {
		pr_err("%s(): failed to create links to gdp planes\n", __func__);
		goto subdev_init_failed;
	}

	/*
	 * Create disabled links with encoder as sink
	 */
	ret = stm_v4l2_create_links(MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_DECODER,
			RAW_VID_SRC_PAD, NULL, MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_ENCODER,
			0, NULL, !MEDIA_LNK_FL_ENABLED);
	if (ret) {
		pr_err("%s(): failed to create links to video encoder\n", __func__);
		goto subdev_init_failed;
	}

	pr_debug("%s(): OUT>> create subdev for raw video decoder\n", __func__);

	return 0;

subdev_init_failed:
	for (i = i - 1; i >= 0; i--)
		rawvid_subdev_exit(&rawvid_ctx[i]);
	kfree(stm_video_data);
data_alloc_failed:
	kfree(stm_v4l2_rawvid_ctx);
ctx_alloc_failed:
	return ret;
}

/**
 * stm_v4l2_rawvid_subdev_exit() - exit v4l2 video deocder subdev
 */
static void stm_v4l2_rawvid_subdev_exit(void)
{
	int i;

	for (i = 0; i < V4L2_MAX_PLAYBACKS; i++)
		rawvid_subdev_exit(&stm_v4l2_rawvid_ctx[i]);

	kfree(stm_v4l2_rawvid_ctx);
}

/**
 * stm_v4l2_video_decoder_init() - init v4l2 video decoder
 */
int __init stm_v4l2_video_decoder_init(void)
{
	/*
	 * For the moment only subdev to init
	 */
	return stm_v4l2_rawvid_subdev_init();
}

/**
 * stm_v4l2_video_decoder_exit() - exit v4l2 video decoder
 */
void stm_v4l2_video_decoder_exit(void)
{
	stm_v4l2_rawvid_subdev_exit();
}
