/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/
#include <linux/v4l2-mediabus.h>
#include <media/v4l2-subdev.h>
#include <media/v4l2-event.h>

#include <stm_display.h>

#include "linux/dvb/dvb_v4l2_export.h"
#include "linux/stm/stmedia_export.h"
#include "stmvout_driver.h"
#include "stm_v4l2_pixel_capture.h"
#include "stv_debug.h"

#define OUTPUT_EVT_RESOLUTION		0x1

#define MUTEX_INTERRUPTIBLE(mutex)		\
	if (mutex_lock_interruptible(mutex))	\
		return -ERESTARTSYS;

static int output_configure_compo_cap(const struct media_pad *local,
					const struct media_pad *remote,
					stm_display_mode_t *mode);
/**
 * output_open_display() - display open for output operations
 */
static int output_open_display(struct output_plug *output_ctx)
{
	int ret = 0;

	if (output_ctx->disp_refcount) {
		output_ctx->disp_refcount++;
		goto open_done;
	}

	/*
	 * Error out in case there's no display device for this output
	 */
	if (unlikely(output_ctx->display_device_id == -1)) {
		stv_err("output without a display\n");
		ret = -EINVAL;
		goto open_done;
	}

	if (output_ctx->hDisplay) {
		stv_debug("already opened\n");
		goto open_done;
	}

	ret = stm_display_open_device(output_ctx->display_device_id,
						&output_ctx->hDisplay);
	if (ret) {
		stv_err("failed to open display device (%d)\n", ret);
		goto open_done;
	}
	output_ctx->disp_refcount++;

open_done:
	return ret;
}

/**
 * output_close_display() - display close for output closure
 */
static inline void output_close_display(struct output_plug *output_ctx)
{
	/*
	 * Close the display device
	 */
	if (--output_ctx->disp_refcount)
		return;

	stm_display_device_close(output_ctx->hDisplay);
	output_ctx->hDisplay = NULL;
}

/**
 * output_get_display_mode() - get output display mode
 */
static inline int output_get_display_mode(struct output_plug *output_ctx,
						stm_display_mode_t *mode)
{
	int ret;

	/*
	 * Get the display mode. If the display is not started, then, cannot
	 * get any information out of compositor
	 */
	ret = stm_display_output_get_current_display_mode(output_ctx->hOutput, mode);
	if (ret) {
		stv_err("failed to get current display mode\n");
		goto get_mode_failed;
	}
	if (mode->mode_id == STM_TIMING_MODE_RESERVED) {
		stv_err("No output, so, compo capture will be setup later\n");
		ret = -EHOSTDOWN;
	}

get_mode_failed:
	return ret;
}

/*
 * output_event_queue() - queue to the event to device
 */
static void output_event_queue(struct v4l2_subdev *sd, __u32 type, __u32 val)
{
	struct v4l2_event evt;

	memset(&evt, 0, sizeof(evt));

	switch (type) {
	case OUTPUT_EVT_RESOLUTION:
		evt.id = 1;
		evt.type = V4L2_EVENT_SOURCE_CHANGE;
		evt.u.src_change.changes = val;
		break;
	}

	if (sd->devnode)
		v4l2_event_queue(sd->devnode, &evt);
}

/**
 * output_event_callback() - callback notifying the status of output
 */
static void output_event_callback(int evt_num, stm_event_info_t *events)
{
	int i, ret;
	struct output_plug *output_ctx = events->cookie;
	struct media_pad *output_pad = &output_ctx->pads[SOURCE_PAD_INDEX];
	struct v4l2_subdev *compo_cap_sd;

	if (mutex_lock_interruptible(&output_ctx->lock))
		return;

	if (!output_ctx->compo_cap_pad)
		goto event_processed;

	compo_cap_sd = media_entity_to_v4l2_subdev(output_ctx->compo_cap_pad->entity);

	/*
	 * Process the events
	 */
	for (i = 0; i < evt_num; i++) {
		switch (events[i].event.event_id) {
		case STM_OUTPUT_STOP_EVENT:
		{
			bool is_stable = 0;

			stv_debug("IN >> Processing output stop\n");

			/*
			 * Output has stopped, so, stop capture
			 */
			ret = v4l2_subdev_call(compo_cap_sd, core, ioctl,
					VIDIOC_STI_S_VIDEO_INPUT_STABLE, &is_stable);
			if (ret) {
				stv_err("failed to notify capture of output stop\n");
				break;
			}

			output_event_queue(&output_ctx->sdev, OUTPUT_EVT_RESOLUTION,
							!V4L2_EVENT_SRC_CH_RESOLUTION);

			stv_debug("OUT >> Output stop processesd\n");

			break;
		}

		case STM_OUTPUT_MODE_CHANGE_EVENT:
		{
			stm_display_mode_t mode;

			stv_debug("IN >> Processing output mode change event\n");

			/*
			 * Get the display mode
			 */
			memset(&mode, 0, sizeof(mode));
			ret = stm_display_output_get_current_display_mode(output_ctx->hOutput, &mode);
			if (ret) {
				stv_err("failed to get current display mode\n");
				break;
			}

			/*
			 * Reconfigure connection with new output params
			 */
			ret = output_configure_compo_cap(output_pad,
						output_ctx->compo_cap_pad, &mode);
			if (ret) {
				stv_err("failed to reconfigure compo capture\n");
				break;
			}

			output_event_queue(&output_ctx->sdev,
				       OUTPUT_EVT_RESOLUTION, V4L2_EVENT_SRC_CH_RESOLUTION);

			stv_debug("OUT >> Output event mode change processed\n");

			break;
		}

		default:
			break;
		}
	}

event_processed:
	mutex_unlock(&output_ctx->lock);
}

/**
 * output_get_dv_timings() - push the dv timings to compo capture
 */
static void output_get_dv_timings(stm_display_mode_t *mode,
				struct v4l2_dv_timings *dv_timings)
{
	struct v4l2_bt_timings *bt = &dv_timings->bt;
	stm_display_mode_timing_t *timing = &mode->mode_timing;
	stm_display_mode_parameters_t *params = &mode->mode_params;

	/*
	 * Convert the values to v4l2 compliant structure
	 */
	bt->interlaced = (params->scan_type == STM_PROGRESSIVE_SCAN ?
			V4L2_FIELD_NONE: V4L2_FIELD_INTERLACED);
	bt->pixelclock = timing->pixel_clock_freq;

	bt->width = params->active_area_width;
	bt->hfrontporch = params->active_area_start_pixel;
	bt->hsync = timing->hsync_width;
	bt->hbackporch = timing->pixels_per_line - (bt->width
					+ bt->hfrontporch + bt->hsync);

	bt->height = params->active_area_height;
	bt->vfrontporch = params->active_area_start_line;
	bt->vsync = timing->vsync_width;
	bt->vbackporch = timing->lines_per_frame - (bt->height
				+ bt->vfrontporch + bt->vsync);
}

/**
 * output_configure_mbus_fmt() - configure mbus fmt for compo capture
 */
static int output_configure_mbus_fmt(struct output_plug *output_ctx,
				stm_display_mode_t *mode,
				struct v4l2_subdev *compo_cap_sd,
				struct v4l2_mbus_framefmt *mbus_fmt)
{
	int ret;
	uint32_t colorspace;
	stm_rational_t aspect_ratio;
	stm_display_mode_parameters_t *params = &mode->mode_params;

	/*
	 * FIXME: No way to propagate the pixel aspect ratio
	 */
	ret = stm_display_output_get_compound_control(output_ctx->hOutput,
						OUTPUT_CTRL_DISPLAY_ASPECT_RATIO,
						&aspect_ratio);
	if (ret) {
		stv_err("failed to get the pixel aspect ratio\n");
		goto get_ctrl_failed;
	}

	/*
	 * Prepare information for s_mbus_fmt on compo subdev
	 */
	ret = stm_display_output_get_control(output_ctx->hOutput,
					OUTPUT_CTRL_YCBCR_COLORSPACE,
					&colorspace);
	if (ret) {
		stv_err("failed to get the display colorspace\n");
		goto get_ctrl_failed;
	}

	/*
	 * Configure the mbus fmt
	 */
	mbus_fmt->width = params->active_area_width;
	mbus_fmt->height = params->active_area_height;
	mbus_fmt->field = (params->scan_type == STM_PROGRESSIVE_SCAN ?
				V4L2_FIELD_NONE: V4L2_FIELD_INTERLACED);

	switch (colorspace) {
	case STM_YCBCR_COLORSPACE_AUTO_SELECT:
		mbus_fmt->colorspace = (params->output_standards &
				STM_OUTPUT_STD_HD_MASK) ? V4L2_COLORSPACE_REC709:
				V4L2_COLORSPACE_SMPTE170M;
		mbus_fmt->code = (mbus_fmt->colorspace == V4L2_COLORSPACE_REC709) ?
						V4L2_MBUS_FMT_YUV8_1X24:
						V4L2_MBUS_FMT_RGB888_1X24;
		break;

	case STM_YCBCR_COLORSPACE_709:
		mbus_fmt->colorspace = V4L2_COLORSPACE_REC709;
		mbus_fmt->code = V4L2_MBUS_FMT_YUV8_1X24;
		break;

	case STM_YCBCR_COLORSPACE_601:
		mbus_fmt->colorspace = V4L2_COLORSPACE_SMPTE170M;
		mbus_fmt->code = V4L2_MBUS_FMT_YUV8_1X24;
		break;

	case STM_COLORSPACE_BT2020:
		mbus_fmt->colorspace = V4L2_COLORSPACE_BT2020;
		mbus_fmt->code = V4L2_MBUS_FMT_YUV420_2X10;
		break;

	default:
		mbus_fmt->colorspace = V4L2_COLORSPACE_SRGB;
		mbus_fmt->code = V4L2_MBUS_FMT_RGB888_1X24;
		break;
	}

	ret = v4l2_subdev_call(compo_cap_sd, video, s_mbus_fmt, mbus_fmt);
	if (ret)
		stv_err("failed to set mbus fmt of compo subdev\n");

get_ctrl_failed:
	return ret;
}

/**
 * output_configure_compo_cap() - output configure compo-capture
 */
static int output_configure_compo_cap(const struct media_pad *local,
					const struct media_pad *remote,
					stm_display_mode_t *mode)
{
	int ret;
	struct v4l2_dv_timings dv_timings;
	struct v4l2_mbus_framefmt mbus_fmt;
	struct v4l2_subdev *compo_cap_sd;
	struct output_plug *output_ctx;

	output_ctx = output_entity_to_output_plug(local->entity);
	compo_cap_sd = media_entity_to_v4l2_subdev(remote->entity);

	/*
	 * Configure compo capture with input dv timings
	 */
	memset(&dv_timings, 0, sizeof(struct v4l2_dv_timings));
	output_get_dv_timings(mode, &dv_timings);
	ret = v4l2_subdev_call(compo_cap_sd, video, s_dv_timings, &dv_timings);
	if (ret) {
		stv_err("failed to setup dv timings for compo capture\n");
		goto configure_failed;
	}

	/*
	 * Configure mbus format on compo capture
	 */
	memset(&mbus_fmt, 0, sizeof(mbus_fmt));
	ret = output_configure_mbus_fmt(output_ctx, mode,
					compo_cap_sd, &mbus_fmt);
	if (ret)
		stv_err("failed to set mbus format on compo capture\n");

configure_failed:
	return ret;
}

/**
 * output_subscribe_events() - subscribe to output events
 */
static int output_subscribe_events(struct output_plug *output_ctx)
{
	int ret = 0;
	stm_object_h output_type;
	stm_event_subscription_h subs;
	stm_event_subscription_entry_t subs_entry;

	/*
	 * This function may be called from both media-ctl callback
	 * and subdev open, so, we register only once, obviously
	 */
	if (output_ctx->subs) {
		stv_debug("output events already subscribed\n");
		goto subs_failed;
	}

	/*
	 * Get the output object type, it is required to subscribe to events
	 */
	ret = stm_registry_get_object_type(output_ctx->hOutput, &output_type);
	if (ret) {
		stv_err("failed to get type of output\n");
		goto subs_failed;
	}

	/*
	 * Create a subscription and register for it's events
	 */
	subs_entry.object = output_type;
	subs_entry.event_mask = STM_OUTPUT_MODE_CHANGE_EVENT | STM_OUTPUT_STOP_EVENT;
	subs_entry.cookie = output_ctx;
	ret = stm_event_subscription_create(&subs_entry, 1, &subs);
	if (ret) {
		stv_err("failed to create event subscription\n");
		goto subs_failed;
	}

	/*
	 * Set the call back function for subscribed events
	 */
	ret = stm_event_set_handler(subs, (stm_event_handler_t)output_event_callback);
	if (ret) {
		stv_err("failed to register the signal handler\n");
		goto set_handler_failed;
	}

	output_ctx->subs = subs;
	output_ctx->output_type = output_type;

	return 0;

set_handler_failed:
	if (stm_event_subscription_delete(subs))
		stv_err("failed to delete the subscription\n");
subs_failed:
	return ret;
}

/**
 * output_connect_compo_cap() - connect output to compo
 */
static int output_connect_compo_cap(const struct media_pad *local,
					const struct media_pad *remote)
{
	int ret = 0;
	struct output_plug *output_ctx;
	stm_display_mode_t mode;

	output_ctx = output_entity_to_output_plug(local->entity);

	MUTEX_INTERRUPTIBLE(&output_ctx->lock);

	/*
	 * Open display device
	 */
	ret = output_open_display(output_ctx);
	if (ret) {
		stv_err("failed to open display device\n");
		goto open_failed;
	}

	/*
	 * Subscribe to the events
	 */
	ret = output_subscribe_events(output_ctx);
	if (ret) {
		stv_err("failed to subscribe to %s events\n", local->entity->name);
		goto evt_subs_failed;
	}

	/*
	 * Check if display is started
	 */
	memset(&mode, 0, sizeof(mode));
	ret = output_get_display_mode(output_ctx, &mode);
	if (ret && (ret != -EHOSTDOWN)) {
		stv_err("Failed to get display mode\n");
		goto get_mode_failed;
	}

	/*
	 * Configure compo capture
	 */
	ret = output_configure_compo_cap(local, remote, &mode);
	if (ret) {
		stv_err("failed to configure compo capture\n");
		goto get_mode_failed;
	}

	output_ctx->compo_cap_pad = remote;

	mutex_unlock(&output_ctx->lock);

	return 0;

get_mode_failed:
	if (stm_event_subscription_delete(output_ctx->subs))
		stv_err("failed to delete event subscription\n");
evt_subs_failed:
	output_close_display(output_ctx);
open_failed:
	mutex_unlock(&output_ctx->lock);
	return ret;
}

/**
 * output_disconnect_compo_cap() - disconnect output to compo
 */
static int output_disconnect_compo_cap(const struct media_pad *local,
					const struct media_pad *remote)
{
	struct output_plug *output_ctx = output_entity_to_output_plug(local->entity);
	MUTEX_INTERRUPTIBLE(&output_ctx->lock);
	output_close_display(output_entity_to_output_plug(local->entity));
	output_ctx->compo_cap_pad = NULL;
	mutex_unlock(&output_ctx->lock);
	return 0;
}

/**
 * output_queue_initial_status() - queue the initial status of event as an event
 */
static int output_queue_initial_status(struct output_plug *output_ctx,
			struct v4l2_subscribed_event *sev, unsigned int elems)
{
	int ret;
	stm_display_mode_t mode;
	struct v4l2_event event;

	memset(&event, 0, sizeof(event));
	sev->elems = elems;

	/*
	 * Get the current output mode, so, as to ascertain whether it is started
	 */
	ret = stm_display_output_get_current_display_mode(output_ctx->hOutput, &mode);
	if (ret) {
		stv_err("failed to get current display mode\n");
		goto queue_done;
	}

	/*
	 * Since, only event is being exposed, so, need for introducing check
	 * based on sev->type
	 */
	event.id = sev->id;
	event.type = V4L2_EVENT_SOURCE_CHANGE;
	event.u.src_change.changes = V4L2_EVENT_SRC_CH_RESOLUTION;
	if (mode.mode_id == STM_TIMING_MODE_RESERVED)
		event.u.src_change.changes = !V4L2_EVENT_SRC_CH_RESOLUTION;

	v4l2_event_queue_fh(sev->fh, &event);

queue_done:
	return ret;
}

/*
 * output_event_ops_add() - handling mainly for initial feeback
 */
static int output_event_ops_add(struct v4l2_subscribed_event *sev, unsigned int elems)
{
	int ret = 0;
	struct v4l2_fh *fh = sev->fh;
	struct v4l2_subdev *sd = vdev_to_v4l2_subdev(fh->vdev);
	struct output_plug *output_ctx = v4l2_get_subdevdata(sd);

	if (sev->flags & V4L2_EVENT_SUB_FL_SEND_INITIAL)
		ret = output_queue_initial_status(output_ctx, sev, elems);

	return ret;
}

/**
 * output_event_ops_replace() - replacing the proprietary events
 */
static void output_event_ops_replace(struct v4l2_event *old,
	       				const struct v4l2_event *new)
{
	old->u.src_change.changes = new->u.src_change.changes;
}

/*
 * output event ops
 */
const struct v4l2_subscribed_event_ops output_event_ops = {
	.add = output_event_ops_add,
	.replace = output_event_ops_replace,
};

/**
 * output_subdev_core_subscribe_event() - subscribe to events
 */
static int output_subdev_core_subscribe_event(struct v4l2_subdev *sd,
			struct v4l2_fh *fh, struct v4l2_event_subscription *sub)
{
	int ret = 0;
	struct output_plug *output_ctx = v4l2_get_subdevdata(sd);

	/*
	 * Currently event subscription is only on source pad
	 */
	if (sub->id != SOURCE_PAD_INDEX) {
		stv_err("event subscription allowed on pad 1 only\n");
		ret = -EINVAL;
		goto subscribe_failed;
	}

	/*
	 * A single event supported for now
	 */
	if (!(sub->type & V4L2_EVENT_SOURCE_CHANGE)) {
		stv_err("invalid subscription type specified\n");
		ret = -EINVAL;
		goto subscribe_failed;
	}

	MUTEX_INTERRUPTIBLE(&output_ctx->lock);

	ret = v4l2_event_subscribe(fh, sub, 1, &output_event_ops);
	if (ret)
		stv_err("Unable to subscribe the event\n");

	mutex_unlock(&output_ctx->lock);

subscribe_failed:
	return ret;
}

/**
 * output_g_dv_timings() - retrieve the dv timigs from compositor
 */
static int output_g_dv_timings(struct v4l2_subdev *sd,
				struct v4l2_dv_timings *timings)
{
	int ret;
	stm_display_mode_t mode;
	struct output_plug *output_ctx = v4l2_get_subdevdata(sd);

	if (strncmp(sd->name, "analog_hdout", strlen("analog_hdout"))) {
		stv_err("dv timings supported for main mixer only\n");
		ret = -EINVAL;
		goto g_timing_failed;
	}

	MUTEX_INTERRUPTIBLE(&output_ctx->lock);

	/*
	 * Open display device
	 */
	ret = output_open_display(output_ctx);
	if (ret) {
		stv_err("failed to open display device\n");
		goto open_failed;
	}

	/*
	 * Get the display mode. If the display is not started, then
	 * there are no dv timings
	 */
	memset(&mode, 0, sizeof(mode));
	ret = output_get_display_mode(output_ctx, &mode);
	if (ret) {
		stv_err("Failed to get display mode\n");
		goto get_mode_failed;
	}

	/*
	 * Get the dv timings
	 */
	output_get_dv_timings(&mode, timings);

get_mode_failed:
	output_close_display(output_ctx);
open_failed:
	mutex_unlock(&output_ctx->lock);
g_timing_failed:
	return ret;
}

/*
 * v4l2 subdev core ops
 */
static const struct v4l2_subdev_core_ops output_subdev_core_ops = {
	.subscribe_event = output_subdev_core_subscribe_event,
	.unsubscribe_event = v4l2_event_subdev_unsubscribe,
};

static const struct v4l2_subdev_video_ops output_subdev_video_ops = {
	.g_dv_timings = output_g_dv_timings,
};

/*
 * output subdev ops
 */
static const struct v4l2_subdev_ops output_subdev_ops = {
	.core = &output_subdev_core_ops,
	.video = &output_subdev_video_ops,
};

/**
 * output_link_setup() - output link setup for tunneled compo
 */
static int output_link_setup(struct media_entity *entity,
			const struct media_pad *local,
			const struct media_pad *remote,
			u32 flags)
{
	int ret = 0;

	switch (remote->entity->type) {
	case MEDIA_ENT_T_V4L2_SUBDEV_PLANE_VIDEO:
	case MEDIA_ENT_T_V4L2_SUBDEV_PLANE_GRAPHIC:

		/*
		 * Nothing to do here for plane connect to output
		 */

		break;

	case MEDIA_ENT_T_V4L2_SUBDEV_COMPO:
		/*
		 * Tunneled compo configuration for compo capture
		 */
		if (flags & MEDIA_LNK_FL_ENABLED)
			ret = output_connect_compo_cap(local, remote);
		else
			ret = output_disconnect_compo_cap(local, remote);

		break;

	default:
		break;
	}

	return ret;
}

static const struct media_entity_operations output_media_entity_ops = {
	.link_setup = output_link_setup,
};

/**
 * output_get_slot() - fetch the available output index
 */
static inline int output_get_slot(struct output_plug *output_ctx)
{
	int i;

	for (i = 0; i < STMEDIA_MAX_OUTPUTS; i++) {

		if (!strnlen(output_ctx[i].name, sizeof(output_ctx[i].name) - 1)) {
			output_ctx[i].display_device_id = -1;
			break;
		}
	}

	if (i == STMEDIA_MAX_OUTPUTS)
		i = -1;

	return i;
}

/**
 * output_subdev_internal_open() - output subdev internal open
 */
static int output_subdev_internal_open(struct v4l2_subdev *sd,
					struct v4l2_subdev_fh *fh)
{
	int ret;
	struct output_plug *output_ctx = v4l2_get_subdevdata(sd);

	MUTEX_INTERRUPTIBLE(&output_ctx->lock);

	ret = output_subscribe_events(output_ctx);
	if (ret)
		stv_err("failed to subscribe to %s events\n", sd->name);

	mutex_unlock(&output_ctx->lock);

	return ret;
}

/**
 * output_subdev_internal_close() - output subdev internal close
 */
static int output_subdev_internal_close(struct v4l2_subdev *sd,
					struct v4l2_subdev_fh *fh)
{
	struct output_plug *output_ctx = v4l2_get_subdevdata(sd);

	mutex_lock(&output_ctx->lock);

	if (stm_event_subscription_delete(output_ctx->subs))
		stv_err("failed to delete event subscription\n");

	output_ctx->subs = 0;
	output_ctx->output_type = NULL;

	mutex_unlock(&output_ctx->lock);

	return 0;
}

struct v4l2_subdev_internal_ops output_subdev_internal_ops = {
	.open = output_subdev_internal_open,
	.close = output_subdev_internal_close,
};

/**
 * stm_v4l2_output_subdev_init() - initialize and register output subdev
 */
int stm_v4l2_output_subdev_init(struct output_plug *output_ctx,
				int index, const char *name,
				stm_display_output_h hOutput,
				const int display_device_id)
{
	int slot, ret = 0;
	struct v4l2_subdev *sd;
	struct media_entity *me;
	struct media_pad *pad;

	/*
	 * Look up for available output slot
	 */
	slot = output_get_slot(output_ctx);
	if (slot == -1) {
		stv_err("max number of output exhausted\n");
		ret = -EOVERFLOW;
		goto init_done;
	}

	/*
	 * Update the output context
	 */
	output_ctx[slot].display_device_id = display_device_id;
	strlcpy(output_ctx[slot].name, name, sizeof(output_ctx[slot].name));
	output_ctx[slot].id = index;
	output_ctx[slot].hOutput = hOutput;
	mutex_init(&output_ctx[slot].lock);

	/*
	 * Initialize output sub-device:
	 */
	sd = &output_ctx[slot].sdev;
	me = &sd->entity;
	pad = output_ctx[slot].pads;

	v4l2_subdev_init(sd, &output_subdev_ops);
	strlcpy(sd->name, output_ctx[slot].name, sizeof(sd->name));
	sd->flags = V4L2_SUBDEV_FL_HAS_EVENTS | V4L2_SUBDEV_FL_HAS_DEVNODE;
	v4l2_set_subdevdata(sd, &output_ctx[slot]);

	/*
	 * Generally outputs have one pad only (a SINK), however the first
	 * compositor of some devices also has a output PAD where it is
	 * possible to capture composited frames.
	 */
	output_ctx[slot].input_pads = 1;
	output_ctx[slot].output_pads = 0;
	pad[0].flags = MEDIA_PAD_FL_SINK;

	/*
	 * The Main compo output has the compo capture capabilities.
	 * FIXME, should do the check via a capabilities rather the name
	 */
	if (!strcmp(name, "analog_hdout0")) {
		output_ctx[slot].output_pads = 1;
		pad[1].flags = MEDIA_PAD_FL_SOURCE;
		sd->internal_ops = &output_subdev_internal_ops;
	}

	/*
	 * The Aux compo output has the compo capture capabilities.
	 * FIXME, should do the check via a capabilities rather the name
	 */
	if (!strcmp(name, "analog_sdout0")) {
		output_ctx[slot].output_pads = 1;
		pad[1].flags = MEDIA_PAD_FL_SOURCE;
		sd->internal_ops = &output_subdev_internal_ops;
	}


	/*
	 * Initialize the output media entity
	 */
	ret = media_entity_init(me, (output_ctx[slot].input_pads +
					output_ctx[slot].output_pads), pad, 0);
	if (ret) {
		stv_err("failed to initialize output media entity\n");
		goto media_init_failed;
	}
	me->ops = &output_media_entity_ops;
	me->type = MEDIA_ENT_T_V4L2_SUBDEV_OUTPUT;

	/*
	 * Register display output sub-device
	 */
	ret = stm_media_register_v4l2_subdev(sd);
	if (ret) {
		stv_err("failed to register %s subdev\n", name);
		goto sd_init_failed;
	}

	return 0;

sd_init_failed:
	media_entity_cleanup(me);
media_init_failed:
	memset(&output_ctx[slot].name, 0, sizeof(output_ctx[slot].name));
init_done:
	return ret;
}

/**
 * stm_v4l2_output_subdev_exit() - removes the output subdev
 */
void stm_v4l2_output_subdev_exit(struct output_plug *output_ctx)
{
	struct v4l2_subdev *sd = &output_ctx->sdev;
	struct media_entity *me = &sd->entity;

	stm_media_unregister_v4l2_subdev(sd);

	media_entity_cleanup(me);

	memset(output_ctx, 0, sizeof(*output_ctx));
	output_ctx->display_device_id = -1;
}
