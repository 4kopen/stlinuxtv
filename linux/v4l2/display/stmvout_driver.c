/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include <linux/slab.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/poll.h>
#include <linux/mm.h>
#include <media/v4l2-dev.h>
#include <media/v4l2-common.h>
#include <linux/slab.h>
#include <linux/semaphore.h>
#include <asm/page.h>

/* For bpa2 buffer access from user space, we must have the access file ops. */
#include <asm/io.h>
#if defined(CONFIG_ARM) || defined(CONFIG_HCE_ARCH_ARM)
#include <asm/mach/map.h>
#endif

#include <stm_display.h>
#include <linux/stm/stmcoredisplay.h>

#include "stmedia.h"
#include "linux/stm/stmedia_export.h"
#include "stmvout_driver.h"
#include "linux/stmvout.h"
#include "stmvout_mc.h"
#include "linux/dvb/dvb_v4l2_export.h"
#include "stm_v4l2_common.h"
#include "stv_debug.h"

/* #include "stm_v4l2.h" */

#if 0				/* no longer */
#include "__out_driver.h"
static int video_nr = -1;
module_param(video_nr, int, 0444);
#endif

#define SYNC_MODE	1

#undef DEBUG_SHOW_OUTPUTS
#undef DEBUG_SHOW_PLANES

/* Global structures
 * Should be converted to double linked list sometime in the future...
 * also suggest to change voutData in voutPlane
 *
 *     g_voutPlug: describes the Display output entitites
 *     g_voutRoot: describes the root entity (/dev/video1) of stvout
 *                 device. This structure contains all the MC pads
 *                 connecting Display Planes.
 *
 *     TODO: info and queues relevant to buffer management and
 *           user open controls, should be moved to /dev/video1
 *           entity.
 *           Names should be more evocative.
 */
static struct output_plug g_voutPlug[STMEDIA_MAX_OUTPUTS];

struct stm_v4l2_output_root {
	struct video_device video1;
	struct media_pad pads[STMEDIA_MAX_PLANES];	/* all SOURCE pads */
	struct _stvout_device pDev[STMEDIA_MAX_PLANES];	/* shortcut to real output device */
};

static struct stm_v4l2_output_root g_voutRoot;	/* /dev/video1 */

struct stm_v4l2_vout_fh {
	struct v4l2_fh fh;
	open_data_t *pOpenData;
};

/**
 * stmvout_plane_set_ctrl() - set sync/async control on plane
 * @sync      : true if sync control
 * @ctrl_block: valid control block
 */
int stmvout_plane_set_ctrl(bool sync, stm_object_h plane,
				struct stmvout_ctrl *ctrl)
{
	int ret = 0, listener_id;
	stm_ctrl_listener_t listener;
	unsigned long timeout_jiffies = 0;

	/*
	 * Process for synchronous handling
	 */
	if (sync) {
		memset(&listener, 0, sizeof(listener));
		listener.data = ctrl->priv;
		listener.notify = ctrl->callback;
		ret = stm_display_plane_set_asynch_ctrl_listener(plane,
						&listener, &listener_id);
		if (ret) {
			pr_err("%s(): failed to set listner\n", __func__);
			goto set_ctrl_done;
		}
	}

	/*
	 * Set control (common for sync/async)
	 */
	switch (ctrl->id) {
	case PLANE_CTRL_CONNECT_TO_SOURCE:
	case PLANE_CTRL_DISCONNECT_FROM_SOURCE:
		ret = stm_display_plane_set_compound_control(plane,
							ctrl->id, ctrl->data);
		break;

	default:
		ret = -EINVAL;
	}

	/*
	 * Finish off with the sync winding up
	 */
	if (sync) {
		/* set the timeout of 1 second */
		timeout_jiffies = msecs_to_jiffies(1000);
		if (down_timeout(ctrl->sem, timeout_jiffies))
			ret = -ERESTARTSYS;
		stm_display_plane_unset_asynch_ctrl_listener(plane, listener_id);
	}

set_ctrl_done:
	return ret;

}

/**
 * stmvout_plane_get_src() - get the source to be connected to this plane
 * @plane    : plane handle
 * @src_info : to be filled in from here
 * Retrieve the display of the plane and find a suitable source
 */
int stmvout_plane_get_src(stm_object_h plane, struct stmvout_src_info *src_info)
{
	int ret = 0, src_id = 0;
	uint32_t display_id, status;

	/*
	 * Get the display id of the plane to which it is connected
	 */
	ret = stm_display_plane_get_device_id(plane, &display_id);
	if (ret) {
		pr_err("%s: failed to get display id\n", __func__);
		goto get_display_failed;
	}

	/*
	 * Get display from the display id
	 */
	ret = stm_display_open_device(display_id, &src_info->display);
	if (ret) {
		pr_err("%s: failed to get display handle\n", __func__);
		goto get_display_failed;
	}

	do {
		/*
		 * Get the requested src from the source id
		 */
		ret = stm_display_device_open_source(src_info->display,
							src_id, &src_info->src);
		if (ret) {
			pr_err("%s(): failed to get src at id: %d\n", __func__, src_id);
			goto get_src_failed;
		}

		/*
		 * Check for the availablity of source
		 */
		ret = stm_display_source_get_status(src_info->src, &status);
		if (ret) {
			pr_err("%s(): failed to get status of src\n", __func__);
			goto get_status_failed;
		}
		if (!(status & (STM_STATUS_QUEUE_LOCKED | STM_STATUS_SOURCE_CONNECTED))) {
			pr_debug("%s(): found the source with id: %d\n", __func__, src_id);
			break;
		}

		stm_display_source_close(src_info->src);

	} while (++src_id);

	stm_display_device_close(src_info->display);
	src_info->display = NULL;

	return 0;

get_status_failed:
	stm_display_source_close(src_info->src);
get_src_failed:
	stm_display_device_close(src_info->display);
get_display_failed:
	src_info->display = NULL;
	src_info->src = NULL;
	return ret;
}

/**
 * stmvout_plane_put_src() - release the source
 * @src_info : valid bunch of handles
 * Close the source and the associated display
 */
void stmvout_plane_put_src(struct stmvout_src_info *src_info)
{
	stm_display_source_close(src_info->src);
	src_info->src = NULL;
}

/**
 * stmvout_source_get_status() - get source status
 */
bool stmvout_source_get_status(struct stmvout_src_info *src_info)
{
	bool status = false;
	u32 src_status;

	if (!src_info->src)
		goto get_status_done;
	else
		return true;

	/* Need to be revisited later */
	if (stm_display_source_get_status(src_info->src, &src_status)) {
		pr_err("%s(): unable to get source status\n", __func__);
		goto get_status_done;
	}
	if (src_status & (STM_STATUS_SOURCE_CONNECTED | STM_STATUS_QUEUE_LOCKED))
		status = true;

get_status_done:
	return status;
}

EXPORT_SYMBOL(stmvout_plane_set_ctrl);
EXPORT_SYMBOL(stmvout_plane_get_src);
EXPORT_SYMBOL(stmvout_plane_put_src);
EXPORT_SYMBOL(stmvout_source_get_status);

/**
 * vout_display_open() - opens the display device
 */
static inline int vout_display_open(struct _stvout_device *vout)
{
	int ret = 0;

	/*
	 * Check for initial conditions before opening display
	 */
	if(vout->display_device_id == -1) {
		stv_err("No display associated with %s\n", vout->name);
		ret = -ENODEV;
		goto open_done;
	}

	if (vout->hDisplay) {
		stv_debug("Display already opened for %s\n", vout->name);
		goto open_done;
	}

	/*
	 * Open display device
	 */
	ret = stm_display_open_device(vout->display_device_id, &vout->hDisplay);
	if (ret)
		stv_err("Failed to open display (%d)\n", ret);

open_done:
	return ret;
}

/**
 * vout_display_close() - close the display device
 */
static inline void vout_display_close(struct _stvout_device *vout)
{
	/*
	 * Check for initial conditions before closing display
	 */
	if(vout->display_device_id == -1) {
		stv_err("No display associated with %s\n", vout->name);
		goto close_done;
	}

	if (!vout->hDisplay) {
		stv_debug("Display already closed for %s\n", vout->name);
		goto close_done;
	}

	stm_display_device_close(vout->hDisplay);
	vout->hDisplay = NULL;

close_done:
	return;
}

/* ----------------------------------------------------------------------- */

static int init_device_locked(struct _stvout_device *const device)
{
	int ret;

	stmvout_init_buffer_queues(device);

	if ((ret = stmvout_allocate_clut(device)) < 0)
		return ret;

	if((device->display_device_id != -1) && !device->hDisplay) {
		if(stm_display_open_device(device->display_device_id, &device->hDisplay) < 0) {
			device->hDisplay = NULL;
			return -ENODEV;
		}
	}

	/* Note: with Media Controller, at this point, Display planes are
	 *       already opened (display plane handle is valid).
	 */

	return 0;
}

static int
stmvout_close_locked(struct _open_data *open_data,
		     struct _stvout_device *device)
{
	unsigned long flags;

	if (open_data->isFirstUser) {
		debug_msg("%s: turning off streaming and releasing buffers\n",
			  __FUNCTION__);

		/* Discard the pending queue */
		debug_msg("%s: delete buffers from pending queue\n",
			  __FUNCTION__);
		write_lock_irqsave(&device->pendingStreamQ.lock, flags);
		stmvout_delete_buffers_from_list(&device->pendingStreamQ.list);
		write_unlock_irqrestore(&device->pendingStreamQ.lock, flags);

		/* Turn off streaming in case it's on. */
		if (stmvout_streamoff(device) < 0) {
			if (signal_pending(current))
				return -ERESTARTSYS;

			printk(KERN_ERR
			       "Failed to turn off streaming but there wasn't a signal pending, that should not happen");
			BUG();
		}

		if (!stmvout_deallocate_mmap_buffers(device)) {
			printk(KERN_ERR
			       "failed to unmap/deallocate buffers!\n");
			BUG();
		}

	}

	if (device->open_count == 1) {
		debug_msg("%s: last user closed device\n", __FUNCTION__);
		/*
		 * With Media Controller, Display planes are kept always open since
		 * the construction of entities and MC graph (init stage).
		 * Should never close the plane handle
		 */

		stmvout_deallocate_clut(device);

		if(device->hDisplay)
		{
			stm_display_device_close(device->hDisplay);
			device->hDisplay = NULL;
		}
	}

	device->open_count--;
	open_data->isOpen = 0;
	open_data->isFirstUser = 0;
	open_data->pDev = NULL;
	open_data->padId = 0;

	return 0;
}

static int stmvout_close(struct file *file)
{
	struct v4l2_fh *fh = file->private_data;
	struct stm_v4l2_vout_fh *vout_fh = container_of(fh, struct stm_v4l2_vout_fh, fh);
	open_data_t *pOpenData = vout_fh->pOpenData;

	struct _stvout_device *device;
	int ret = 0;

	if (!pOpenData) {
		debug_msg("%s BUG: Opendata not set!\n", __FUNCTION__);
		goto free_fh;
	}

	debug_msg("%s in.\n", __FUNCTION__);

	/* get shortcut to output plane */
	/* was: device = pOpenData->pDev; */
	device = pOpenData->pDev;

	/* Note that this is structured to allow the call to be interrupted by a
	   signal and then restarted without corrupting the internal state. */
	if (down_interruptible(&device->devLock))
		return -ERESTARTSYS;

	ret = stmvout_close_locked(pOpenData, device);

	list_del(&pOpenData->open_list);
	up(&device->devLock);

	kfree(pOpenData);

free_fh:
	/*
	 * Remove V4L2 file handlers
	 */
	v4l2_fh_del(fh);
	v4l2_fh_exit(fh);

	file->private_data = NULL;
	kfree(vout_fh);

	debug_msg("%s out.\n", __FUNCTION__);

	return ret;
}

/* Media Controller Helper:
 * Starting from Plane descriptor, follows the ENABLED link which
 * connects the Plane to Display output sub-device and gets the
 * output Display handle.
 */
stm_display_output_h stmvout_get_output_display_handle(stvout_device_t * pDev)
{
	struct media_pad *downStream_pad = NULL;
	struct output_plug *outputPlug;

	/* via Media Controller get the SINK (downstream) entity connected
	 * to the source PAD of this entity (pDev)
	 */
	downStream_pad =
	    media_entity_remote_source(&pDev->pads[SOURCE_PAD_INDEX]);
	if (downStream_pad == NULL)
		return NULL;	/* link doesn't exists or is not ENABLED */

	/* having the output media entity, get its container! */
	outputPlug = output_entity_to_output_plug(downStream_pad->entity);

	/* and finally return the Display output handle */
	return (outputPlug->hOutput);
}

static int stmvout_get_interface( stm_display_source_h source,
					 stm_display_source_interface_params_t *iface_params,
					 void **hiface)
{
	int ret = 0;

	ret = stm_display_source_get_interface(source,
					       *iface_params,
					       hiface);
	if (ret) {
		debug_msg("%s: get queue interface failed\n",
			  __PRETTY_FUNCTION__);
		*hiface = NULL;
		return signal_pending(current) ? -ERESTARTSYS : ret;
	}

	return 0;
}

static int stmvout_release_interface( void *hiface,
			stm_display_source_interfaces_t interface_type)
{

	if(interface_type == STM_SOURCE_QUEUE_IFACE)
		return stm_display_source_queue_release(hiface);
	else
		return stm_display_source_pixelstream_release(hiface);
}

/**
 * vout_src_setup_callback() - listener callback for src connect/disconnect
 */
static void vout_src_setup_callback(void *data, const stm_time64_t vsync_time,
				const stm_asynch_ctrl_status_t *status, int status_cnt)
{
	int i;
	stvout_device_t *vout = data;

	for (i = 0; i < status_cnt; i++) {
		if (vout->ctrl_id == status[i].ctrl_id) {
			vout->vsync_event_err = status[i].error_code;
			up(&vout->listener_sem);
		}
	}
}

/**
 * vout_setup_plane_to_source() - manage connect/disconnect of source and plane
 */
static int vout_setup_plane_to_source(u32 ctrl_id, stvout_device_t *vout,
						stm_display_source_h source)
{
	int ret = 0;
	struct stmvout_ctrl vout_ctrl;

	vout->ctrl_id = ctrl_id;
	memset(&vout_ctrl, 0, sizeof(vout_ctrl));
	vout_ctrl.id = ctrl_id;
	vout_ctrl.data = source;
	vout_ctrl.callback = vout_src_setup_callback;
	vout_ctrl.priv = (void *)vout;
	vout_ctrl.sem = &vout->listener_sem;
	ret = stmvout_plane_set_ctrl(SYNC_MODE, vout->hPlane, &vout_ctrl);
	if (ret) {
		pr_err("%s(): command failed to connect src to plane\n", __func__);
		goto connect_failed;
	}
	if (SYNC_MODE && vout->vsync_event_err)
		pr_err("%s(): failed to connect src with plance\n", __func__);

	vout->ctrl_id = 0;

connect_failed:
	return ret;
}

static int stmvout_get_iface_source(stvout_device_t *vout,
				    stm_display_source_interface_params_t *iface_params,
				    struct stm_source_info *src_info)
{
	int ret = 0;
	uint32_t sourceID = 0;
	uint32_t status = 0;
	void *hiface;
	stm_display_source_h hSource;

	for(sourceID = 0;;sourceID++){
		ret = stm_display_device_open_source(vout->hDisplay,
						    sourceID,
						    &hSource);
		if (ret){
			if (ret == -ENODEV) {
				debug_msg
				    ("%s: Failed to get a source for plane \n",
				     __FUNCTION__);
				return -EBUSY;
			}
			return signal_pending(current) ? -ERESTARTSYS : ret;
		}

		/* Try to get an interface on this source */
		ret = stmvout_get_interface( hSource, iface_params,
						    &hiface);
		if (ret) {
			debug_msg("%s: get interface failed\n",
				  __PRETTY_FUNCTION__);
			stm_display_source_close(hSource);

			if (ret == -EOPNOTSUPP)
				continue;

			return signal_pending(current) ? -ERESTARTSYS : ret;
		}

		ret = stm_display_source_get_status(hSource, &status);
		if ((ret != 0) ||
			((ret == 0)
			&& (status & (STM_STATUS_QUEUE_LOCKED | STM_STATUS_SOURCE_CONNECTED)))) {
			/* This source is already used to feed another plane */
			stmvout_release_interface(hiface, iface_params->interface_type);
			stm_display_source_close(hSource);
			continue;
		}

		ret = stm_display_source_queue_lock(hiface);
		if (ret) {
			pr_err("%s(): failed to take a lock \n", __func__);
			stmvout_release_interface(hiface, iface_params->interface_type);
			stm_display_source_close(hSource);
			continue;
		}

		/*
		 * Connect source to plane
		 */
		ret = vout_setup_plane_to_source(PLANE_CTRL_CONNECT_TO_SOURCE, vout, hSource);
		if (ret) {
			pr_err("%s(): failed to connect src to plane\n", __func__);
			stm_display_source_queue_unlock(hiface);
			stmvout_release_interface(hiface, iface_params->interface_type);
			stm_display_source_close(hSource);
			continue;
		}
		debug_msg("%s: Plane %p successfully connected to Source %p\n",
			     __FUNCTION__, vout->hPlane, hSource);
		break;	/* break the loop as this source is ok */
	}

	src_info->source = hSource;
	src_info->iface_handle = hiface;
	src_info->iface_type = iface_params->interface_type;
	/* The below values are zero for queue buffer sources */
	src_info->pixeltype = iface_params->interface_params.pixel_stream_params->source_type;
	src_info->pixel_inst = iface_params->interface_params.pixel_stream_params->instance_number;
	return 0;
}

static inline int stmvout_get_number_of_outputs(void)
{
	int i;

	for (i = 0; i < STMEDIA_MAX_PLANES; i++) {
		if (g_voutRoot.pDev[i].name[0] == '\0')
			break;
	}
	return (i);
}

static int stmvout_vidioc_querycap(struct file *file, void *fh,
						struct v4l2_capability *cap)
{
	strlcpy(cap->driver, "Planes", sizeof(cap->driver));
	strlcpy(cap->card, "STMicroelectronics", sizeof(cap->card));
	strlcpy(cap->bus_info, "", sizeof(cap->bus_info));

	cap->version = LINUX_VERSION_CODE;
	cap->capabilities = (0
			     | V4L2_CAP_VIDEO_OUTPUT
			     | V4L2_CAP_STREAMING);
	return 0;
}

static int get_vout_device_locked(void *fh, stvout_device_t **pDev, bool *isFirstUser)
{
	struct stm_v4l2_vout_fh *vout_fh = container_of(fh, struct stm_v4l2_vout_fh, fh);
	open_data_t *pOpenData = vout_fh->pOpenData;
	struct semaphore *pLock = NULL;

	if(!pOpenData)
		return -EINVAL;

	BUG_ON(pOpenData->pDev == NULL);
		/* get shortcut to real video device (plane) */
	*pDev = pOpenData->pDev;
	pLock = &((*pDev)->devLock);

	if((pOpenData->pDev->display_device_id != -1) && !pOpenData->pDev->hDisplay) {
		if(stm_display_open_device(pOpenData->pDev->display_device_id, &pOpenData->pDev->hDisplay) < 0) {
			pOpenData->pDev->hDisplay = NULL;
			return -ENODEV;
		}
	}

	if (down_interruptible(pLock))
		return -ERESTARTSYS;

	if(isFirstUser){
		if (pOpenData->isFirstUser)
			*isFirstUser = true;
		else
			*isFirstUser = false;
	}
	return 0;
}

static void put_vout_device(stvout_device_t *pDev)
{
	struct semaphore *pLock = NULL;
	if(pDev){
		pLock = &(pDev->devLock);
		up(pLock);
	}
}


static int stmvout_vidioc_enum_fmt_vid_out(struct file *file, void *fh,
					    struct v4l2_fmtdesc *f)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	ret = get_vout_device_locked(fh, &pDev, NULL);
	if(ret)
		goto error;

	ret = stmvout_enum_buffer_format(pDev, f);
error:
	put_vout_device(pDev);
	return ret;
}

static int stmvout_vidioc_g_fmt_vid_out(struct file *file, void *fh,
					struct v4l2_format *fmt)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	ret = get_vout_device_locked(fh, &pDev, NULL);
	if(ret)
		goto error;

	fmt->fmt.pix = pDev->bufferFormat.fmt.pix;
error:
	put_vout_device(pDev);
	return ret;
}

static int stmvout_vidioc_s_fmt_vid_out(struct file *file, void *fh,
				struct v4l2_format *fmt)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	bool isFirstUser = false;
	ret = get_vout_device_locked(fh, &pDev, &isFirstUser);
	if(ret)
		goto error;

	/* Check ioctl is legal */
	if (!isFirstUser) {
		err_msg
		    ("%s: VIDIOC_S_FMT: changing IO format not available on "
		     "this file descriptor\n",
		     __FUNCTION__);
		ret = -EINVAL;
		goto error;
	}

	if (pDev->isStreaming) {
		err_msg
		    ("%s: VIDIOC_S_FMT: device is streaming data, cannot "
		     "change format\n", __FUNCTION__);
		ret = -EINVAL;
		goto error;
	}

	ret = stmvout_set_buffer_format(pDev, fmt, 1);
error:
	put_vout_device(pDev);
	return ret;
}

static int stmvout_vidioc_try_fmt_vid_out(struct file *file, void *fh,
				  struct v4l2_format *fmt)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	ret = get_vout_device_locked(fh, &pDev, NULL);
	if(ret)
		goto error;

	ret = stmvout_set_buffer_format(pDev, fmt, 0);
error:
	put_vout_device(pDev);
	return ret;
}

static int stmvout_vidioc_cropcap(struct file *file, void *fh,
				struct v4l2_cropcap *a)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	ret = get_vout_device_locked(fh, &pDev, NULL);
	if(ret)
		goto error;

	if (a->type != V4L2_BUF_TYPE_VIDEO_OUTPUT) {
		debug_msg("%s: VIDIOC_CROPCAP: invalid crop type %d\n",
					     __FUNCTION__, a->type);
		ret = -EINVAL;
		goto error;
	}
	ret = stmvout_get_display_size(pDev, a);
error:
	put_vout_device(pDev);
	return ret;
}

static int stmvout_vidioc_g_crop(struct file *file, void *fh,
					struct v4l2_crop *crop)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	ret = get_vout_device_locked(fh, &pDev, NULL);
	if(ret)
		goto error;

	switch (crop->type) {
	case V4L2_BUF_TYPE_VIDEO_OUTPUT:
		crop->c = pDev->outputCrop.c;
		break;

	case V4L2_BUF_TYPE_PRIVATE:
		crop->c = pDev->srcCrop.c;
		break;

	default:
		err_msg("%s: VIDIOC_G_CROP: invalid crop type %d\n",
		     __FUNCTION__, crop->type);
		ret = -EINVAL;
	}
error:
	put_vout_device(pDev);
	return ret;
}

static int STM_V4L2_FUNC(stmvout_vidioc_s_crop,
		struct file *file, void *fh, struct v4l2_crop *crop)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	ret = get_vout_device_locked(fh, &pDev, NULL);
	if(ret)
		goto error;

	switch (crop->type) {
	case V4L2_BUF_TYPE_VIDEO_OUTPUT:
		if ((ret = stmvout_set_output_crop(pDev,
				(const struct v4l2_crop *) crop)) < 0)
			ret = -EINVAL;
		break;

	case V4L2_BUF_TYPE_PRIVATE:
		/*
		 * This is a private interface for specifying the crop of the
		 * source buffer to be displayed, as opposed to the crop on the
		 * screen. V4L2 has no way of doing this but we need to be able to
		 * to do this where:
		 *
		 * 1. the actual image size is only known once you start decoding
		 *    (generally too late to set the buffer format and allocate
		 *    display buffers)
		 * 2. the buffer dimensions may be larger than the image size due to
		 *    HW restrictions (e.g. Macroblock formats, 720 pixel wide video
		 *    must be in a 736 pixel wide buffer to have an even number of
		 *    macroblocks)
		 * 3. the image size can change dynamically in the stream.
		 * 4. we need to support pan/scan and center cutout with widescreen->
		 *    4:3 scaling
		 *
		 */
		if ((ret = stmvout_set_buffer_crop(pDev,
					(const struct v4l2_crop *) crop)) < 0)
			ret = -EINVAL;
		break;

	default:
		err_msg("%s: VIDIOC_S_CROP: invalid buffer type %d\n",
		     __FUNCTION__, crop->type);
		ret = -EINVAL;
	}
error:
	put_vout_device(pDev);
	return ret;
}

int stmvout_vidioc_g_parm(struct file *file, void *fh,
					struct v4l2_streamparm *sp)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	ret = get_vout_device_locked(fh, &pDev, NULL);
	if(ret)
		goto error;

	if (sp->type != V4L2_BUF_TYPE_VIDEO_OUTPUT){
		ret = -EINVAL;
		goto error;
	}

	/*
	 * The output streaming params are only relevant when using write(),
	 * not when using real streaming buffers. As we do not support the
	 * write() interface this is always zeroed.
	 */
	memset(&sp->parm.output, 0,
	       sizeof(struct v4l2_outputparm));
error:
	put_vout_device(pDev);
	return ret;
}

int stmvout_vidioc_reqbufs(struct file *file, void *fh,
					struct v4l2_requestbuffers *req)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	bool isFirstUser = false;
	unsigned long flags;
	ret = get_vout_device_locked(fh, &pDev, &isFirstUser);
	if(ret)
		goto error;

	if (!isFirstUser) {
		err_msg("%s: VIDIOC_REQBUFS: illegal in non-io open\n",
		     __FUNCTION__);
		ret = -EBUSY;
		goto error;
	}

	if (req->type != V4L2_BUF_TYPE_VIDEO_OUTPUT
	    || (req->memory != V4L2_MEMORY_MMAP
		&& req->memory != V4L2_MEMORY_USERPTR)) {
		err_msg("%s: VIDIOC_REQBUFS: unsupported type (%d) of memory "
		     "parameters (%d)\n", __FUNCTION__, req->type, req->memory);
		ret = -EINVAL;
		goto error;
	}

	if (pDev->bufferFormat.type != V4L2_BUF_TYPE_VIDEO_OUTPUT) {
		err_msg
		    ("%s: VIDIOC_REQBUFS: pixel format not set, cannot allocate "
		     "buffers\n", __FUNCTION__);
		ret = -EINVAL;
		goto error;
	}

	/*
	 * A subsequent call to VIDIOC_REQBUFS is allowed to change the number of
	 * buffers mapped. This is however, not allowed if any number of buffers are
	 * already mapped.
	 */
	write_lock_irqsave(&pDev->pendingStreamQ.lock, flags);
	stmvout_delete_buffers_from_list(&pDev->pendingStreamQ.list);
	write_unlock_irqrestore(&pDev->pendingStreamQ.lock, flags);

	if (!stmvout_deallocate_mmap_buffers(pDev)) {
		err_msg
		    ("%s: VIDIOC_REQBUFS: video buffer(s) still mapped, cannot "
		     "change\n", __FUNCTION__);
		ret = -EBUSY;
		goto error;
	}

	if (req->memory == V4L2_MEMORY_MMAP && req->count != 0
	    && !stmvout_allocate_mmap_buffers(pDev, req)) {
		err_msg("%s: VIDIOC_REQBUFS: unable to allocate video buffers\n",
		     __FUNCTION__);
		ret = -ENOMEM;
		goto error;
	}
error:
	put_vout_device(pDev);
	return ret;
}

int stmvout_vidioc_querybuf(struct file *file, void *fh,
							struct v4l2_buffer *pBuf)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	bool isFirstUser = false;
	streaming_buffer_t *pStrmBuf;

	ret = get_vout_device_locked(fh, &pDev, &isFirstUser);
	if(ret)
		goto error;

	pStrmBuf = &pDev->streamBuffers[pBuf->index];

	if (!isFirstUser) {
		err_msg("%s: VIDIOC_QUERYBUF: IO not available on this file "
		     "descriptor\n", __FUNCTION__);
		ret = -EBUSY;
		goto error;
	}

	if (pBuf->type != V4L2_BUF_TYPE_VIDEO_OUTPUT) {
		err_msg("%s: VIDIOC_QUERYBUF: illegal stream type\n",
		     __FUNCTION__);
		ret = -EINVAL;
		goto error;
	}

	if (pBuf->index >= pDev->n_streamBuffers || !pStrmBuf->physicalAddr) {
		err_msg
		("%s: VIDIOC_QUERYBUF: bad parameter v4l2_buffer.index: %u\n",
		__FUNCTION__, pBuf->index);
		ret = -EINVAL;
		goto error;
	}

	*pBuf = pStrmBuf->vidBuf;
error:
	put_vout_device(pDev);
	return ret;
}

int stmvout_vidioc_qbuf(struct file *file, void *fh,
						struct v4l2_buffer *pBuf)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	bool isFirstUser = false;

	ret = get_vout_device_locked(fh, &pDev, &isFirstUser);
	if(ret)
		goto error;



	if (!isFirstUser) {
		err_msg
		    ("%s: VIDIOC_QBUF: IO not available on this file descriptor\n",
		     __FUNCTION__);
		ret = -EBUSY;
		goto error;
	}

	if (pBuf->type != V4L2_BUF_TYPE_VIDEO_OUTPUT) {
		err_msg("%s: VIDIOC_QBUF: illegal stream type\n", __FUNCTION__);
		ret = -EINVAL;
		goto error;
	}

	if (pBuf->memory != V4L2_MEMORY_MMAP
			&& pBuf->memory != V4L2_MEMORY_USERPTR) {
		err_msg("%s: VIDIOC_QBUF: illegal memory type %d\n",
						__FUNCTION__, pBuf->memory);
		ret = -EINVAL;
		goto error;
	}

	if (pDev->outputCrop.type != V4L2_BUF_TYPE_VIDEO_OUTPUT) {
		err_msg
		    ("%s: VIDIOC_QBUF: no valid output crop set, cannot queue "
		     "buffer\n", __FUNCTION__);
		ret = -EINVAL;
		goto error;
	}

	/* Media Controller coherency check:
	 * also checks if the Plane is connected to an Output */
	if ((stmvout_get_output_display_handle(pDev)) == NULL) {
		err_msg
		    ("%s: VIDIOC_QBUF: while plane has no output connected!\n",
		     __FUNCTION__);
		ret = -EINVAL;
		goto error;
	}

	ret = stmvout_queue_buffer(pDev, pBuf);
error:
	put_vout_device(pDev);
	return ret;

}

int stmvout_vidioc_dqbuf(struct file *file, void *fh,
						struct v4l2_buffer *pBuf)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	bool isFirstUser = false;

	ret = get_vout_device_locked(fh, &pDev, &isFirstUser);
	if(ret)
		goto error;

	if (!isFirstUser) {
		err_msg
		    ("%s: VIDIOC_DQBUF: IO not available on this file descriptor\n",
		     __FUNCTION__);
		ret = -EBUSY;
		goto error;
	}

	if (pBuf->type != V4L2_BUF_TYPE_VIDEO_OUTPUT) {
		err_msg("%s: VIDIOC_DQBUF: illegal stream type\n", __FUNCTION__);
		ret = -EINVAL;
		goto error;
	}

	/*
	 * The API documentation states that the memory field must be set
	 * correctly by the application for the call to succeed. It isn't clear
	 * under what circumstances this would ever be needed and the requirement
	 * seems a bit over zealous, but we honour it anyway. I bet users will
	 * miss the small print and log lots of bugs that this call doesn't work.
	 */
	if (pBuf->memory != V4L2_MEMORY_MMAP
		&& pBuf->memory != V4L2_MEMORY_USERPTR) {
		err_msg("%s: VIDIOC_DQBUF: illegal memory type %d\n",
		     __FUNCTION__, pBuf->memory);
		ret = -EINVAL;
		goto error;
	}

	/* Media Controller coherency check:
	 * also checks if the Plane is connected to an Output */
	if ((stmvout_get_output_display_handle(pDev)) == NULL) {
		err_msg
		    ("%s: VIDIOC_DQBUF: while plane has no output connected!\n",
		     __FUNCTION__);
		ret = -EINVAL;
		goto error;
	}

	if (!pDev->isStreaming) {
		stv_debug("Device already streamed off\n");
		ret = -EINVAL;
		goto error;
	}

	/*
	 * We can release the driver lock now as stmvout_dequeue_buffer is safe
	 * (the buffer queues have their own access locks), this makes
	 * the logic a bit simpler particularly in the blocking case.
	 */
	put_vout_device(pDev);

	if ((file->f_flags & O_NONBLOCK) == O_NONBLOCK) {
		debug_msg("%s: VIDIOC_DQBUF: Non-Blocking dequeue\n",
			__FUNCTION__);
		if ((ret = stmvout_dequeue_buffer(pDev, pBuf)) != 1)
			ret = -EAGAIN;
	} else {
		/*debug_msg ("%s: VIDIOC_DQBUF: Blocking dequeue\n", __FUNCTION__); */
		ret = wait_event_interruptible(pDev->wqBufDQueue,
				stmvout_dequeue_buffer(pDev, pBuf) || !pDev->isStreaming);
	}
	if (ret)
		return ret;
	return 0;
error:
	put_vout_device(pDev);
	return ret;
}


int stmvout_vidioc_streamon(struct file *file, void *fh,
						enum v4l2_buf_type type)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	bool isFirstUser = false;
	ret = get_vout_device_locked(fh, &pDev, &isFirstUser);
	if(ret)
		goto error;

	if (type != V4L2_BUF_TYPE_VIDEO_OUTPUT) {
		err_msg
		    ("%s: VIDIOC_STREAMON: illegal stream type %d\n",
		     __FUNCTION__, type);
		ret = -EINVAL;
		goto error;
	}

	if (pDev->isStreaming) {
		err_msg("%s: VIDIOC_STREAMON: device already streaming\n",
		     __FUNCTION__);
		ret = -EBUSY;
		goto error;
	}
#ifdef _STRICT_V4L2_API
	if (!stmvout_has_queued_buffers(pDev)) {
		/* The API states that at least one buffer must be queued before
		   STREAMON succeeds. */
		err_msg("%s: VIDIOC_STREAMON: no buffers queued on stream\n",
		     __FUNCTION__);
		ret = -EINVAL;
		goto error;
	}
#endif

	if ((ret = stmvout_streamon(pDev)) < 0)
		goto error;

	debug_msg("%s: VIDIOC_STREAMON: Starting the stream\n",
		  __FUNCTION__);

	pDev->isStreaming = 1;
	wake_up_interruptible(&(pDev->wqStreamingState));

	/* Make sure any frames already on the pending queue are written to the
	   hardware */
	stmvout_send_next_buffer_to_display(pDev);
error:
	put_vout_device(pDev);
	return ret;
}

int stmvout_vidioc_streamoff(struct file *file, void *fh, enum v4l2_buf_type type)
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	bool isFirstUser = false;
	ret = get_vout_device_locked(fh, &pDev, &isFirstUser);
	if(ret)
		goto error;

	if (type != V4L2_BUF_TYPE_VIDEO_OUTPUT) {
		err_msg("%s: VIDIOC_STREAMOFF: illegal stream type\n",
		     __FUNCTION__);
		ret = -EINVAL;
		goto error;
	}

	ret = stmvout_streamoff(pDev);
error:
	put_vout_device(pDev);
	return ret;
}

int stmvout_vidioc_g_output(struct file *file, void *fh, unsigned int *output)
{
	struct stm_v4l2_vout_fh *vout_fh = container_of(fh, struct stm_v4l2_vout_fh, fh);
	open_data_t *pOpenData = vout_fh->pOpenData;

	*output = -1;	/* a priori output is not set! */
	/* exit if not yet set */
	if(!pOpenData)
		return 0;


	/* simply return the in use PAD id of /dev/video1 */
	*output = pOpenData->padId;

	/* Now let we assume this output still connected,
	 * In reality this request should be protected
	 * against simultaneous MC graph changes
	 */
	 return 0;
}

int stmvout_vidioc_s_output(struct file *file, void *fh, unsigned int output)
{
	struct stm_v4l2_vout_fh *vout_fh = container_of(fh, struct stm_v4l2_vout_fh, fh);
	open_data_t *pOpenData = vout_fh->pOpenData;
	stvout_device_t *pDev = NULL;
	struct semaphore *pLock = NULL;

	stvout_device_t *const new_dev = &g_voutRoot.pDev[output];
	struct list_head *list = &new_dev->openData.open_list;
	struct semaphore *new_lock = NULL;
	open_data_t *open_data = NULL;
	int ret = 0;


	if(pOpenData){
		BUG_ON(pOpenData->pDev == NULL);
		/* get shortcut to real video device (plane) */
		pDev = pOpenData->pDev;
		pLock = &(pDev->devLock);

		if (down_interruptible(pLock))
			return -ERESTARTSYS;
	}

	if (output >= stmvout_get_number_of_outputs()) {
		err_msg
		    ("S_OUTPUT: Output number %d out of range %d...%d\n",
		     output, 0,
		     (stmvout_get_number_of_outputs() - 1));
		ret = -EINVAL;
		goto error;
	}

	/* Is the max open count is reached? */
	if (new_dev->open_count > MAX_OPENS){
		ret = -EBUSY;
		goto error;
	}

	/* same output as is already set? */
	if (new_dev == pDev){
		goto error;
	}


	/* if current output is busy, we can't change it. */
	if (pDev) {
		/* it's busy if the first opener is streaming or has buffers queued.
		   Following openers may not start streaming/queueing buffers, so we
		   only need to do this check for the first user.
		   Following openers may change the output associated with their
		   filehandle, though. */
		if (pOpenData->isFirstUser && (pDev->isStreaming
			|| stmvout_has_queued_buffers(pDev))) {
			err_msg
			    ("S_OUTPUT: Output %d is active, cannot disassociate\n",
			     output);
			ret = -EBUSY;
			goto error;
		}
	}

	open_data = kzalloc(sizeof(open_data_t), GFP_KERNEL);
	if (!open_data) {
		printk(KERN_ERR "%s(): Unable to allocate memory for open data\n",
		       __func__);
		ret = -ENOMEM;
		goto error;
	}

	/* we should first check if we will be able to support another open on
	   the new device and allocate it for use by ourselves, so that we
	   don't end up without any device in case this fails... */
	new_lock = &new_dev->devLock;
	if (down_interruptible(new_lock)){
		kfree(open_data);
		ret = -ERESTARTSYS;
		goto error;
	}

	open_data->isOpen = 1;
	open_data->pDev = new_dev;
	open_data->padId = output;
	if (list_empty(list)) {
		open_data->isFirstUser = 1;
	} else {
		open_data->isFirstUser = 0;
	}
	list_add_tail(&(open_data->open_list), list);

	new_dev->open_count++;

	if (new_dev->open_count == 1) {
		if ((ret = init_device_locked(new_dev)) != 0) {
			up(new_lock);
			kfree(open_data);
			goto error;
		}
	}

	/* we have the device now, release the old one */
	if (pDev) {
		if ((ret =  stmvout_close_locked(pOpenData, pDev)) != 0) {
			up(new_lock);
			/* FIXME hows the pOpenData removed from the llist?*/
			kfree(open_data);
			goto error;
		}
		up(pLock);
	}

	pDev = new_dev;
	pLock = new_lock;

	debug_msg("v4l2_open: isFirstUser: %c\n",
		  open_data->isFirstUser ? 'y' : 'n');

	((struct v4l2_fh *)fh)->ctrl_handler = &pDev->ctrl_handler;

	vout_fh->pOpenData = open_data;

error:
	if (pDev) {
		up(pLock);
	}
	return ret;
}

int stmvout_vidioc_enum_output(struct file *file, void *fh,
						struct v4l2_output *output)
{
	int index = output->index;
	int ret = 0;
	if (index < 0 || index >= stmvout_get_number_of_outputs())
		return -EINVAL;

	snprintf(output->name, sizeof(output->name),
		 "%s", g_voutRoot.pDev[index].name);
	output->type = V4L2_OUTPUT_TYPE_ANALOG;
	output->audioset = 0;
	output->modulator = 0;

	/* Returned standard = 0 if the Display plane has not been linked
	 * with an output (Media Controller).
	 */
	if ((ret = stmvout_get_supported_standards(&g_voutRoot.pDev[index],
					     &output->std)) < 0)
		output->std = 0;

	return 0;
}

#if (LINUX_VERSION_CODE < KERNEL_VERSION(3, 10, 0))
static long stmvout_vidioc_private_ioctl(struct file *file, void *fh,
				bool valid_prio, int cmd, void *arg)
#else
static long stmvout_vidioc_private_ioctl(struct file *file, void *fh,
				bool valid_prio, unsigned int cmd, void *arg)
#endif
{
	stvout_device_t *pDev = NULL;
	int ret = 0;
	ret = get_vout_device_locked(fh, &pDev, NULL);
	if(ret)
		goto error;

	switch(cmd){
	case VIDIOC_G_OUTPUT_STD:
		{
			v4l2_std_id *const std_id = arg;

			if ((ret =
			     stmvout_get_current_standard(pDev, std_id)) < 0)
				goto error;
		}
		break;

	case VIDIOC_S_OUTPUT_STD:
		{
			v4l2_std_id *const std_id = arg;

			if ((ret =
			     stmvout_set_current_standard(pDev, *std_id)) < 0)
				goto error;
		}
		break;

	case VIDIOC_ENUM_OUTPUT_STD:
		{
			struct v4l2_standard *const std = arg;

			if ((ret = stmvout_get_display_standard(pDev, std)) < 0)
				goto error;
		}
		break;

	case VIDIOC_S_OUTPUT_ALPHA:
		{
			unsigned int alpha = *(unsigned int *)arg;

			if (alpha > 255)
				goto error;

			pDev->globalAlpha = alpha;
			if ((ret = stm_display_plane_set_control(pDev->hPlane,
				PLANE_CTRL_TRANSPARENCY_VALUE,
				pDev->globalAlpha)) !=0) {
				err_msg("Cannot set transparency (%d)\n",ret);
				goto error;
			}
			if( pDev->alpha_control != CONTROL_ON) {
				if ((ret = stm_display_plane_set_control(pDev->hPlane,
					PLANE_CTRL_TRANSPARENCY_STATE,
					CONTROL_ON)) !=0) {
					err_msg("Cannot set transparency state to ON (%d)\n",ret);
					goto error;
				}
				pDev->alpha_control = CONTROL_ON;
			} else if ( pDev->globalAlpha == 255 ) {
				if ((ret = stm_display_plane_set_control(pDev->hPlane,
					PLANE_CTRL_TRANSPARENCY_STATE,
					CONTROL_OFF)) !=0) {
					err_msg("Cannot set transparency state to OFF (%d)\n",ret);
					goto error;
				}
				pDev->alpha_control = CONTROL_OFF;
			}
		}
		break;
	default:
		ret = -ENOTTY;
		goto error;
	}

error:
	put_vout_device(pDev);
	return ret;
}

/**********************************************************
 * mmap helper functions
 **********************************************************/
static void stmvout_vm_open(struct vm_area_struct *vma)
{
	streaming_buffer_t *pBuf = vma->vm_private_data;

	debug_msg("%s: %p [count=%d,vma=%08lx-%08lx]\n", __FUNCTION__, pBuf,
		  pBuf->mapCount, vma->vm_start, vma->vm_end);

	pBuf->mapCount++;
}

static void stmvout_vm_close(struct vm_area_struct *vma)
{
	streaming_buffer_t *const pBuf = vma->vm_private_data;

	debug_msg("%s: %p [count=%d,vma=%08lx-%08lx]\n", __FUNCTION__, pBuf,
		  pBuf->mapCount, vma->vm_start, vma->vm_end);

	if (--pBuf->mapCount == 0) {
		debug_msg("%s: %p clearing mapped flag\n", __FUNCTION__, pBuf);
		pBuf->vidBuf.flags &= ~V4L2_BUF_FLAG_MAPPED;
	}

	return;
}

/* VSOC - HCE - for bpa2 buffer access from user space, we must have the      */
/* access file ops. This should also be done on SoC for access thru debugger! */
static int stmvout_vm_access(struct vm_area_struct *vma, unsigned long addr,
			void *buf, int len, int write)
{
	void *page_addr;
	unsigned long page_frame;

	/*
	* Validate access limits
	*/
	if (addr >= vma->vm_end)
		return 0;
	if (len > vma->vm_end - addr)
		len = vma->vm_end - addr;
	if (len == 0)
		return 0;

	/*
	 * Note that this assumes an identity mapping between the page offset and
	 * the pfn of the physical address to be mapped. This will get more complex
	 * when the 32bit SH4 address space becomes available.
	*/
	page_addr = (void*)((addr - vma->vm_start) + (vma->vm_pgoff << PAGE_SHIFT));

	page_frame = ((unsigned long) page_addr >> PAGE_SHIFT);

	if (pfn_valid (page_frame)) {
		if (write)
			memcpy(__va(page_addr), buf, len);
		else
			memcpy(buf, __va(page_addr), len);
		return len;
	}

#if defined(CONFIG_ARM) || defined(CONFIG_HCE_ARCH_ARM)
	{
		void __iomem *ioaddr = __arm_ioremap((unsigned long)page_addr, len, MT_MEMORY_RWX);
		if (write)
			memcpy_toio(ioaddr, buf, len);
		else
			memcpy_fromio(buf, ioaddr, len);
		iounmap(ioaddr);
	}
	return len;
/*#elif defined(CONFIG_HAVE_IOREMAP_PROT) FIXME for stlinuxtv unknown symbol while module loading in case of hdk7108
    return generic_access_phys(vma, addr, buf, len, write);*/
#else
	return 0;
#endif
}

static int stmvout_vm_fault(struct vm_area_struct *vma, struct vm_fault *vmf)
{
	struct page *page;
	void *page_addr;
	unsigned long page_frame;
	unsigned long vaddr = (unsigned long)vmf->virtual_address;

	if (vaddr > vma->vm_end)
		return VM_FAULT_SIGBUS;

	/*
	 * Note that this assumes an identity mapping between the page offset and
	 * the pfn of the physical address to be mapped. This will get more complex
	 * when the 32bit SH4 address space becomes available.
	 */
	page_addr =
	    (void *)((vaddr - vma->vm_start) + (vma->vm_pgoff << PAGE_SHIFT));

	page_frame = ((unsigned long)page_addr >> PAGE_SHIFT);

	if (!pfn_valid(page_frame))
		return VM_FAULT_SIGBUS;

	page = virt_to_page(__va(page_addr));

	get_page(page);

	vmf->page = page;
	return 0;
}

static struct vm_operations_struct stmvout_vm_ops_memory = {
	.open = stmvout_vm_open,
	.close = stmvout_vm_close,
	.fault = stmvout_vm_fault,
	.access = stmvout_vm_access,
};

static struct vm_operations_struct stmvout_vm_ops_ioaddr = {
	.open = stmvout_vm_open,
	.close = stmvout_vm_close,
	.fault = NULL,
	.access = stmvout_vm_access,
};

/* --------------------------------------------------------------- */
/*  File Operations */

static int stmvout_mmap(struct file *file, struct vm_area_struct *vma)
{
	struct stm_v4l2_vout_fh *vout_fh = container_of(file->private_data, struct stm_v4l2_vout_fh, fh);
	open_data_t *pOpenData = vout_fh->pOpenData;

	stvout_device_t *pDev;
	streaming_buffer_t *pBuf;

	if (!pOpenData) {
		debug_msg("%s bug: file private data not set!\n", __FUNCTION__);
		return -ENODEV;
	}

	if (!(vma->vm_flags & VM_WRITE)) {
		debug_msg("mmap app bug: PROT_WRITE please\n");
		return -EINVAL;
	}

	if (!(vma->vm_flags & VM_SHARED)) {
		debug_msg("mmap app bug: MAP_SHARED please\n");
		return -EINVAL;
	}

	BUG_ON(pOpenData == NULL);

	/* was: pDev = pOpenData->pDev; */
	pDev = pOpenData->pDev;

	if (down_interruptible(&pDev->devLock))
		return -ERESTARTSYS;

	if (!pOpenData->isFirstUser) {
		err_msg
		    ("mmap() another open file descriptor exists and is doing IO\n");
		goto err_busy;
	}

	debug_msg("v4l2_mmap offset = 0x%08X\n", (int)vma->vm_pgoff);
	pBuf =
	    stmvout_get_buffer_from_mmap_offset(pDev,
						vma->vm_pgoff * PAGE_SIZE);
	if (!pBuf) {
		/* no such buffer */
		err_msg("%s: Invalid offset parameter\n", __FUNCTION__);
		goto err_inval;
	}

	debug_msg("v4l2_mmap pBuf = 0x%08lx\n", (unsigned long)pBuf);

	if (pBuf->vidBuf.length != (vma->vm_end - vma->vm_start)) {
		/* wrong length */
		err_msg("%s: Wrong length parameter\n", __FUNCTION__);
		goto err_inval;
	}

	if (!pBuf->cpuAddr) {
		/* not requested */
		err_msg("%s: Buffer is not available for mapping\n",
			__FUNCTION__);
		goto err_inval;
	}

	if (pBuf->mapCount > 0) {
		err_msg("%s: Buffer is already mapped\n", __FUNCTION__);
		goto err_busy;
	}

#if (LINUX_VERSION_CODE < KERNEL_VERSION(3, 10, 0))
	vma->vm_flags |= VM_RESERVED | VM_IO | VM_DONTEXPAND | VM_LOCKED;
#else
	vma->vm_flags |= VM_DONTDUMP | VM_IO | VM_DONTEXPAND | VM_LOCKED;
#endif
	vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
	vma->vm_private_data = pBuf;

	if (virt_addr_valid(pBuf->cpuAddr)) {
		debug_msg("%s: remapping memory.\n", __FUNCTION__);
		vma->vm_ops = &stmvout_vm_ops_memory;
	} else {
		debug_msg("%s: remapping IO space.\n", __FUNCTION__);
		/* Note that this assumes an identity mapping between the page offset and
		   the pfn of the physical address to be mapped. This will get more
		   complex when the 32bit SH4 address space becomes available. */
		if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff,
				    (vma->vm_end - vma->vm_start),
				    vma->vm_page_prot)) {
			up(&pDev->devLock);
			return -EAGAIN;
		}

		vma->vm_ops = &stmvout_vm_ops_ioaddr;
	}

	pBuf->mapCount = 1;
	pBuf->vidBuf.flags |= V4L2_BUF_FLAG_MAPPED;

	up(&pDev->devLock);

	return 0;

err_busy:
	up(&pDev->devLock);
	return -EBUSY;

err_inval:
	up(&pDev->devLock);
	return -EINVAL;
}

/************************************************
 * v4l2_poll   TODO
 ************************************************/
static unsigned int stmvout_poll(struct file *file, poll_table * table)
{
	struct stm_v4l2_vout_fh *vout_fh = container_of(file->private_data, struct stm_v4l2_vout_fh, fh);
	open_data_t *pOpenData = vout_fh->pOpenData;

	stvout_device_t *pDev;
	unsigned int mask;

	if (!pOpenData) {
		debug_msg("%s bug: file private data not set!\n", __FUNCTION__);
		return -ENODEV;
	}

	BUG_ON(pOpenData == NULL);

	/* was: pDev = pOpenData->pDev; */
	pDev = pOpenData->pDev;

	if (down_interruptible(&pDev->devLock))
		return -ERESTARTSYS;

	/* Add DQueue wait queue to the poll wait state */
	poll_wait(file, &pDev->wqBufDQueue, table);
	poll_wait(file, &pDev->wqStreamingState, table);

	if (!pOpenData->isFirstUser)
		mask = POLLERR;
	else if (!pDev->isStreaming)
		mask = POLLOUT;	/* Write is available when not streaming */
	else if (stmvout_has_completed_buffers(pDev))
		mask = POLLIN;	/* Indicate we can do a DQUEUE ioctl */
	else
		mask = 0;

	up(&pDev->devLock);

	return mask;
}

/*
 * Search a plane entity by name. Return the entity address
 * Return NULL if not found.
 */
struct media_entity *stmvout_get_voutPlug_entity_by_name(char *name)
{
	int i;
	for (i = 0; i < STMEDIA_MAX_OUTPUTS; i++) {
		if (g_voutPlug[i].name[0] != '\0') {
			if (strncmp
			    (g_voutPlug[i].name, name,
			     sizeof(g_voutPlug[i].name)) == 0)
				return (&g_voutPlug[i].sdev.entity);
		}
	}
	return (NULL);
}

static void stmvout_show_voutPlug(void)
{
#ifdef DEBUG_SHOW_OUTPUTS
	int i;
	printk(" %s:%s =====  MC output table ======\n", PKMOD, __func__);
	printk("  (In) represent MC input PAD flags\n");
	printk("  disp.H stand for Display handle; S for subdevice addr.\n");
	for (i = 0; i < STMEDIA_MAX_OUTPUTS; i++) {
		if (g_voutPlug[i].name[0] == '\0')
			break;	/* end of table */
		printk(" [%1d] %16s:%d type 0x%x disp.H %p S %p (%lx)\n",
		       i, g_voutPlug[i].name, g_voutPlug[i].id,
		       g_voutPlug[i].type,
		       g_voutPlug[i].hOutput, &g_voutPlug[i].sdev,
		       g_voutPlug[i].pads[0].flags);
	}
#endif
}

/**
 * stmvout_probe_media_outputs
 * This initializes the media output. This function
 * is called during module_init.
 */
int stmvout_probe_media_outputs(void)
{
	stm_display_device_h hDisplay;
	stm_display_output_h hOutput;
	int display_device_id, Output, ret = 0;

	debug_mc("==== %s executing!\n", __func__);
	/* scan all existing displays...
	 * Assume no more Displays when the open fails
	 */
	for (display_device_id = 0;; display_device_id++) {
		ret = stm_display_open_device(display_device_id, &hDisplay);
		if (ret) {
			if (display_device_id == 0) {
				printk(KERN_ERR
				       "%s: SoC without Display !?!?\n",
				       __func__);
				ret = -EFAULT;
				break;
			} else {
				/* Ok, assume no more display, just exit from loop */
				ret = 0;
				break;
			}
		}

		/* Search for all outputs of a Display, These will be part of MC graph.
		 * - Get plane capabilities, name and type.
		 * - Use name extracted from Display as MC entity and sub-device name.
		 * - A display without outputs is accepted (make sense?)
		 * - Valid outputs are kept open
		 * - failure of "open output" mean the Display has no more outputs
		 * - failure of any other "output operation" just skip the output
		 */
		for (Output = 0;; Output++) {
			const char *name = "";
			char *msg = "???";

			/* if the open fails, assumes no more planes! */
			if (stm_display_device_open_output
			    (hDisplay, Output, &hOutput))
				break;

			if (stm_display_output_get_name(hOutput, &name)) {
				msg = "has no name";
				goto skip_output;
			}

			/* record the output, this may fail:
			 *   - because of output table overflow
			 *   - because a V4L registering primitive fails
			 * in any case skip the output and try to continue!
			 */
			if (stm_v4l2_output_subdev_init
			    (g_voutPlug, Output, name, hOutput, display_device_id)) {
				msg = "V4L2 registration failed";
				goto skip_output;
			}
			continue;
skip_output:
			/* close "bad" output plugs */
			stm_display_output_close(hOutput);
			printk(KERN_WARNING
			       "output plug (%d) %s skipped: %s! (%d)\n",
			       Output, name, msg, ret);

		}

		printk(KERN_INFO "%s: Display %d: %d output Plug added\n",
		       __func__, display_device_id, Output);

		stm_display_device_close(hDisplay);
	}

	stmvout_show_voutPlug();	/* debug only */

	return (ret);
}

/**
 * stmvout_release_media_outputs
 * This function closes all the output devices opened by the driver.
 * See: stmvout_probe_media_outputs
 */

static void stmvout_release_media_outputs(void)
{
	int display_device_id;
	stm_display_output_h hOutput;
	stm_display_device_h hDevice;
	int index, count;

	for (index = 0; index < STMEDIA_MAX_OUTPUTS; index++) {
		if (g_voutPlug[index].display_device_id < 0)
			continue;

		/*
		 * Open the Display device
		 */
		if(!g_voutPlug[index].hDisplay) {
			if(stm_display_open_device(g_voutPlug[index].display_device_id, &g_voutPlug[index].hDisplay)) {
				printk(KERN_ERR "%s: failed to open display device\n",
						__func__);
				return;
			}
		}
		hDevice = g_voutPlug[index].hDisplay;

		display_device_id = g_voutPlug[index].display_device_id;
		for (count = index; count < STMEDIA_MAX_OUTPUTS; count++) {
			if (g_voutPlug[count].display_device_id != display_device_id)
				continue;

			hOutput = g_voutPlug[count].hOutput;
			stm_v4l2_output_subdev_exit(&g_voutPlug[count]);
			stm_display_output_close(hOutput);
		}

		/*
		 * Close the display device
		 */
		stm_display_device_close(hDevice);

	}
}

/* =======================================================================
 * Plane subdevice operations:
 */

static const struct v4l2_subdev_core_ops stmvout_core_ops = {
	.queryctrl = v4l2_subdev_queryctrl,
	.querymenu = v4l2_subdev_querymenu,
	.g_ctrl = v4l2_subdev_g_ctrl,
	.s_ctrl = v4l2_subdev_s_ctrl,
	.g_ext_ctrls = v4l2_subdev_g_ext_ctrls,
	.try_ext_ctrls = v4l2_subdev_try_ext_ctrls,
	.s_ext_ctrls = v4l2_subdev_s_ext_ctrls,
};

static int stmedia_plane_pad_get_crop(struct v4l2_subdev *sd,
				      struct v4l2_subdev_fh *fh,
				      struct v4l2_subdev_crop *crop)
{
	struct _stvout_device *plane;
	stm_rect_t rect;
	int ret;

	/* FIXME - this is not really good implementation, we should handle which field properly */

	if (crop->pad > 1)
		return -EINVAL;

	plane = v4l2_get_subdevdata(sd);

	ret = stm_display_plane_get_compound_control(plane->hPlane,
						     (crop->pad == 0)?PLANE_CTRL_INPUT_WINDOW_VALUE:PLANE_CTRL_OUTPUT_WINDOW_VALUE,
						     &rect);
	if (ret){
		debug_msg("%s: failed to get plane window value\n",
			  __FUNCTION__);
		return -EIO;
	}

	crop->rect.left = rect.x;
	crop->rect.top = rect.y;
	crop->rect.width = rect.width;
	crop->rect.height = rect.height;

	return 0;
}

/**
 * stmedia_plane_pad_set_crop() - configure input/output window dimensions
 * This subdev callback is executed in response to VIDIOC_S_FMT and
 * VIDIOC_S_CROP on V4L2 AV capture device. In response to these,
 * decoder finds out the respective plane connected and then perform
 * the operation on it.
 */
static int stmedia_plane_pad_set_crop(struct v4l2_subdev *sd,
				      struct v4l2_subdev_fh *fh,
				      struct v4l2_subdev_crop *crop)
{
	struct _stvout_device *plane;
	stm_compound_control_range_t range;
	stm_plane_mode_t mode;
	stm_rect_t rect;
	int ret;

	/*
	 * v4l2_subdev_crop.which indicates whether it's a try control
	 * or set control. At the moment, only set control is supported
	 */

	if (crop->pad > 1) {
		ret = -EINVAL;
		goto subdev_s_crop_done;
	}

	plane = v4l2_get_subdevdata(sd);

	/*
	 * Fill in the rectangle parameters for window configuration
	 */
	rect.x = crop->rect.left;
	rect.y = crop->rect.top;
	rect.width = crop->rect.width;
	rect.height = crop->rect.height;

	/*
	 * First check here, if the plane is configured in auto mode or
	 * manual mode. In auto mode, VIBE can override the values, and
	 * in manual mode, the only error possible is out of bounds
	 * values, which will be taken care. The point here is to indicate
	 * the user that AUTO MODE may not be setting the expected values.
	 */
	ret = stm_display_plane_get_control(plane->hPlane,
				(crop->pad == 0 ? PLANE_CTRL_INPUT_WINDOW_MODE :
				PLANE_CTRL_OUTPUT_WINDOW_MODE),
				&mode);
	if (ret) {
		err_msg("%s(): Failed to get mode for %s window\n",
				__func__, (crop->pad == 0 ? "Input" : "Output"));
		goto subdev_s_crop_done;
	}

	/*
	 * Throw a warning in case the plane is configured in AUTO mode
	 */
	if (mode ==  AUTO_MODE)
		printk(KERN_WARNING "%s(): %s() plane is configured in AUTO mode"
			"Expected cropping may not be achieved\n", __func__,
			(crop->pad == 0) ? "input" : "output");

	/*
	 * Get the range of the input/output window and adjust the request
	 * in accordance to these parameters
	 */
	ret = stm_display_plane_get_compound_control_range(plane->hPlane,
					(crop->pad == 0 ? PLANE_CTRL_INPUT_WINDOW_VALUE:
					PLANE_CTRL_OUTPUT_WINDOW_VALUE),
					&range);
	if (ret) {
		err_msg("%s(): Failed to get plane window range\n", __func__);
		goto subdev_s_crop_done;
	}

	/*
	 * Compare the control range values with the user-specified values
	 * and find out the correct values to set.
	 * TODO: At the moment, rectangle max values contain a value of
	 * (x,y) as (0,0). So, there is no need to cap here the (x,y) values.
	 * It has also been noticed that out of bounds values of (x,y) are
	 * handled by CoreDisplay but not of width and height. So, in case,
	 * there is an update in behaviour of CoreDisplay API, then we need
	 * not get the range, and directly call the API to sent the cropping
	 * parameters.
	 */
	if (rect.x < range.min_val.x)
		rect.x = range.min_val.x;

	if (rect.y < range.min_val.y)
		rect.y = range.min_val.y;

	/*
	 * If we set the width/height to CoreDisplay min, then, CoreDisplay
	 * errors out, so, adding 1 to make CoreDisplay happy
	 */
	if (rect.width <= range.min_val.width)
		rect.width = range.min_val.width + 1;
	else if (rect.width > range.max_val.width)
		rect.width = range.max_val.width;

	if (rect.height <= range.min_val.height)
		rect.height = range.min_val.height + 1;
	else if (rect.height > range.max_val.height)
		rect.height = range.max_val.height;

	ret = stm_display_plane_set_compound_control(plane->hPlane,
					(crop->pad == 0) ? PLANE_CTRL_INPUT_WINDOW_VALUE :
					PLANE_CTRL_OUTPUT_WINDOW_VALUE,
					&rect);
	if (ret) {
		err_msg("%s: failed to set plane window value\n", __func__);
		goto subdev_s_crop_done;
	}

	/*
	 * It's VIDIOC_S_FMT for output plane, so, update the cropping
	 * parameters with the latest value
	 */
	if (crop->pad == 1) {
		ret = stm_display_plane_get_compound_control(plane->hPlane,
					PLANE_CTRL_OUTPUT_WINDOW_VALUE,
					&rect);
		if (ret) {
			err_msg("%s(): failed to get output window params\n", __func__);
			goto subdev_s_crop_done;
		}

		crop->rect.left = rect.x;
		crop->rect.top = rect.y;
		crop->rect.width = rect.width;
		crop->rect.height = rect.height;
	}

subdev_s_crop_done:
	return ret;
}

static const struct v4l2_subdev_pad_ops stmedia_plane_pad_ops = {
	.get_crop = &stmedia_plane_pad_get_crop,
	.set_crop = &stmedia_plane_pad_set_crop,
};

/** map_mbusfmt_to_pixelstream() - Maps the mbusfmt to the pixelstream timing
 * 			           structure.
 * @pre The system should be initialized.
 * @param[in] *fmt the mbus fmt structure
 * @param[out] *pixelstream_param The pixel stream timing structure.
 * @return 0 if mappping successful.
 *     -EINVAL if error.
 */
static int map_mbusfmt_to_pixelstream(struct v4l2_mbus_framefmt *fmt,
					  stm_display_source_pixelstream_params_t
					  *pixelstream_param)
{
	/* this will replace the existing flags set by s_dv_timings.
	But its OK and it ensures that flags are reset*/
	pixelstream_param->flags = 0;
	pixelstream_param->visible_area.width = fmt->width;
	pixelstream_param->visible_area.height = fmt->height;

	switch(fmt->code) {
		case V4L2_MBUS_FMT_STM_RGB8_3x8:
		case V4L2_MBUS_FMT_STM_RGB10_3x10:
		case V4L2_MBUS_FMT_STM_RGB12_3x12:
			pixelstream_param->colorType = STM_PIXELSTREAM_SRC_RGB;
			break;
		case V4L2_MBUS_FMT_YUYV8_2X8:
		case V4L2_MBUS_FMT_YUYV10_2X10:
			pixelstream_param->colorType = STM_PIXELSTREAM_SRC_YUV_422;
			break;
		case V4L2_MBUS_FMT_STM_YUV8_3X8:
		case V4L2_MBUS_FMT_STM_YUV10_3X10:
		case V4L2_MBUS_FMT_STM_YUV12_3X12:
			pixelstream_param->colorType = STM_PIXELSTREAM_SRC_YUV_444;
			break;
	}
	switch(fmt->colorspace) {
		case V4L2_COLORSPACE_REC709:
			pixelstream_param->flags |=
			    STM_PIXELSTREAM_SRC_COLORSPACE_709;
			break;
		default:
			break;
	}

	if (fmt->field == V4L2_FIELD_INTERLACED) {
		pixelstream_param->flags |= STM_PIXELSTREAM_SRC_INTERLACED;
	}

	return 0;
}


/** map_dvtimings_to_pixelstream() - Maps the dvtiming info to the pixelstream timing
 * 			           structure.
 * @pre The system should be initialized.
 * @param[in] *timings the dv_timings structure
 * @param[out] *pixelstream_param The pixel stream timing structure.
 * @return 0 if mappping successful.
 *     -EINVAL if error.
 */
static int map_dvtimings_to_pixelstream(struct v4l2_dv_timings *timings,
					  stm_display_source_pixelstream_params_t
					  *pixelstream_param)
{
	uint32_t total_lines = 0;

	pixelstream_param->htotal = timings->bt.width + timings->bt.hsync + timings->bt.hfrontporch + timings->bt.hbackporch;
	pixelstream_param->vtotal = timings->bt.height + timings->bt.vsync + timings->bt.vfrontporch + timings->bt.vbackporch;
	total_lines =
	    pixelstream_param->htotal * pixelstream_param->vtotal;
	if(total_lines){
		timings->bt.pixelclock *= 1000;/* we want frame rate multiplied by 1000*/
		do_div(timings->bt.pixelclock, total_lines);
		/* do_div has strange sematics*/
		pixelstream_param->src_frame_rate = timings->bt.pixelclock;
	}
	else
		pixelstream_param->src_frame_rate = 0;

	pixelstream_param->active_window.x = timings->bt.hfrontporch;
	pixelstream_param->active_window.y = timings->bt.vfrontporch;
	pixelstream_param->active_window.width = timings->bt.width;
	pixelstream_param->active_window.height = timings->bt.height;
	pixelstream_param->visible_area.x = 0;
	pixelstream_param->visible_area.y = 0;
	pixelstream_param->visible_area.width = timings->bt.width;
	pixelstream_param->visible_area.height = timings->bt.height;
	pixelstream_param->pixel_aspect_ratio.numerator = 1;/* probably this should move to s_crop*/
	pixelstream_param->pixel_aspect_ratio.denominator = 1;

	if (timings->bt.interlaced){
		pixelstream_param->flags |= STM_PIXELSTREAM_SRC_INTERLACED;
		pixelstream_param->visible_area.height *= 2; /* for interlaced need to multiply by 2 */
	}else {
		pixelstream_param->flags &= ~STM_PIXELSTREAM_SRC_INTERLACED;
	}

	return 0;
}

static int stmedia_plane_video_s_dv_timings(struct v4l2_subdev *sd,
			struct v4l2_dv_timings *timings)
{
	struct _stvout_device *plane;
	int ret;
	struct stm_source_info *src_info;
	if (!timings)
		return -EINVAL;

	plane = v4l2_get_subdevdata(sd);
	src_info = plane->source_info;

	if(src_info->iface_type != STM_SOURCE_PIXELSTREAM_IFACE)
		return -EINVAL;

	ret = map_dvtimings_to_pixelstream(timings, &(src_info->pixel_param));

	return ret;
}

static int stmedia_plane_video_s_mbus_fmt(struct v4l2_subdev *sd,
						struct v4l2_mbus_framefmt *fmt)
{
	struct _stvout_device *plane;
	struct stm_source_info *src_info;
	stm_display_source_pixelstream_params_t *param;
	int ret;

	if (!fmt)
		return -EINVAL;

	plane = v4l2_get_subdevdata(sd);

	src_info = plane->source_info;
	param = &src_info->pixel_param;

	if(src_info->iface_type != STM_SOURCE_PIXELSTREAM_IFACE)
		return -EINVAL;

	ret = map_mbusfmt_to_pixelstream(fmt, param);

	if (src_info->pixeltype == 0) /* 0 stands for PIX_HDMI source*/
		param->flags |= STM_PIXELSTREAM_SRC_CAPTURE_BASED_ON_DE;

	printk(KERN_DEBUG "%s(): Will try to set following pixelstream params\n", __func__);
	printk(KERN_DEBUG "--> htotal = %d\n", param->htotal);
	printk(KERN_DEBUG "--> vtotal = %d\n", param->vtotal);
	printk(KERN_DEBUG "--> source frame rate = %d\n", param->src_frame_rate);
	printk(KERN_DEBUG "--> flags = 0x%x\n", param->flags);
	printk(KERN_DEBUG "--> active window height = %d\n", param->active_window.height);
	printk(KERN_DEBUG "--> active window width = %d\n", param->active_window.width);
	printk(KERN_DEBUG "--> active window X = %d\n", param->active_window.x);
	printk(KERN_DEBUG "--> active window Y = %d\n", param->active_window.y);
	printk(KERN_DEBUG "%s(): pixelstream params ends\n", __func__);

	ret = stm_display_source_pixelstream_set_input_params
					(src_info->iface_handle, param);
	if (ret)
		printk(KERN_ERR "%s(): Failed to set pixelstream params\n", __func__);

	return ret;
}

static const struct v4l2_subdev_video_ops stmedia_plane_video_ops = {
	.s_dv_timings = &stmedia_plane_video_s_dv_timings,
	.s_mbus_fmt = &stmedia_plane_video_s_mbus_fmt,
};

/**
 * plane_media_source_link_setup() - link setup for plane to output
 */
static int plane_media_source_link_setup(struct media_entity *entity,
					 const struct media_pad *local,
					 const struct media_pad *remote,
					 u32 flags)
{
	int ret = 0;
	struct _stvout_device *vout;
	struct output_plug *output;

	vout = plane_entity_to_stvout_device(local->entity);
	output = output_entity_to_output_plug(remote->entity);

	/*
	 * Open the display device
	 */
	ret = vout_display_open(vout);
	if (ret) {
		stv_err("Failed to open associated display\n");
		goto link_setup_done;
	}

	switch (remote->entity->type) {
	case MEDIA_ENT_T_V4L2_SUBDEV_OUTPUT:

		/*
		 * Open display and connect plane to output
		 */
		if (flags & MEDIA_LNK_FL_ENABLED)
			ret = stm_display_plane_connect_to_output(vout->hPlane, output->hOutput);
		else
			ret = stm_display_plane_disconnect_from_output(vout->hPlane, output->hOutput);
		break;
	}

	/*
	 * Close the display device
	 */
	vout_display_close(vout);

link_setup_done:
	return ret;
}

struct subdev_name_fbtype {
	char *name;
	int type;
};


struct subdev_name_fbtype subdev_names[] = {
	{STM_HDMIRX_SUBDEV, 0},
	{STM_VXI_SUBDEV, 2},
};

char* search_subdev_name(const char *name, int *index)
{
	char *substr;
	if(*index == (ARRAY_SIZE(subdev_names))){
		*index = -1;
		return NULL;
	}

	substr = strstr(name, subdev_names[*index].name);
	if(!substr) {
		(*index)++;
		substr = search_subdev_name(name, index);
	}

	return substr;
}

static int stmvout_put_source_info(struct stm_source_info **src_info)
{
	if((*src_info)->iface_handle) {
		stm_display_source_queue_unlock((*src_info)->iface_handle);

		stmvout_release_interface((*src_info)->iface_handle, (*src_info)->iface_type);
	}
	stm_display_source_close((*src_info)->source);

	kfree(*src_info);
	*src_info = NULL;
	return 0;
}

static int stmvout_plane_disconnect_from_src(stvout_device_t *vout)
{
	int ret;

	if(vout->source_info == NULL) /* Already freed ?*/
		return 0;

	debug_msg("we are disconecting the source %p \n", vout->hSource);


	if(down_interruptible(&vout->devLock))
		return -ERESTARTSYS;

	ret = vout_setup_plane_to_source(PLANE_CTRL_DISCONNECT_FROM_SOURCE,
							vout, vout->hSource);
	if (ret) {
		pr_err("%s(): failed to disconnect source from plane\n", __func__);
		up(&vout->devLock);
		return -EINVAL;
	}

	ret = stmvout_put_source_info(&(vout->source_info));
	if(ret) {
		up(&vout->devLock);
		return -EINVAL;
	}

	vout->hQueueInterface = NULL;
	vout->hSource= NULL;

	up(&vout->devLock);
	return 0;
}

/**
 * stmvout_plane_connect_to_src() - get source and connect to plane
 */
static int stmvout_plane_connect_to_src(stvout_device_t *vout,
					stm_display_source_interface_params_t *src_param,
					u32 remote_source_type)
{
	int ret = 0;

	if(down_interruptible(&vout->devLock))
		return -ERESTARTSYS;

	/*
	 * Allocate once the source info
	 */
	if (!vout->source_info) {
		vout->source_info = kzalloc(sizeof(*vout->source_info), GFP_KERNEL);
		if(!vout->source_info){
			pr_err("%s(): out of memory for source info\n", __func__);
			ret = -ENOMEM;
			goto connect_done;
		}
	}

	ret = stmvout_get_iface_source(vout, src_param, vout->source_info);
	if(ret) {
		pr_err("%s(): failed to get source/connect to plane\n", __func__);
		goto connect_done;
	}

	vout->hSource = vout->source_info->source;
	vout->hQueueInterface = vout->source_info->iface_handle;

connect_done:
	up(&vout->devLock);
	return ret;
}

/*
 * plane_config_for_compo_capture: Helper function which configure the
 * plane properly when being used for tunneled compo capture
 */
static int plane_config_for_compo_capture(struct _stvout_device *stvout_device)
{
	stm_display_output_h hOutput;
	stm_display_mode_t mode_line = { STM_TIMING_MODE_RESERVED };
	stm_rect_t o_win;
	int ret;

	/* Set the INPUT mode to manual */
	ret = stm_display_plane_set_control(stvout_device->hPlane,
				    PLANE_CTRL_INPUT_WINDOW_MODE,
				    MANUAL_MODE);
	if (ret){
		printk(KERN_ERR "%s: failed to set input mode (%d)\n",
				__func__, ret);
		goto failed;
	}

	/* Get the output resolution and set the Plane output window */
	hOutput = stmvout_get_output_display_handle( stvout_device );
	if (!hOutput){
		printk(KERN_ERR "%s: failed to get handle\n",
				__func__);
		goto failed;
	}

	ret = stm_display_output_get_current_display_mode( hOutput,
						&mode_line);
	if (ret){
		printk(KERN_ERR "%s: failed to get mode (%d)\n",
				__func__, ret);
		goto failed;
	}

	o_win.x = o_win.y = 0;
	o_win.width = mode_line.mode_params.active_area_width;
	o_win.height = mode_line.mode_params.active_area_height;
	ret = stm_display_plane_set_compound_control(
					stvout_device->hPlane,
					PLANE_CTRL_OUTPUT_WINDOW_VALUE,
					&o_win);
	if (ret){
		printk(KERN_ERR "%s: failed to set output win (%d)\n",
				__func__, ret);
		goto failed;
	}

	ret = stm_display_plane_set_control(stvout_device->hPlane,
				    PLANE_CTRL_OUTPUT_WINDOW_MODE,
				    MANUAL_MODE);
	if (ret){
		printk(KERN_ERR "%s: failed to set output mode (%d)\n",
				__func__, ret);
		goto failed;
	}

	return 0;

failed:
	return ret;
}

/**********************************************************************
* 1. Store plane handle to be used by video decoder
* 2. Update media-ctl link in case if decoder side of enable call
*    has already disconnected the any source previously connected to
*    plane.
**********************************************************************/
static int plane_media_sink_link_setup(struct media_entity *entity,
					 const struct media_pad *local,
					 const struct media_pad *remote,
					 u32 flags)
{
	int ret = 0;
	struct _stvout_device *stvout_device;

	stvout_device = plane_entity_to_stvout_device(entity);

	/*
	 * Close display device when closing the connection
	 */
	if (!(flags & MEDIA_LNK_FL_ENABLED)) {
		pr_debug("%s(): %s XXX %s\n", __func__,
				entity->name, remote->entity->name);
		goto setup_done;
	}

	if((stvout_device->display_device_id != -1) && !stvout_device->hDisplay) {
		if(stm_display_open_device(stvout_device->display_device_id, &stvout_device->hDisplay) < 0)
			return -ENODEV;
	}

	/*
	 * Video decoders and stmvout
	 */
	switch (remote->entity->type) {
	case MEDIA_ENT_T_DVB_SUBDEV_VIDEO_DECODER:
	case MEDIA_ENT_T_V4L2_SUBDEV_VIDEO_DECODER:
	default:
	{
		struct media_pad *temp_pad=NULL;
		struct media_link *link = NULL;

		pr_debug("%s(): %s -> %s\n", __func__,
				entity->name, remote->entity->name);

		/*
		 * If this plane is connected to another decoder, we
		 * disable it's link as at stkpi level it is already gone
		 */
		temp_pad = media_entity_remote_source((struct media_pad *)local);
		if (!temp_pad)
			break;

		link = media_entity_find_link(temp_pad, &entity->pads[0]);

		if (link && (link->flags & MEDIA_LNK_FL_ENABLED)) {
			link->flags = 0;
			link->reverse->flags = link->flags;
		}
		break;
	}
	}

	if(stvout_device->hDisplay) {
		stm_display_device_close(stvout_device->hDisplay);
		stvout_device->hDisplay = NULL;
	}


setup_done:
	return ret;
}

static int stmedia_plane_media_link_setup(struct media_entity *entity,
					  const struct media_pad *local,
					  const struct media_pad *remote,
					  u32 flags)
{
	if (local->flags == MEDIA_PAD_FL_SOURCE)
		return plane_media_source_link_setup(entity,
						     local,
						     remote,
						     flags);
	else
		return plane_media_sink_link_setup(entity,
					local, remote, flags);

}

static const struct media_entity_operations stmvout_plane_media_ops = {
	.link_setup = stmedia_plane_media_link_setup,
};

/* V4L2 sub-device operations include sub-device core operation
 * and media PAD operations*/
static const struct v4l2_subdev_ops stmedia_plane_ops = {
	.core      = &stmvout_core_ops,
	.pad	   = &stmedia_plane_pad_ops,
	.video     = &stmedia_plane_video_ops,
};

/*
 * Search for a free slot in g_voutData table
 * Assumption: a slot which has a null name is free!
 * Return the index or -1 in case of overflow.
 */
static inline int stmvout_get_free_plane(void)
{
	int i;
	for (i = 0; i < STMEDIA_MAX_PLANES; i++) {
		if (g_voutRoot.pDev[i].name[0] == '\0') {
			/* ... just to be sure, not mandatory */
			memset(&g_voutRoot.pDev[i], '\0',
			       sizeof(g_voutRoot.pDev[i]));
			g_voutRoot.pDev[i].display_device_id = -1;
			return (i);
		}
	}
	return (-1);
}

/*
 * Search for shared planes
 * Scans the entire g_voutData table to search a plane with "name"
 * already exists. Search only different Display.
 * Returns the intdex of first Shared (if any), returns 0 if not found
 */
static int stmvout_plane_already_added(const int display_device_id,
				       const char *name)
{
	int i;
	for (i = 0; i < STMEDIA_MAX_PLANES; i++) {
		if (g_voutRoot.pDev[i].display_device_id == display_device_id)
			if (strncmp
			    (g_voutRoot.pDev[i].name, name,
			     sizeof(g_voutRoot.pDev[i].name)) == 0)
				return (i);
	}
	return (0);
}

static void stmvout_show_planes(void)
{
#ifdef DEBUG_SHOW_PLANES
	int i;
	printk(" %s:%s =====  MC plane table ======\n", PKMOD, __func__);
	printk("  (0xIn, 0xOut) represent MC PAD flag for Input Output\n");
	printk("  disp.H stand for Display handle; S for subdevice addr.\n");
	for (i = 0; i < STMEDIA_MAX_PLANES; i++) {
		if (g_voutRoot.pDev[i].name[0] == '\0')
			break;	/* end of table */
		printk(" [%2d] %14s:%d type 0x%x disp.H %p S %p (%lx,%lx)\n",
		       i, g_voutRoot.pDev[i].name, g_voutRoot.pDev[i].id,
		       g_voutRoot.pDev[i].type,
		       g_voutRoot.pDev[i].hPlane, &g_voutRoot.pDev[i].stmedia_sdev.sdev,
		       g_voutRoot.pDev[i].pads[0].flags,
		       g_voutRoot.pDev[i].pads[1].flags);
	}
#endif
}

/*
 * Configure a OUTPUT plane:
 * initialize & register a V4L2 subdev and media controller entity
 * Return 0 if ok, not zero in all other cases.
 */
static int stmvout_add_plane_subdev(int Plane,
				    const char *name,
				    stm_display_plane_h hPlane,
				    unsigned int type,
				    const int display_device_id)
{
	int slot, ret = 0;	/* default is ok */
	char *msg = "???";

	struct v4l2_subdev *sd = NULL;
	struct media_entity *me = NULL;
	struct media_pad *pad = NULL;

	debug_mc("%s ========== plane %d %s\n", __func__, Plane, name);

	if ((slot = stmvout_get_free_plane()) == -1) {
		msg = "g_voutRoot.pDev table OVERFLOW";
		ret = -255;	/* arbitrary */
		goto drop_plane;
	}

	/* there is an available slot! ... go ahead */
	sd = &g_voutRoot.pDev[slot].stmedia_sdev.sdev;
	me = &sd->entity;
	pad = g_voutRoot.pDev[slot].pads;

	/* save all useful references to Display */
	g_voutRoot.pDev[slot].display_device_id = display_device_id;
	strlcpy(g_voutRoot.pDev[slot].name, name,
		sizeof(g_voutRoot.pDev[slot].name));
	g_voutRoot.pDev[slot].id = Plane;	/* TODO: useful ? */
	g_voutRoot.pDev[slot].hPlane = hPlane;
	g_voutRoot.pDev[slot].stmedia_sdev.stm_obj = hPlane;
	g_voutRoot.pDev[slot].type = type;	/* video or graphic plane */
	g_voutRoot.pDev[slot].source_info = NULL;

	init_waitqueue_head(&g_voutRoot.pDev[slot].wqBufDQueue);
	init_waitqueue_head(&g_voutRoot.pDev[slot].wqStreamingState);
	sema_init(&g_voutRoot.pDev[slot].devLock, 1);
	sema_init(&g_voutRoot.pDev[slot].listener_sem, 0);

	/* Set plane initially to opaque, and do not connect plane
	 * to output, this will happen later  */
	g_voutRoot.pDev[slot].globalAlpha = 255;	/* Initially set to opaque */
	g_voutRoot.pDev[slot].alpha_control = CONTROL_OFF;
	g_voutRoot.pDev[slot].currentOutputNum = -1;

	/* initialize output plane sub-device:
	 *    - name =  keep Display plane names
	 *    - sub-device private data --> back to this output object
	 */
	v4l2_subdev_init(sd, &stmedia_plane_ops);
	strlcpy(sd->name, g_voutRoot.pDev[slot].name, sizeof(sd->name));
	v4l2_set_subdevdata(sd, &g_voutRoot.pDev[slot]);
	sd->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE;

	/* Prepare PADs (output planes have two pads: a SINK and a SOURCE) */
	g_voutRoot.pDev[slot].input_pads = 1;
	g_voutRoot.pDev[slot].output_pads = 1;
	pad[0].flags = MEDIA_PAD_FL_SINK;
	pad[1].flags = MEDIA_PAD_FL_SOURCE;

	/* assume NO extra link initializing the entity */
	debug_mc("    [%d] adding entity %p pads %d (extra %d)\n",
		 slot, me,
		 (g_voutRoot.pDev[slot].input_pads +
		  g_voutRoot.pDev[slot].output_pads), 0);

	ret = media_entity_init(me,
				(g_voutRoot.pDev[slot].input_pads +
				 g_voutRoot.pDev[slot].output_pads), pad, 0);
	if (ret < 0) {
		msg = "initializing output sub-device";
		goto drop_plane;
	}

	/* complete the entity setup */
	me->ops = &stmvout_plane_media_ops;
	if (type == STMEDIA_PLANE_TYPE_VIDEO) {
		me->type = MEDIA_ENT_T_V4L2_SUBDEV_PLANE_VIDEO;
	} else if (type == STMEDIA_PLANE_TYPE_GRAPHICS) {
		me->type = MEDIA_ENT_T_V4L2_SUBDEV_PLANE_GRAPHIC;
	} else {
		me->type = MEDIA_ENT_T_V4L2_SUBDEV_PLANE_VBI;
	}

	/* Initialize the control handler*/
	ret = stvmout_init_ctrl_handler(&g_voutRoot.pDev[slot]);
	if (ret < 0) {
		msg = "Control handler init failed";
		goto drop_plane;
	}

	sd->ctrl_handler = &g_voutRoot.pDev[slot].ctrl_handler;
	/* Register the sub-device  */
	ret = stm_media_register_v4l2_subdev(sd);
	if (ret < 0) {
		msg = "registering output sub-device";
		goto drop_plane;
	}

	debug_mc("MC: [%d] + %s/%d CD (display %d, plane %p) pads I/O %d/%d\n",
		 slot, g_voutRoot.pDev[slot].name, g_voutRoot.pDev[slot].id,
		 display_device_id, g_voutRoot.pDev[slot].hPlane,
		 g_voutRoot.pDev[slot].input_pads,
		 g_voutRoot.pDev[slot].output_pads);

	return (0);		/* ok */

drop_plane:
	/* this is signaled as an error, however the initialization skips the
	 * entity and attempts to go ahead!
	 * If not overflow, erase the g_voutData slot content!
	 */
	printk(KERN_ERR "output %s: MC failure - %s (%d)\n", name, msg, ret);
	if (slot != -1) {
		memset(&g_voutRoot.pDev[slot], '\0', sizeof(g_voutRoot.pDev[slot]));
		g_voutPlug[slot].display_device_id = -1;
	}
	return (ret);
}

/**
 * stmvout_remove_plane_subdev
 * This removes the sub-device from the v4l2-device and cleans up
 * the registration with media entity driver
 */
static void stmvout_remove_plane_subdev(int count)
{
	struct v4l2_subdev *sd = &g_voutRoot.pDev[count].stmedia_sdev.sdev;
	struct media_entity *me =  &sd->entity;

	v4l2_ctrl_handler_free(&g_voutRoot.pDev[count].ctrl_handler);

	stm_media_unregister_v4l2_subdev(sd);
	media_entity_cleanup(me);

	/* Reset the gvoutData particular device context */
	memset(&g_voutRoot.pDev[count], 0, sizeof(g_voutRoot.pDev[count]));
	g_voutRoot.pDev[count].display_device_id = -1;
}

/**
 * stmvout_probe_media_planes
 * This function probes for the display planes
 */

static int stmvout_probe_media_planes(void)
{
	stm_display_device_h hDisplay = NULL;
	stm_display_plane_h hPlane = NULL;
	int display_device_id, Plane, SkippedPlanes = 0, ret = 0;

	debug_mc("==== %s executing!\n", __func__);
	/* scan all existing displays...
	 * There should be only one! ... but let we ignore this limit
	 * since harmless for MC logic
	 */

	for (display_device_id = 0;; display_device_id++)
		/* while (true) */
	{
		ret = stm_display_open_device(display_device_id, &hDisplay);
		if (ret) {
			if (display_device_id == 0) {
				printk(KERN_ERR
				       "%s: SoC without Display !?!?\n",
				       __func__);
				ret = -EFAULT;
				break;
			} else {
				/* assume no more display, just exit from loop */
				ret = 0;
				break;
			}
		}

		/* Search for all planes of a Display, these will be part of MC graph.
		 * - Get plane capabilities, name and type.
		 * - Use name extracted from Display as MC entity and sub-device name.
		 * - A display without planes is accepted
		 * - Valid planes are kept opened
		 * - failure of "open plane" mean the Display has no more planes
		 * - failure of any other "plane operation" just skip the plane
		 */
		for (Plane = 0;; Plane++) {
			stm_plane_capabilities_t plane_caps;
			unsigned int type;
			const char *name = "";
			char *msg = "???";

			/* if the open fails, assumes no more planes! */
			if (stm_display_device_open_plane
			    (hDisplay, Plane, &hPlane))
				break;

			if (stm_display_plane_get_name(hPlane, &name)) {
				msg = "has no name";
				goto skip_plane;
			}

			if (stm_display_plane_get_capabilities
			    (hPlane, &plane_caps)) {
				msg = "has no capabilities";
				goto skip_plane;
			}

			if (plane_caps & PLANE_CAPS_VIDEO)
				type = STMEDIA_PLANE_TYPE_VIDEO;
			else if (plane_caps & PLANE_CAPS_GRAPHICS)
				type = STMEDIA_PLANE_TYPE_GRAPHICS;
			else if (plane_caps & PLANE_CAPS_VBI)
				type = STMEDIA_PLANE_TYPE_VBI;
			else {
				msg = "nor graphic nor video nor vbi type";
				goto skip_plane;
			}

			/* record the plane, this may fail:
			 *   - because of plane table overflow
			 *   - because a V4L registering primitive fails
			 * in any case skip the plane and try to continue!
			 */

			/* Search first if this plane has already been added, in
			 * principle this should never happen!
			 * The assumption is that different displays do not share
			 * planes, moreover plane names are always different
			 * Likely no longer useful with the current Display model
			 * while the old Display had the concept of shared planes
			 * among outputs.
			 */
			if ((ret = stmvout_plane_already_added(display_device_id, name))) {
				msg = "already added to MC";
				goto skip_plane;
			}

			if (stmvout_add_plane_subdev
			    (Plane, name, hPlane, type, display_device_id)) {
				msg = "V4L2 registration failed";
				goto skip_plane;
			}

			continue;

skip_plane:
			/* close "bad" planes */
			SkippedPlanes++;
			stm_display_plane_close(hPlane);
			printk(KERN_WARNING "plane (%d) %s skipped: %s! (%d)\n",
			       Plane, name, msg, ret);
		}

		printk(KERN_INFO "%s: Display %d  found %d planes (skipped %d)\n",
		       __func__, display_device_id, (Plane-SkippedPlanes), SkippedPlanes);

		stm_display_device_close(hDisplay);
	}

	stmvout_show_planes();	/* debug only */

	return (ret);
}

/**
 * stmvout_release_media_planes
 * This function closes the probed media planes. The probing
 * is managed by stmvout_probe_media_planes.
 */
static void stmvout_release_media_planes(void)
{
	int index, count;
	int display_device_id;
	stm_display_plane_h hPlane;
	stm_display_device_h hDevice;

	/* Get the display device and close all the display planes associated with it */
	for (index = 0; index < STMEDIA_MAX_PLANES; index++) {
		if (g_voutRoot.pDev[index].display_device_id < 0)
			continue;

		/*
		 * Open the Display device
		 */
		if(!g_voutRoot.pDev[index].hDisplay) {
			if(stm_display_open_device(g_voutRoot.pDev[index].display_device_id, &g_voutRoot.pDev[index].hDisplay)) {
				printk(KERN_ERR "%s: failed to open display device\n",
						__func__);
				return;
			}
		}
		hDevice = g_voutRoot.pDev[index].hDisplay;

		/* Find all the planes of this particular display device */
		display_device_id = g_voutRoot.pDev[index].display_device_id;
		for (count = index; count < STMEDIA_MAX_PLANES; count++) {
			if (g_voutRoot.pDev[count].display_device_id != display_device_id)
				continue;

			if(g_voutRoot.pDev[count].hSource){
				stmvout_plane_disconnect_from_src(&(g_voutRoot.pDev[count]));
			}

			hPlane = g_voutRoot.pDev[count].hPlane;
			stmvout_remove_plane_subdev(count);
			stm_display_plane_close(hPlane);
		}

		/*
		 * Close the display device
		 */
		stm_display_device_close(hDevice);
	}
}


/* =======================================================================
 * V4l2 output Video device (root  as /dev/video1)
 */

/*
 * It should exist a set of objects (one per /dev/video1 PAD) shared among
 * all the users of /dev/video1.  The object is feed when the user perform
 * VIDIO SET OUTPUT, at that point it know the PAD to work with.
 * It has also to keep the count of all users sharing that PAD
 */
static int stm_v4l2_video_output_open(struct file *file)
{
	struct video_device *viddev = &g_voutRoot.video1;
	struct stm_v4l2_vout_fh *vout_fh;

	/* Allocate memory */
	vout_fh = kzalloc(sizeof(struct stm_v4l2_vout_fh), GFP_KERNEL);
	if (NULL == vout_fh) {
		printk("%s: nomem on v4l2 open\n", __FUNCTION__);
		return -ENOMEM;
	}
	v4l2_fh_init(&vout_fh->fh, viddev);
	file->private_data = &vout_fh->fh;
	vout_fh->pOpenData = NULL;
	v4l2_fh_add(&vout_fh->fh);
	debug_mc("%s (file %p)\n", __func__, file);
	return 0;
}

/*
 * These are V4L2 vout ioctl ops
 */
struct v4l2_ioctl_ops stm_v4l2_video_output_ioctl_ops = {
	.vidioc_querycap = stmvout_vidioc_querycap,
	.vidioc_enum_fmt_vid_out = stmvout_vidioc_enum_fmt_vid_out,
	.vidioc_g_fmt_vid_out = stmvout_vidioc_g_fmt_vid_out,
	.vidioc_s_fmt_vid_out = stmvout_vidioc_s_fmt_vid_out,
	.vidioc_try_fmt_vid_out = stmvout_vidioc_try_fmt_vid_out,
	.vidioc_cropcap = stmvout_vidioc_cropcap,
	.vidioc_g_crop = stmvout_vidioc_g_crop,
	.vidioc_s_crop = stmvout_vidioc_s_crop,
	.vidioc_g_parm = stmvout_vidioc_g_parm,
	.vidioc_reqbufs = stmvout_vidioc_reqbufs,
	.vidioc_querybuf = stmvout_vidioc_querybuf,
	.vidioc_qbuf = stmvout_vidioc_qbuf,
	.vidioc_dqbuf = stmvout_vidioc_dqbuf,
	.vidioc_streamon = stmvout_vidioc_streamon,
	.vidioc_streamoff = stmvout_vidioc_streamoff,
	.vidioc_g_output = stmvout_vidioc_g_output,
	.vidioc_s_output = stmvout_vidioc_s_output,
	.vidioc_enum_output = stmvout_vidioc_enum_output,
	.vidioc_default = stmvout_vidioc_private_ioctl,
};

static struct v4l2_file_operations stm_v4l2_video_output_fops = {
	.owner = THIS_MODULE,
	.read = NULL,
	.write = NULL,
	.poll = stmvout_poll,
	.ioctl = NULL,
	.unlocked_ioctl = video_ioctl2,
	.mmap = stmvout_mmap,	/* stm_v4l2_video_output_mmap, */
	.open = stm_v4l2_video_output_open,
	.release = stmvout_close,
};

/**
 * stm_v4l2_video_output_media_link_setup() - link setup for video output
 */
static int stm_v4l2_video_output_media_link_setup(struct media_entity *entity,
						  const struct media_pad *local,
						  const struct media_pad
						  *remote, u32 flags)
{
	int ret;
	struct _stvout_device *stvout_device;
	struct media_pad *temp_pad=NULL;
	stm_display_source_interface_params_t src_param = {0};
	stm_display_pixel_stream_params_t  pix_params = {0};

	stvout_device = plane_entity_to_stvout_device(remote->entity);

	if((stvout_device->display_device_id != -1) && !stvout_device->hDisplay) {
		if(stm_display_open_device(stvout_device->display_device_id, &stvout_device->hDisplay) < 0)
			return -ENODEV;
	}


	if (!(flags & MEDIA_LNK_FL_ENABLED)) {
		ret = stmvout_plane_disconnect_from_src(stvout_device);
		goto exit;
	}

	/*
	 * If the remote entity is already connected to any source,
	 * we should  reject connection of stmvout to that entity.
	 */
	temp_pad = media_entity_remote_source((struct media_pad *)remote);
	if (temp_pad) {
		ret = -EBUSY;
		goto exit;
	}

	/* Do some specific initialization of the plane */
	if (entity->type == MEDIA_ENT_T_V4L2_SUBDEV_OUTPUT) {
		ret = plane_config_for_compo_capture(stvout_device);
		if (ret)
			goto exit;
	}

	src_param.interface_params.pixel_stream_params = &pix_params;
	src_param.interface_type = STM_SOURCE_QUEUE_IFACE;
	ret = stmvout_plane_connect_to_src(stvout_device, &src_param, entity->type);

	debug_msg("Plane link setup: Plane %s is connected to source %p\n", remote->entity->name, stvout_device->stmedia_sdev.stm_obj);
exit:
	if(stvout_device->hDisplay) {
		stm_display_device_close(stvout_device->hDisplay);
		stvout_device->hDisplay = NULL;
	}

	return ret;
}

static const struct media_entity_operations stm_v4l2_video_output_entity_ops = {
	.link_setup = stm_v4l2_video_output_media_link_setup,
};

static void stm_v4l2_video_output_release(struct video_device *outvid)
{
	/* Nothing to do, but need by V4L2 stack */
}

static int stmvout_create_video_output_device(void)
{
	int i, ret = 0;
	struct media_pad *pads = NULL;

	for (i = 0; i < STMEDIA_MAX_PLANES; i++)
		INIT_LIST_HEAD(&g_voutRoot.pDev[i].openData.open_list);

	/* Initialize output video device (/dev/video1) */
	strncpy(g_voutRoot.video1.name, "STM Video Output",
		sizeof(g_voutRoot.video1.name));
	g_voutRoot.video1.fops = &stm_v4l2_video_output_fops;
	g_voutRoot.video1.minor = -1;
	g_voutRoot.video1.release = stm_v4l2_video_output_release;
	g_voutRoot.video1.entity.ops = &stm_v4l2_video_output_entity_ops;
	g_voutRoot.video1.ioctl_ops = &stm_v4l2_video_output_ioctl_ops;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3, 7, 0))
	g_voutRoot.video1.vfl_dir = VFL_DIR_TX;
#endif

	/* for each plane in g_voutData (which has a SINK pad) creates a
	 * a corresponding SOURCE pad of video output device
	 */
	pads = g_voutRoot.pads;
	for (i = 0; i < STMEDIA_MAX_PLANES; i++) {
		/* consider a null name as end of plane table */
		if (g_voutRoot.pDev[i].name[0] == '\0')
			break;
		pads[i].flags = MEDIA_PAD_FL_SOURCE;
	}

	/* initialize the /dev/video1 entity
	 * "i" is the number of PADs
	 */
	ret =
	    media_entity_init(&g_voutRoot.video1.entity, i, g_voutRoot.pads, 0);
	if (ret < 0) {
		printk(KERN_ERR
		       "%s: failed to initialize the video output driver (%d)\n",
		       __func__, ret);
		goto end;
	}
	/* In our device model, Video output is /dev/video1 */
	ret = stm_media_register_v4l2_video_device(&g_voutRoot.video1,
						   VFL_TYPE_GRABBER, 1);
	if (ret < 0)
		printk(KERN_ERR
		       "%s: failed to register the video output driver (%d)\n",
		       __func__, ret);

end:
	return (ret);
}

/**
 * stmvout_remove_video_output_device
 * This unregisters the ST V4L2 video device.
 */

static void stmvout_remove_video_output_device(void)
{
	stm_media_unregister_v4l2_video_device(&g_voutRoot.video1);

	media_entity_cleanup(&g_voutRoot.video1.entity);

	memset(&g_voutRoot.video1, 0, sizeof(g_voutRoot.video1));
}

/* =======================================================================
 * V4l2 output Creation of Media Controller Links:
 *    - from planes to output plugs
 *    - from video device (/dev/video1) to Planes
 */
static void stmvout_link_plane_to_all_outputs(stm_display_plane_h hPlane,
					      struct media_entity *ePlane,
					      char *name)
{

	int output, ret, display_disconnect;
	struct media_entity *eOutput = NULL;
	stm_display_output_h hOutput;

	for (output = 0; output < STMEDIA_MAX_OUTPUTS; output++) {
		display_disconnect = true;

		/* assume end of Output set if output handle is NULL */
		hOutput = g_voutPlug[output].hOutput;
		if (!hOutput)
			break;

		/* try to connect the plane with the output, if Display
		 * allows it, create the disabled MC link
		 */
		ret = stm_display_plane_connect_to_output(hPlane, hOutput);
		switch (ret) {
		case (-EALREADY):	/* assume this is GDP0 -> FB, link as well */
		case (-EBUSY):	/* assume this is a Shared FB plane, link as well */
			display_disconnect = false;
		case 0:
			break;
		default:
			continue;	/* assume this link is not allowed, continue */
		}
		/* get the MC output entity */
		eOutput = &g_voutPlug[output].sdev.entity;

		/* create the media link */
		ret = media_entity_create_link(ePlane, SOURCE_PAD_INDEX,
					       eOutput, SINK_PAD_INDEX, 0);
		if (ret < 0) {
			/* Most likely that's an internal error */
			printk(KERN_ERR "%s: failed to create link (%d)\n",
			       __func__, ret);
			BUG();
		}

		/* disconnect the plane, restore Display state */
		if (display_disconnect)
			ret =
			    stm_display_plane_disconnect_from_output(hPlane,
								     hOutput);
			if (ret < 0)
				printk(KERN_ERR "%s: failed to disconnect plane from output (%d)\n",
			       __func__, ret);
	}
}

static int stmvout_create_video_output_links(void)
{
	int plane, ret = 0;
	unsigned int flags = 0;

	/* Step1: link planes SRC PAD to output plugs SINK PAD:
	 *     Let the Display HW layer to drive this, attempt to connect
	 *     each plane with each output, if the Display allows it than
	 *     creates a DISABLED Media Controller link.
	 */
	for (plane = 0; plane < STMEDIA_MAX_PLANES; plane++) {
		stm_display_plane_h hPlane;
		struct media_entity *ePlane = NULL;
		if ((hPlane = g_voutRoot.pDev[plane].hPlane)) {
			ePlane = &g_voutRoot.pDev[plane].stmedia_sdev.sdev.entity;
			stmvout_link_plane_to_all_outputs(hPlane, ePlane,
							  g_voutRoot.
							  pDev[plane].name);
		}
	}

	/* Step2: links all planes SINK PADs  with output video device
	 *        (/dev/video1) SOURCE PADs
	 */
	for (plane = 0; plane < STMEDIA_MAX_PLANES; plane++) {
		struct media_entity *src, *ePlane = NULL;
		src = &g_voutRoot.video1.entity;	/* /dev/video1 */

		/* if the plane exists ... */
		if (g_voutRoot.pDev[plane].hPlane) {
			ePlane = &g_voutRoot.pDev[plane].stmedia_sdev.sdev.entity;
			ret =
			    media_entity_create_link(src, plane, ePlane, 0, flags);
			if (ret < 0) {
				/* Most likely that's an internal error */
				printk(KERN_ERR
				       "%s: failed to create link (%d)\n",
				       __func__, ret);
				BUG();
			}
		}
	}

	return (ret);
}

static int stmvout_create_compo_link(void)
{
	int output, ret = 0;
	struct media_entity *eOutput = NULL;
	struct media_entity *sink;

	/* Search for the Main compo */
	for (output = 0; output < STMEDIA_MAX_OUTPUTS; output++) {
		if (g_voutPlug[output].display_device_id < 0)
			continue;

		/* FIXME - shouldn't rely on string compare */
		if (!strcmp(g_voutPlug[output].name, "analog_hdout0")){
			eOutput = &g_voutPlug[output].sdev.entity;
			break;
		}
	}

	if (!eOutput) {
		/* That shouldn't happen, we always have a main compo */
		printk(KERN_ERR "Couldn't find the main compo\n");
		return -1;
	}

	/* Create a link between the Main compo and all GDP planes */
	sink = stm_media_find_entity_with_type_first( MEDIA_ENT_T_V4L2_SUBDEV_PLANE_GRAPHIC);
	while (sink) {
		ret = media_entity_create_link(eOutput, 1, sink, 0, 0);
		if (ret < 0) {
			printk(KERN_ERR "failed output->plane link\n");
			/* FIXME ... */
			return -1;
		}

		sink = stm_media_find_entity_with_type_next( sink, MEDIA_ENT_T_V4L2_SUBDEV_PLANE_GRAPHIC);
	}

	/* Create a link between the Main compo and all VID planes */
	sink = stm_media_find_entity_with_type_first( MEDIA_ENT_T_V4L2_SUBDEV_PLANE_VIDEO);
	while (sink) {
		ret = media_entity_create_link(eOutput, 1, sink, 0, 0);
		if (ret < 0) {
			printk(KERN_ERR "failed output->plane link\n");
			/* FIXME ... */
			return -1;
		}

		sink = stm_media_find_entity_with_type_next( sink, MEDIA_ENT_T_V4L2_SUBDEV_PLANE_VIDEO);
	}

	return (ret);
}

/**
 * stmvout_remove_compo_capture_links
 * This function check the compo capture link and disable it if needed
 */

static void stmvout_remove_compo_capture_links(void)
{
	int count;
	struct media_entity * entity = NULL;
	struct media_pad *local, *remote;
	unsigned int id = 0;

	for (count = 0; count < STMEDIA_MAX_OUTPUTS; count++) {
		if (g_voutPlug[count].hOutput == NULL)
			continue;

		if (!strcmp(g_voutPlug[count].name, "analog_hdout0")){
			entity = &g_voutPlug[count].sdev.entity;
			local = &entity->pads[SOURCE_PAD_INDEX];
			break;
		}
	}

	if (!entity)
		/* That shouldn't happen - nothing to do */
		return;

	remote = stm_media_find_remote_pad(local, MEDIA_LNK_FL_ENABLED, &id);
	if (!remote)
		/* Not connected - nothing to do */
		return;

	/* Fire the media controller link detach */
	media_entity_call(entity, link_setup,
				remote, local, 0);
}

/**
 * stmvout_remove_video_output_links
 * This function removes
 */

static void __init stmvout_reset_video_display_devices(void)
{
	int count;

	for (count = 0; count < STMEDIA_MAX_OUTPUTS; count++) {
		g_voutPlug[count].hDisplay = NULL;
		g_voutPlug[count].display_device_id = -1;
	}
	for (count = 0; count < STMEDIA_MAX_PLANES; count ++) {
		g_voutRoot.pDev[count].hDisplay = NULL;
		g_voutRoot.pDev[count].display_device_id = -1;
	}
}

static int __init stmvout_probe_display_devices(void)
{
	int index, count;

	for (index = 0; index < STMEDIA_MAX_PLANES; index ++) {
		if((g_voutRoot.pDev[index].display_device_id != -1) && !g_voutRoot.pDev[index].hDisplay) {
			if(stm_display_open_device(g_voutRoot.pDev[index].display_device_id, &g_voutRoot.pDev[index].hDisplay) < 0) {
				g_voutRoot.pDev[index].hDisplay = NULL;
				return -ENODEV;
			}
		}
		for (count = 0; count < STMEDIA_MAX_OUTPUTS; count++) {
			if(g_voutPlug[count].display_device_id == g_voutRoot.pDev[index].display_device_id)
				g_voutPlug[count].hDisplay = g_voutRoot.pDev[index].hDisplay;
		}
	}

	return 0;
}

static void __init stmvout_remove_video_display_devices(void)
{
	int index, count;

	for (index = 0; index < STMEDIA_MAX_PLANES; index ++) {
		if (g_voutRoot.pDev[index].hDisplay) {
			for (count = 0; count < STMEDIA_MAX_OUTPUTS; count++) {
				if(g_voutPlug[count].display_device_id == g_voutRoot.pDev[index].display_device_id)
					g_voutPlug[count].hDisplay = NULL;
			}
			stm_display_device_close(g_voutRoot.pDev[index].hDisplay);
			g_voutRoot.pDev[index].hDisplay = NULL;
		}
	}
}

stm_display_plane_h stmvout_get_plane_hdl_from_entity(struct media_entity *
						      entity)
{
	return plane_entity_to_stvout_device(entity)->hPlane;
}

EXPORT_SYMBOL(stmvout_get_plane_hdl_from_entity);

stm_display_output_h stmvout_get_output_hdl_from_connected_plane_entity(struct
									media_entity
									*
									entity)
{
	struct media_pad *downStream_pad = NULL;
	struct output_plug *outputPlug;

	downStream_pad =
	    media_entity_remote_source(&entity->pads[SOURCE_PAD_INDEX]);
	if (downStream_pad == NULL)
		return NULL;

	outputPlug = output_entity_to_output_plug(downStream_pad->entity);

	return (outputPlug->hOutput);
}

EXPORT_SYMBOL(stmvout_get_output_hdl_from_connected_plane_entity);

static void __exit stmvout_exit_mc(void)
{
	stmvout_remove_compo_capture_links();

	stmvout_remove_video_output_device();

	stmvout_release_media_outputs();

	stmvout_release_media_planes();
}

static int __init stmvout_init_mc(void)
{
	int ret;

	memset(&g_voutRoot, 0, sizeof(struct stm_v4l2_output_root));

	stmvout_reset_video_display_devices();

	if ((ret = stmvout_probe_media_planes()))
		goto exit;
	if ((ret = stmvout_probe_media_outputs()))
		goto exit;

	/* Open all required display devices for the current probe */
	if ((ret = stmvout_probe_display_devices()))
		goto exit;

	/* create the V4L2 output gate: /dev/video1 */
	if ((ret = stmvout_create_video_output_device()))
		goto exit;

	/* Now create all possible MC links, but none is ENABLED!
	 * Mean at the end of this initialization any output operation
	 * performed via /dev/video1 will succeed!
	 * The creation of "default output graph" (or customized graph)
	 * is thought to be in the scope of user application setup, most
	 * likely immediatilly after the module loading.
	 * The "output graph setup" uses the /dev/media device.
	 */

	ret = stmvout_create_video_output_links();
	if (ret)
		goto exit;

	ret = stmvout_create_compo_link();
	if (ret)
		goto exit;

exit:
	stmvout_remove_video_display_devices();

	return (ret);
}

module_init(stmvout_init_mc);
module_exit(stmvout_exit_mc);

MODULE_LICENSE("GPL");
