/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef __STMVBI_DRIVER_H
#define __STMVBI_DRIVER_H

#ifdef DEBUG
#define PKMOD "stmvbi: "
#define debug_msg(fmt,arg...) printk(PKMOD fmt,##arg)
#define err_msg(fmt,arg...) printk(PKMOD fmt,##arg)
#define info_msg(fmt,arg...) printk(PKMOD fmt,##arg)
#else
#define debug_msg(fmt,arg...)
#define err_msg(fmt,arg...)
#define info_msg(fmt,arg...)
#endif

struct stm_v4l2_queue {
	rwlock_t lock;
	struct list_head list;	/* of type struct _streaming_buffer */
};

typedef struct _streaming_buffer {
	struct list_head node;
	struct v4l2_buffer vbiBuf;
	struct stmvbi_dev *pDev;
	int useCount;
	int botFieldFirst;
	stm_display_metadata_t *topField;
	stm_display_metadata_t *botField;
} streaming_buffer_t;

#define STMVBI_MAX_OUTPUT_NB	2
struct stmvbi_dev {
	struct video_device viddev;
	struct media_pad *pads;

	struct stm_v4l2_queue pendingStreamQ;
	struct stm_v4l2_queue completeStreamQ;

	struct v4l2_format bufferFormat;

	struct file *owner;
	int isStreaming;

	stm_display_device_h hDevice;
	stm_display_output_h hOutput[STMVBI_MAX_OUTPUT_NB];
	const char *out_name[STMVBI_MAX_OUTPUT_NB];

	unsigned int output_id;
	stm_display_output_capabilities_t output_caps;
	unsigned int max_output_nb;

	wait_queue_head_t wqBufDQueue;
	wait_queue_head_t wqStreamingState;
	struct semaphore devLock;
};

static inline int stmvbi_has_queued_buffers(struct stmvbi_dev *pDev)
{
	return !list_empty(&pDev->pendingStreamQ.list);
}

static inline int stmvbi_has_completed_buffers(struct stmvbi_dev *pDev)
{
	return !list_empty(&pDev->completeStreamQ.list);
}

#endif /* __STMVOUT_DRIVER_H */
