/************************************************************************
Copyright (C) 2015 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * STM TS/PES marker creation declarations
************************************************************************/
#ifndef __STM_DVB_DEMUX_MARKER_H__
#define __STM_DVB_DEMUX_MARKER_H__

/* Market type definition */
/* Request for a time alarm (PTS based) from the stream player */
#define STM_MARKER_CMD_TIMING_REQUEST		0x00
/* Forward injection break */
#define STM_MARKER_BREAK_FORWARD		0x01
/* Backward injection break */
#define STM_MARKER_BREAK_BACKWARD		0x02
/* Backward injection break (allowing smooth reverse) */
#define STM_MARKER_BREAK_BACKWARD_SMOOTH_REVERSE	0x03
/* Break because of end of stream. */
#define STM_MARKER_BREAK_END_OF_STREAM		0x04

 /*
  * specify a PTS interval that is propagated inband with the data
  */
#define STM_MARKER_SET_PTS_INTERVAL              0x06

/* Additionnal tag "OR'ed" to previous markers */
/* Tag indicating the last fram is complete and is to be decoded */
#define STM_MARKER_ORedTAG_LAST_FRAME_COMPLETE	0x80


/* Marker size */
/* Size of basic MPEG2-TS packet. */
#define STM_MARKER_TS_SIZE	188
/* Size of basic MPEG2-PES packet. */
#define STM_MARKER_PES_SIZE	26
/* Maximum user data size. */
#define STM_MARKER_USER_DATA_SIZE	10

int stm_ts_marker_create(
	__u8	marker_type,
	void	*address,
	__u16	size,
	__u16	*marker_size,
	const __u16	pid,
	const __u8	*user_data,
	const __u8	user_data_size);

int stm_pes_marker_create(
	__u8	marker_type,
	void	*address,
	__u16	size,
	__u16	*marker_size,
	const __u8	*user_data,
	const __u8	user_data_size);
#endif
