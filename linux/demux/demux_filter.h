/************************************************************************
Copyright (C) 2011 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.

Source file name : demux_filter.h

Filters/Chains handling functions header

Date        Modification                                    Name
----        ------------                                    --------

 ************************************************************************/

#ifndef _DEMUX_FILTER_H
#define _DEMUX_FILTER_H

#include "stm_selector.h"
#include <dvb/dvb_adaptation.h>
#include <linux/dvb/stm_dmx.h>

#define	DEMUX_PCR_DETACH	0x0
#define	DEMUX_PCR_ATTACH	0x1
#define DEMUX_PCR_LOCKED	0x2

enum map_e {
	SECTION_MAP = 0,
	TS_MAP,
	PES_MAP,
	PCR_MAP,
	INDEX_MAP,
	MAX_MAP
};

enum chain_type {
	PES_CHAIN = 0,
	PCR_CHAIN,
	SECTION_CHAIN,
	INDEX_CHAIN
};

enum input_filter_type {
	INPUT_PID_FILTER = 0,
	INSERT_PID_FILTER,
	REPLACE_PID_FILTER
};

struct stm_dvb_filter_s {
	stm_te_object_h handle;
	int users;
	stm_memsink_h memsink_interface;
	stm_event_subscription_h evt_subs;
	stm_event_subscription_entry_t *evt_subs_entry;
	stm_object_h decoder;
	wait_queue_head_t wait;

	/* The last 3 fields are used to buffer data from the TE in some cases.
	 * This is necessary in case of index, since otherwise we can't know properly how much data to received
	 */
	char *buf_data;
	unsigned int buf_offset;
	unsigned int buf_len;
};

/*
 * Pid filter params required for its creation
 * @pid_filter: non-NULL if already created
 * @pid       : pid for the filter
 * @pid_type  : type of pid filter
 */
struct stm_demux_pid_filter_params {
	struct stm_dvb_filter_s *pid_filter;
	__u32 pid;
	__u32 pid_type;
};

/*
 * Pes filter params for its creation
 * @pes_filter: non-NULL if already created
 * @pes_output: dmx_output_t, output of pes filter
 * @pes_type  : type of pes filter
 */
struct stm_demux_pes_filter_params {
	struct stm_dvb_filter_s *pes_filter;
	__u32 pes_output;
	__u32 pes_map;
	__u32 pes_type;
};

/*
 * To be passed to demux_add_chain
 * @pid_params: pid filter params for its creation
 * @pes_params: pes filter params for its creation
 * @priv      : new chain private data
 */
struct stm_demux_chain_params {
	struct stm_demux_pid_filter_params pid_params;
	struct stm_demux_pes_filter_params pes_params;
	void *priv;
};

struct stm_dvb_filter_chain_s {
	struct list_head input_list;
	struct list_head output_list;
	struct list_head selector_list;
	struct stm_dvb_filter_s *input_filter;
	struct stm_dvb_filter_s *output_filter;
	stm_selector_h selector;
	stm_selector_pad_h pad;
	int pid;
#define CHAIN_STOPPED	0
#define CHAIN_STARTED	1
	int state;
	int type;
	void *filter;
};

void init_filter(struct stm_dvb_filter_s *filter);

struct stm_dvb_filter_chain_s *stm_demux_add_chain(struct stm_dvb_demux_s *stm_demux,
						struct stm_demux_chain_params *params);

int attach_sink_to_chain(struct stm_dvb_filter_chain_s *chain,
			 stm_object_h sink, char *memsink_name,
			 stm_data_mem_type_t address);

int stm_dmx_delete_chain(struct stm_dvb_demux_s *stm_demux,
				struct stm_dvb_filter_chain_s *chain);

int del_all_chains_from_demux(struct stm_dvb_demux_s *demux);
int del_filters_from_chain(struct stm_dvb_filter_chain_s *chain);

struct stm_dvb_filter_chain_s *get_primary_ts_chain(struct stm_dvb_demux_s
						    *demux);
struct stm_dvb_filter_s *get_pid_filter(struct stm_dvb_demux_s *demux, int pid);
struct stm_dvb_filter_chain_s *get_primary_ts_chain(struct stm_dvb_demux_s
						    *demux);
struct stm_dvb_filter_chain_s *match_filter_input_pid(struct stm_dvb_demux_s
						      *demux, int pid);
struct stm_dvb_filter_chain_s *match_filter_output_pid(struct stm_dvb_demux_s
						       *demux, int pid,
						       enum map_e);
struct stm_dvb_filter_chain_s *match_chain_from_filter(struct stm_dvb_demux_s
						       *demux, struct dmxdev_filter
						       *filter);
struct stm_dvb_filter_chain_s *match_chain_from_decoder(struct
							     stm_dvb_demux_s
							     *demux,
							     stm_object_h
							     decoder);
struct stm_dvb_filter_s *get_filter_pacing_output(struct stm_dvb_demux_s *demux);

struct stm_dvb_filter_chain_s *demux_get_pcr_by_type(struct stm_dvb_demux_s *stm_demux,
								dmx_pes_type_t pcr_type);
int stm_dvb_demux_setup_pcr(struct stm_dvb_filter_chain_s *pcr_chain,
			struct PlaybackDeviceContext_s *playback_ctx, u32 flags);

#if defined CONFIG_STLINUXTV_FRONTEND_DEMUX
extern int stm_dvb_demux_attach_decoder(struct stm_dvb_demux_s *stm_demux,
			stm_object_h pes_filter, stm_object_h play_stream);
extern int stm_dvb_demux_detach_decoder(struct stm_dvb_demux_s *stm_demux,
			stm_object_h pes_filter, stm_object_h play_stream);
extern int stm_dvb_demux_attach_pcr(struct stm_dvb_demux_s *stm_demux,
			struct PlaybackDeviceContext_s *playback_ctx);
extern int stm_dvb_demux_detach_pcr(struct stm_dvb_demux_s *stm_demux,
			struct PlaybackDeviceContext_s *playback_ctx);
#else
/*
 * These function are intended for hw demux, so, do nothing
 * for sw demux, but allow the calls to complete successfully
 */
static inline int stm_dvb_demux_attach_decoder(struct stm_dvb_demux_s *stm_demux,
			stm_object_h pes_filter, stm_object_h play_stream)
{
	return 0;
}

static inline int stm_dvb_demux_detach_decoder(struct stm_dvb_demux_s *stm_demux,
			stm_object_h pes_filter, stm_object_h play_stream)
{
	return 0;
}

static inline int stm_dvb_demux_attach_pcr(struct stm_dvb_demux_s *stm_demux,
			struct PlaybackDeviceContext_s *playback_ctx)
{
	return 0;
}

static inline int stm_dvb_demux_detach_pcr(struct stm_dvb_demux_s *stm_demux,
				struct PlaybackDeviceContext_s *playback_ctx)
{
	return 0;
}
#endif

int stm_dvb_chain_start( struct stm_dvb_filter_chain_s *chain );
int stm_dvb_chain_stop( struct stm_dvb_filter_chain_s *chain );
int stm_dvb_demux_set_control(struct stm_dvb_demux_s *stm_demux,
				struct dmxdev_filter *dmxdevfilter,
				struct dmx_ctrl *ctrl);
int stm_dvb_demux_get_control(struct stm_dvb_demux_s *stm_demux,
                struct dmxdev_filter *dmxdevfilter, struct dmx_ctrl *ctrl);
int demux_set_sel_mode(struct stm_dvb_filter_chain_s *chain, struct dmx_ctrl *ctrl);
int demux_attach_eos_memsrc(struct stm_dvb_demux_s *stm_demux,
		             bool nonblocking, stm_memsrc_h *memsrc);
int demux_detach_eos_memsrc(struct stm_dvb_demux_s *stm_demux,
		                       stm_memsrc_h memsrc);

#define ALLOC_FILTER(__p, _error_) 						\
	do {									\
		__p = kzalloc(sizeof(struct stm_dvb_filter_s), GFP_KERNEL); 	\
                if (!(__p)) { 							\
			result = -ENOMEM;					\
			goto _error_;						\
		}								\
		init_waitqueue_head(&__p->wait);				\
	} while (0)

#endif /* _DEMUX_FILTER_H */
