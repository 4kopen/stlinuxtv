/************************************************************************
Copyright (C) 2015 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * ST Demuxer controls implementation
************************************************************************/
#include <linux/types.h>
#include <linux/errno.h>

#include "demux_filter.h"
#include "demux_common.h"
#include "linux/dvb/stm_dmx.h"
#include "stm_dvb_demux_marker.h"
#include "stv_debug.h"

/**
 * demux_validate_ctrl() - check for supported controls
 */
static int demux_validate_ctrl(__u32 id)
{
	int ret = 0;

	switch (id) {
	case DMX_CTRL_PES_SELECTOR_MODE:
	case DMX_CTRL_PES_READ_IN_QUANTIZATION_UNITS:
	case DMX_CTRL_TS_INPUT_TYPE:
	case DMX_CTRL_PS_INPUT_SUB_STREAM_ID:
	case DMX_CTRL_OUTPUT_BUFFER_STATUS:
	case DMX_CTRL_SET_PTS_INTERVAL:
	case DMX_CTRL_TSPRIORITY_BIT_FILTERING:
		break;

	default:
		ret = -EINVAL;
	}

	return ret;
}

/**
 * demux_get_ts_input_type() - map stlinuxtv input type to dvb
 */
static __u32 demux_get_ts_input_type(__u32 val)
{
	__u32 te_val;

	switch (val) {
	case DMX_CTRL_VALUE_TS_AUTO:
		te_val = STM_TE_INPUT_TYPE_UNKNOWN;
		break;

	case DMX_CTRL_VALUE_TS_DVB:
		te_val = STM_TE_INPUT_TYPE_DVB;
		break;

	default:
		te_val = ~0;
		break;
	}

	return te_val;
}

/**
 * demux_get_priority_filtering_mode() - map stlinuxtv enums to linuxtv enums
 * for priority bit filtering.
 */
static __u32 demux_get_priority_filtering_mode(__u32 val)
{
       __u32 te_val = ~0;

       switch (val) {
       case DMX_CTRL_VALUE_TS_PRIORITY_0_FILTERING:
               te_val = STM_TE_TS_PRIORITY_0_FILTERING;
               break;

       case DMX_CTRL_VALUE_TS_PRIORITY_1_FILTERING:
               te_val = STM_TE_TS_PRIORITY_1_FILTERING;
               break;

       case DMX_CTRL_VALUE_TS_PRIORITY_IGNORE_FILTERING:
               te_val = STM_TE_TS_IGNORE_PRIORITY_FILTERING;
               break;

       default:
               te_val = ~0;
               break;
       }

       return te_val;
}

/**
 * demux_set_sel_mode() - set selector mode control
 */
int demux_set_sel_mode(struct stm_dvb_filter_chain_s *chain, struct dmx_ctrl *ctrl)
{
	int ret = 0;
	__u32 val;

	/*
	 * Set the control now
	 */
	stv_debug("Changing connect mode for %p to %d\n", chain, ctrl->value);

	if (get_video_dec_pes_type(ctrl->pes_type) >= 0) {
		val = ctrl->value;

		/*
		* If the ctrl is pass through, then, it's a control,
		* else we have a start code for caching to start
		*/
		if (val != DMX_CTRL_VALUE_PES_PASS_THROUGH_MODE
			&& val != DMX_CTRL_VALUE_PES_FORCE_PASS_THROUGH_MODE) {
			ret = stm_selector_pad_set_start_code(chain->pad,
					ctrl->start_code, sizeof(ctrl->start_code));
			if (ret) {
				pr_err("%s(): failed to set start code on filter\n", __func__);
				goto err;
			}
			val = DMX_CTRL_VALUE_PES_CACHED_MODE;
		}
		ret = stm_selector_pad_set_control(chain->pad, ctrl->id, val);
		if (ret) {
			stv_err("failed to set control %d to %d\n", ctrl->id, val);
			goto err;
		}
	} else if ((get_audio_dec_pes_type(ctrl->pes_type) >= 0) ||
			(get_pcr_pes_type(ctrl->pes_type) >= 0)) {

		ret = stm_selector_pad_set_control(chain->pad,
				ctrl->id, ctrl->value);
		if (ret) {
			pr_err("%s(): unable to set control for pad %p\n",
					__func__, chain->pad);
			goto err;
		}
	} else {
		pr_err("%s(): invalid pes_type for DMX_CTRL_PES_SELECTOR_MODE ctrl\n", __func__);
		ret = -EINVAL;
		goto err;
	}

	/*
	 * Change output filter quantisation for Audio or Video filter
	 */
	if ((get_video_dec_pes_type(ctrl->pes_type) >= 0 ||
			get_audio_dec_pes_type(ctrl->pes_type) >= 0)) {
		if (ctrl->value != DMX_CTRL_VALUE_PES_PASS_THROUGH_MODE
				&& ctrl->value != DMX_CTRL_VALUE_PES_FORCE_PASS_THROUGH_MODE) {
			ret = stm_te_filter_set_control(chain->output_filter->handle,
					STM_TE_OUTPUT_FILTER_CONTROL_READ_IN_QUANTISATION_UNITS,
					1);
		} else {
			ret = stm_te_filter_set_control(chain->output_filter->handle,
					STM_TE_OUTPUT_FILTER_CONTROL_READ_IN_QUANTISATION_UNITS,
					0);
		}
	}

err:
	if (ret)
		pr_err("%s(): Set control failed for pes_type :%d\n", __func__, ctrl->pes_type);

	return ret;
}

/**
 * demux_set_pts_interval() - propagate pts interval to demux.
 */
static int demux_set_pts_interval(struct stm_dvb_demux_s *stm_demux,
				struct dmxdev_filter *dmxdev_filter,
				struct dmx_ctrl *ctrl)
{
	__u16 size;
	uint32_t written, partial_write;
	int ret = 0, retry = 5;
	stm_memsrc_h pes_marker_memsrc;
	__u8 ts_packet[STM_MARKER_TS_SIZE];
	struct dmx_pes_filter_params *pes;
	struct dmx_pes_marker_mdata mdata;
	struct stm_dvb_filter_chain_s *chain;

	/*
	 * The default memsrc is in USER mode, so, cannot be used for PTS
	 * Interval injection, so, we get a new memsrc for this.
	 */
	ret = demux_attach_eos_memsrc(stm_demux, true, &pes_marker_memsrc);
	if (ret) {
		stv_err("Failed to attach pes marker memsrc to TE\n");
		goto pes_marker_memsrc_failed;
	}

	ret = copy_from_user(&mdata,
			(const void __user *)ctrl->data, ctrl->size);
	if (ret) {
		stv_err("Failed to copy user metadata\n");
		goto push_failed;
	}

	chain = match_chain_from_filter(stm_demux, dmxdev_filter);
	if (!chain) {
		ret = -EINVAL;
		stv_err("Filter is not yet created\n");
		goto push_failed;
	}

	pes = &dmxdev_filter->params.pes;

	/*
	 * return error for other than audio/video pes type
	 */
	if ((get_video_dec_pes_type(pes->pes_type) == -1) &&
			(get_audio_dec_pes_type(pes->pes_type) == -1)) {
		ret = -EINVAL;
		stv_err("invalid pes type \n");
		goto push_failed;
	}

	stv_debug("Creating pes marker frame for pid: %d\n", pes->pid);

	/*
	 * Create a marker frame with PTS interval  and push to TE
	 */
	ret = stm_ts_marker_create(mdata.marker_type,
			&ts_packet, sizeof(ts_packet),
			&size, pes->pid, mdata.user_data, mdata.size);
	if (ret || (size != STM_MARKER_TS_SIZE)) {
		stv_err("Failed to create TS with PTS interval marker for pid: %d\n", pes->pid);
		goto push_failed;
	}

	/*
	 * Push the ts packet with inband pts interval
	 */
	ret = stm_memsrc_push_data(pes_marker_memsrc, ts_packet,
			sizeof(ts_packet), &written);
	if (ret) {
		stv_err("Failed to push ts with PES Marker (%d)\n", ret);
		goto push_failed;
	}

	/*
	 * First time injection is success, so, good
	 */
	if (written == sizeof(ts_packet)) {
		stv_debug("Successfully sent PES marker with pid: %d\n", pes->pid);
		goto push_failed;
	}

	/*
	 * When TE rejected this inject, because of non-blocking mode, let's
	 * try for (5 * 100) ms. We don't block forever here.
	 */
	while (retry-- >= 0) {

		if (retry < 0) {
			ret = -ETIMEDOUT;
			stv_err("Giving up pushing TS PTS Interval\n");
			goto push_failed;
		}

		/*
		 * TE can handle the injection in partial mode, so, we
		 * may end up here, in case of no push at all or partial
		 * write remaining.
		 */
		ret = stm_memsrc_push_data(pes_marker_memsrc, ts_packet + written,
				sizeof(ts_packet) - written, &partial_write);
		if (ret) {
			stv_err("Failed to push TS packet (%d)\n", ret);
			goto push_failed;
		}
		written += partial_write;

		if (written == sizeof(ts_packet)) {
			retry = 5;
			break;
		}

		/*
		 * if still the injection is incomplete, lets wait for 100 ms
		 * before trying to inject again.
		 */
		if (written < sizeof(ts_packet)) {

			set_current_state(TASK_INTERRUPTIBLE);
			ret = schedule_timeout(msecs_to_jiffies(100));
			if (ret) {
				stv_err("Task interrupted while pushing TS packet with pts interval marker\n");
				goto push_failed;
			}
		}
	}

	stv_debug("Successfully sent TS with PTS Interval Marker of pid: %d\n", pes->pid);

	goto push_failed;

push_failed:
	demux_detach_eos_memsrc(stm_demux, pes_marker_memsrc);
pes_marker_memsrc_failed:
	return ret;
}

/**
 * demux_set_ctrl() - set control on pes filter
 */
static int demux_set_ctrl(struct stm_dvb_demux_s *stm_demux,
				struct dmxdev_filter *dmxdevfilter,
				struct dmx_ctrl *ctrl)
{
	int ret = 0;
	struct stm_dvb_filter_chain_s *chain = NULL;

	/*
	 * Initial processing for control	 */
	switch (ctrl->id) {
	case DMX_CTRL_PES_SELECTOR_MODE:
	case DMX_CTRL_PES_READ_IN_QUANTIZATION_UNITS:
	case DMX_CTRL_PS_INPUT_SUB_STREAM_ID:
	case DMX_CTRL_TSPRIORITY_BIT_FILTERING:

		/*
		 * Check if we have filter created to apply a control
		 */
		chain = match_chain_from_filter(stm_demux, dmxdevfilter);
		if (!chain) {
			pr_err("%s(): no filter found for this instance\n", __func__);
			ret = -EINVAL;
		}

		break;
	}

	if (ret) {
		pr_err("%s(): failed to process the control type\n", __func__);
		goto set_ctrl_failed;
	}

	/*
	 * Set control now
	 */
	switch (ctrl->id) {
	case DMX_CTRL_PES_SELECTOR_MODE:
	{
		if (ctrl->output != DMX_OUT_SELECTOR) {
			pr_err("%s(): invalid pes output for this control\n", __func__);
			ret = -EINVAL;
			break;
		}

		ret = demux_set_sel_mode(chain, ctrl);
		break;
	}

	case DMX_CTRL_PES_READ_IN_QUANTIZATION_UNITS:
		ret = stm_te_filter_set_control(chain->output_filter->handle,
				STM_TE_OUTPUT_FILTER_CONTROL_READ_IN_QUANTISATION_UNITS,
				ctrl->value);
		break;
	case DMX_CTRL_TS_INPUT_TYPE:
	{
		__u32 val;

		/*
		 * Map the dvb control value to TE
		 */
		val = demux_get_ts_input_type(ctrl->value);
		if (val == ~0) {
			pr_err("%s(): invalid control value received\n", __func__);
			break;
		}

		ret = stm_te_demux_set_control(stm_demux->demux_object,
					STM_TE_DEMUX_CNTRL_INPUT_TYPE,
					val);
		break;
	}

	case DMX_CTRL_PS_INPUT_SUB_STREAM_ID:
	{
		ret = stm_te_filter_set_control(chain->input_filter->handle,
				STM_TE_PS_STREAM_FILTER_CONTROL_SUB_STREAM_ID,
				ctrl->value);
		if (ret)
			stv_err("Set control value failed %d for value %d\n", ret, ctrl->value);
		break;
	}

	case DMX_CTRL_SET_PTS_INTERVAL:
	{
		ret = demux_set_pts_interval(stm_demux, dmxdevfilter, ctrl);
		if (ret)
			stv_err("Set pts interval failed %d for value %d\n", ret, ctrl->value);
		break;
	}

	case DMX_CTRL_TSPRIORITY_BIT_FILTERING:
	{
		__u32 val;

		/*
		 * Map the dvb control value to TE
		 */
		val = demux_get_priority_filtering_mode(ctrl->value);
		if (val == ~0) {
			stv_err("Invalid control value(%d) received\n", ctrl->value);
			ret = -EINVAL;
			break;
		}

		ret = stm_te_filter_set_control(chain->input_filter->handle,
				STM_TE_INPUT_FILTER_CONTROL_TSPRIORITY_FILTERING,
				val);
		break;
	}

	}

set_ctrl_failed:
	return ret;
}

/**
 * demux_get_ctrl() - get control on pes filter
 */
static int demux_get_ctrl(struct stm_dvb_demux_s *stm_demux,
		struct dmxdev_filter *dmxdevfilter, struct dmx_ctrl *ctrl)
{
	int ret = 0;
	struct stm_dvb_filter_chain_s *chain = NULL;
	stm_te_output_filter_stats_t output_filter_status;

	/*
	 * Initial processing for control
	 */
	switch (ctrl->id) {
	case DMX_CTRL_OUTPUT_BUFFER_STATUS:

		/*
		 * Check if we have filter created to apply a control
		 */
		chain = match_chain_from_filter(stm_demux, dmxdevfilter);
		if (!chain) {
			stv_err("no filter found for this instance\n");
			ret = -EINVAL;
		}

		break;
	}

	if (ret) {
		stv_err("failed to process the control\n");
		goto get_ctrl_failed;
	}

	/*
	 * Get control now
	 */
	switch (ctrl->id) {
	case DMX_CTRL_OUTPUT_BUFFER_STATUS:
		ret = stm_te_filter_get_compound_control(chain->output_filter->handle,
				STM_TE_OUTPUT_FILTER_CONTROL_STATUS,
				&output_filter_status);
		ctrl->value = output_filter_status.bytes_in_buffer;

		if (ret)
			stv_err("get control value failed %d\n", ret);
		break;

	}

get_ctrl_failed:
	return ret;
}

/**
 * stm_dvb_demux_set_control() - set control
 */
int stm_dvb_demux_set_control(struct stm_dvb_demux_s *stm_demux,
			struct dmxdev_filter *dmxdevfilter,
			struct dmx_ctrl *ctrl)
{
	int ret = 0;

	/*
	 * Check if the control is exposed to user-space
	 */
	ret = demux_validate_ctrl(ctrl->id);
	if (ret) {
		pr_err("%s(): invalid control received\n", __func__);
		goto set_control_done;
	}

	/*
	 * Is the control for valid pes_type?
	 */
	if (ctrl->pes_type != dmxdevfilter->params.pes.pes_type) {
		pr_err("%s(): pes type does not match with filter pes type\n", __func__);
		ret = -EINVAL;
		goto set_control_done;
	}

	/*
	 * Set the control on demux
	 */
	ret = demux_set_ctrl(stm_demux, dmxdevfilter, ctrl);
	if (ret)
		pr_err("%s(): failed to set control on demux\n", __func__);

set_control_done:
	return ret;
}

/**
 * stm_dvb_demux_get_control() - get control
 */
int stm_dvb_demux_get_control(struct stm_dvb_demux_s *stm_demux,
			struct dmxdev_filter *dmxdevfilter, struct dmx_ctrl *ctrl)
{
	int ret = 0;

	/*
	 * Check if the control is exposed to user-space
	 */
	ret = demux_validate_ctrl(ctrl->id);
	if (ret) {
		stv_err("invalid control received\n");
		goto set_control_done;
	}

	/*
	 * Is the control for valid pes_type?
	 */
	if (ctrl->pes_type != dmxdevfilter->params.pes.pes_type) {
		pr_err("%s(): pes type does not match with filter pes type\n", __func__);
		ret = -EINVAL;
		goto set_control_done;
	}

	/*
	 * Get the control on demux
	 */
	ret = demux_get_ctrl(stm_demux, dmxdevfilter, ctrl);
	if (ret)
		stv_err("failed to get control on demux\n");

set_control_done:
	return ret;
}
