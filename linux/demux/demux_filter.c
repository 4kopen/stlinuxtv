/************************************************************************
Copyright (C) 2011 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.

Source file name : demux_filter.c

Filters/chains handling functions

Date        Modification                                    Name
----        ------------                                    --------

 ************************************************************************/

#include <dvb/dvb_adaptation.h>
#include "demux_filter.h"
#include "demux_common.h"
#include "dvb_module.h"
#include "linux/dvb/stm_dmx.h"
#include "stv_debug.h"

/**
 * stm_dmx_subscription_create() - create a memsink attached subscription
 * @filter: filter
 * Create a subscription for data events coming from TE
 */
static int stm_dmx_subscription_create(struct stm_dvb_filter_s *filter)
{
	stm_event_subscription_h evt_subs;
	stm_event_subscription_entry_t *evt_entry = NULL;
	int ret = 0;

	if (!filter || !filter->memsink_interface) {
		printk(KERN_ERR "No output filter/output memsink created\n");
		ret = -EINVAL;
		goto evt_subs_done;
	}

	evt_entry = kzalloc(sizeof(stm_event_subscription_entry_t), GFP_KERNEL);
	if (!evt_entry) {
		printk(KERN_ERR "Out of memory for subscription entry\n");
		ret = -ENOMEM;
		goto evt_subs_done;
	}

	/*
	 * Create the event subscription with the following events
	 */
	evt_entry->object = filter->memsink_interface;
	evt_entry->event_mask = STM_MEMSINK_EVENT_DATA_AVAILABLE |
				STM_MEMSINK_EVENT_BUFFER_OVERFLOW;
	evt_entry->cookie = NULL;

	ret = stm_event_subscription_create(evt_entry, 1, &evt_subs);
	if (ret) {
		printk(KERN_ERR "Failed to create event subscription\n");
		goto evt_subs_create_failed;
	}

	filter->evt_subs = evt_subs;
	filter->evt_subs_entry = evt_entry;

	return 0;

evt_subs_create_failed:
	kfree(evt_entry);
evt_subs_done:
	return ret;
}

/**
 * stm_dmx_subscription_remove() - delete memsink attached subscription
 * @filter: filter
 * Remove the data event subscriptions coming from Transport Engine
 */
static void stm_dmx_subscription_remove(struct stm_dvb_filter_s *filter)
{
	stm_event_subscription_h evt_subs;
	stm_event_subscription_entry_t *evt_entry;

	/*
	 * If there's no event entry, then there won't be any subcription
	 * created, so, return
	 */
	if (!filter || !filter->evt_subs_entry) {
		printk(KERN_DEBUG "There's no filter or its subscrption entry\n");
		return;
	}

	evt_subs = filter->evt_subs;
	evt_entry = filter->evt_subs_entry;

	/*
	 * Delete the event subscription entry
	 */
	if (stm_event_subscription_modify(evt_subs, evt_entry,
					STM_EVENT_SUBSCRIPTION_OP_REMOVE))
		printk(KERN_ERR "Failed to delete subscription entry\n");

	/*
	 * Delete the subscription
	 */
	if (stm_event_subscription_delete(evt_subs))
		printk(KERN_ERR "Failed to delete subscription\n");

	if (evt_entry)
		kfree(evt_entry);

	filter->evt_subs = NULL;
	filter->evt_subs_entry = NULL;
}


/**
 * demux_update_sel_pad_size - Update source demux buffer size on selector pad
 * @chain   : fills the chain with selector and pad info
 * selector pad needs the size information of its source i.e. demux filter.
 * This is to manage overflow scenarios on source buffers.
 */
static int demux_update_sel_pad_size(struct stm_dvb_filter_chain_s *chain)
{
	int ret = 0;
	uint32_t buf_size;

	if (IS_ERR_OR_NULL(chain->pad)) {
		stv_debug("no valid pad found\n");
		goto exit;
	}

	/*
	 * Selector pad needs the size of source TE buffer attached to it.
	 * Read the size of attached TE buffer here.
	 */
	ret = stm_te_filter_get_control(chain->output_filter->handle,
			STM_TE_OUTPUT_FILTER_CONTROL_BUFFER_SIZE, &buf_size);
	if (ret) {
		stv_err("failed to get output buffer size err %d\n", ret);
		goto exit;
	}

	/*
	 * Update source TE buffer size to selector pad
	 */
	ret = stm_selector_pad_set_control(chain->pad,
				STM_SELECTOR_PAD_CONTROL_BUFFER_SIZE, buf_size);
	if (ret)
		stv_err("failed to set selector pad buffer size err %d\n", ret);

exit:
	return ret;
}

 /**
 * demux_open_selector() - create a selector and pad
 * @pes_type: Audio/Video/PCR (dmx_pes_type_t)
 * @chain   : fills the chain with selector and pad info
 * Create a selector and its pad and configure in cached mode
 */
static int demux_open_selector(__u32 pes_type, struct stm_dvb_filter_chain_s *chain)
{
	u32 type;
	char name [32];
	int dec_id, ret = 0;

	/*
	 * Generate a name for selector
	 */
	memset(name, 0, sizeof(name));

	if ((dec_id = get_audio_dec_pes_type(pes_type)) >= 0) {

		type = STM_SELECTOR_TYPE_AUDIO;
		snprintf(name, sizeof(name), "dmx_pes_audio%d", dec_id);

	} else if ((dec_id = get_video_dec_pes_type(pes_type)) >= 0) {

		type = STM_SELECTOR_TYPE_VIDEO;
		snprintf(name, sizeof(name), "dmx_pes_video%d", dec_id);

	} else if ((dec_id = get_pcr_pes_type(pes_type)) >= 0) {

		type = STM_SELECTOR_TYPE_PCR;
		snprintf(name, sizeof(name), "dmx_pes_pcr%d", dec_id);

	} else {
		ret = -EINVAL;
		goto open_failed;
	}

	/*
	 * Open a selector and configure it for a specific type
	 */
	ret = stm_selector_open(name, &chain->selector);
	if (ret) {
		pr_err("%s(): failed to open selector\n", __func__);
		goto open_failed;
	}
	ret = stm_selector_set_control(chain->selector,
				STM_SELECTOR_CONTROL_DATA_INTERFACE_TYPE,
				type);
	if (ret) {
		pr_err("%s(): failed to configure selector type\n", __func__);
		goto set_ctrl_failed;
	}

	/*
	 * Create an input pad on selector and move it to rebae mode
	 */
	ret = stm_selector_pad_new(chain->selector, &chain->pad);
	if (ret) {
		pr_err("%s(): failed to create new pad\n", __func__);
		goto set_ctrl_failed;
	}
	ret = stm_selector_pad_set_control(chain->pad,
				STM_SELECTOR_PAD_CONTROL_CONNECT_MODE,
				STM_SELECTOR_PAD_CONNECT_REBASE_MODE);
	if (ret) {
		pr_err("%s(): failed to configure pad to cached mode\n", __func__);
		goto set_cached_failed;
	}

	if (type != STM_SELECTOR_TYPE_PCR) {
		ret = stm_te_filter_set_control(chain->output_filter->handle,
			STM_TE_OUTPUT_FILTER_CONTROL_READ_IN_QUANTISATION_UNITS, 1);
		if (ret) {
			pr_err("%s(): failed to connect pes filter with selector pad\n", __func__);
			goto set_cached_failed;
		}
	}

	/*
	 * Selector pad uses its source(demux) buffers for caching PES data.
	 * Hence inform selector pad about its source buffers size to manage
	 * overflow scenarios.
	 */
	ret = demux_update_sel_pad_size(chain);
	if (ret) {
		stv_err("failed to update buffer size to selector err %d\n", ret);
		goto set_filter_attach_failed;
	}

	/*
	 * Connect selector pad with output filter
	 */
	ret = stm_te_filter_attach(chain->output_filter->handle, chain->pad);
	if (ret) {
		pr_err("%s(): failed to connect pes filter with selector pad\n", __func__);
		goto set_filter_attach_failed;
	}

	return 0;

set_filter_attach_failed:
	if (stm_te_filter_set_control(chain->output_filter->handle,
			STM_TE_OUTPUT_FILTER_CONTROL_READ_IN_QUANTISATION_UNITS, 0))
		pr_err("%s(): failed to disable quant from out filter %p\n",
						__func__, chain->output_filter->handle);
set_cached_failed:
	if (stm_selector_pad_delete(chain->pad))
		pr_err("%s(): failed to delete pad\n", __func__);
set_ctrl_failed:
	if (stm_selector_close(chain->selector))
		pr_err("%s(): failed to close selector\n", __func__);
open_failed:
	return ret;
}

/**
 * demux_del_selector_from_chain() - close selector and delete its pad
 * @chain: filter chain
 * Delete pad and close selector from this chain configuration
 */
static void demux_del_selector_from_chain(struct stm_dvb_filter_chain_s *chain)
{
	if (!chain->selector)
		goto delete_done;

	if (stm_te_filter_detach(chain->output_filter->handle, chain->pad))
		pr_err("%s(): failed to detach output filter from pad\n", __func__);

	if (stm_selector_pad_delete(chain->pad))
		pr_err("%s(): failed to delete pad\n", __func__);

	if (stm_selector_close(chain->selector))
		pr_err("%s(): failed to delete selector\n", __func__);

	chain->pad = NULL;
	chain->selector = NULL;

delete_done:
	return;
}

/**
 * demux_add_selector_to_chain() - add selector info to filter chain
 */
static int demux_add_selector_to_chain(struct stm_dvb_demux_s *stm_demux,
				__u32 pes_type, struct stm_dvb_filter_chain_s *chain)
{
	int ret = 0;

	/*
	 * Open selector
	 */
	ret = demux_open_selector(pes_type, chain);
	if (ret) {
		pr_err("%s(): failed to open selector\n", __func__);
		goto open_selector_failed;
	}

	ret = stm_selector_associate_pad(stm_demux->dvb_demux_name, chain->pad);
	if (ret) {
		pr_err("%s(): failed to associate selector entries\n", __func__);
		goto asso_failed;
	}

	return 0;

asso_failed:
	demux_del_selector_from_chain(chain);
open_selector_failed:
	return ret;
}

/**
 * stm_dmx_delete_chain() - delete chain from demux
 * @stm_demux : stm demuxer handle
 * @chain     : filter chain
 * Clean up TE data connections for this filter chain
 */
int stm_dmx_delete_chain(struct stm_dvb_demux_s *stm_demux,
					struct stm_dvb_filter_chain_s *chain)
{
	int ret = 0;
	struct dmxdev_filter *dmxdevfilter;
	struct dmx_pes_filter_params *pes;

	/*
	 * No chain, so, nothing to delete
	 */
	if (!chain)
		return 0;

	dmxdevfilter = chain->filter;
	pes = &dmxdevfilter->params.pes;

	/*
	 * Detach input filter from output filter
	 */
	stm_dvb_chain_stop(chain);

	if (0 == (--(chain->input_filter->users))) {
		if (0 != stm_te_filter_delete(chain->input_filter->handle)) {
			printk(KERN_ERR "Unable to delete input filter\n");
			chain->input_filter->users++;
			return -EINVAL;
		}
		kfree(chain->input_filter);
	}

	if (0 == (--(chain->output_filter->users))) {
		if (chain->output_filter->memsink_interface) {
			ret = stm_te_filter_detach(chain->output_filter->handle,
						(void *)chain->output_filter->
						 memsink_interface);
			if (ret)
				printk(KERN_ERR "Unable to detach filter "\
					"object from its attached sink (%d)\n",
					ret);
		}

		if (chain->output_filter->memsink_interface) {
			/*
			 * Delete the subscription associated
			 * with the output filter
			 */
			stm_dmx_subscription_remove(chain->output_filter);

			if (stm_memsink_delete(chain->output_filter->
					   memsink_interface))
				printk(KERN_ERR
					"%s: failed to delete memsink\n", __func__);
		}

		/*
		 * Detaching for tunneled data flow
		 */
		if (chain->output_filter->decoder) {

			if (chain->type == PCR_CHAIN && stm_demux->PlaybackContext)
				ret = stm_dvb_demux_setup_pcr(chain,
						stm_demux->PlaybackContext,
						DEMUX_PCR_DETACH | DEMUX_PCR_LOCKED);
			else
				ret = stm_te_filter_detach(chain->output_filter->handle,
							chain->output_filter->decoder);

			if (ret)
				pr_err("%s(): failed to detach filter from from SE object\n", __func__);

		}

		if (pes->output == (unsigned int)DMX_OUT_SELECTOR)
			demux_del_selector_from_chain(chain);

		if (chain->output_filter->buf_data)
			kfree(chain->output_filter->buf_data);
		if (0 != stm_te_filter_delete(chain->output_filter->handle))
			printk(KERN_ERR "Unable to delete filter object\n");
		kfree(chain->output_filter);
	}

	/*
	 * Remove this particular chain from the global list
	 */
	list_del(&chain->input_list);
	list_del(&chain->output_list);
	kfree(chain);

	return 0;
}

void dump_demux_chain_data(struct stm_dvb_demux_s *demux)
{
	struct list_head *temp;
	struct stm_dvb_filter_chain_s *match;

	printk(KERN_DEBUG "Listing the pid filter list\n");
	list_for_each_prev(temp, &demux->pid_filter_handle_list) {
		match =
		    list_entry(temp, struct stm_dvb_filter_chain_s, input_list);
		if (match) {
			printk(KERN_DEBUG
			       "\tPid is %d input is 0x%p output is 0x%p\n",
			       match->pid, match->input_filter,
			       match->output_filter);
		}
	}

	printk(KERN_DEBUG "Listing the PES_output filter list\n");
	list_for_each_prev(temp, &demux->pes_filter_handle_list) {
		match =
		    list_entry(temp, struct stm_dvb_filter_chain_s,
			       output_list);
		if (match) {
			printk(KERN_DEBUG
			       "\tPid is %d input is 0x%p output is 0x%p\n",
			       match->pid, match->input_filter,
			       match->output_filter);
		}
	}

	printk(KERN_DEBUG "Listing the PCR_output filter list\n");
	list_for_each_prev(temp, &demux->pcr_filter_handle_list) {
		match =
		    list_entry(temp, struct stm_dvb_filter_chain_s,
			       output_list);
		if (match) {
			printk(KERN_DEBUG
			       "\tPid is %d input is 0x%p output is 0x%p\n",
			       match->pid, match->input_filter,
			       match->output_filter);
		}
	}

	printk(KERN_DEBUG "Listing the TS_output filter list\n");
	list_for_each_prev(temp, &demux->ts_filter_handle_list) {
		match =
		    list_entry(temp, struct stm_dvb_filter_chain_s,
			       output_list);
		if (match) {
			printk(KERN_DEBUG
			       "\tPid is %d input is 0x%p output is 0x%p\n",
			       match->pid, match->input_filter,
			       match->output_filter);
		}
	}

	printk(KERN_DEBUG "Listing the INDEX_output filter list\n");
	list_for_each_prev(temp, &demux->index_filter_handle_list) {
		match =
		    list_entry(temp, struct stm_dvb_filter_chain_s,
			       output_list);
		if (match) {
			printk(KERN_DEBUG
			       "\tPid is %d input is 0x%p output is 0x%p\n",
			       match->pid, match->input_filter,
			       match->output_filter);
		}
	}
}

/**
 * demux_alloc_filter() - allocate filter for filter chain
 */
static inline struct stm_dvb_filter_s *demux_alloc_filter(void)
{
	struct stm_dvb_filter_s *filter;

	filter = kzalloc(sizeof(*filter), GFP_KERNEL);
	if (!filter) {
		pr_err("%s(): out of memory for chain\n", __func__);
		goto alloc_failed;
	}

	init_waitqueue_head(&filter->wait);				\

alloc_failed:
	return filter;
}

/**
 * demux_add_pid_filter() - demux add pid filter
 * @stm_demux : stlinuxtv demux handle
 * @pid_params: pid filter creation params
 */
static struct stm_dvb_filter_s *
demux_add_pid_filter(struct stm_dvb_demux_s *stm_demux,
			struct stm_demux_pid_filter_params *pid_params)
{
	int ret;
	struct stm_dvb_filter_s *pid_filter;

	/*
	 * Reuse already created pid filter
	 */
	if (pid_params->pid_filter) {
		pid_filter = pid_params->pid_filter;
		pid_filter->users++;
		goto add_pid_filter_done;
	}

	pid_filter = demux_alloc_filter();
	if (!pid_filter) {
		pr_err("%s(): alloc failed for pid filter\n", __func__);
		goto add_pid_filter_done;
	}

	/*
	 * Create a pid filter
	 */
	switch (pid_params->pid_type) {
	case INPUT_PID_FILTER:
		if (stm_demux->dmx_type == DVB_STI_PSDEMUX) {
			ret = stm_te_stream_filter_new(stm_demux->demux_object, pid_params->pid,
									&pid_filter->handle);
			if (ret)
				stv_err("failed to create a input stream filter (%d)\n", ret);

		} else {
			ret = stm_te_pid_filter_new(stm_demux->demux_object, pid_params->pid,
									&pid_filter->handle);
			if (ret)
				stv_err("failed to create a input pid filter (%d)\n", ret);
		}

		break;

	case INSERT_PID_FILTER:
		ret = stm_te_pid_ins_filter_new(stm_demux->demux_object,
					pid_params->pid, &pid_filter->handle);
		if (ret)
			pr_err("%s(): failed to create an insert pid filter (%d)\n", __func__, ret);
		break;

	case REPLACE_PID_FILTER:
		ret = stm_te_pid_rep_filter_new(stm_demux->demux_object,
					pid_params->pid, &pid_filter->handle);
		if (ret)
			pr_err("%s(): failed to create a replace pid filter (%d)\n", __func__, ret);
		break;

	default:
		ret = -EINVAL;
	}
	if (ret)
		goto pid_new_failed;

	pid_filter->users++;

	return pid_filter;

pid_new_failed:
	kfree(pid_filter);
	pid_filter = NULL;
add_pid_filter_done:
	return pid_filter;
}

/**
 * demux_del_pid_filter() - delete pid filter
 */
static void demux_del_pid_filter(struct stm_dvb_filter_s *pid_filter)
{
	if (--(pid_filter->users))
		goto delete_done;

	if (stm_te_filter_delete(pid_filter->handle))
		pr_err("%s(): failed to delete pid filter\n", __func__);

	kfree(pid_filter);

delete_done:
	return;
}

/**
 * demux_add_pes_filter() - demux add pes filter
 * @stm_demux : stlinuxtv demux handle
 * @pes_params: pes filter creation params
 */
static struct stm_dvb_filter_s *
demux_add_pes_filter(struct stm_dvb_demux_s *stm_demux,
			struct stm_demux_pes_filter_params *pes_params)
{
	int ret;
	struct stm_dvb_filter_s *pes_filter;

	/*
	 * If output filter already exist, we reuse one
	 */
	if (pes_params->pes_filter) {
		pes_filter = pes_params->pes_filter;
		pes_filter->users++;
		goto add_pes_filter_done;
	}

	pes_filter = demux_alloc_filter();
	if (!pes_filter) {
		pr_err("%s(): alloc failed for pes filter\n", __func__);
		goto add_pes_filter_done;
	}

	/*
	 * Create a new pes filter
	 */
	switch (pes_params->pes_map) {
	case PES_MAP:
	{
		__u32 type = STM_TE_INPUT_TYPE_UNKNOWN;
		stm_te_filter_t pes_type = STM_TE_PES_FILTER;

		/*
		 * Select the type of output filter
		 */
		if (stm_demux->dmx_type == DVB_STI_PSDEMUX)
			pes_type = STM_TE_PS_PES_FILTER;
		else {
			ret = stm_te_demux_get_control(stm_demux->demux_object,
						STM_TE_DEMUX_CNTRL_INPUT_TYPE,
						&type);
			if (ret) {
				stv_err("Failed to get stream input type (%d)\n", ret);
				break;
			}
		}

		/*
		 * Create a new output filter
		 */
		ret = stm_te_output_filter_new(stm_demux->demux_object,
				pes_type, &pes_filter->handle);
		if (ret) {
			stv_err("failed to create pes filter (%d)\n", ret);
			break;
		}

		/*
		 * For supporting DirectTV, DSS format (packet length 130 bytes instead
		 * of 188 bytes), output of demux'ing out a pid is expected to ES not
		 * PES. So, configure the video pes filter for receiving only ES.
		 * Other filters do not contain non-ES data.
		 */
		if (get_video_dec_pes_type(pes_params->pes_type) < 0)
			break;

		break;
	}

	case TS_MAP:
		ret = stm_te_output_filter_new(stm_demux->demux_object,
				STM_TE_TS_FILTER, &pes_filter->handle);
		if (ret)
			pr_err("%s(): failed to create TS filter (%d)\n", __func__, ret);
		break;

	case SECTION_MAP:
		ret = stm_te_output_filter_new(stm_demux->demux_object,
				STM_TE_SECTION_FILTER, &pes_filter->handle);
		if (ret)
			pr_err("%s(): failed to create section filter (%d)\n", __func__, ret);
		break;

	case PCR_MAP: {
		stm_te_filter_t pcr_type = STM_TE_PCR_FILTER;

		if (stm_demux->dmx_type == DVB_STI_PSDEMUX)
			pcr_type = STM_TE_PS_SCR_FILTER;

		ret = stm_te_output_filter_new(stm_demux->demux_object,
				pcr_type, &pes_filter->handle);
		if (ret)
			stv_err("failed to create PCR filter (%d)\n", ret);

		break;
	}

	case INDEX_MAP:
		ret = stm_te_output_filter_new(stm_demux->demux_object,
				STM_TE_TS_INDEX_FILTER, &pes_filter->handle);
		if (ret)
			pr_err("%s(): failed to create index filter (%d)\n", __func__, ret);
		break;

	case MAX_MAP:
	default:
		ret = -EINVAL;
	}

	if (ret)
		goto pes_new_failed;

	pes_filter->users++;

	return pes_filter;

pes_new_failed:
	kfree(pes_filter);
	pes_filter = NULL;
add_pes_filter_done:
	return pes_filter;
}

/**
 * demux_del_pes_filter() - delete pes filter
 */
static void demux_del_pes_filter(struct stm_dvb_filter_s *pes_filter)
{
	if (--(pes_filter->users))
		goto delete_done;

	if (stm_te_filter_delete(pes_filter->handle))
		pr_err("%s(): failed to delete pes filter\n", __func__);

	kfree(pes_filter);

delete_done:
	return;
}

/**
 * demux_add_chain() - add a filter chain to a particular demux
 * @stm_demux: stlinuxtv demux handle
 * @params   : chain input params
 * Create a filter chain based on its input parameters
 */
struct stm_dvb_filter_chain_s *stm_demux_add_chain(struct stm_dvb_demux_s *stm_demux,
						struct stm_demux_chain_params *params)
{
	int ret;
	__u32 chain_type;
	struct list_head *demux_list;
	struct stm_dvb_filter_chain_s *chain;

	/*
	 * Allocate a new chain for this demux
	 */
	chain = kzalloc(sizeof(*chain), GFP_KERNEL);
	if (!chain) {
		pr_err("%s: out of memory for new filter chain\n", __func__);
		goto chain_alloc_failed;
	}
	chain->state = CHAIN_STOPPED;

	/*
	 * Allocate a new pid filter
	 */
	chain->input_filter = demux_add_pid_filter(stm_demux, &params->pid_params);
	if (!chain->input_filter) {
		pr_err("%s(): failed to create new pid filter\n", __func__);
		goto pid_filter_add_failed;
	}

	/*
	 * Allocate a new pes filter
	 */
	chain->output_filter = demux_add_pes_filter(stm_demux, &params->pes_params);
	if (!chain->output_filter) {
		pr_err("%s(): failed to create new pes filter\n", __func__);
		goto pes_filter_add_failed;
	}

	/*
	 * Add chain to demux list
	 */
	switch (params->pes_params.pes_map) {
	case PES_MAP:
		demux_list = &stm_demux->pes_filter_handle_list;
		chain_type = PES_CHAIN;
		break;

	case TS_MAP:
		demux_list = &stm_demux->ts_filter_handle_list;
		chain_type = PES_CHAIN;
		break;

	case SECTION_MAP:
		demux_list = &stm_demux->sec_filter_handle_list;
		chain_type = SECTION_CHAIN;
		break;

	case PCR_MAP:
		demux_list = &stm_demux->pcr_filter_handle_list;
		chain_type = PCR_CHAIN;
		break;

	case INDEX_MAP:
		demux_list = &stm_demux->index_filter_handle_list;
		chain_type = INDEX_CHAIN;
		break;

	case MAX_MAP:
	default:
		goto pes_filter_add_failed;
	}

	/*
	 * Create a selector if pes filter is setup for FCC
	 */
	if (params->pes_params.pes_output == (unsigned int)DMX_OUT_SELECTOR) {
		ret= demux_add_selector_to_chain(stm_demux, params->pes_params.pes_type, chain);
		if (ret) {
			pr_err("%s(): failed to add selector to chain\n", __func__);
			goto add_selector_failed;
		}
	}

	/*
	 * Fill in the chain information
	 */
	chain->pid = params->pid_params.pid;
	chain->filter = (void *)params->priv;
	chain->type = chain_type;

	list_add(&chain->input_list, &stm_demux->pid_filter_handle_list);
	list_add(&chain->output_list, demux_list);

	dump_demux_chain_data(stm_demux);

	return chain;

add_selector_failed:
	demux_del_pes_filter(chain->output_filter);
pes_filter_add_failed:
	demux_del_pid_filter(chain->input_filter);
pid_filter_add_failed:
	kfree(chain);
chain_alloc_failed:
	return NULL;
}

/**
 * attach_sink_to_chain
 * Attach output filter to sink. The sink may be
 * a. memsink to fetch the data from demux
 * b. decoder handle to push data directly to decoder
 */
int attach_sink_to_chain(struct stm_dvb_filter_chain_s *chain,
			 stm_object_h sink, char *memsink_name,
			 stm_data_mem_type_t address)
{
	int ret;
	struct stm_dvb_filter_s *output_filter;

	output_filter = chain->output_filter;

	if (sink == NULL) {
		/* Need to create a proper memsink object and connect it to the output filter */
		ret = stm_memsink_new(memsink_name,
				STM_IOMODE_BLOCKING_IO,
				address,
				&output_filter->memsink_interface);
		if (ret) {
			printk(KERN_ERR "%s: failed to create memsink (%d)\n",
					__func__, ret);
			goto error_memsink_new;
		}

		ret = stm_te_filter_attach(output_filter->handle,
				output_filter->memsink_interface);
		if (ret) {
			printk(KERN_ERR
					"%s: failed to attach the memsink to the output filter (%d)\n",
					__func__, ret);
			goto error_memsink_attach;
		}

		/*
		 * Create the event subscription for TE events
		 */
		ret = stm_dmx_subscription_create(output_filter);
		if (ret) {
			printk(KERN_ERR "Failed to create subscription for TE events\n");
			goto error_filter_subs_create;
		}

		/*
		 * Set a wait queue for poll
		 */
		ret = stm_event_set_wait_queue(output_filter->evt_subs,
				&output_filter->wait, true);
		if (ret) {
			printk(KERN_ERR "Failed to setup wait queue for TE events\n");
			goto error_filter_set_wait_q_failed;
		}

	} else {
		/* Simply attach the provided sink */
		output_filter->decoder = sink;

		if (chain->type != PCR_CHAIN) {

			ret = stm_te_filter_attach(output_filter->handle,
							output_filter->decoder);

			if (ret) {
				printk(KERN_ERR
						"%s: failed to attach the sink to the output filter (%d)\n",
						__func__, ret);
				goto error_sink_attach;
			}
		}
	}

	return 0;

error_sink_attach:
	return ret;

error_filter_set_wait_q_failed:
	stm_dmx_subscription_remove(output_filter);
error_filter_subs_create:
	if (stm_te_filter_detach(output_filter->handle,
					output_filter->memsink_interface))
		printk(KERN_ERR "Failed to detach output filter from memsink\n");
error_memsink_attach:
	if (stm_memsink_delete(output_filter->memsink_interface))
		printk(KERN_ERR "%s: failed to delete the memsink\n", __func__);
error_memsink_new:
	return ret;
}

struct stm_dvb_filter_chain_s *get_primary_ts_chain(struct stm_dvb_demux_s
						    *demux)
{
	struct list_head *pos;

	list_for_each(pos, &demux->ts_filter_handle_list) {
		return list_entry(pos, struct stm_dvb_filter_chain_s,
				  output_list);
	}

	return NULL;
}

struct stm_dvb_filter_chain_s *match_filter_output_pid(struct stm_dvb_demux_s
						       *demux, int pid,
						       enum map_e map)
{
	struct stm_dvb_filter_chain_s *match = NULL;
	struct list_head *list_to_search;
	struct list_head *pos;

	switch (map) {
	case PES_MAP:
		list_to_search = &demux->pes_filter_handle_list;
		break;
	case TS_MAP:
		list_to_search = &demux->ts_filter_handle_list;
		break;
	case SECTION_MAP:
		list_to_search = &demux->sec_filter_handle_list;
		break;
	case PCR_MAP:
		list_to_search = &demux->pcr_filter_handle_list;
		break;
	case INDEX_MAP:
		list_to_search = &demux->index_filter_handle_list;
		break;
	case MAX_MAP:
	default:
		return NULL;

	}

	list_for_each(pos, list_to_search) {
		match =
		    list_entry(pos, struct stm_dvb_filter_chain_s, output_list);
		if (match && (match->pid == pid)) {
			return match;
		}
	}

	return NULL;
}

struct stm_dvb_filter_chain_s *match_chain_from_filter(struct stm_dvb_demux_s
						       *demux, struct dmxdev_filter
						       *filter)
{
	struct dmxdev_filter *dmxdevfilter = filter;
	struct stm_dvb_filter_chain_s *match = NULL;
	struct list_head *list_to_search;
	struct list_head *pos;

	/* check pcr filter */
	list_to_search = &demux->pcr_filter_handle_list;
	list_for_each(pos, list_to_search) {
		match =
		    list_entry(pos, struct stm_dvb_filter_chain_s, output_list);
		if (match && (match->filter == (void *)filter)) {
			return match;
		}
	}

	/* check the index filter */
	list_to_search = &demux->index_filter_handle_list;
	list_for_each(pos, list_to_search) {
		match =
		    list_entry(pos, struct stm_dvb_filter_chain_s, output_list);
		if (match && (match->filter == (void *)filter)) {
			return match;
		}
	}

	if (dmxdevfilter->type == DMXDEV_TYPE_PES) {
		if ((dmxdevfilter->params.pes.output == DMX_OUT_TAP) ||
			(dmxdevfilter->params.pes.output == DMX_OUT_DECODER) ||
			(dmxdevfilter->params.pes.output == (__u32)DMX_OUT_SELECTOR)) {
			list_to_search = &demux->pes_filter_handle_list;
		} else {
			list_to_search = &demux->ts_filter_handle_list;
		}
	} else if (dmxdevfilter->type == DMXDEV_TYPE_SEC) {
		list_to_search = &demux->sec_filter_handle_list;
	} else {
		/* insert/replace filter will fall here due to DMXDEV_TYPE_NONE
		 * type.
		 * TODO: use additional check to identify insert/replace filter
		 * to avoid unnecessary search for other invalid filters
		 */
		list_to_search = &demux->ts_filter_handle_list;
	}

	list_for_each(pos, list_to_search) {
		match =
		    list_entry(pos, struct stm_dvb_filter_chain_s, output_list);
		if (match && (match->filter == (void *)filter)) {
			return match;
		}
	}

	return NULL;
}

/* Finds the filter chain based on the input filter */
struct stm_dvb_filter_chain_s *match_filter_input_pid(struct stm_dvb_demux_s
						      *demux, int pid)
{
	struct stm_dvb_filter_chain_s *match = NULL;
	struct list_head *pos;

	list_for_each(pos, &demux->pid_filter_handle_list) {
		match =
		    list_entry(pos, struct stm_dvb_filter_chain_s, input_list);
		if (match && (match->pid == pid)) {
			printk(KERN_DEBUG "Match was 0x%p - %d\n", match, pid);
			return match;
		}
	}

	return NULL;
}

/* Finds the filter chain based on the output filter streaming engine interface */
struct stm_dvb_filter_chain_s *match_filter_from_decoder(struct
							      stm_dvb_demux_s
							      *demux,
							      stm_object_h
							      interface)
{
	struct stm_dvb_filter_chain_s *match = NULL;
	struct list_head *pos;

	list_for_each(pos, &demux->pes_filter_handle_list) {
		match =
		    list_entry(pos, struct stm_dvb_filter_chain_s, output_list);
		if (match && match->output_filter
		    && match->output_filter->decoder == interface) {
			printk(KERN_DEBUG "Match was 0x%p\n", match);
			return match;
		}
	}

	return NULL;
}

/* Finds the filter chain based on the output filter handle */
struct stm_dvb_filter_chain_s *match_filter_from_output_handler(struct
							      stm_dvb_demux_s
							      *demux,
							      stm_object_h
							      demux_filter)
{
	struct stm_dvb_filter_chain_s *match = NULL;
	struct list_head *pos;

	list_for_each(pos, &demux->pes_filter_handle_list) {
		match =
		    list_entry(pos, struct stm_dvb_filter_chain_s, output_list);
		if (match && match->output_filter
		    && match->output_filter->handle == demux_filter) {
			printk(KERN_DEBUG "Match was 0x%p\n", match);
			return match;
		}
	}

	return NULL;
}

struct stm_dvb_filter_s *get_pid_filter(struct stm_dvb_demux_s *demux, int pid)
{
	struct stm_dvb_filter_chain_s *chain;
	struct stm_dvb_filter_s *filter = NULL;
	int map = 0;

#ifdef SINGLE_LIST
	chain = match_filter_pid(demux, pid, map);
	if (chain && chain->input_filter) {
		filter = chain->input_filter;
		goto out;
	}
#else
	for (map = 0; map < MAX_MAP; map++) {
		chain = match_filter_input_pid(demux, pid);
		if (chain && chain->input_filter) {
			filter = chain->input_filter;
			goto out;
		}
	}
#endif

out:
	return filter;
}

struct stm_dvb_filter_s *get_filter_pacing_output(struct stm_dvb_demux_s *demux)
{
	struct list_head *temp;
	dmx_pes_type_t pes_type;
	struct dmxdev_filter *dmxdevfilter;
	struct stm_dvb_filter_s *filter = NULL;
	struct stm_dvb_filter_chain_s *match, *aud, *ttx, *subt, *pcr, *sec;

	/*
	 * We need to find a new output filter on which to set pacing on.
	 * A. Scan the list and find out PES filter if any. The priority is:
	 *    - DMX_PES_VIDEOxx
	 *    - DMX_PES_AUDIOxx
	 *    - DMX_PES_TELETEXTxx
	 *    - DMX_PES_SUBTITLExx
	 *    - DMX_PES_PCRxx
	 * B. If no PES filter, look out for section filter
	 */
	aud = ttx = subt = pcr = sec = NULL;
	list_for_each_prev(temp, &demux->pid_filter_handle_list) {

		match = list_entry(temp,
				struct stm_dvb_filter_chain_s, input_list);

		dmxdevfilter = match->filter;

		if (dmxdevfilter->type == DMXDEV_TYPE_PES) {

			pes_type = dmxdevfilter->params.pes.pes_type;

			/*
			 * Is there's video chain, return output filtera now
			 */
			if (get_video_dec_pes_type(pes_type) > 0)
				return match->output_filter;

			/*
			 * Iterate over the complete list for this demux and
			 * return the output_filter in the same priority order
			 * as stated above.
			 */
#ifdef CONFIG_STLINUXTV_DECODE_DISPLAY
			if (!aud) {
				if (get_audio_dec_pes_type(pes_type) > 0) {
					aud = match;
					continue;
				}
			}
#endif

			if (!ttx) {
				if (stm_dmx_ttx_get_pes_type(pes_type) > 0) {
					ttx = match;
					continue;
				}
			}

			if (!subt) {
				if (stm_dmx_subt_get_pes_type(pes_type) > 0) {
					subt = match;
					continue;
				}
			}

			if (!pcr) {
				if (get_pcr_pes_type(pes_type) > 0) {
					pcr = match;
					continue;
				}
			}

		} else if ((dmxdevfilter->type == DMXDEV_TYPE_SEC) && !sec) {

			sec = match;
		}
	}

	/*
	 * Find out which output filter to send back
	 */
	if (aud)
		filter = aud->output_filter;
	else if (ttx)
		filter = ttx->output_filter;
	else if (subt)
		filter = subt->output_filter;
	else if (pcr)
		filter = pcr->output_filter;
	else if (sec)
		filter = sec->output_filter;

	return filter;
}

int del_all_chains_from_demux(struct stm_dvb_demux_s *demux)
{
	struct list_head *pos;
	struct list_head *temph;
	struct stm_dvb_filter_chain_s *chain_entry;

	list_for_each_safe(pos, temph, &demux->pid_filter_handle_list) {
		chain_entry =
		    list_entry(pos, struct stm_dvb_filter_chain_s, input_list);
		if (chain_entry) {
			printk(KERN_DEBUG "Trying to delete %d\n",
			       chain_entry->pid);
			dump_demux_chain_data(demux);
			stm_dmx_delete_chain(demux, chain_entry);
			printk(KERN_DEBUG "Deleted\n");
			chain_entry = NULL;
			dump_demux_chain_data(demux);
		}
	}

	return 0;
}

struct stm_dvb_filter_chain_s *demux_get_pcr_by_type(struct stm_dvb_demux_s *stm_demux,
								dmx_pes_type_t pcr_type)
{
	struct list_head *pos;
	struct stm_dvb_filter_chain_s *pcr_chain;
	struct dmxdev_filter *dmxdev_filter;

	list_for_each(pos, &stm_demux->pcr_filter_handle_list) {
		pcr_chain = list_entry(pos,  struct stm_dvb_filter_chain_s, output_list);
		dmxdev_filter = pcr_chain->filter;
		if (dmxdev_filter->params.pes.pes_type == pcr_type)
			return pcr_chain;
	}

	return NULL;
}

/* Finds the PCR filter chain based on the output filtere */
struct stm_dvb_filter_chain_s *find_pcr_filter(struct stm_dvb_demux_s *demux)
{
	struct stm_dvb_filter_chain_s *match = NULL;
	struct list_head *pos;
	list_for_each(pos, &demux->pcr_filter_handle_list) {
		match =
			list_entry(pos, struct stm_dvb_filter_chain_s, output_list);
		if (match && match->output_filter) {
			printk(KERN_DEBUG "Match was 0x%p\n", match);
			return match;
		}
	}

	return NULL;
}

/**
 * demux_get_matched_playback_pcr() - retrieve the pcr chain of a particular playback
 * @stm_demux : demuxer to search for pcr list
 * @pcr_sink  : selector/decoder which is connected to pcr pes filter
 * Returns the valid chain if the playback is associated with any pcr chain
 */
static struct stm_dvb_filter_chain_s *
demux_get_matched_playback_pcr(struct stm_dvb_demux_s *stm_demux, stm_object_h pcr_sink)
{
	struct list_head *pos;
	struct stm_dvb_filter_chain_s *pcr_chain;

	list_for_each(pos, &stm_demux->pcr_filter_handle_list) {

		pcr_chain = list_entry(pos, struct stm_dvb_filter_chain_s, output_list);

		if (pcr_chain && pcr_chain->output_filter &&
				(pcr_chain->output_filter->decoder == pcr_sink))
			return pcr_chain;
	}

	return NULL;
}

/**
 * stm_dvb_demux_setup_pcr() - set the pcr on playback
 * @pcr_chain    : handle to pcr chain
 * @playback_ctx : playback context
 * @flags        : attach/detach pcr
 * Attach/detach pcr to/from playback. Locking order: demux/playback
 */
int stm_dvb_demux_setup_pcr(struct stm_dvb_filter_chain_s *pcr_chain,
			struct PlaybackDeviceContext_s *playback_ctx, u32 flags)
{
	int ret = 0;
	__u32 action = flags & ~(DEMUX_PCR_LOCKED);
	stm_object_h pcr_filter, playback;

	if (flags & DEMUX_PCR_LOCKED)
		if (mutex_lock_interruptible(&playback_ctx->mutex))
			return -ERESTARTSYS;

	/*
	 * This means that the pcr filter is already detached
	 */
	if (!playback_ctx->Playback)
		goto setup_done;

	playback = playback_ctx->Playback->Handle;
	pcr_filter = pcr_chain->output_filter->handle;

	switch (action) {
	case DEMUX_PCR_ATTACH:

		pr_debug("%s(): Attaching playback to pcr\n", __func__);

		if (pcr_chain->selector) {
			ret = stm_selector_attach(pcr_chain->selector, playback);
			if (ret == -EBUSY) {
				pr_err("%s(): re-attach request, so, we don't error out\n", __func__);
				ret = 0;
			}
		} else
			ret = stm_te_filter_attach(pcr_filter, playback);

		if (ret) {
			pr_err("%s(): failed to setup pcr filter %d\n", __func__, ret);
			break;
		}

		if (pcr_chain->selector) {
			playback_ctx->pcr_selector = pcr_chain->selector;
			pcr_chain->output_filter->decoder = pcr_chain->selector;
		} else {
			playback_ctx->pcr_filter = pcr_filter;
			pcr_chain->output_filter->decoder = playback;
		}
		break;

	case DEMUX_PCR_DETACH:

		pr_debug("%s(): Detaching playback from pcr\n", __func__);

		if (pcr_chain->selector) {

			ret = stm_selector_detach(pcr_chain->selector, playback);
			if (ret)
				pr_err("%s(): failed to detach selector from playback\n", __func__);
			playback_ctx->pcr_selector = NULL;

		} else {

			ret = stm_te_filter_detach(pcr_filter, playback);
			if (ret)
				pr_err("%s(): failed to detach playback\n", __func__);

			playback_ctx->pcr_filter = NULL;
		}

		pcr_chain->output_filter->decoder = NULL;

		break;

	default:
		ret = -EINVAL;
	}

setup_done:
	if (flags & DEMUX_PCR_LOCKED)
		mutex_unlock(&playback_ctx->mutex);
	return ret;
}

/**
 * stm_dvb_demux_detach_pcr() - detach pcr filter from playback
 * @stm_demux    : stm demuxer handle
 * @playback_ctx : playback context
 * Detach demuxer pcr chain output filter with playback
 */
int stm_dvb_demux_detach_pcr(struct stm_dvb_demux_s *stm_demux,
			struct PlaybackDeviceContext_s *playback_ctx)
{
	int ret = 0;
	stm_object_h pcr_sink = NULL;
	struct stm_dvb_filter_chain_s *chain;

	if (!stm_demux)
		goto detach_done;


	if (playback_ctx->pcr_filter)
		pcr_sink = playback_ctx->Playback->Handle;
	else if (playback_ctx->pcr_selector)
		pcr_sink = playback_ctx->pcr_selector;

	/*
	 * Search for PCR output filter and detach it from SE playback
	 */
	chain = demux_get_matched_playback_pcr(stm_demux, pcr_sink);
	if (!chain)
		goto detach_done;

	ret = stm_dvb_demux_setup_pcr(chain, playback_ctx, DEMUX_PCR_DETACH);
	if (ret)
		pr_err("%s(): failed to detach pcr from playback\n", __func__);

detach_done:
	return ret;
}

/**
 * stm_dvb_demux_attach_decoder() - attach play stream with demuxer
 * @stm_demux    : stm demuxer handle
 * @pes_filter   : pes filter handle (audio/video)
 * @play_stream  : play stream (audio/video)
 * Attach demuxer pes chain output filter with output filter
 */
int stm_dvb_demux_attach_decoder(struct stm_dvb_demux_s *stm_demux,
			stm_object_h pes_filter, stm_object_h play_stream)
{
	int ret = 0;
	struct stm_dvb_filter_chain_s *chain;

	/*
	 * We are supporting sw/hw demux. This call is made by
	 * audio/video devices to detach demux, but for sw demux
	 * this cannot proceed furthur.
	 */
	if (!stm_demux)
		goto attach_done;

	/*
	 * Attach pes filter with play stream
	 */
	chain = match_filter_from_output_handler(stm_demux, pes_filter);
	if (!chain) {
		printk(KERN_ERR "%s: couldn't find the output filter object\n", __func__);
		ret = -EINVAL;
		goto attach_done;
	}
	ret = stm_te_filter_attach(pes_filter, play_stream);
	if (ret) {
		printk(KERN_ERR "%s: failed to attach play stream to output filter\n", __func__);
		goto attach_done;
	}
	chain->output_filter->decoder = play_stream;

attach_done:
	return ret;
}

/**
 * stm_dvb_demux_detach_decoder() - detach play stream from demux
 * @stm_demux    : stm demuxer handle
 * @pes_filter   : pes filter handle (audio/video)
 * @play_stream  : play stream (audio/video)
 * Detach demuxer pes chain output filter from play stream
 */
int stm_dvb_demux_detach_decoder(struct stm_dvb_demux_s *stm_demux,
			stm_object_h pes_filter, stm_object_h play_stream)
{
	int ret = 0;
	struct stm_dvb_filter_chain_s *chain;

	/*
	 * We are supporting sw/hw demux. This call is made
	 * by audio/video devices to detach demux, but for
	 * sw demux this cannot proceed furthur.
	 */
	if (!stm_demux)
		goto detach_done;

	/*
	 * Check if the decoder is still known by the demux (ie still attached).
	 * It might not be there anymore if the demux has already been stopped.
	 */
	chain = match_filter_from_decoder(stm_demux, play_stream);
	if (!chain || (chain->output_filter->handle != pes_filter))
		goto detach_done;

	ret = stm_te_filter_detach(pes_filter, play_stream);
	if (ret)
		printk(KERN_ERR "%s(): failed to detach play stream from pes filter\n", __func__);

	chain->output_filter->decoder = NULL;

detach_done:
	return ret;
}

int stm_dvb_chain_start( struct stm_dvb_filter_chain_s *chain )
{
	int ret;

	if (chain->state == CHAIN_STARTED)
		return 0;

	ret = stm_te_filter_attach(chain->input_filter->handle,
				   chain->output_filter->handle);
	if (ret) {
		printk(KERN_ERR
		       "%s: failed to attach TE input & output filters (%d)\n",
		       __func__, ret);
		goto error_filter_attach;
	}

	/*
	 * Selector pad uses its source(demux) buffers for caching PES data.
	 * Hence inform selector pad about its source buffers size to manage
	 * overflow scenarios.
	 */
	ret = demux_update_sel_pad_size(chain);
	if (ret) {
		stv_err("failed to update selector pad size err %d\n", ret);
		goto error_update_sel_pad_size;
	}

	chain->state = CHAIN_STARTED;
	return 0;

error_update_sel_pad_size:
	ret = stm_te_filter_detach(chain->input_filter->handle,
				chain->output_filter->handle);
	if (ret)
		stv_err("failed to detach TE input & outpur filters (%d)\n",
				ret);
error_filter_attach:
	return -EIO;
}

int stm_dvb_chain_stop( struct stm_dvb_filter_chain_s *chain )
{
	int ret;

	if (chain->state == CHAIN_STOPPED)
		return 0;

	ret = stm_te_filter_detach(chain->input_filter->handle,
				   chain->output_filter->handle);
	if (ret) {
		printk(KERN_ERR
		       "%s: failed to detach TE input & output filters (%d)\n",
		       __func__, ret);
		goto error_filter_detach;
	}

	chain->state = CHAIN_STOPPED;
	return 0;

error_filter_detach:
	return -EIO;
}
