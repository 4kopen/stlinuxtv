/************************************************************************
 * Copyright (C) 2011 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.

Source file name : dvb_init.c

Main entry point for the STLinuxTv module

Date        Modification                                    Name
----        ------------                                    --------

************************************************************************/

#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/device.h>
#ifdef CONFIG_OF
#include <linux/of.h>
#include <linux/of_address.h>
#endif
#include "dvb_data.h"
#include "dvb_adapt_demux.h"
#include "stm_dvb_psdemux.h"

#ifdef CONFIG_STLINUXTV_DECODE_DISPLAY
#include "dvb_module.h"
#include "dvb_audio.h"
#include "dvb_video.h"
#endif
#include "stat.h"
#include "stv_debug.h"

static int packet_count = 0;
module_param(packet_count, int, 0660);

static int hw_path = 1;
module_param(hw_path, int, 0660);

static int audio_device_nb = 25;
module_param(audio_device_nb, int, 0660);

static int video_device_nb = 25;
module_param(video_device_nb, int, 0660);

static struct dvb_adapter stm_dvb_adapter = {
	.num = -1,
};

#ifdef CONFIG_STLINUXTV_DECODE_DISPLAY
static struct DvbContext_s DvbContext;
#endif


int dvb_check_i2c_device_id(unsigned int *base_address, int num)
{
	int ret = -1;
	struct i2c_adapter *adap;
	struct platform_device *pdev;

	adap = i2c_get_adapter(num);
	if (adap == NULL) {
		printk(KERN_ERR "%s: incorrect i2c device id %d\n",
								__func__, num);
		return ret;
	}
	pdev = (struct platform_device *) to_platform_device(adap->dev.parent);
	if (!pdev) {
		pr_err("%s: pdev NULL (i2c id:%d)\n", __func__, num);
		i2c_put_adapter(adap);
		return ret;
	}
	if (pdev->resource->start != (unsigned int) base_address) {
		printk(KERN_ERR"%s:base address not matched %u",
			__func__, (unsigned int) base_address);
		i2c_put_adapter(adap);
		return ret;
	}
	i2c_put_adapter(adap);
	return 0;
}

int32_t stm_dvb_init(void)
{
	struct stm_dvb_demux_s *dvb_demux = NULL;
    stm_te_caps_t te_caps;
	uint32_t obj_cnt = 0;
	int32_t  ret;
	short int ids[DVB_MAX_ADAPTERS] = { 0 };


	initDataSets();

	if (0 > (ret = dvb_register_adapter(&stm_dvb_adapter,
					    "ST DVB driver",
					    THIS_MODULE, NULL, ids))) {
		printk(KERN_ERR "Error registering adapter %d : %d\n", *ids,
		       ret);
		return 0;
	}


	if ((ret = stm_te_get_capabilities(&te_caps))) {
		printk(KERN_ERR "Failed to get capabilities of Demux\n");
		return ret;
	}

	/* Filter numbers are hard coded at the moment until the te_caps is filled correctly */

	printk(KERN_INFO
	       "Found %u demuxes, %d pid filters, %d output filters\n",
	       te_caps.max_demuxes, te_caps.max_pid_filters,
	       te_caps.max_output_filters);

#ifdef CONFIG_STLINUXTV_DECODE_DISPLAY
	/* Initialize the DvbContext structure */
	memset(&DvbContext, 0, sizeof(struct DvbContext_s));
	mutex_init(&DvbContext.Lock);

	/* Store the max demux available, the same number of
	 * audio/video devices to be created and released */
	DvbContext.demux_dev_nb = te_caps.max_demuxes;
	DvbContext.audio_dev_off = 0;
	DvbContext.audio_dev_nb = audio_device_nb;
	DvbContext.video_dev_off = audio_device_nb;
	DvbContext.video_dev_nb = video_device_nb;

	DvbContext.AudioDeviceContext =
	    kzalloc(sizeof(struct AudioDeviceContext_s) * audio_device_nb,
		    GFP_KERNEL);
	if (DvbContext.AudioDeviceContext == NULL) {
		printk(KERN_ERR
		       "%s: failed to init memory for Audio device context\n",
		       __func__);
		return -ENOMEM;
	}
	DvbContext.VideoDeviceContext =
	    kzalloc(sizeof(struct VideoDeviceContext_s) * video_device_nb,
		    GFP_KERNEL);
	if (DvbContext.VideoDeviceContext == NULL) {
		printk(KERN_ERR
		       "%s: failed to init memory for Video device context\n",
		       __func__);
		return -ENOMEM;
	}
	DvbContext.PlaybackDeviceContext =
	    kzalloc(sizeof(struct PlaybackDeviceContext_s) *\
			(audio_device_nb + video_device_nb) , GFP_KERNEL);
	if (DvbContext.PlaybackDeviceContext == NULL) {
		printk(KERN_ERR
		       "%s: failed to init memory for Playback device context\n",
		       __func__);
		return -ENOMEM;
	}
	for (obj_cnt = 0; obj_cnt < (audio_device_nb + video_device_nb); obj_cnt++ ){
		DvbContext.PlaybackDeviceContext[obj_cnt].Id = obj_cnt;
		mutex_init(&DvbContext.PlaybackDeviceContext[obj_cnt].DemuxWriteLock);
		mutex_init(&DvbContext.PlaybackDeviceContext[obj_cnt].mutex);
	}

	DvbContext.DvbAdapter = &stm_dvb_adapter;
#endif

	for (obj_cnt = 0; obj_cnt < te_caps.max_demuxes; obj_cnt++) {
		stm_dvb_demux_attach(&stm_dvb_adapter,
				     &dvb_demux,
				     te_caps.max_output_filters,
				     te_caps.max_output_filters);
		if (dvb_demux) {
			addObjectToList(&dvb_demux->list, DEMUX);
		} else {
			printk(KERN_ERR "Failed to add demux.%d\n", obj_cnt);
			goto demux_err;
		}

#ifdef CONFIG_STLINUXTV_DECODE_DISPLAY
		dvb_demux->PlaybackContext = NULL;
		dvb_demux->DvbContext = &DvbContext;
#endif
	}


	for (obj_cnt = 0; obj_cnt < te_caps.max_psdemuxes; obj_cnt++) {
		ret = stm_dvb_psdemux_attach(&stm_dvb_adapter,
				     &dvb_demux,
				     te_caps.max_ps_output_filters,
				     te_caps.max_ps_output_filters);
		if (ret) {
			stv_err("Failed to add psdemux.%d\n", obj_cnt);
			goto demux_err;
		} else {
			addObjectToList(&dvb_demux->list, PSDEMUX);
		}

#ifdef CONFIG_STLINUXTV_DECODE_DISPLAY
		dvb_demux->PlaybackContext = NULL;
		dvb_demux->DvbContext = &DvbContext;
#endif
	}

#ifdef CONFIG_STLINUXTV_DECODE_DISPLAY
	for (obj_cnt = 0; obj_cnt < audio_device_nb; obj_cnt++) {
		struct AudioDeviceContext_s *audio;
		char InitialName[32];
		/* Create audio/video devices and initialize them */
		audio = &DvbContext.AudioDeviceContext[obj_cnt];
		audio->Id = obj_cnt;
		audio->StreamType = STREAM_TYPE_TRANSPORT;
		audio->DvbContext = &DvbContext;
		audio->PlaySpeed = DVB_SPEED_NORMAL_PLAY;

		mutex_init(&audio->auddev_mutex);
		mutex_init(&audio->audops_mutex);
		mutex_init(&audio->audwrite_mutex);
		mutex_init(&audio->AudioEvents.Lock);

		dvb_register_device(&stm_dvb_adapter,
				    &audio->AudioDevice,
				    AudioInit(audio),
				    audio, DVB_DEVICE_AUDIO);
		AudioInitSubdev(audio);
		stlinuxtv_stat_init_device(&audio->AudioClassDevice);
		snprintf(InitialName, sizeof(InitialName), "dvb%d.audio%d",
			 DvbContext.DvbAdapter->num, obj_cnt);
		audio->AudioClassDevice.init_name = InitialName;
		ret = device_register(&audio->AudioClassDevice);
		if (ret != 0) {
			printk(KERN_ERR "Unable to register %s stat device\n",
			       InitialName);
		}
#if defined (SDK2_ENABLE_STLINUXTV_STATISTICS)
		ret = AudioInitSysfsAttributes(audio);
		if (ret != 0) {
			printk(KERN_ERR "Uanbel to create attributes for %s\n",
			       InitialName);
		}
#endif
	}

	for (obj_cnt = 0; obj_cnt < video_device_nb; obj_cnt++) {
		struct VideoDeviceContext_s *video;
		char InitialName[32];
		video = &DvbContext.VideoDeviceContext[obj_cnt];
		video->Id = obj_cnt;
		video->StreamType = STREAM_TYPE_TRANSPORT;
		video->DvbContext = &DvbContext;
		video->PlaySpeed = DVB_SPEED_NORMAL_PLAY;

		mutex_init(&video->viddev_mutex);
		mutex_init(&video->vidops_mutex);
		mutex_init(&video->vidwrite_mutex);
		mutex_init(&video->VideoEvents.Lock);
		sema_init(&video->listener_sem, 0);

		dvb_register_device(&stm_dvb_adapter,
				    &video->VideoDevice,
				    VideoInit(video),
				    video, DVB_DEVICE_VIDEO);
		VideoInitSubdev(video);

		stlinuxtv_stat_init_device(&video->VideoClassDevice);
		snprintf(InitialName, sizeof(InitialName), "dvb%d.video%d",
			 DvbContext.DvbAdapter->num, obj_cnt);
		video->VideoClassDevice.init_name = InitialName;
		ret = device_register(&video->VideoClassDevice);
		if (ret != 0) {
			printk(KERN_ERR "Unable to register %s stat device\n",
			       InitialName);
		}

#if defined(SDK2_ENABLE_STLINUXTV_STATISTICS)
		ret = VideoInitSysfsAttributes(video);
		if (ret != 0) {
			printk(KERN_ERR "Uanbel to create attributes for %s\n",
			       InitialName);
		}
#endif
	}
#endif
	return 0;

demux_err:
	printk(KERN_ERR
	       "Conguration called for %d demux devices, only %d could be created\n",
	       te_caps.max_demuxes, obj_cnt);
	return 0;
}

void stm_dvb_exit(void)
{
	struct stm_dvb_demux_s *dvb_demux;
#ifdef CONFIG_STLINUXTV_DECODE_DISPLAY
	struct AudioDeviceContext_s *AudioDeviceContext;
	struct VideoDeviceContext_s *VideoDeviceContext;
	int i;
#endif
	int ret, inst = 0;

#ifdef CONFIG_STLINUXTV_DECODE_DISPLAY
	/* Remove the Audio subdev context */
	for (i = 0; i < DvbContext.audio_dev_nb; i++) {
		AudioDeviceContext = &DvbContext.AudioDeviceContext[i];

#if defined(SDK2_ENABLE_STLINUXTV_STATISTICS)
		/* Remove Audio subdev registrations */
		AudioRemoveSysfsAttributes(AudioDeviceContext);
#endif

		device_unregister(&AudioDeviceContext->AudioClassDevice);

		AudioReleaseSubdev(AudioDeviceContext);

		dvb_unregister_device(AudioDeviceContext->AudioDevice);
	}

	/* Remove the Video subdev context */
	for (i = 0; i < DvbContext.video_dev_nb; i++) {
		VideoDeviceContext = &DvbContext.VideoDeviceContext[i];

		stm_dvb_video_release_src(VideoDeviceContext);

#if defined(SDK2_ENABLE_STLINUXTV_STATISTICS)
		/* Remove Video subdev registrations */
		VideoRemoveSysfsAttributes(VideoDeviceContext);
#endif

		device_unregister(&VideoDeviceContext->VideoClassDevice);

		VideoReleaseSubdev(VideoDeviceContext);

		dvb_unregister_device(VideoDeviceContext->VideoDevice);
	}

	kfree(DvbContext.AudioDeviceContext);

	kfree(DvbContext.VideoDeviceContext);

	kfree(DvbContext.PlaybackDeviceContext);
#endif

	inst = 0;

	do {
		GET_DEMUX_OBJECT(dvb_demux, inst++);

		if (dvb_demux) {
			list_del(&dvb_demux->list);
			ret = stm_dvb_demux_delete(dvb_demux);
			if (ret)
				printk(KERN_ERR
				       "Failed stm_te_demux_delete %d\n", ret);
		} else
			break;
	} while (1);

	inst = 0;
	do {
		GET_PSDEMUX_OBJECT(dvb_demux, inst++);

		if (dvb_demux) {
			list_del(&dvb_demux->list);
			ret = stm_dvb_psdemux_delete(dvb_demux);
			if (ret)
				stv_err("Failed stm_dvb_psdemux_delete %d\n", ret);
		} else
			break;
	} while (1);



	if (0 <= stm_dvb_adapter.num)
		dvb_unregister_adapter(&stm_dvb_adapter);


}
