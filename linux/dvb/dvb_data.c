/************************************************************************
Copyright (C) 2011 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.

Source file name : dvb_data.c

STLinuxTV object list handling code

Date        Modification                                    Name
----        ------------                                    --------

 ************************************************************************/

#include "dvb_data.h"

static struct list_head object_lists[MAX_IDX];

void initDataSets(void)
{
	int idx;
	for (idx = 0; idx < MAX_IDX; idx++) {
		INIT_LIST_HEAD(&object_lists[idx]);
	}
}

void addObjectToList(struct list_head *to_add, DVB_LIST_T list_idx)
{
	if (list_idx < MAX_IDX) {
		list_add_tail(to_add, &object_lists[list_idx]);
	}
}

void removeObjectFromList(struct list_head *to_del, DVB_LIST_T list_idx)
{
	list_del(to_del);
}

void removeObjectFromlistIdx(int idx, DVB_LIST_T list_idx)
{

}

void *getObjectFromlistIdx(int idx, DVB_LIST_T list_idx)
{
	struct list_head *pos;

	if (list_idx >= MAX_IDX) {
		return NULL;
	}

	list_for_each(pos, &object_lists[list_idx]) {
		switch (list_idx) {

		case DEMUX:
			{
				struct stm_dvb_demux_s *dvb_demux;
				dvb_demux =
				    list_entry(pos, struct stm_dvb_demux_s,
					       list);
				if (dvb_demux->demux_id == idx)
					return dvb_demux;
			}
			break;

		case PSDEMUX:
			{
				struct stm_dvb_demux_s *dvb_demux;
				dvb_demux =
				    list_entry(pos, struct stm_dvb_demux_s,
					       list);
				if (dvb_demux->demux_id == idx)
					return dvb_demux;
			}
			break;
		default:
			return NULL;
		}
	}
	return NULL;
}

/**
 * stm_dvb_object_get_by_index
 * This function returns the element from the list based on
 * the position in the list.
 */
void *stm_dvb_object_get_by_index(DVB_LIST_T list_id, int index)
{
	struct list_head *pos;
	void *obj;
	int count = 0;

	if (list_id >= MAX_IDX)
		return NULL;

	list_for_each(pos, &object_lists[list_id]) {
		switch (list_id) {

		case DEMUX:
			obj = list_entry(pos, struct stm_dvb_demux_s, list);
			if (index == count++)
				return obj;

			break;

		case PSDEMUX:
			obj = list_entry(pos, struct stm_dvb_demux_s, list);
			if (index == count++)
				return obj;

			break;

		default:
			return NULL;
		}
	}

	return NULL;
}
