/************************************************************************
Copyright (C) 2011 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.

Source file name : dvb_adaptation.h
Author :           Rahul.V

Header for STLinuxTV package

Date        Modification                                    Name
----        ------------                                    --------
16-Jun-11   Created                                         Rahul.V

 ************************************************************************/

#ifndef _DVB_ADAPTATION_H
#define _DVB_ADAPTATION_H

#include <linux/rwsem.h>

#include <stm_registry.h>
#include <stm_te.h>
#include <dvb_demux.h>
#include <dmxdev.h>
#include <linux/list.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/platform_device.h>
#include <linux/i2c.h>
#include <linux/kthread.h>

#include <linux/platform_device.h>
#include <dvb_frontend.h>

#include <stm_memsink.h>
#include <stm_memsrc.h>

#ifdef CONFIG_STLINUXTV_DECODE_DISPLAY
#include "dvb_module.h"
#endif

#define MAX_NAME_LEN  (31)
#define MAX_NAME_LEN_SIZE (MAX_NAME_LEN + 1)


typedef enum dmx_type_e {
	DVB_STI_TSDEMUX	= 0x0000,
	DVB_STI_PSDEMUX	= 0x0001
}dmx_type_t;

struct stm_dvb_demux_s {
	struct list_head list;
	char dvb_demux_name[MAX_NAME_LEN];
	uint32_t demux_id;
	dmx_pes_type_t pcr_type;
#define DEMUX_SOURCE_FE	0
#define DEMUX_SOURCE_DVR 1
	uint32_t demux_source;
	uint32_t demux_default_source;
	uint32_t demod_id;
	stm_te_object_h demux_object;
	struct dvb_demux dvb_demux;
	struct dmxdev dmxdev;
	struct dmx_frontend hw_frontend;
	struct list_head pes_filter_handle_list;
	struct list_head sec_filter_handle_list;
	struct list_head pcr_filter_handle_list;
	struct list_head ts_filter_handle_list;
	struct list_head index_filter_handle_list;
	struct list_head pid_filter_handle_list;
	uint32_t filter_count;
	stm_memsrc_h memsrc_input;
	bool demux_running;
	struct rw_semaphore rw_sem;
	struct task_struct *read_task;
	struct dmx_frontend *frontend;
	struct file_operations dvr_ops;
	struct file_operations dmx_ops;
	int32_t users;

	/*
	 * @input_type: input stream type, 188 bytes, 192 bytes etc.
	 *              This is stored to make sure that the type is
	 *              reset correctly after injecting marker. Only
	 *              used there.
	 */
	__u32 input_type;

	struct DvbContext_s *DvbContext;
	struct PlaybackDeviceContext_s *PlaybackContext;
	enum dmx_type_e dmx_type;
};

#endif /* _DVB_ADAPTATION_H */
