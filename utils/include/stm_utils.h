/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Configuring and starting dvp capture
************************************************************************/
#ifndef __STM_UTILS_H__
#define __STM_UTILS_H__

#define BLUE		"\033[34m"
#define RED		"\033[31m"
#define YELLOW		"\033[33m"
#define NONE		"\033[0m"
#define BOLDBLACK	"\033[1m\033[30m"

#define util_err(msg, ...)	printf("%sFILE: %s %s(): " msg"%s", RED, __FILE__, __func__, ##__VA_ARGS__, NONE)
#define util_sys_err(msg, ...)	printf("%sFILE: %s %s(): (%s) " msg"%s", __FILE__, __func__, RED, strerror(errno), ##__VA_ARGS__, NONE)
#define util_info(msg, ...)	printf("%s"msg"%s", BOLDBLACK, ##__VA_ARGS__, NONE)
#define util_log(msg, ...)	printf("%s"msg"%s", BLUE, ##__VA_ARGS__, NONE)

#endif
