/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * hdmirx capture definitions
************************************************************************/
#ifndef __HDMIRX_CAPTURE_H__
#define __HDMIRX_CAPTURE_H__

#include <stdbool.h>
#include <pthread.h>

#include <linux/v4l2-mediabus.h>
#include <linux/v4l2-subdev.h>

#define EDID_BLOCK_SIZE		128

enum event_type {
	HOTPLUG,
	SIGNAL_STATUS,
	AUDIO,
	VIDEO,
};

struct hdmirx_context {
	bool exit;

	char viddec_name[32];
	char auddec_name[32];
	char plane_name[32];

	pthread_mutex_t vid_mutex, aud_mutex;

	bool debug_info, tunnel;
	bool video_streaming, audio_streaming;

	enum v4l2_mbus_pixelcode dvp_mbus_code;

	struct media_device *media;

	struct media_entity *hdmirx_entity, *dvp_entity;
	struct media_entity *viddec_entity, *auddec_entity;
	struct media_entity *plane_entity;
};

void *keyboard_monitor(void *arg);
void *dvp_grab_monitor(void *arg);
int hdmirx_subscribe_events(enum event_type event,
			struct hdmirx_context *hdmirx_ctx);
int hdmirx_unsubscribe_events(enum event_type event,
			struct hdmirx_context *hdmirx_ctx);

int hdmirx_edid_write_verify(struct v4l2_subdev_edid *set_edid, bool verify);
void hdmi_dump_edid(FILE *file, __u32 blocks, __u8 *edid_data);
void *hdmirx_edid_monitor(void *arg);
#endif
