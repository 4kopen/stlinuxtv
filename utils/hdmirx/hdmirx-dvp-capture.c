/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * DVP grab from user space
************************************************************************/
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdbool.h>
#include <semaphore.h>
#include <poll.h>

#include <linux/videodev2.h>
#include <mediactl/mediactl.h>

#include "linux/stm_v4l2_hdmi_export.h"
#include "stm_v4l2_capture.h"
#include "stm_v4l2_output.h"
#include "hdmirx-capture.h"
#include "stm_utils.h"

#define DVP_CAPTURE_BUF_COUNT		1
#define DVP_CAPTURE_WIDTH		600
#define DVP_CAPTURE_HEIGHT		800
#define DVP_CAPTURE_PIXFMT		V4L2_PIX_FMT_BGR24
#define DVP_CAPTURE_COLORSPACE		V4L2_COLORSPACE_SRGB
#define DVP_CAPTURE_FIELD		V4L2_FIELD_NONE
#define DVP_CAPTURE_PIXEL_SIZE		3 /* V4L2_PIX_FMT_BGR24 */

enum dvp_command {
	DVP_S_INPUT,
	DVP_S_FMT,
	DVP_STREAMON,
	DVP_STREAMOFF,
};

struct buf_metadata {
	void *ptr;
	int length;
};

struct dvp_data {
	sem_t sem;
	bool streaming;
	int fd, disp_fd;
	struct buf_metadata *meta;
	pthread_t dvp_streamon_thread;
	struct hdmirx_context *hdmirx_ctx;
};

static int dvp_buf_num = 1, display_buf_num = 2;

int setup_mmap_buffers(int fd, int count, struct buf_metadata *meta)
{
	int i, ret;
	struct v4l2_buffer buf;

	for (i = 0; i < count; i++) {
		memset(&buf, 0, sizeof(buf));
		buf.index = i;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
		ret = ioctl(fd, VIDIOC_QUERYBUF, &buf);
		if (ret) {
			util_err("Failed to querybuf display at index %d\n", i);
			goto setup_done;
		}
		meta[i].ptr = mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buf.m.offset);
		if (meta[i].ptr == MAP_FAILED) {
			util_err("Failed to mmap region for dvp capture\n");
			ret = errno;
			goto mmap_failed;
		}
		meta[i].length = buf.length;
	}

	return 0;

mmap_failed:
	for (i = i - 1; i > 0;)
		munmap(meta[i].ptr, meta[i].length);
setup_done:
	return ret;
}

static void *dvp_streamon_monitor(void *arg)
{
	int i, count, ret;
	struct v4l2_buffer dvp_buf;
	struct dvp_data *dvp_data = arg;
	struct buf_metadata *meta = dvp_data->meta;

	count = dvp_buf_num > display_buf_num ? dvp_buf_num : display_buf_num;

	memset(&dvp_buf, 0, sizeof(dvp_buf));
	dvp_buf.index = 0;
	dvp_buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	dvp_buf.field = DVP_CAPTURE_FIELD;
	dvp_buf.memory = V4L2_MEMORY_MMAP;
	dvp_buf.length = meta[0].length;

	/*
	 * Queue one buffer to display, so, that the first captured frame is displayed
	 */
	ret = ioctl(dvp_data->disp_fd, VIDIOC_QBUF, &dvp_buf);
	if (ret) {
		util_err("Failed to start display pipe\n");
		goto qbuf_failed;
	}

	/*
	 * Let's rock and roll
	 */
	i = 1;
	dvp_data->streaming = true;
	while (dvp_data->streaming) {
		/*
		 * Start capturing data
		 */
		dvp_buf.index = 0;
		dvp_buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		dvp_buf.field = DVP_CAPTURE_FIELD;
		dvp_buf.memory = V4L2_MEMORY_USERPTR;
		dvp_buf.m.userptr = (unsigned long)meta[i].ptr;
		dvp_buf.length = meta[i].length;
		ret = ioctl(dvp_data->fd, VIDIOC_QBUF, &dvp_buf);
		if (ret) {
			util_err("Failed to qbuf to dvp\n");
			goto qbuf_failed;
		}
		ret = ioctl(dvp_data->fd, VIDIOC_DQBUF, &dvp_buf);
		if (ret) {
			util_err("Failed to dqbuf from dvp\n");
			goto qbuf_failed;
		}

		/*
		 * Start displaying data
		 */
		dvp_buf.index = i;
		dvp_buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
		dvp_buf.memory = V4L2_MEMORY_MMAP;
		memset(&dvp_buf.m, 0, sizeof(dvp_buf.m));
		memset(&dvp_buf.timestamp, 0, sizeof(dvp_buf.timestamp));
		ret = ioctl(dvp_data->disp_fd, VIDIOC_QBUF, &dvp_buf);
		if (ret) {
			util_err("Failed to queue to display\n");
			goto qbuf_failed;
		}
		ret = ioctl(dvp_data->disp_fd, VIDIOC_DQBUF, &dvp_buf);
		if (ret) {
			util_err("Failed to dqbuf from display\n");
			goto qbuf_failed;
		}
		i = dvp_buf.index;
	}

qbuf_failed:
	for (i = 0; i < count; i++)
		munmap(meta[i].ptr, meta[i].length);
	sem_post(&dvp_data->sem);
	return NULL;
}

/**
 * dvp_s_fmt() - setup dvp capture and streaming
 */
static int dvp_s_fmt(struct dvp_data *dvp_data)
{
	int ret;
	struct v4l2_format fmt;
	struct hdmirx_context *hdmirx_ctx = dvp_data->hdmirx_ctx;

	memset(&fmt, 0, sizeof(fmt));
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width = DVP_CAPTURE_WIDTH;
	fmt.fmt.pix.height = DVP_CAPTURE_HEIGHT;
	fmt.fmt.pix.pixelformat = DVP_CAPTURE_PIXFMT;
	fmt.fmt.pix.colorspace = DVP_CAPTURE_COLORSPACE;
	fmt.fmt.pix.field = DVP_CAPTURE_FIELD;
	fmt.fmt.pix.bytesperline = DVP_CAPTURE_WIDTH * DVP_CAPTURE_PIXEL_SIZE;
	ret = stm_v4l2_capture_streamon(dvp_data->fd, dvp_buf_num, V4L2_MEMORY_USERPTR, &fmt);
	if (ret) {
		util_err("Failed to setup dvp fmt and streamon\n");
		goto exit;
	}

	/*
	 * Setup the stmvout driver for displaying the captured data
	 */
	dvp_data->disp_fd = stm_v4l2_setup_output("Planes", hdmirx_ctx->plane_name);
	if (dvp_data->disp_fd < 0) {
		util_err("Failed to find plane for displaying data\n");
		goto exit;
	}

	memset(&fmt, 0, sizeof(fmt));
	fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	fmt.fmt.pix.width = DVP_CAPTURE_WIDTH;
	fmt.fmt.pix.height = DVP_CAPTURE_HEIGHT;
	fmt.fmt.pix.pixelformat = DVP_CAPTURE_PIXFMT;
	fmt.fmt.pix.colorspace = DVP_CAPTURE_COLORSPACE;
	fmt.fmt.pix.field = DVP_CAPTURE_FIELD;
	fmt.fmt.pix.bytesperline = DVP_CAPTURE_WIDTH * DVP_CAPTURE_PIXEL_SIZE;
	ret = stm_v4l2_output_streamon(dvp_data->disp_fd, display_buf_num, V4L2_MEMORY_MMAP, &fmt);
	if (ret) {
		util_err("Failed to setup and streamon the plane\n");
		goto exit;
	}

	/*
	 * mmap the address from display driver to be passed to dvp-capture
	 */
	ret = setup_mmap_buffers(dvp_data->disp_fd, display_buf_num, dvp_data->meta);
	if (ret)
		util_err("Failed to setup buffers for dvp capture\n");

exit:
	return ret;
}

/**
 * dvp_grab_process_cmd() - dvp monitor to manage the capture commands
 */
static int dvp_grab_process_cmd(struct dvp_data *dvp_data, __u32 command)
{
	int ret = 0;

	switch (command) {
	case DVP_S_INPUT:
	{
		int count;

		/*
		 * Set the input for dvp capture
		 */
		dvp_data->fd = stm_v4l2_capture_set_input("DVP", "stm_input_hdmirx");
		if (dvp_data->fd < 0) {
			util_err("Failed to set dvp capture input\n");
			ret = errno;
			break;
		}

		/*
		 * Allocate all the buffers context
		 */
		count = dvp_buf_num > display_buf_num ? dvp_buf_num : display_buf_num;
		dvp_data->meta = malloc(sizeof(*(dvp_data->meta)) * count);
		if (!dvp_data->meta) {
			util_err("alloc failed for metadata\n");
			ret = -ENOMEM;
		}

		break;
	}

	case DVP_S_FMT:
		ret = dvp_s_fmt(dvp_data);
		if (ret)
			util_err("Failed to setup dvp for capture\n");

		break;

	case DVP_STREAMON:
		/*
		 * Create a dvp capture and streamer thread
		 */
		ret = pthread_create(&dvp_data->dvp_streamon_thread, NULL,
						dvp_streamon_monitor, dvp_data);
		if (ret) {
			util_err("Failed to create dvp streamer thread\n");
			break;
		}

		break;

	case DVP_STREAMOFF:
	{
		enum v4l2_buf_type buf_type;
		if (!dvp_data->streaming)
			break;

		dvp_data->streaming = false;
		buf_type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
		ret = ioctl(dvp_data->disp_fd, VIDIOC_STREAMOFF, &buf_type);
		if (ret)
			util_err("Failed to streamoff display\n");
		buf_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		ret = ioctl(dvp_data->fd, VIDIOC_STREAMOFF, &buf_type);
		if (ret)
			util_err("Failed to streamoff dvp capture\n");
		sem_wait(&dvp_data->sem);
		break;
	}
	}

	return ret;
}

/**
 * dvp_grab_monitor() - dvp capture thread
 */
void *dvp_grab_monitor(void *arg)
{
	int ret;
	struct v4l2_event event;
	struct dvp_data dvp_data;
	struct pollfd hdmirx_pollfd;
	struct hdmirx_context *hdmirx_ctx = arg;
	struct media_entity *hdmirx_entity = hdmirx_ctx->hdmirx_entity;

	/*
	 * Subscribe to hdmirx events
	 */
	ret = hdmirx_subscribe_events(HOTPLUG, hdmirx_ctx);
	if (ret) {
		util_err("Failed to subscribe to HPD event\n");
		goto hpd_subs_failed;
	}
	ret = hdmirx_subscribe_events(VIDEO, hdmirx_ctx);
	if (ret) {
		util_err("Failed to subscribe to video event\n");
		goto video_subs_failed;
	}

	/*
	 * Setup input of DVP
	 */
	memset(&dvp_data, 0, sizeof(dvp_data));
	sem_init(&dvp_data.sem, 0, 0);
	dvp_data.hdmirx_ctx = hdmirx_ctx;
	ret = dvp_grab_process_cmd(&dvp_data, DVP_S_INPUT);
	if (ret) {
		util_err("Failed to set dvp input\n");
		goto s_input_failed;
	}

	/*
	 * HDMIRx status and dvp grab monitor
	 */
	while (!hdmirx_ctx->exit) {
		memset(&hdmirx_pollfd, 0, sizeof(hdmirx_pollfd));
		hdmirx_pollfd.fd = hdmirx_entity->fd;
		hdmirx_pollfd.events = POLLPRI;

		if (poll(&hdmirx_pollfd, 1, 100)) {

			if (hdmirx_pollfd.revents & POLLERR) {
				goto poll_failed;
			}
		} else
			continue;

		memset(&event, 0, sizeof(event));
		ret = ioctl(hdmirx_entity->fd, VIDIOC_DQEVENT, &event);
		if (ret) {
			util_err("Failed to dq hdmirx event\n");
			continue;
		}

		/*
		 * Process hotplug event. If cable out, then stop the
		 * capture immediately
		 */
		if (event.type == V4L2_EVENT_STI_HOTPLUG) {
			if (event.u.data[0] == 1) {
				util_log("Hotplug detected: Status IN\n");
				continue;
			}

			util_log("HPD Low: Stop DVP Streaming ...\n");
			dvp_grab_process_cmd(&dvp_data, DVP_STREAMOFF);
			continue;
		}

		/*
		 * Process video property change event
		 */
		if ((event.id == 1) && (event.type == V4L2_EVENT_SOURCE_CHANGE) &&
			(event.u.src_change.changes != V4L2_EVENT_SRC_CH_RESOLUTION)) {
			util_log("No Active Video: Stop DVP Streaming ...\n");
			dvp_grab_process_cmd(&dvp_data, DVP_STREAMOFF);
			continue;
		}

		if ((event.id == 1) && (event.type == V4L2_EVENT_SOURCE_CHANGE) &&
			(event.u.src_change.changes == V4L2_EVENT_SRC_CH_RESOLUTION)) {
			util_log("Active Video: Start DVP Streaming ...\n");
			dvp_grab_process_cmd(&dvp_data, DVP_S_FMT);
			dvp_grab_process_cmd(&dvp_data, DVP_STREAMON);
			continue;
		}
	}

poll_failed:
	dvp_grab_process_cmd(&dvp_data, DVP_STREAMOFF);
s_input_failed:
	hdmirx_unsubscribe_events(VIDEO, hdmirx_ctx);
video_subs_failed:
	hdmirx_unsubscribe_events(HOTPLUG, hdmirx_ctx);
hpd_subs_failed:
	return NULL;
}
