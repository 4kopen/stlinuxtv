/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * HDMIRx edid repeater implementation
************************************************************************/
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <linux/videodev2.h>
#include <linux/v4l2-subdev.h>
#include <mediactl/mediactl.h>

#include "linux/kernel/drivers/stm/hdmi/stmhdmi.h"

#include "hdmirx-capture.h"

#define HDMITX_EDID_MAX_BLOCK			5
#define HDMITX_EDID_MAX_EXTENDED_BLOCKS		(HDMITX_EDID_MAX_BLOCK - 1)
#define HDMIRX_PORT_ID				1
#define SRC_ADDR_SIZE				2

/*
 * @src_addr_idx     : used to store default src addr index in VSDB of HDMIRx
 * @hdmirx_edid_data : stores the default EDID, used to flash new src addr
 * @src_addr         : stores the default source address (boot up one) for restore
 */
struct hdmirx_edid_ctx {
	int hdmitx_fd;

	int src_addr_idx;
	__u8 *hdmirx_edid_data, src_addr[SRC_ADDR_SIZE];

	struct hdmirx_context *hdmirx_ctx;
};

static char debug_level;
static struct hdmirx_edid_ctx edid_ctx;

/**
 * hdmirx_print_src_addr() - print the physical address
 */
static void hdmirx_print_src_addr(__u8 physical_address[])
{
	int i;

	for (i = 0; i < SRC_ADDR_SIZE; i++) {

		printf("%d.", (physical_address[i] & 0xF0) >> 4);

		if (i == SRC_ADDR_SIZE - 1)
			printf("%d", physical_address[i] & 0x0F);
		else
			printf("%d.", physical_address[i] & 0x0F);
	}
}

/**
 * hdmi_edid_get_src_addr_idx() - return index of src addr from EDID
 */
static int hdmi_edid_get_src_addr_idx(__u8 *edid_block)
{
	int block, idx;
	__u8 *ptr = edid_block + EDID_BLOCK_SIZE;

	for (block = 1; block <= edid_block[0x7E]; block++) {

		for (idx = 0; idx < 124; idx++) {

			/*
			 * Particular format to be present to identify src address
			 */
			if ((ptr[idx] == 0x03) &&
					(ptr[idx + 1] == 0x0c) &&
					(ptr[idx + 2] == 0x0))

				return (block * EDID_BLOCK_SIZE) +  (idx + 3);
		}
	}

	return -1;
}

/**
 * hdmi_edid_compute_checksum() - calculate checksum of edid block
 */
static void hdmi_edid_compute_checksum(__u8 *edid_block)
{
	int i;
	unsigned char checksum = 0;

	for (i = 0; i < EDID_BLOCK_SIZE - 1; i++)
		checksum += edid_block[i];

	checksum = (0x100 - checksum);

	if (debug_level)
		printf("Old checksum = 0x%02x, New checksum = 0x%02x\n",
					edid_block[EDID_BLOCK_SIZE - 1], checksum);

	edid_block[EDID_BLOCK_SIZE - 1] = checksum;
}

/**
 * hdmi_edid_verify_checksum() - verify checksum of edid block
 */
static int hdmi_edid_verify_checksum(__u8 *edid_block)
{
	int i, ret = 0;
	unsigned char checksum = 0;

	for (i = 0; i < EDID_BLOCK_SIZE; i++)
		checksum += edid_block[i];

	if(checksum != 0)
		ret = -EINVAL;

	return ret;
}

/**
 * hdmitx_read_edid() - read edid from HDMITx
 */
int hdmitx_read_edid(__u8 *hdmitx_edid)
{
	off_t offset;
	int block, ret=0;

	/*
	 * Read EDID of Block0
	 */
	ret = pread(edid_ctx.hdmitx_fd, hdmitx_edid, EDID_BLOCK_SIZE, 0);
	if(ret < 0) {
		if (errno != EAGAIN)
			printf("%s(): failed to read HDMITx EDID block0\n", __func__);
		goto exit;
	}

	/*
	 * Verify block0 checksum
	 */
	ret = hdmi_edid_verify_checksum(hdmitx_edid);
	if (ret) {
		printf("%s(): checksum failed for HDMITx EDID block0\n", __func__);
		goto exit;
	}

	/*
	 * Extract extended block number
	 */
	if (hdmitx_edid[0x7E] > HDMITX_EDID_MAX_EXTENDED_BLOCKS) {
		printf("%s(): Max %d extended blocks supported\n",
					__func__, HDMITX_EDID_MAX_EXTENDED_BLOCKS);
		ret = -EINVAL;
		goto exit;
	}

	/*
	 * Read the remaining extended blocks
	 */
	for (block = 1; block <= hdmitx_edid[0x7E]; block++) {

		offset = EDID_BLOCK_SIZE * block;
		ret = pread(edid_ctx.hdmitx_fd, hdmitx_edid + offset,
						EDID_BLOCK_SIZE, offset);
		if(ret < 0) {
			printf("%s(): failed to read extended block %d\n", __func__, block);
			goto exit;
		}

		ret = hdmi_edid_verify_checksum(hdmitx_edid + (EDID_BLOCK_SIZE * block));
		if (ret) {
			printf("%s(): checksum failed for extended block %d\n", __func__, block);
			goto exit;
		}
	}

	if (debug_level) {
		printf("==================================\n");
		printf("Connected Display EDID from HDMITx\n");
		printf("==================================\n");
		hdmi_dump_edid(stdout, hdmitx_edid[126] + 1, hdmitx_edid);
		printf("==================================\n");
	}

exit:
	return ret;
}

/**
 * hdmitx_edid_get_stb_phys_addr() - Get physical address from edid block
 */
int hdmitx_edid_get_stb_phys_addr(__u8 *edid_block, __u8 physical_address[])
{
	int ret = 0, idx;

	/*
	 * Get the src index starting from the beginning of block
	 */
	idx = hdmi_edid_get_src_addr_idx(edid_block);
	if(idx < 0) {
		printf("%s(): no extended block contained the phys add\n", __func__);
		ret = -EINVAL;
		goto g_phys_failed;
	}

	physical_address[0] = edid_block[idx];
	physical_address[1] = edid_block[idx + 1];

	printf("Physical address of this STB: ");
	hdmirx_print_src_addr(physical_address);
	printf("\n");

g_phys_failed:
	return ret;
}

/**
 * hdmirx_update_src_addr() - update the source address of STB
 */
static void hdmirx_update_src_addr(__u8 stb_addr[], __u8 stb_src_addr[])
{
	/*
	 * The logic for manipulation of src address is:
	 * a. Check each nibble for non-zero value
	 * b. If each nibble is non-zero and then src addr = 0xFF
	 * c. else increment the zero nibble by 1
	 */
	stb_src_addr[0] = stb_addr[0];
	stb_src_addr[1] = stb_addr[1];
	if (!(stb_addr[0] & 0x0F))
		stb_src_addr[0] |= HDMIRX_PORT_ID;
	else if (!(stb_addr[1] & 0xF0))
		stb_src_addr[1] |= (HDMIRX_PORT_ID << 4);
	else if (!(stb_addr[1] & 0x0F))
		stb_src_addr[1] |= HDMIRX_PORT_ID;
	else {
		stb_src_addr[0] = 0xFF;
		stb_src_addr[1] = 0xFF;
	}
}

/**
 * hdmirx_update_edid() - update EDID of HDMIRx
 */
static inline int hdmirx_update_edid(__u8 stb_src_addr[])
{
	int ret, src_idx, block = 0;
	struct v4l2_subdev_edid edid;

	edid_ctx.hdmirx_edid_data[edid_ctx.src_addr_idx] = stb_src_addr[0];
	edid_ctx.hdmirx_edid_data[edid_ctx.src_addr_idx + 1] = stb_src_addr[1];

	printf("New source address from this STB: ");
	hdmirx_print_src_addr(stb_src_addr);
	printf("\n");

	/*
	 * Compute checksum for the block which is being updated
	 */
	src_idx = edid_ctx.src_addr_idx;
	while ((src_idx -= EDID_BLOCK_SIZE) > 0)
		block++;

	hdmi_edid_compute_checksum(edid_ctx.hdmirx_edid_data
					+ (block * EDID_BLOCK_SIZE));
	ret = hdmi_edid_verify_checksum(edid_ctx.hdmirx_edid_data
					+ (block * EDID_BLOCK_SIZE));
	if (ret) {
		printf("%s(): Wrong checksum in hdmirx edid block %d\n", __func__, block);
		goto update_failed;
	}

	if (debug_level) {
		printf("============================\n");
		printf("EDID to be written on HDMIRx\n");
		printf("============================\n");
		hdmi_dump_edid(stdout, edid_ctx.hdmirx_edid_data[0x7E] + 1,
							edid_ctx.hdmirx_edid_data);
		printf("=============================\n");
	}

	memset(&edid, 0, sizeof(edid));
	edid.blocks = edid_ctx.hdmirx_edid_data[0x7E] + 1;
	edid.edid = edid_ctx.hdmirx_edid_data;
	ret = hdmirx_edid_write_verify(&edid, 1);
	if (ret)
		printf("%s(): failed to update EDID\n", __func__);

update_failed:
	return ret;
}

/**
 * hdmirx_process_edid() - process HDMITx EDID for HDMIRx
 */
static int hdmirx_process_edid()
{
	int ret;
	__u8 stb_addr[SRC_ADDR_SIZE], stb_src_addr[SRC_ADDR_SIZE];
	__u8 hdmitx_edid[HDMITX_EDID_MAX_BLOCK * EDID_BLOCK_SIZE];

	/*
	 * Read HDMITx EDID
	 */
	memset(&hdmitx_edid, 0, sizeof(hdmitx_edid));
	ret = hdmitx_read_edid(hdmitx_edid);
	if (ret) {
		if (errno != EAGAIN)
			printf("%s(): failed to read hdmitx edid\n", __func__);
		goto read_failed;
	}

	/*
	 * Get the src address of this STB from display EDID
	 */
	ret = hdmitx_edid_get_stb_phys_addr(hdmitx_edid, stb_addr);
	if (ret) {
		printf("%s(): failed to get display phys addr\n", __func__);
		goto read_failed;
	}

	/*
	 * Update HDMIRx EDID with new physical address
	 */
	hdmirx_update_src_addr(stb_addr, stb_src_addr);
	ret = hdmirx_update_edid(stb_src_addr);
	if (ret)
		printf("%s(): failed to update hdmirx edid with src addr\n", __func__);

read_failed:
	return ret;
}

/**
 * hdmirx_setup_edid_manager() - initialize the context for edid manager
 */
static int hdmirx_setup_edid_manager()
{
	int ret;
	char hdmitx_path[32];
	struct v4l2_subdev_edid hdmirx_edid;
	struct media_entity *hdmirx_entity = edid_ctx.hdmirx_ctx->hdmirx_entity;

	/*
	 * Open HDMITx device
	 */
	snprintf(hdmitx_path, sizeof(hdmitx_path), "/dev/hdmi0.0");
	edid_ctx.hdmitx_fd = open(hdmitx_path, O_RDONLY | O_NONBLOCK);
	if (edid_ctx.hdmitx_fd < 0) {
		printf("%s(): failed to open hdmitx device\n", __func__);
		ret = errno;
		goto hdmitx_open_failed;
	}

	/*
	 * Store the original EDID of box. This will be restored once
	 * the connected display asserts HPD low
	 */
	memset(&hdmirx_edid, 0, sizeof(hdmirx_edid));
	hdmirx_edid.blocks = 1;
	hdmirx_edid.edid = malloc(hdmirx_edid.blocks * EDID_BLOCK_SIZE);
	if (!hdmirx_edid.edid) {
		printf("%s(): out of memory for edid data\n", __func__);
		ret = -ENOMEM;
		goto alloc_failed;
	}
	ret = ioctl(hdmirx_entity->fd, VIDIOC_SUBDEV_G_EDID, &hdmirx_edid);
	if (ret) {
		printf("%s(): failed to read edid from HDMIRx\n", __func__);
		goto g_edid_failed;
	}

	/*
	 * Check the number of blocks present
	 */
	if ((hdmirx_edid.edid[0x7E] + 1) == hdmirx_edid.blocks) {
		printf("%s(): no VSDB present in the default EDID\n", __func__);
		ret = -EINVAL;
		goto g_edid_failed;
	}

	/*
	 * Fetch all the blocks
	 */
	hdmirx_edid.blocks = (hdmirx_edid.edid[0x7E] + 1);
	hdmirx_edid.edid = realloc(hdmirx_edid.edid, hdmirx_edid.blocks * EDID_BLOCK_SIZE);
	if (!hdmirx_edid.edid) {
		printf("%s(): failed to realloc to available blocks\n", __func__);
		goto g_edid_failed;
	}
	ret = ioctl(hdmirx_entity->fd, VIDIOC_SUBDEV_G_EDID, &hdmirx_edid);
	if (ret) {
		printf("%s(): failed to re-read edid from HDMIRx\n", __func__);
		goto g_edid_failed;
	}

	edid_ctx.hdmirx_edid_data = hdmirx_edid.edid;

	/*
	 * Src default address, when the box is acting as CEC root
	 * A.B.C.D (1.0.0.0)
	 */
	edid_ctx.src_addr_idx = hdmi_edid_get_src_addr_idx(hdmirx_edid.edid);
	edid_ctx.src_addr[0] = hdmirx_edid.edid[edid_ctx.src_addr_idx];
	edid_ctx.src_addr[1] = hdmirx_edid.edid[edid_ctx.src_addr_idx + 1];

	printf("Boot up src address from STB: ");
	hdmirx_print_src_addr(edid_ctx.src_addr);
	printf("\n");

	return 0;

g_edid_failed:
	free(hdmirx_edid.edid);
alloc_failed:
	close(edid_ctx.hdmitx_fd);
hdmitx_open_failed:
	return ret;
}

/**
  * hdmirx_edid_monitor() - monitor HPD and update HDMIRx EDID
  */
void *hdmirx_edid_monitor(void *arg)
{
	int ret, f_flags;
	static bool isedidupdated = false;
	struct hdmi_event hdmitx_event;
	struct hdmirx_context *hdmirx_ctx = arg;
	struct hdmi_event_subscription evt_subs = {0};

	/*
	 * Setup EDID manager
	 */
	edid_ctx.hdmirx_ctx = hdmirx_ctx;
	debug_level = hdmirx_ctx->debug_info;
	ret = hdmirx_setup_edid_manager();
	if (ret) {
		printf("%s(): failed to setup EDID manager\n", __func__);
		goto setup_failed;
	}

	/*
	 * Subscribe to HDMITx events
	 */
	evt_subs.type = HDMI_EVENT_DISPLAY_CONNECTED |
			HDMI_EVENT_DISPLAY_DISCONNECTED;
	ret = ioctl(edid_ctx.hdmitx_fd, STMHDMIIO_SUBSCRIBE_EVENT, &evt_subs);
	if (ret) {
		printf("%s(): failed to subscribe to display connection event\n", __func__);
		goto hdmitx_event_subs_failed;
	}

	/*
	 * Try reading the EDID and process for repeater. If we have EDID then,
	 * we setup the STB as repeater else, we go into the state machine
	 * waiting for the event, as it is not reported until there's an
	 * interrupt
	 */
	ret = hdmirx_process_edid();
	if (ret && errno != EAGAIN) {
		printf("%s(): failed to process for EDID repeater\n", __func__);
		goto edid_process_failed;
	} else
		isedidupdated = true;

	/*
	 * Change the mode to blocking, as we want to block on event
	 */
	f_flags = fcntl(edid_ctx.hdmitx_fd, F_GETFL);
	if (f_flags == -1) {
		ret = errno;
		printf("%s(): failed to get the hdmitx status flags\n", __func__);
		goto edid_process_failed;
	}

	ret = fcntl(edid_ctx.hdmitx_fd, F_SETFL, f_flags & ~O_NONBLOCK);
	if (ret) {
		printf("%s(): failed to set the fd to block mode\n", __func__);
		goto edid_process_failed;
	}

	/*
	 * Monitor events and process them
	 */
	while(!hdmirx_ctx->exit) {

		/*
		 * DQ the event
		 */
		memset(&hdmitx_event, 0, sizeof(hdmitx_event));
		ret = ioctl(edid_ctx.hdmitx_fd, STMHDMIIO_DQEVENT, &hdmitx_event);
		if(ret) {
			printf("%s(): failed to DQ event\n", __func__);
			break;
		}

		/*
		 * Process the events
		 */
		switch (hdmitx_event.type) {
		case HDMI_EVENT_DISPLAY_CONNECTED:

			switch(hdmitx_event.u.data[0]) {
			case HDMI_SINK_IS_HDMI:

				printf("HDMITx Hotplug: Display connected is HDMI !!!\n");

				isedidupdated = true;
				ret = hdmirx_process_edid();
				if (ret) {
					printf("%s(): HDMITx EDID processing failed\n", __func__);
					goto edid_process_failed;
				}

				break;

			case HDMI_SINK_IS_DVI:
				printf("HDMITx Hotplug: Display connected is DVI !!!\n");
				break;

			case HDMI_SINK_IS_SAFEMODE_DVI:
			case HDMI_SINK_IS_SAFEMODE_HDMI:
			default:
				printf("HDMITx Hotplug: Display connected is in SAFEMODE !!!\n");
				break;
			}

			break;

		case HDMI_EVENT_DISPLAY_DISCONNECTED:

			printf("HDMITx Hotplug: Display disconnected !!!\n");

			if (!isedidupdated)
				break;

			ret = hdmirx_update_edid(edid_ctx.src_addr);
			if (ret) {
				printf("%s(): failed to write edid\n", __func__);
				goto edid_process_failed;
			}
			isedidupdated = false;

			break;

		default:
			break;
		}
	}

	/*
	 * Restore back the original EDID
	 */
	if (isedidupdated == true)
		hdmirx_update_edid(edid_ctx.src_addr);

edid_process_failed:
	if (ioctl(edid_ctx.hdmitx_fd, STMHDMIIO_UNSUBSCRIBE_EVENT))
		printf("%s(): failed to unsubscribe HDMITx events\n", __func__);
hdmitx_event_subs_failed:
	free(edid_ctx.hdmirx_edid_data);
	close (edid_ctx.hdmitx_fd);
setup_failed:
	return NULL;
}
