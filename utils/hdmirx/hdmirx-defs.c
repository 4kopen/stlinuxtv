/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Common definitions for hdmirx capture
************************************************************************/
#include <stdio.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <mediactl/mediactl.h> /* for struct media_device */
#include <mediactl/v4l2subdev.h>
#include <linux/v4l2-subdev.h>

#include "linux/stm_v4l2_hdmi_export.h"

#include "hdmirx-capture.h"
#include "stm_v4l2_helper.h"

/**
 * hdmirx_subscribe_events() - subscribe to hdmirx subdev events
 * @event_type : enum event_type
 * @hdmirx_ctx : hdmirx parent context
 */
int hdmirx_subscribe_events(enum event_type event,
				struct hdmirx_context *hdmirx_ctx)
{
	int ret;
	char type[16];
	struct v4l2_event_subscription subs;

	/*
	 * Subscribe to hdmirx events
	 * a. Hotplug (pad = 0, means subs.id = 0)
	 * b. Video format change (pad = 1, means subs.id = 1)
	 * c. Audio format change (pad = 2, means subs.id = 2)
	 */

	/*
	 * Subscribe to hdmirx events
	 * a. Hotplug (pad = 0, means subs.id = 0)
	 * b. Video format change (pad = 1, means subs.id = 1)
	 * c. Audio format change (pad = 2, means subs.id = 2)
	 */

	memset(&subs, 0, sizeof(subs));

	switch (event) {
	case HOTPLUG:
		subs.id = 0;
		subs.type = V4L2_EVENT_STI_HOTPLUG;
		subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
		snprintf(type, sizeof(type) - 1, "hotplug");
		break;

	case SIGNAL_STATUS:
		subs.id = 0;
		subs.type = V4L2_EVENT_STI_HDMI_SIG;
		subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
		snprintf(type, sizeof(type) - 1, "status");
		break;

	case AUDIO:
		subs.id = 2;
		subs.type = V4L2_EVENT_SOURCE_CHANGE;
		subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
		snprintf(type, sizeof(type) - 1, "audio");
		break;

	case VIDEO:
		subs.id = 1;
		subs.type = V4L2_EVENT_SOURCE_CHANGE;
		subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
		snprintf(type, sizeof(type) - 1, "video");
		break;
	}

	/*
	 * Do subscribe
	 */
	ret = ioctl(hdmirx_ctx->hdmirx_entity->fd, VIDIOC_SUBSCRIBE_EVENT, &subs);
	if (ret)
		printf("%s(): failed to subscribe to hdmirx %s event\n", __func__, type);

	return ret;
}

/**
 * hdmirx_unsubscribe_events() - unsubscribe to hdmirx subdev events
 * @event_type : enum event_type
 * @hdmirx_ctx : hdmirx parent context
 */
int hdmirx_unsubscribe_events(enum event_type event,
					struct hdmirx_context *hdmirx_ctx)
{
	int ret;
	char type[16];
	struct v4l2_event_subscription subs;

	memset(&subs, 0, sizeof(subs));

	switch (event) {
	case HOTPLUG:
		subs.id = 0;
		subs.type = V4L2_EVENT_STI_HOTPLUG;
		subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
		snprintf(type, sizeof(type) - 1, "hotplug");
		break;

	case SIGNAL_STATUS:
		subs.id = 0;
		subs.type = V4L2_EVENT_STI_HDMI_SIG;
		subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
		snprintf(type, sizeof(type) - 1, "status");
		break;

	case AUDIO:
		subs.id = 2;
		subs.type = V4L2_EVENT_SOURCE_CHANGE;
		subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
		snprintf(type, sizeof(type) - 1, "audio");
		break;

	case VIDEO:
		subs.id = 1;
		subs.type = V4L2_EVENT_SOURCE_CHANGE;
		subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
		snprintf(type, sizeof(type) - 1, "video");
		break;
	}

	ret = ioctl(hdmirx_ctx->hdmirx_entity->fd, VIDIOC_UNSUBSCRIBE_EVENT, &subs);
	if (ret)
		printf("%s(): failed to unsubscribe to hdmirx %s event\n", __func__, type);

	return ret;
}
