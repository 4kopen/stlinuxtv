/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Keyboard thread to accept runtime options
************************************************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <poll.h>
#include <mediactl/mediactl.h> /* for struct media_device */
#include <mediactl/v4l2subdev.h>
#include <linux/v4l2-subdev.h>

#include "linux/stm_v4l2_hdmi_export.h"
#include "linux/stm_v4l2_export.h"
#include "linux/stm_v4l2_decoder_export.h"
#include "hdmirx-capture.h"
#include "stm_v4l2_helper.h"

#define COMMAND_LINE_SIZE	128

enum commands {
	AUDIO_START,
	AUDIO_STOP,
	VIDEO_START,
	VIDEO_STOP,
	WINDOW_RESIZE,
};

/**
 * hdmirx_audio_start() - start audio streaming
 */
static int hdmirx_audio_start(struct hdmirx_context *hdmirx_ctx)
{
	int ret = 0;

	pthread_mutex_lock(&hdmirx_ctx->aud_mutex);

	if (hdmirx_ctx->audio_streaming)
		goto streamon_done;

	ret = hdmirx_subscribe_events(AUDIO, hdmirx_ctx);
	if (ret)
		printf("%s(): failed to subscribe to audio events\n", __func__);

	/*
	 * No need to streamon, as it is event based, once, we subscribe
	 * to the event, the state machine will automatically decide when
	 * to start streaming.
	 */

streamon_done:
	pthread_mutex_unlock(&hdmirx_ctx->aud_mutex);
	return ret;
}

/**
 * hdmirx_audio_stop() - stop audio streaming
 */
static void hdmirx_audio_stop(struct hdmirx_context *hdmirx_ctx)
{
	int ret;

	pthread_mutex_lock(&hdmirx_ctx->aud_mutex);

	if (!hdmirx_ctx->audio_streaming)
		goto streamoff_done;

	hdmirx_unsubscribe_events(AUDIO, hdmirx_ctx);

	ret = ioctl(hdmirx_ctx->auddec_entity->fd,
			VIDIOC_SUBDEV_STI_STREAMOFF, NULL);
	if (ret)
		printf("%s(): failed to streamoff audio\n", __func__);
	else
		hdmirx_ctx->audio_streaming = false;

streamoff_done:
	pthread_mutex_unlock(&hdmirx_ctx->aud_mutex);
}

/**
 * hdmirx_video_start() - start video streaming
 */
static int hdmirx_video_start(struct hdmirx_context *hdmirx_ctx)
{
	int ret = 0;

	pthread_mutex_lock(&hdmirx_ctx->vid_mutex);

	if (hdmirx_ctx->video_streaming)
		goto streamon_done;

	ret = hdmirx_subscribe_events(VIDEO, hdmirx_ctx);
	if (ret)
		printf("%s(): failed to subscribe to video events\n", __func__);

	/*
	 * No need to streamon, as it is event based, once, we subscribe
	 * to the event, the state machine will automatically decide when
	 * to start streaming.
	 */

streamon_done:
	pthread_mutex_unlock(&hdmirx_ctx->vid_mutex);
	return ret;
}

/**
 * hdmirx_video_stop() - stop video streaming
 */
static void hdmirx_video_stop(struct hdmirx_context *hdmirx_ctx)
{
	int ret;

	pthread_mutex_lock(&hdmirx_ctx->vid_mutex);

	if (!hdmirx_ctx->video_streaming)
		goto streamoff_done;

	hdmirx_unsubscribe_events(VIDEO, hdmirx_ctx);

	ret = ioctl(hdmirx_ctx->viddec_entity->fd,
			VIDIOC_SUBDEV_STI_STREAMOFF, NULL);
	if (ret)
		printf("%s(): failed to streamoff audio\n", __func__);
	else
		hdmirx_ctx->video_streaming = false;

streamoff_done:
	pthread_mutex_unlock(&hdmirx_ctx->vid_mutex);
}

/**
 * hdmirx_resize_dvp() - resize the plane
 */
static int hdmirx_resize_dvp(struct hdmirx_context *hdmirx_ctx)
{
	int ret;
	FILE *fp;
	char buffer[COMMAND_LINE_SIZE];
	struct v4l2_mbus_framefmt dvp_fmt;

	/*
	 * Open dvp subdev
	 */
	ret = stm_v4l2_subdev_open(hdmirx_ctx->media,
				"dvp0", &hdmirx_ctx->dvp_entity);
	if (ret) {
		printf("%s(): failed to dvp0 subdev\n", __func__);
		goto resize_done;
	}

	/*
	 * Get the format and modify dimensions
	 */
	memset(&dvp_fmt, 0, sizeof(dvp_fmt));
	ret = v4l2_subdev_get_format(hdmirx_ctx->dvp_entity,
				&dvp_fmt, 1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret) {
		printf("%s(): failed to get dvp format\n", __func__);
		goto resize_done;
	}

	/*
	 * Find out the dvp resize parameters from resize.conf
	 */
	fp = fopen("resize.conf", "r");
	if (!fp) {
		printf("%s(): no resize.conf file present\n", __func__);
		goto resize_done;
	}

	while (fgets(buffer, sizeof(buffer), fp)) {

		char *ptr;

		/*
		 * Skip comments from the conf file
		 */
		if (strchr(buffer, '#'))
			continue;

		ptr = strrchr(buffer, ' ');
		if (!strncmp(buffer, "dvp_width", strlen("dvp_width")))
			sscanf(ptr + 1, "%d", &dvp_fmt.width);
		else if (!strncmp(buffer, "dvp_height", strlen("dvp_height")))
			sscanf(ptr + 1, "%d", &dvp_fmt.height);
	}

	printf("Setting dvp capture resize: (width: %d, height: %d)\n",
						dvp_fmt.width, dvp_fmt.height);

	/*
	 * Set dvp resize info
	 */
	ret = v4l2_subdev_set_format(hdmirx_ctx->dvp_entity,
				&dvp_fmt, 1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret)
		printf("%s(): failed to set dvp resizing info\n", __func__);

resize_done:
	return ret;
}

/**
 * hdmirx_resize_plane() - resize the plane
 */
static int hdmirx_resize_plane(struct hdmirx_context *hdmirx_ctx, char *plane_name)
{
	int ret;
	FILE *fp;
	char buffer[COMMAND_LINE_SIZE];
	struct v4l2_control ctrl;
	struct v4l2_subdev_crop crop;
	struct media_entity *plane_entity = NULL;

	/*
	 * Open plane subdev
	 */
	strncpy(hdmirx_ctx->plane_name, plane_name, sizeof(hdmirx_ctx->plane_name));
	ret = stm_v4l2_subdev_open(hdmirx_ctx->media, plane_name, &plane_entity);
	if (ret) {
		printf("%s(): failed to open plane subdev\n", __func__);
		goto resize_done;
	}
	hdmirx_ctx->plane_entity = plane_entity;

	/*
	 * Move the plane to MANUAL mode
	 */
	memset(&ctrl, 0, sizeof(ctrl));
	ctrl.id = V4L2_CID_STM_OUTPUT_WINDOW_MODE;
	ctrl.value = VCSPLANE_MANUAL_MODE;
	ret = ioctl(hdmirx_ctx->plane_entity->fd, VIDIOC_S_CTRL, &ctrl);
	if (ret) {
		printf("%s(): failed to set %s output to MANUAL mode\n", __func__, plane_name);
		goto set_manual_failed;
	}

	/*
	 * Find out the plane parameters from resize.conf
	 */
	memset(&crop, 0, sizeof(crop));
	fp = fopen("resize.conf", "r");
	if (!fp) {
		printf("%s(): no resize.conf file present\n", __func__);
		goto set_manual_failed;
	}

	while (fgets(buffer, sizeof(buffer), fp)) {

		char *ptr;

		/*
		 * Skip comments from the conf file
		 */
		if (strchr(buffer, '#'))
			continue;

		ptr = strrchr(buffer, ' ');
		if (!strncmp(buffer, "plane_left", strlen("plane_left")))
			sscanf(ptr + 1, "%d", &crop.rect.left);
		else if (!strncmp(buffer, "plane_top", strlen("plane_top")))
			sscanf(ptr + 1, "%d", &crop.rect.top);
		else if (!strncmp(buffer, "plane_width", strlen("plane_width")))
			sscanf(ptr + 1, "%d", &crop.rect.width);
		else if (!strncmp(buffer, "plane_height", strlen("plane_height")))
			sscanf(ptr + 1, "%d", &crop.rect.height);
	}

	printf("Setting window with: (x: %d, y: %d), (width: %d, height: %d)\n",
				crop.rect.left, crop.rect.top, crop.rect.width, crop.rect.height);

	/*
	 * Set cropping on the output window
	 */
	crop.which = V4L2_SUBDEV_FORMAT_ACTIVE;
	crop.pad = 1;
	ret = ioctl(hdmirx_ctx->plane_entity->fd, VIDIOC_SUBDEV_S_CROP, &crop);
	if (ret) {
		printf("%s(): cropping output window failed\n", __func__);
		goto set_manual_failed;
	}

	return 0;

set_manual_failed:
	stm_v4l2_subdev_close(hdmirx_ctx->plane_entity);
	hdmirx_ctx->plane_entity = NULL;
resize_done:
	return ret;
}

/**
 * keyboard_help() - keyboard help
 */
void hdmirx_keyboard_help()
{
	printf("  resize\n");
	printf("   This option applies to both plane and dvp, resizing info comes from resize.conf file\n\n");
	printf("  aud-start\n");
	printf("   Start audio streaming\n\n");
	printf("  aud-stop\n");
	printf("   Stop audio streaming\n\n");
	printf("  vid-start\n");
	printf("   Start video streaming\n\n");
	printf("  vid-stop\n");
	printf("   Stop video streaming\n\n");
}

/**
 * find_command() - finds out if the command is supported
 */
static int find_command(char *command_line)
{
	int command = -1;

	if (!strncmp(command_line, "resize", strlen("resize"))) {
		command = WINDOW_RESIZE;
	} else if(!strncmp(command_line, "aud-start", strlen("aud-start"))) {
		command = AUDIO_START;
	} else if (!strncmp(command_line, "aud-stop", strlen("aud-stop"))) {
		command = AUDIO_STOP;
	} else if (!strncmp(command_line, "vid-start", strlen("vid-start"))) {
		command = VIDEO_START;
	} else if (!strncmp(command_line, "vid-stop", strlen("vid-stop"))) {
		command = VIDEO_STOP;
	}

	return command;
}

/**
 * process_command() - process command
 */
static void process_command(struct hdmirx_context *hdmirx_ctx,
				enum commands command,
				char *command_line,
				int size)
{
	switch (command) {
	case AUDIO_START:
		hdmirx_audio_start(hdmirx_ctx);
		break;

	case AUDIO_STOP:
		hdmirx_audio_stop(hdmirx_ctx);
		break;

	case VIDEO_START:
		hdmirx_video_start(hdmirx_ctx);
		break;

	case VIDEO_STOP:
		hdmirx_video_stop(hdmirx_ctx);
		break;

	case WINDOW_RESIZE:
	{
		char *ptr, *eol;

		ptr = strchr(command_line, '=');
		if (!ptr) {
			printf("%s(): no value specified\n", __func__);
			break;
		}

		ptr += 1;
		while (isblank(*ptr))
			ptr += 1;

		eol = strchr(command_line, '\n');
		*eol = '\0';

		if (!strncmp(ptr, "dvp", strlen("dvp")))
			hdmirx_resize_dvp(hdmirx_ctx);
		else
			hdmirx_resize_plane(hdmirx_ctx, ptr);
		break;
	}
	}
}

/**
 * keyboard_monitor() - keyboard monitor thread
 */
void *keyboard_monitor(void *arg)
{
	int command;
	char command_line[COMMAND_LINE_SIZE];
	struct hdmirx_context *hdmirx_ctx = arg;

	/*
	 * Read the command line and parse it for the options
	 */
	while (fgets(command_line, sizeof(command_line), stdin)) {

		command = find_command(command_line);
		if (command < 0) {
			hdmirx_keyboard_help();
			continue;
		}

		process_command(hdmirx_ctx, command, command_line, sizeof(command_line));
	}

	return NULL;
}
