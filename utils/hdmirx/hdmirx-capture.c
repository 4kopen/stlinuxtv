/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Configuring and starting hdmirx capture
************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <poll.h>
#include <signal.h>
#include <limits.h>
#include <mediactl/mediactl.h> /* for struct media_device */
#include <mediactl/v4l2subdev.h>
#include <linux/v4l2-subdev.h>

#include "linux/stm_v4l2_hdmi_export.h"
#include "linux/stm_v4l2_export.h"
#include "linux/stm_v4l2_decoder_export.h"
#include "hdmirx-edid.h"
#include "hdmirx-capture.h"
#include "stm_v4l2_helper.h"

#define EDID_READ_BLOCKS	2

static struct hdmirx_context hdmirx_ctx;
static pthread_t hdmirx_thread, keyboard_thread, dvp_grab_thread, hdmirx_edid_thread;

/**
 * hdmi_dump_edid() - dump edid data
 */
void hdmi_dump_edid(FILE *file, __u32 blocks, __u8 *edid_data)
{
	int i, j;

	for (i = 0; i < blocks; i++) {
		unsigned char *ptr = &edid_data[i * 128];
		fprintf(file, "--> Data for block: %d <--\n", i);
		for (j = 0 ; j < 128;) {
			fprintf(file, "0x%x \t 0x%x \t  0x%x \t  0x%x \t 0x%x \t 0x%x \t 0x%x \t 0x%x\n",
				ptr[j], ptr[j+1], ptr[j+2], ptr[j+3], ptr[j+4], ptr[j+5], ptr[j+6], ptr[j+7]);
			j += 8;
		}
	}
}

/**
 * hdmirx_edid_read_test() - edid read test
 */
static int hdmirx_edid_read_test()
{
	FILE *dump_file;
	int ret = 0;
	struct v4l2_subdev_edid edid;
	struct media_entity *hdmirx_entity = hdmirx_ctx.hdmirx_entity;

	/*
	 * Open the file where to dump edid data
	 */
	dump_file = fopen("edid.dump", "w+");
	if (!dump_file) {
		printf("%s(): failed to open edid dump file\n", __func__);
		ret = errno;
		goto file_open_failed;
	}

	/*
	 * Allocate memory for reading 'n' number of blocks
	 */
	memset(&edid, 0, sizeof(edid));
	edid.pad = 0;
	edid.blocks = EDID_READ_BLOCKS;
	edid.start_block = 0;
	edid.edid = malloc(edid.blocks * EDID_BLOCK_SIZE);
	if (!edid.edid) {
		printf("%s(): out of memory for edid data\n", __func__);
		ret = -ENOMEM;
		goto alloc_failed;
	}

	/*
	 * Get EDID from block 0
	 */
	ret = ioctl(hdmirx_entity->fd, VIDIOC_SUBDEV_G_EDID, &edid);
	if (ret) {
		printf("%s(): failed to retrieve edid\n", __func__);
		goto g_edid_failed;
	}

	/*
	 * Dump EDID to the file
	 */
	fprintf(dump_file, "--------------------------------------------------------------------------\n");
	fprintf(dump_file, "Test Case 1\n");
	fprintf(dump_file, "Input Parameters : start_block = %d, blocks = %d\n", edid.start_block, EDID_READ_BLOCKS);
	fprintf(dump_file, "Output Parameters: start_block: %d, blocks = %d\n", edid.start_block, edid.blocks);
	fprintf(dump_file, "--------------------------------------------------------------------------\n");
	hdmi_dump_edid(dump_file, edid.blocks, edid.edid);

	/*
	 * Get EDID from block 1
	 */
	edid.start_block = 1;
	ret = ioctl(hdmirx_entity->fd, VIDIOC_SUBDEV_G_EDID, &edid);
	if (ret) {
		printf("%s(): failed to retrieve edid %s\n", __func__, strerror(errno));
		printf("%s(): failed to retrieve edid\n", __func__);
		goto g_edid_failed;
	}

	/*
	 * Dump EDID to the file
	 */
	fprintf(dump_file, "--------------------------------------------------------------------------\n");
	fprintf(dump_file, "Test Case 2\n");
	fprintf(dump_file, "Input Parameters : start_block = %d, blocks = %d\n", edid.start_block, EDID_READ_BLOCKS);
	fprintf(dump_file, "Output Parameters: start_block: %d, blocks = %d\n", edid.start_block, edid.blocks);
	fprintf(dump_file, "--------------------------------------------------------------------------\n");
	hdmi_dump_edid(dump_file, edid.blocks, edid.edid);

g_edid_failed:
	free(edid.edid);
alloc_failed:
	fclose(dump_file);
file_open_failed:
	return ret;
}

/**
 * hdmirx_edid_verify() - Verify edid on hdmirx
 * @set_edid : reference edid
 * @verify   : verify edid if 1
 */
int hdmirx_edid_write_verify(struct v4l2_subdev_edid *set_edid, bool verify)
{
	int ret;
	struct v4l2_subdev_edid cmp_edid;
	struct media_entity *hdmirx_entity = hdmirx_ctx.hdmirx_entity;

	/*
	 * Write EDID
	 */
	ret = ioctl(hdmirx_entity->fd, VIDIOC_SUBDEV_S_EDID, set_edid);
	if (ret) {
		printf("%s(): cannot set edid on block: %d\n", __func__, set_edid->blocks);
		goto s_edid_failed;
	}

	if (!verify)
		goto s_edid_failed;

	/*
	 * Read back the edid
	 */
	memcpy(&cmp_edid, set_edid, sizeof(cmp_edid));
	cmp_edid.edid = malloc(set_edid->blocks * EDID_BLOCK_SIZE);
	if (!cmp_edid.edid) {
		printf("%s(): alloc failed for edid blocks\n", __func__);
		ret = -ENOMEM;
		goto s_edid_failed;
	}
	ret = ioctl(hdmirx_entity->fd, VIDIOC_SUBDEV_G_EDID, &cmp_edid);
	if (ret) {
		printf("%s(): failed to retrieve edid\n", __func__);
		goto g_edid_failed;
	}

	/*
	 * Compare now
	 */
	if (!memcmp(cmp_edid.edid, set_edid->edid, set_edid->blocks * EDID_BLOCK_SIZE))
		printf("EDID updated/verified successfully\n");
	else {
		ret = -EINVAL;
		printf("EDID update failed !!!!!!!\n");
	}

g_edid_failed:
	free(cmp_edid.edid);
s_edid_failed:
	return ret;
}

/**
 * hdmirx_edid_write_test() - do an edid write on hdmirx
 */
static int hdmirx_edid_write_test()
{
	int ret = 0;
	struct v4l2_subdev_edid edid;

	/*
	 * Write EDID
	 */
	memset(&edid, 0, sizeof(edid));
	edid.pad = 0;
	edid.start_block = 0;
	edid.blocks = sizeof(hdmirx_edid) / EDID_BLOCK_SIZE;
	edid.edid = hdmirx_edid;
	ret = hdmirx_edid_write_verify(&edid, 1);
	if (ret)
		printf("%s(): failed to write edid\n", __func__);

	return ret;
}

/**
 * hdmirx_write_file_edid() -write edid from file
 */
static int hdmirx_write_file_edid(char *filename)
{
	FILE *fp;
	__u8 *edid_blocks;
	char line[256], *ptr;
	struct v4l2_subdev_edid edid;
	int ret = 0, num_blocks, count = 0;

	/*
	 * Open edid conf file and access edid data
	 */
	fp = fopen(filename, "r");
	if (!fp) {
		printf("%s(): failed to open edid conf file\n", __func__);
		ret = -EINVAL;
		goto open_failed;
	}

	/*
	 * Ignore comments and new lines
	 */
	do {
		ptr = fgets(line, sizeof(line), fp);
		if (!ptr) {
			printf("%s(): empty file, cannot read edid\n", __func__);
			ret = -EINVAL;
			goto read_done;
		}
	} while (strchr(line, '#') || (*line == '\n'));

	/*
	 * Get number of blocks
	 */
	if (strncmp(line, "num_blocks", strlen("num_blocks"))) {
		printf("%s(): number of blocks not specified\n", __func__);
		goto read_done;
	}
	ptr = strchr(line, '=');
	num_blocks = strtol(ptr + 1, NULL, 10);
	if ((errno == ERANGE && (num_blocks == LONG_MAX || num_blocks == LONG_MIN))
					|| (errno != 0 && num_blocks == 0)) {
		printf("%s(): wrong parameter recevied\n", __func__);
		goto read_done;
	}

	/*
	 * Read the data from the file
	 */
	edid_blocks = calloc(num_blocks, EDID_BLOCK_SIZE);
	if (!edid_blocks) {
		printf("%s(): out of memory for edid blocks\n", __func__);
		goto read_done;
	}

	do {
		ptr = fgets(line, sizeof(line), fp);
		if (!ptr) {
			printf("%s(): EDID read done\n", __func__);
			break;
		}

		if (*ptr == '\n')
			continue;

		do {
			edid_blocks[count++] = strtol(ptr, NULL, 16);
			if ((errno == ERANGE && (num_blocks == LONG_MAX || num_blocks == LONG_MIN))
						|| (errno != 0 && num_blocks == 0)) {
				printf("%s(): wrong parameter recevied\n", __func__);
				goto read_done;
			}
			ptr = strchr(ptr, ' ');
			if (!ptr)
				break;

		} while ((ptr = strchr(ptr, '0')));

	} while (!feof(fp));

	if (hdmirx_ctx.debug_info) {
		int i;
		__u8 *info = edid_blocks;
		for (i = 0; i < num_blocks * EDID_BLOCK_SIZE; i += 8)
			printf("0x%x\t 0x%x\t  0x%x\t 0x%x\t 0x%x\t 0x%x\t 0x%x\t 0x%x\t\n",
				info[i], info[i + 1], info[i + 2], info[i + 3],
				info[i + 4], info[i + 5], info[i + 6], info[i + 7]);
	}

	/*
	 * Set the EDID
	 */
	memset(&edid, 0, sizeof(edid));
	edid.pad = 0;
	edid.start_block = 0;
	edid.blocks = num_blocks;
	edid.edid = edid_blocks;
	ret = hdmirx_edid_write_verify(&edid, 1);
	if (ret)
		printf("%s(): failed to write edid\n", __func__);

read_done:
	fclose(fp);
open_failed:
	return ret;
}

/**
 * hdmirx_configure_video_repeater() - configure video decoder for a specific repeater mode
 */
static int hdmirx_configure_decoder(char *decoder_name, int mode)
{
	int ret = 0, is_vid = 0;
	struct v4l2_control ctrl;
	struct media_entity *decoder_entity;

	/*
	 * Get the decoder subdev
	 */
	if (!strncmp(decoder_name, "v4l2.video", strlen("v4l2.video"))) {

		ret = stm_v4l2_subdev_open(hdmirx_ctx.media,
					decoder_name, &hdmirx_ctx.viddec_entity);
		if (ret) {
			printf("%s(): failed to open %s decoder\n", __func__, decoder_name);
			goto decoder_open_failed;
		}
		decoder_entity = hdmirx_ctx.viddec_entity;
		is_vid = 1;

	} else if (!strncmp(decoder_name, "v4l2.audio", strlen("v4l2.audio"))) {

		ret = stm_v4l2_subdev_open(hdmirx_ctx.media,
						decoder_name, &hdmirx_ctx.auddec_entity);
		if (ret) {
			printf("%s(): failed to open %s decoder\n", __func__, decoder_name);
			goto decoder_open_failed;
		}

		decoder_entity = hdmirx_ctx.auddec_entity;
	} else {

		printf("\nUse --dec-id or -d option before asking for capture policy setting\n\n");
		ret = -EINVAL;
		goto decoder_open_failed;
	}

	/*
	 * Set the requested control
	 */
	memset(&ctrl, 0, sizeof(ctrl));
	ctrl.id = V4L2_CID_MPEG_STI_CAPTURE_PROFILE;
	ctrl.value = mode;

	ret = ioctl(decoder_entity->fd, VIDIOC_S_CTRL, &ctrl);
	if (ret) {
		printf("%s(): failed to set control\n", __func__);
		goto s_ctrl_failed;
	}

	return 0;

s_ctrl_failed:
	if (is_vid) {
		stm_v4l2_subdev_close(hdmirx_ctx.viddec_entity);
		hdmirx_ctx.viddec_entity = NULL;
	} else {
		stm_v4l2_subdev_close(hdmirx_ctx.auddec_entity);
		hdmirx_ctx.auddec_entity = NULL;
	}
decoder_open_failed:
	return ret;
}

/**
 * hdmirx_display_supported_capture_fmts() - print supported capture fmts
 */
static void hdmirx_display_supported_capture_fmts()
{
	int i, ret;
	struct v4l2_subdev_mbus_code_enum code;
	struct v4l2_subdev_frame_size_enum fse;

	if (!hdmirx_ctx.debug_info)
		goto display_done;

	printf("====================================\n");
	printf("=====Supported Capture formats======\n");
	for (i = 0; ;i++) {
		memset(&code, 0, sizeof(code));
		code.pad = 1;
		code.index = i;
		ret = ioctl(hdmirx_ctx.dvp_entity->fd,
				VIDIOC_SUBDEV_ENUM_MBUS_CODE,
				&code);
		if (ret)
			break;

		memset(&fse, 0, sizeof(fse));
		fse.index=i;
		fse.pad = 1;
		ret = ioctl(hdmirx_ctx.dvp_entity->fd,
				VIDIOC_SUBDEV_ENUM_FRAME_SIZE,
				&fse);
		if (ret) {
			printf("%s(): failed to get the frame size info\n", __func__);
			break;
		}
		printf("\tPixel code: 0x%x\n", code.code);
		printf("\t\t--Resizing caps--\n");
		printf("\t\tmin_width : %d\n", fse.min_width);
		printf("\t\tmax_width : %d\n", fse.max_width);
		printf("\t\tmin_height: %d\n", fse.min_height);
		printf("\t\tmax_height: %d\n", fse.max_height);
	}
	printf("====================================\n");

display_done:
	return;
}

/**
 * hdmirx_get_viddec_remote_sink() - returns the video decoder remote sink
 */
struct media_pad *hdmirx_get_viddec_remote_sink()
{
	char pad_name[32], *parse_end;
	struct media_pad *dec_pad, *plane_pad;

	/*
	 * Prepare pad name
	 */
	memset(pad_name, 0, sizeof(pad_name));
	snprintf(pad_name, sizeof(pad_name), "\"%s\":1", hdmirx_ctx.viddec_name);
	pad_name[sizeof(pad_name) - 1] = '\0';

	/*
	 * Get the decoder pad
	 */
	dec_pad = media_parse_pad(hdmirx_ctx.media, pad_name, &parse_end);
	if (!dec_pad) {
		printf("%s(): failed to find src pad of %s\n", __func__, hdmirx_ctx.viddec_name);
		goto parse_pad_failed;
	}

	/*
	 * Get the plane connected to decoder
	 */
	plane_pad = stm_media_entity_sink(dec_pad);
	if (!plane_pad) {
		printf("%s(): No plane connected to %s\n", __func__, hdmirx_ctx.viddec_name);
		goto parse_pad_failed;
	}

	return plane_pad;

parse_pad_failed:
	return NULL;
}

/**
 * hdmirx_configure_dvp_output() - manage output fmt for dvp dynamically
 */
static int hdmirx_configure_dvp_output(struct v4l2_mbus_framefmt *dvp_fmt)
{
	int i, ret = 0;
	bool hqvdp, vdp, gdp;
	struct media_pad *plane_pad;
	struct v4l2_mbus_framefmt hdmirx_vid_fmt;
	struct v4l2_subdev_mbus_code_enum code;

	hqvdp = vdp = gdp = false;

	/*
	 * Get the input format
	 */
	memset(&hdmirx_vid_fmt, 0, sizeof(hdmirx_vid_fmt));
	ret = v4l2_subdev_get_format(hdmirx_ctx.hdmirx_entity,
				&hdmirx_vid_fmt, 1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret) {
		printf("%s(): failed to get hdmirx video format\n", __func__);
		goto get_fmt_failed;
	}

	/*
	 * Get the connected plane
	 */
	plane_pad = hdmirx_get_viddec_remote_sink();
	if (!plane_pad) {
		printf("%s(): failed to get remote of plane\n", __func__);
		goto get_fmt_failed;
	}

	printf("Reconfiguring output format of DVP for %s\n", plane_pad->entity->info.name);

	if (!strncmp(plane_pad->entity->info.name, "Main-VID", strlen("Main-VID")))
		hqvdp = true;
	else if (!strncmp(plane_pad->entity->info.name, "Main-PIP", strlen("Main-PIP")))
		vdp = true;
	else
		gdp = true;

	/*
	 * List of input formats that can come to dvp
	 */
	switch (hdmirx_vid_fmt.code) {
	case V4L2_MBUS_FMT_RGB888_1X24:
	case V4L2_MBUS_FMT_RGB101010_1X30:
	case V4L2_MBUS_FMT_YUV8_1X24:
	case V4L2_MBUS_FMT_YUV10_1X30:
	case V4L2_MBUS_FMT_YUYV8_2X8:
	case V4L2_MBUS_FMT_YUYV10_2X10:
	{
		bool found = false;

		for (i = 0; !found;i++) {

			memset(&code, 0, sizeof(code));
			code.pad = 1;
			code.index = i;
			ret = ioctl(hdmirx_ctx.dvp_entity->fd,
						VIDIOC_SUBDEV_ENUM_MBUS_CODE,
						&code);
			if (ret) {
				printf("%s(): cannot find any more suppported formats by dvp\n", __func__);
				goto get_fmt_failed;
			}

			/*
			 * List of formats supported by HQVDP
			 */
			if (hqvdp) {
				switch (code.code) {
				case V4L2_MBUS_FMT_YUV8_1X24:
				case V4L2_MBUS_FMT_YUV10_1X30:
				case V4L2_MBUS_FMT_YUYV8_2X8:
				case V4L2_MBUS_FMT_YUYV10_2X10:
					if ((hdmirx_vid_fmt.field == V4L2_FIELD_NONE) ||
						(hdmirx_vid_fmt.field == V4L2_FIELD_ANY)) {
						found = true;
						break;
					} else {
						if (code.code == V4L2_MBUS_FMT_YUYV8_2X8) {
							found = true;
							break;
						}
					}
				}
			} else if (vdp) {
				switch (code.code) {
				case V4L2_MBUS_FMT_YUYV8_2X8:
					found = true;
					break;
				}
			} else {
				switch (code.code) {
				case V4L2_MBUS_FMT_RGB888_1X24:
				case V4L2_MBUS_FMT_YUV8_1X24:
					found = true;
					break;
				}
			}
		}

		break;
	}

	default:
		ret = -EINVAL;
		goto get_fmt_failed;

	}

	/*
	 * Configure dvp for capture format
	 */
	dvp_fmt->code = code.code;
	ret = v4l2_subdev_set_format(hdmirx_ctx.dvp_entity,
				dvp_fmt, 1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret) {
		printf("%s(): failed to set dvp format and start capture\n", __func__);
		goto get_fmt_failed;
	}

	if (hdmirx_ctx.debug_info)
		printf("DVP output configured with pixel format: 0x%x\n", code.code);

get_fmt_failed:
	return ret;
}

/**
 * hdmirx_start_dvp_capture() - configure and start video capture
 */
static int hdmirx_start_dvp_capture()
{
	int ret = 0;
	struct v4l2_mbus_framefmt dvp_fmt;
	struct media_entity *decoder_entity;

	printf("Configure and start DVP capture\n");

	pthread_mutex_lock(&hdmirx_ctx.vid_mutex);

	/*
	 * Open dvp subdevice
	 */
	ret = stm_v4l2_subdev_open(hdmirx_ctx.media,
					"dvp0", &hdmirx_ctx.dvp_entity);
	if (ret) {
		printf("%s(): failed to open dvp0 subdevice\n", __func__);
		goto dvp_open_failed;
	}

	/*
	 * Get the format and modify the mbus code.
	 */
	ret = v4l2_subdev_get_format(hdmirx_ctx.dvp_entity,
				&dvp_fmt, 1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret) {
		printf("%s(): failed to get dvp capture format\n", __func__);
		goto get_fmt_failed;
	}

	if (hdmirx_ctx.dvp_mbus_code) {

		/*
		 * Force the setting as done from command line
		 */
		dvp_fmt.code = hdmirx_ctx.dvp_mbus_code;
		ret = v4l2_subdev_set_format(hdmirx_ctx.dvp_entity,
				&dvp_fmt, 1, V4L2_SUBDEV_FORMAT_ACTIVE);
		if (ret) {
			printf("%s(): failed to set dvp format and start capture\n", __func__);
			goto get_fmt_failed;
		}
	} else {

		ret = hdmirx_configure_dvp_output(&dvp_fmt);
		if (ret) {
			printf("%s(): No supported formats found\n", __func__);
			goto get_fmt_failed;
		}
	}

	/*
	 * Open video decoder subdevice
	 */
	ret = stm_v4l2_subdev_open(hdmirx_ctx.media,
				hdmirx_ctx.viddec_name, &hdmirx_ctx.viddec_entity);
	if (ret) {
		printf("%s(): failed to open video decoder\n", __func__);
		goto get_fmt_failed;
	}
	decoder_entity = hdmirx_ctx.viddec_entity;

	/*
	 * Start the video decoder
	 */
	ret = ioctl(decoder_entity->fd, VIDIOC_SUBDEV_STI_STREAMON, NULL);
	if (ret) {
		printf("%s(): failed to streamon\n", __func__);
		goto streamon_failed;
	}
	hdmirx_ctx.video_streaming = true;

	hdmirx_display_supported_capture_fmts();

	pthread_mutex_unlock(&hdmirx_ctx.vid_mutex);

	return 0;

streamon_failed:
	stm_v4l2_subdev_close(hdmirx_ctx.viddec_entity);
	hdmirx_ctx.viddec_entity = NULL;
get_fmt_failed:
	stm_v4l2_subdev_close(hdmirx_ctx.dvp_entity);
	hdmirx_ctx.dvp_entity = NULL;
dvp_open_failed:
	pthread_mutex_unlock(&hdmirx_ctx.vid_mutex);
	return ret;
}

/**
 * hdmirx_start_audio_capture() - start audio capture
 */
static int hdmirx_start_audio_capture()
{
	int ret = 0;
	struct media_entity *decoder_entity;

	pthread_mutex_lock(&hdmirx_ctx.aud_mutex);

	printf("Configure and start Audio capture\n");

	/*
	 * Open audio decoder
	 */
	ret = stm_v4l2_subdev_open(hdmirx_ctx.media,
			hdmirx_ctx.auddec_name, &hdmirx_ctx.auddec_entity);
	if (ret) {
		printf("%s(): failed to open %s subdevice\n", __func__, hdmirx_ctx.auddec_name);
		goto auddec_open_failed;
	}

	decoder_entity = hdmirx_ctx.auddec_entity;

	/*
	 * Start the audio decoder
	 */
	ret = ioctl(decoder_entity->fd, VIDIOC_SUBDEV_STI_STREAMON, NULL);
	if (ret) {
		printf("%s(): failed to streamon\n", __func__);
		goto s_ctrl_failed;
	}
	hdmirx_ctx.audio_streaming = true;

	pthread_mutex_unlock(&hdmirx_ctx.aud_mutex);
	return 0;

s_ctrl_failed:
	stm_v4l2_subdev_close(hdmirx_ctx.auddec_entity);
	hdmirx_ctx.auddec_entity = NULL;
auddec_open_failed:
	pthread_mutex_unlock(&hdmirx_ctx.aud_mutex);
	return ret;
}

/**
 * hdmirx_display_input_video_fmt() - display input video format
 */
static void hdmirx_display_input_video_fmt(struct media_entity *hdmirx_entity)
{
	int ret;
	struct v4l2_mbus_framefmt hdmirx_vid_fmt;

	if (!hdmirx_ctx.debug_info)
		goto display_done;

	/*
	 * Get the hdmirx in format for video
	 */
	memset(&hdmirx_vid_fmt, 0, sizeof(hdmirx_vid_fmt));
	ret = v4l2_subdev_get_format(hdmirx_entity,
			&hdmirx_vid_fmt, 1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret) {
		printf("%s(): failed to get hdmirx video format\n", __func__);
		goto display_done;
	}

	/*
	 * Dump the information
	 */
	printf("====================================\n");
	printf("\tHDMIRx Video input properties\n");
	printf("\tWidth      : %d\n", hdmirx_vid_fmt.width);
	printf("\tHeight     : %d\n", hdmirx_vid_fmt.height);
	printf("\tPixelCode  : 0x%x\n", hdmirx_vid_fmt.code);
	printf("\tField      : %d\n", hdmirx_vid_fmt.field);
	printf("\tColorspace : %d\n", hdmirx_vid_fmt.colorspace);
	printf("====================================\n");

display_done:
	return;
}

/**
 * hdmirx_display_input_audio_fmt() - display input audio format
 */
static void hdmirx_display_input_audio_fmt(struct media_entity *hdmirx_entity)
{
	int ret;
	struct stm_v4l2_subdev_audio_fmt audio_fmt;

	if (!hdmirx_ctx.debug_info)
		goto display_done;

	memset(&audio_fmt, 0, sizeof(audio_fmt));
	audio_fmt.which = V4L2_SUBDEV_FORMAT_ACTIVE;
	audio_fmt.pad = 2;
	ret = ioctl(hdmirx_entity->fd,
			VIDIOC_SUBDEV_STI_G_AUDIO_FMT, &audio_fmt);
	if (ret) {
		printf("%s(): Failed to get hdmirx audio fmt\n", __func__);
		goto display_done;
	}

	/*
	 * Dump the information
	 */
	printf("====================================\n");
	printf("\tCodec             : %d\n", audio_fmt.fmt.codec);
	printf("\tChannel Count     : %d\n", audio_fmt.fmt.channel_count);
	printf("\tSampling Freqeuncy: %d\n", audio_fmt.fmt.sampling_frequency);
	printf("====================================\n");

display_done:
	return;
}

/*
 * Configure plane in a particular mode
 */
static int hdmirx_configure_plane_mode(struct hdmirx_context *hdmirx_ctx, __u32 mode)
{
	int ret;
	struct v4l2_control ctrl;

	/*
	 * Open plane sub-device
	 */
	ret = stm_v4l2_subdev_open(hdmirx_ctx->media,
				hdmirx_ctx->plane_name, &hdmirx_ctx->plane_entity);
	if (ret) {
		printf("%s(): failed to open %s\n", __func__, hdmirx_ctx->plane_name);
		goto config_failed;
	}

	/*
	 * Set the input and output window mode
	 */
	memset(&ctrl, 0, sizeof(ctrl));
	ctrl.id = V4L2_CID_STM_INPUT_WINDOW_MODE;
	ctrl.value = mode;
	ret = ioctl(hdmirx_ctx->plane_entity->fd, VIDIOC_S_CTRL, &ctrl);
	if (ret) {
		printf("%s(): failed to set %s input window to %s mode\n",
					__func__, hdmirx_ctx->plane_name,
					(mode == VCSPLANE_AUTO_MODE) ? "AUTO" : "MANUAL");
		goto set_mode_failed;
	}

	memset(&ctrl, 0, sizeof(ctrl));
	ctrl.id = V4L2_CID_STM_OUTPUT_WINDOW_MODE;
	ctrl.value = VCSPLANE_AUTO_MODE;
	ret = ioctl(hdmirx_ctx->plane_entity->fd, VIDIOC_S_CTRL, &ctrl);
	if (ret) {
		printf("%s(): failed to set %s output window to %s mode\n",
					__func__, hdmirx_ctx->plane_name,
					(mode == VCSPLANE_AUTO_MODE) ? "AUTO" : "MANUAL");
		goto set_mode_failed;
	}

	return 0;

set_mode_failed:
	stm_v4l2_subdev_close(hdmirx_ctx->plane_entity);
	hdmirx_ctx->plane_entity = NULL;
config_failed:
	return ret;
}

/**
 * hdmirx_configure_main_plane() - configure main gdp plane to AUTO
 */
static int hdmirx_configure_main_plane()
{
	int ret = 0;
	char *plane_name;
	struct media_pad *plane_pad;

	/*
	 * Find out connected plane to decoder
	 */
	plane_pad = hdmirx_get_viddec_remote_sink();
	if (!plane_pad) {
		printf("%s(): failed to get the plane pad\n", __func__);
		ret = -EINVAL;
		goto config_failed;
	}

	/*
	 * Put the Main input/output window in AUTO mode
	 */
	plane_name = plane_pad->entity->info.name;
	if (strncmp(plane_name, "Main-GDP", strlen("Main-GDP")))
		goto config_failed;

	strncpy(hdmirx_ctx.plane_name, plane_name, sizeof(hdmirx_ctx.plane_name) - 1);
	ret = hdmirx_configure_plane_mode(&hdmirx_ctx, VCSPLANE_AUTO_MODE);
	if (ret)
		printf("%s(): failed to put %s in AUTO mode\n", __func__, plane_name);

config_failed:
	return ret;
}

/**
 * hdmirx_monitor() - hdmirx monitor thread
 */
static void *hdmirx_monitor(void *arg)
{
	int ret;
	struct v4l2_event event;
	struct pollfd hdmirx_pollfd;
	struct media_entity *hdmirx_entity = hdmirx_ctx.hdmirx_entity;

	/*
	 * Main-GDP2 plane is by default in MANUAL mode, so, need to configure
	 * to AUTO mode, so, that hdmirx data can be pushed to display
	 */
	ret = hdmirx_configure_main_plane();
	if (ret) {
		printf("%s(): failed to put GDP in AUTO mode\n", __func__);
		goto exit;
	}

	while (1) {

		memset(&hdmirx_pollfd, 0, sizeof(hdmirx_pollfd));
		hdmirx_pollfd.fd = hdmirx_entity->fd;
		hdmirx_pollfd.events = POLLIN | POLLPRI;

		if (poll(&hdmirx_pollfd, 1, 100)) {

			if (hdmirx_pollfd.revents & POLLERR) {
				goto exit;
			}
		} else
			continue;

		memset(&event, 0, sizeof(event));
		ret = ioctl(hdmirx_entity->fd, VIDIOC_DQEVENT, &event);
		if (ret) {
			printf("%s(): failed to dq hdmirx event\n", __func__);
			continue;
		}

		if (event.type == V4L2_EVENT_STI_HOTPLUG) {
			if (event.u.data[0] == 1)
				printf("HDMIRx: Hotplug detected: Status IN\n");
			else
				printf("HDMIRx: Hotplug detected: Status OUT\n");
			continue;
		}

		if ((event.id == 0) && (event.type == V4L2_EVENT_STI_HDMI_SIG)) {
			if (event.u.data[0] == 1)
				printf("HDMIRx: Signal Detected... ~~~~~~\n");
			else
				printf("HDMIRx: Signal Lost... XXXXXXX\n");
			continue;
		}

		if ((event.id == 1) && (event.type == V4L2_EVENT_SOURCE_CHANGE) &&
			(event.u.src_change.changes != V4L2_EVENT_SRC_CH_RESOLUTION)) {
			hdmirx_ctx.video_streaming = false;
			continue;
		}

		if ((event.id == 2) && (event.type == V4L2_EVENT_SOURCE_CHANGE) &&
			(event.u.src_change.changes != V4L2_EVENT_SRC_CH_RESOLUTION)) {
			hdmirx_ctx.audio_streaming = false;
			continue;
		}

		/*
		 * Start dvp capture
		 */
		if ((event.id == 1) && (event.type == V4L2_EVENT_SOURCE_CHANGE) &&
			(event.u.src_change.changes == V4L2_EVENT_SRC_CH_RESOLUTION)) {

			ret = hdmirx_start_dvp_capture();
			if (ret) {
				printf("%s(): failed to reconfigure dvp\n", __func__);
				continue;
			}
			hdmirx_ctx.video_streaming = true;
			hdmirx_display_input_video_fmt(hdmirx_entity);
			continue;
		}

		/*
		 * Start audio capture
		 */
		if ((event.id == 2) && (event.type == V4L2_EVENT_SOURCE_CHANGE) &&
			(event.u.src_change.changes == V4L2_EVENT_SRC_CH_RESOLUTION)) {
			ret = hdmirx_start_audio_capture();
			if (ret) {
				printf("%s(): failed to reconfigure audio capture\n", __func__);
				continue;
			}
			hdmirx_ctx.audio_streaming = true;
			hdmirx_display_input_audio_fmt(hdmirx_entity);
			continue;
		}
	}

exit:
	return NULL;
}

static void hdmirx_restore_plane()
{
	int ret;

	/*
	 * Find out if we want to move the plane to AUTO mode
	 */
	if (strncmp("Main", hdmirx_ctx.plane_name, strlen("Main")))
		goto restore_done;

	/*
	 * Put Main-VID in AUTO mode and Main-GDP in MANUAL mode
	 */
	if (!strncmp("Main-VID", hdmirx_ctx.plane_name, strlen("Main-VID")))
		ret = hdmirx_configure_plane_mode(&hdmirx_ctx, VCSPLANE_AUTO_MODE);
	 else if (!!strncmp("Main-GDP", hdmirx_ctx.plane_name, strlen("Main-GDP")))
		ret = hdmirx_configure_plane_mode(&hdmirx_ctx, VCSPLANE_MANUAL_MODE);

	if (ret)
		printf("%s(): failed to put %s in default mode\n", __func__, hdmirx_ctx.plane_name);

	stm_v4l2_subdev_close(hdmirx_ctx.plane_entity);

restore_done:
	return;
}

/**
 * hdmirx_subscribe_signal_events() - subscribe to hdmirx signal events
 */
static int hdmirx_subscribe_signal_events()
{
	int ret = 0;

	/*
	 * Subscribe to HPD and Signal status change event
	 */
	ret = hdmirx_subscribe_events(HOTPLUG, &hdmirx_ctx);
	if (ret) {
		printf("%s(): failed to subscribe to HPD event\n", __func__);
		goto hpd_subs_failed;
	}

	ret = hdmirx_subscribe_events(SIGNAL_STATUS, &hdmirx_ctx);
	if (ret) {
		printf("%s(): failed to subscribe to SIGNAL status event\n", __func__);
		goto sig_subs_failed;
	}

	return 0;

sig_subs_failed:
	hdmirx_unsubscribe_events(HOTPLUG, &hdmirx_ctx);
hpd_subs_failed:
	return ret;
}

/**
 * hdmirx_sighandler() - signal handler to manage clean exit
 */
static void hdmirx_sighandler(int signum, siginfo_t *siginfo, void *data)
{
	switch (signum) {
	case SIGINT:
		hdmirx_ctx.exit = true;
		if (hdmirx_ctx.tunnel)
			pthread_cancel(hdmirx_thread);
		pthread_cancel(keyboard_thread);
		hdmirx_restore_plane();
		pthread_mutex_destroy(&hdmirx_ctx.vid_mutex);
		pthread_mutex_destroy(&hdmirx_ctx.aud_mutex);
		printf("\n");
		break;

	default:
		printf("Unhandled signal number: %d\n", signum);
	}
}

/**
 * usage() - help to tbe displayed
 */
static void usage()
{
	printf("hdmirx-capture version-1.0\n");
	printf("Last modified (03 June 2014)\n");
	printf("Compile info:- DATE(%s), TIME(%s)\n", __DATE__, __TIME__);
	printf("Usage: hdmirx-capture [OPTIONS]\n");
	printf("Configure and start hdmirx capture.\n");
	printf("\n");
	printf("  -d, --dec-id\n");
	printf("     The decoder id to use, it is used for audio/video\n\n");
	printf("  --start\n");
	printf("     Start the hdmirx capture, this will start both audio and video capture\n\n");
	printf("  --dvp-fmt\n");
	printf("     Set the user defined capture format for video (DVP) capture\n\n");
	printf("  --vid-capture-profile\n");
	printf("     Configure the mode of video decoder, 0 -> Disabled, 1 -> No audio decoding, no video FRC, 2 ->  audio decoding, video FRC,  \n");
	printf("     3 -> No audio decoding, video FRC,  4 -> audio decoding, no video FRC \n");
	printf("     NOTE: since, audio and video are in same playback, so, setting only once the capture policy is suffice.\n");
	printf("     Two separate options are given, so, as to realize the single stream playback with specific policy\n\n");
	printf("  --aud-capture-profile\n");
	printf("     Configure the mode of audio decoder, 0 -> Disabled, 1 -> No audio decoding, no video FRC, 2 ->  audio decoding, video FRC,\n");
	printf(" 3 -> No audio decoding, video FRC,  4 -> audio decoding, no video FRC \n");
	printf("  --dvp-start\n");
	printf("     Configure and start dvp capture\n\n");
	printf("  --aud-start\n");
	printf("     Configure and start hdmirx audio capture\n\n");
	printf("  --edid-read\n");
	printf("     Do an edid read test, and dump the contents in a file (edid.dump)\n");
	printf("     (start_block = 0, blocks = %d; start_block = 1, blocks = %d\n", EDID_READ_BLOCKS, EDID_READ_BLOCKS);
	printf("     This is a test only option and must not be used in conjuction with any other\n\n");
	printf("  --edid-write\n");
	printf("     Do an edid write test, 0 for no exit and 1 for exit after the test\n");
	printf("     (start_block = 0, blocks = 2\n");
	printf("     This is a test only option with 1 value, else, it can be used in conjuction with any other\n\n");
	printf("  --edid-file\n");
	printf("     Write an EDID from a file. The file must follow the syntax of edid.conf\n\n");
	printf("  --edid-repeater\n");
	printf("     Read the HDMITx EDID, take the physical address and modify the HDMIRx EDID\n\n");
	printf("  --debug\n");
	printf("    Starts printing more debug information such as the input coming in to hdmirx and output of dvp\n\n");
	printf("     -----------------------------------------------------------------------------------------------\n");
	printf("     NOTE: This app is now supporting resize of the output window after the application is started.\n");
	printf("     The app follows the following syntax, for example: resize<enter>. Enter the plane name.\n");
	printf("     App will read resize.conf file placed in /root/ and will resize based on the params provided\n");
	printf("     -----------------------------------------------------------------------------------------------\n\n");
}

/*
 * main() - main entry point
 */
int main(int argc, char *argv[])
{
	bool edid_repeater = false;
	int ret = -EINVAL, option;
	struct sigaction hdmirx_sigaction;

	/*
	 * Construct the arguments to be parsed by this application
	 */
	enum {
		HDMIRX_START,
		DVP_START,
		AUDIO_START,
		DVP_FORMAT,
		VIDEO_CAPTURE_PROFILE,
		AUDIO_CAPTURE_PROFILE,
		EDID_READ_TEST,
		EDID_WRITE_TEST,
		EDID_FILE,
		EDID_REPEATER,
		DEBUG_INFO,
	};

	struct option longopt[] = {
		{"dec-id", 1, NULL, 'd'},
		{"start", 0, NULL, HDMIRX_START},
		{"dvp-start", 0, NULL, DVP_START},
		{"aud-start", 0, NULL, AUDIO_START},
		{"dvp-fmt", 1, NULL, DVP_FORMAT},
		{"vid-capture-profile", 1, NULL, VIDEO_CAPTURE_PROFILE},
		{"aud-capture-profile", 1, NULL, AUDIO_CAPTURE_PROFILE},
		{"edid-read", 0, NULL, EDID_READ_TEST},
		{"edid-write", 1, NULL, EDID_WRITE_TEST},
		{"edid-file", 1, NULL, EDID_FILE},
		{"edid-repeater", 0, NULL, EDID_REPEATER},
		{"debug", 0, NULL, DEBUG_INFO},
		{"tunnel", 0, NULL, 't'},
		{"plane", 0, NULL, 'p'},
		{0, 0, 0, 0}
	};

	memset(&hdmirx_sigaction, 0, sizeof(hdmirx_sigaction));
	hdmirx_sigaction.sa_sigaction = hdmirx_sighandler;
	hdmirx_sigaction.sa_flags = SA_SIGINFO;
	sigaction(SIGINT, &hdmirx_sigaction, NULL);
	hdmirx_ctx.exit = false;

	pthread_mutex_init(&hdmirx_ctx.vid_mutex, NULL);
	pthread_mutex_init(&hdmirx_ctx.aud_mutex, NULL);

	/*
	 * Opens the media device
	 */
	ret = stm_v4l2_media_open(0, &hdmirx_ctx.media);
	if (ret) {
		printf("%s(): failed to open media device\n", __func__);
		goto exit;
	}

	/*
	 * Open hdmirx subdev. This subdevice is the basic
	 * requirement for the data flow
	 */
	ret = stm_v4l2_subdev_open(hdmirx_ctx.media,
			"hdmirx0", &hdmirx_ctx.hdmirx_entity);
	if (ret) {
		printf("%s(): failed to open hdmirx subdev\n", __func__);
		goto media_exit;
	}

	/*
	 * Subscribe to signal events
	 */
	ret = hdmirx_subscribe_signal_events();
	if (ret) {
		printf("%s(): failed to subscribe for hdmirx events\n", __func__);
		goto subs_exit;
	}

	hdmirx_ctx.tunnel = true;
	/*
	 * Parse the arguments
	 */
	while ((option = getopt_long(argc, argv, "d:t:p:", longopt, NULL)) != -1) {

		switch(option) {
		case HDMIRX_START:
			hdmirx_subscribe_events(VIDEO, &hdmirx_ctx);
			hdmirx_subscribe_events(AUDIO, &hdmirx_ctx);
			break;

		case DVP_START:

			/*
			 * It is expected that, before this application is invoked
			 * the basic connections are already done. These basic
			 * connection include:
			 * vidextin -> hdmirx (done at boot up)
			 * hdmirx->dvp (done at boot up)
			 * dvp->v4l2.video (done at boot up)
			 * v4l2.videoX->Main/Aux-Plane (Just before the application)
			 */
			printf("Configure for DVP capture\n");
			hdmirx_subscribe_events(VIDEO, &hdmirx_ctx);
			break;

		case DVP_FORMAT:
			hdmirx_ctx.dvp_mbus_code = atoi(optarg);
			break;

		case AUDIO_START:
			printf("Configure Audio capture\n");
			hdmirx_subscribe_events(AUDIO, &hdmirx_ctx);
			break;

		case 'd':
		{
			int size;

			size = sizeof(hdmirx_ctx.viddec_name) - 1;
			snprintf(hdmirx_ctx.viddec_name, size, "v4l2.video%d", atoi(optarg));
			size = sizeof(hdmirx_ctx.auddec_name) - 1;
			snprintf(hdmirx_ctx.auddec_name, size, "v4l2.audio%d", atoi(optarg));
			break;
		}

		case VIDEO_CAPTURE_PROFILE:
		case AUDIO_CAPTURE_PROFILE:
		{
			int profile = atoi(optarg);

			if ((profile < 0) || (profile > 4)) {
				printf("%s(): invalide profile value\n", __func__);
				break;
			}

			switch (option) {
			case VIDEO_CAPTURE_PROFILE:
				ret = hdmirx_configure_decoder(hdmirx_ctx.viddec_name, profile);
				break;

			case AUDIO_CAPTURE_PROFILE:
				ret = hdmirx_configure_decoder(hdmirx_ctx.auddec_name, profile);
				break;
			}

			break;
		}

		case EDID_READ_TEST:

			ret = hdmirx_edid_read_test();
			goto subs_exit;

		case EDID_WRITE_TEST:

			ret = hdmirx_edid_write_test(atoi(optarg));
			if (optarg[0] == '1')
				goto subs_exit;
			break;

		case EDID_FILE:
			ret = hdmirx_write_file_edid(optarg);
			if (ret)
				printf("%s(): failed to write edid from file\n", __func__);

			goto subs_exit;

		case 't':
			hdmirx_ctx.tunnel = atoi(optarg);
			break;

		case 'p':
			strncpy(hdmirx_ctx.plane_name, optarg, sizeof(hdmirx_ctx.plane_name) - 1);
			hdmirx_ctx.plane_name[sizeof(hdmirx_ctx.plane_name) - 1] = '\0';
			break;

		case EDID_REPEATER:
			edid_repeater = true;
			break;

		case DEBUG_INFO:
			hdmirx_ctx.debug_info = true;
			break;

		default:
			printf("%s(): invalid argument\n", __func__);
			usage();
			goto subs_exit;
		}
	}

	if (ret) {
		printf("%s(): make sure the relevant media-ctl commands are executed\n", __func__);
		goto subs_exit;
	}

	if (hdmirx_ctx.tunnel) {
		/*
		 * Create an hdmirx monitor task to subscribe to hdmirx events
		 * and then reprogram dvp
		 */
		ret = pthread_create(&hdmirx_thread, NULL, hdmirx_monitor, NULL);
		if (ret) {
			printf("%s(): failed to create tunneled hdmirx capture thread\n", __func__);
			goto media_exit;
		}
	} else {
		/*
		 * Create an hdmirx monitor task to subscribe to hdmirx events
		 * and then reprogram dvp
		 */
		ret = pthread_create(&dvp_grab_thread, NULL, dvp_grab_monitor, &hdmirx_ctx);
		if (ret) {
			printf("%s(): failed to create dvp user grab thread\n", __func__);
			goto media_exit;
		}
	}

	if (edid_repeater) {
		ret = pthread_create(&hdmirx_edid_thread, NULL,
					hdmirx_edid_monitor, &hdmirx_ctx);
		if(ret) {
			printf("%s(): failed to create edid mirror monitor\n", __func__);
			goto subs_exit;
		}
	}

	/*
	 * Create the keyboard thread to process the runtime options
	 */
	ret = pthread_create(&keyboard_thread, NULL, keyboard_monitor, &hdmirx_ctx);
	if (ret) {
		printf("%s(): failed to create keyboard thread\n", __func__);
		goto subs_exit;
	}

	if (hdmirx_ctx.tunnel)
		pthread_join(hdmirx_thread, NULL);
	else
		pthread_join(dvp_grab_thread, NULL);
	if (edid_repeater)
		pthread_join(hdmirx_edid_thread, NULL);
	pthread_join(keyboard_thread, NULL);

subs_exit:
	stm_v4l2_subdev_close(hdmirx_ctx.hdmirx_entity);
media_exit:
	stm_v4l2_media_close(hdmirx_ctx.media);
exit:
	pthread_mutex_destroy(&hdmirx_ctx.vid_mutex);
	pthread_mutex_destroy(&hdmirx_ctx.aud_mutex);
	return ret;
}
