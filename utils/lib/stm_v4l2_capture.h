/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Header file for capture functions
************************************************************************/
#ifndef __STM_V4L2_CAPTURE_H__
#define __STM_V4L2_CAPTURE_H__

#include <linux/types.h>
#include <linux/videodev2.h>

int stm_v4l2_capture_set_input(char *name, char *input);
int stm_v4l2_capture_streamon(int fd, int buf_count, __u32 memory, struct v4l2_format *fmt);

#endif
