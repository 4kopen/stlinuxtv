/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Helper file for configuring a capture device
************************************************************************/
#include <string.h>

#include "v4l2_helper.h"
#include "stm_v4l2_capture.h"

int stm_v4l2_capture_set_input(char *name, char *input)
{
	int fd, ret;

	/*
	 * Open the video device
	 */
	fd = v4l2_open_by_name(name, "STMicroelectronics", 0);
	if (fd < 0) {
		printf("%s(): failed to open %s video device\n", __func__, name);
		ret = errno;
		goto open_failed;
	}

	/*
	 * Set the input for capture
	 */
	ret = v4l2_set_input_by_name(fd, input);
	if (ret < 0) {
		printf("%s(): failed to set %s as input\n", __func__, input);
		goto open_failed;
	}
	ret = fd;

open_failed:
	return ret;
}

int stm_v4l2_capture_streamon(int fd, int buf_count, __u32 memory, struct v4l2_format *fmt)
{
	int ret;
	struct v4l2_requestbuffers reqbuf;

	/*
	 * Set format before requesting buffers
	 */
	ret = ioctl(fd, VIDIOC_S_FMT, fmt);
	if (ret) {
		printf("%s(): failed to setup format for capture\n", __func__);
		goto s_fmt_failed;
	}

	/*
	 * Request number of buffers and streamon
	 */
	memset(&reqbuf, 0, sizeof(reqbuf));
	reqbuf.count = buf_count;
	reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	reqbuf.memory = memory;
	ret = ioctl(fd, VIDIOC_REQBUFS, &reqbuf);
	if (ret) {
		printf("%s(): failed to allocate %d buffers\n", __func__, buf_count);
		goto s_fmt_failed;
	}

	/*
	 * Streamon the device
	 */
	ret = ioctl(fd, VIDIOC_STREAMON, &reqbuf.type);
	if (ret) {
		printf("%s(): failed to start streaming\n", __func__);
		goto s_fmt_failed;
	}

s_fmt_failed:
	return ret;
}
