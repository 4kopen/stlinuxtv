/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/poll.h>

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>

#include <linux/fb.h>
#include <linux/videodev2.h>

#include "linux/include/linux/dvb/dvb_v4l2_export.h"
#include "linux/include/linux/stmvout.h"
#include "utils/v4l2_helper.h"

#define FORMAT_STR_SIZE 32

#ifndef V4L2_PIX_FMT_STI_YUV444_10
    /*
    * YUV 4:4:4 10 bpp interleaved packed (no padding) on one plane
    * in increasing byte address:
    * U0[0:9] Y0[0:9] V0[0:9] U1[0:9] Y1[0:9] V1[0:9] ....
    */
    #define V4L2_PIX_FMT_STI_YUV444_10   v4l2_fourcc('S', 'T', 'I', '1')
#endif

#ifndef V4L2_PIX_FMT_STI_YUV422_10
    /*
    * YUV 4:2:2 10 bpp interleaved packed (no padding) on 2 planes
    * in increasing byte address:
    * first plane: Y0[0:9] Y1[0:9] Y2[0:9] Y3[0:9] Y4[0:9] Y5[0:9] ....
    * second plane: U0[0:9] V0[0:9] U2[0:9] V2[0:9] U4[0:9] V4[0:9] ....
    */
    #define V4L2_PIX_FMT_STI_YUV422_10   v4l2_fourcc('S', 'T', 'I', '2')
#endif

#ifndef V4L2_PIX_FMT_STI_YUV420_10
    /*
       YUV 4:2:0 10 bpp interleaved packed (no padding) on 2 planes
       in increasing byte address:
       first plane:
       Y0[0:9] Y1[0:9] Y2[0:9] Y3[0:9] Y4[0:9] Y5[0:9] ....
       second plane:
       U(pixel, line)
       U(0,0)[0:9] V(0,0)[0:9] U(2,0)[0:9] V(2,0)[0:9] U(4,0)[0:9] V(4,0)[0:9] ....
       U(0,2)[0:9] V(0,2)[0:9] U(2,2)[0:9] V(2,2)[0:9] U(4,2)[0:9] V(4,2)[0:9] ....
    */
    #define V4L2_PIX_FMT_STI_YUV420_10   v4l2_fourcc('S', 'T', 'I', '3')
#endif

#define PRG_VERSION "7.5"
#define PRG_TRACE_PREDIX "v4l2gam-info:"

#define unlikely(x)     __builtin_expect(!!(x),0)
#define likely(x)       __builtin_expect(!!(x),1)
#define abs(x)  ({                              \
      long long __x = (x);                      \
      (__x < 0) ? -__x : __x;                   \
    })

#define printf_standard(std)                                            \
  fprintf(stderr,"\t                 %#.16llx - %s\n", std, #std)

int global_verbose = 1;

/*
  int prog__printf (const char *fmt, ...)
  {
  int return_val=0;
  if(global_verbose)
  {
  va_list ap;
  va_start(ap,fmt);
  printf((char*)fmt, ap);
  va_end(ap);
  }
  return return_val;
  }
*/
#define prog__printf if(global_verbose)printf


#ifndef __cplusplus
#ifndef bool
#define bool    unsigned char
#define false   0
#define true    1
#endif
#endif

int global_iterations;
int global_numberofallocatedbuffer = 0;
unsigned int global_colorformat = 0;

struct {
  void *start;
  size_t length;
} *buffers;

typedef struct {
  int x;
  int y;
  int width;
  int height;
} video_window_t;
/*
 * This is a simple program to test V4L2 buffer queueing and presentation time from a gam file.
 *
 * The code is deliberately written in a linear, straight forward, way to show
 * the step by step usage of V4L2 to display video frames.
 */

typedef struct GamPictureHeader_s {
  unsigned short HeaderSize;
  unsigned short Signature;
  unsigned short Type;
  unsigned short Properties;
  unsigned int PictureWidth;    // With 4:2:0 R2B this is a OR between Pitch and Width
  unsigned int PictureHeight;
  unsigned int gh_w01;
  unsigned int gh_w02;
  unsigned char gh_b01;
  unsigned char gh_b02;
  unsigned char gh_b03;
  unsigned char gh_b04;
  unsigned char gh_b05;
  unsigned char gh_b06;
  unsigned char gh_b07;
  unsigned char gh_b08;
} GamPictureHeader_t;

enum gamfile_type {
  GAMFILE_UNKNOWN = 0x00,
  GAMFILE_RGB565 = 0x80,
  GAMFILE_RGB888 = 0x81,
  GAMFILE_xRGB8888 = 0x82,
  GAMFILE_ARGB8565 = 0x84,
  GAMFILE_ARGB8888 = 0x85,
  GAMFILE_ARGB1555 = 0x86,
  GAMFILE_ARGB4444 = 0x87,
  GAMFILE_CLUT1 = 0x88,
  GAMFILE_CLUT2 = 0x89,
  GAMFILE_CLUT4 = 0x8a,
  GAMFILE_CLUT8 = 0x8b,
  GAMFILE_ACLUT44 = 0x8c,
  GAMFILE_ACLUT88 = 0x8d,
  GAMFILE_CLUT8_4444 = 0x8e,
  GAMFILE_YCBCR888 = 0x90,
  GAMFILE_YCBCR101010 = 0x91,
  GAMFILE_YCBCR422R = 0x92,
  GAMFILE_YCBCR420MB = 0x94,
  GAMFILE_YCBCR422MB = 0x95,
  GAMFILE_AYCBCR8888 = 0x96,
  GAMFILE_A1 = 0x98,
  GAMFILE_A8 = 0x99,
  GAMFILE_I420 = 0x9e,
  GAMFILE_YCBCR420R2B = 0xb0,
  GAMFILE_YCBCR422R2B = 0xb1,
  GAMFILE_XY = 0xc8,
  GAMFILE_XYL = 0xc9,
  GAMFILE_XYC = 0xcA,
  GAMFILE_XYLC = 0xcb,
  GAMFILE_RGB101010 = 0x101,

  GAMFILE_CRYCBY422R = 0xE1,
  GAMFILE_YCBYCR422R = 0xE2,
  GAMFILE_YCRYCB422R = 0xE3,
  GAMFILE_CBYCRA444R = 0xE4,
  GAMFILE_ARGB8565_24 = 0xE5,
  GAMFILE_RGB888_24 = 0xE6,
  GAMFILE_RGBA8888 = 0xE7,
  GAMFILE_CBYCR444R = 0xE8,
  GAMFILE_YCBCR_10B_420R2B = 0xB2,
  GAMFILE_YCBCR_10B_422R2B = 0xB3,
  GAMFILE_YCRCB_10B_422R2B = 0xEB,
  GAMFILE_YCBCR_10B_422R = 0xEC,
  GAMFILE_CBYCR_10B_444R = 0xED,
  GAMFILE_RGBX8888 = 0xEE,
  GAMFILE_CBYCRX444R = 0xEF,

};

static void usage_begin_decode(char *prgName)
{
  fprintf(stderr,"info-version : %s\n", PRG_VERSION);
  fprintf(stderr,"Usage: %s file_name [options]\n", prgName);
  fprintf(stderr,"\ta         - Adjust time to minimize presentation delay\n");
  fprintf(stderr,"\tb num     - Number of V4L buffers to use (default is 5. Value below 2 blocks display)\n");
  fprintf(stderr,"\td num     - Presentation delay on first buffer by num seconds, default is 1\n");
  fprintf(stderr,"\ti num     - Do num iterations, default is 200\n");
  fprintf(stderr,"\tI         - Force Interlaced buffers\n");
  fprintf(stderr,"\th         - Scale the buffer down to half size\n");
  fprintf(stderr,"\th1        - Scale the buffer down to half horizontal size and put it in the left area\n");
  fprintf(stderr,"\th2        - Scale the buffer down to half horizontal size and put it in the right area\n");
  fprintf(stderr,"\tq         - Scale the buffer down to quarter size\n");
  fprintf(stderr,"\tv num     - Open V4L2 device /dev/videoNUM, default is 0\n");
  fprintf(stderr,"\tw name    - select plane by name on output, default is first plane\n");
  fprintf(stderr,"\tt std     - select V4L2_STD_... video standard\n");
  fprintf(stderr,"\ts         - set silence\n");
  fprintf(stderr,"\t            here's a selection of some possible standards:\n");
  printf_standard(V4L2_STD_VGA_60);
  printf_standard(V4L2_STD_VGA_59_94);
  printf_standard(V4L2_STD_480P_60);
  printf_standard(V4L2_STD_480P_59_94);
  printf_standard(V4L2_STD_576P_50);
  printf_standard(V4L2_STD_1080P_60);
  printf_standard(V4L2_STD_1080P_59_94);
  printf_standard(V4L2_STD_1080P_50);
  printf_standard(V4L2_STD_1080P_23_98);
  printf_standard(V4L2_STD_1080P_24);
  printf_standard(V4L2_STD_1080P_25);
  printf_standard(V4L2_STD_1080P_29_97);
  printf_standard(V4L2_STD_1080P_30);
  printf_standard(V4L2_STD_1080I_60);
  printf_standard(V4L2_STD_1080I_59_94);
  printf_standard(V4L2_STD_1080I_50);
  printf_standard(V4L2_STD_720P_60);
  printf_standard(V4L2_STD_720P_59_94);
  printf_standard(V4L2_STD_720P_50);
  fprintf(stderr,"\tx num     - Crop the source image at starting at the given pixel number\n");
  fprintf(stderr,"\ty num     - Crop the source image at starting at the given line number\n");
  fprintf(stderr,"\tz         - Do a dynamic horizontal zoom out\n");
  fprintf(stderr,"\te         - vErbose\n");
}

static void usage_during_playback(void)
{
  fprintf(stderr,"\n");
  fprintf(stderr,"The following commands can be issued during playback by pressing the\n");
  fprintf(stderr,"(b)racketed key and pressing return:\n");
  fprintf(stderr,"\n");
  fprintf(stderr,"  (c)continue                  numberofiteration set a new number of iteration, before stop pushing buffer/\n");
  fprintf(stderr,"  Plane (a)lpha mode           Set plane alpha.\n");
  fprintf(stderr,"  Asp(e)ctRatioConvMode        Change aspect ratio conversion mode:\n");
  fprintf(stderr,"                                 0=letter box\n");
  fprintf(stderr,"                                 1=pan&scan\n");
  fprintf(stderr,"                                 2=combined\n");
  fprintf(stderr,"                                 3=ignore\n");
  fprintf(stderr,"  OutputDisplay Asp(E)ctRatio  Change output display aspect ratio:\n");
  fprintf(stderr,"                                 0=16/9\n");
  fprintf(stderr,"                                 1=4/3\n");
  fprintf(stderr,"                                 2=14/9\n");
  fprintf(stderr,"  (h)elp                       Display this help text.\n");
  fprintf(stderr,"  (j)window mode               Set output in auto(1)/manual(0) mode or switch if no parameter provided.\n");
  fprintf(stderr,"  (J)window mode               Set input in auto(1)/manual(0) mode or switch if no parameter provided.\n");
  fprintf(stderr,"  (w)output window value       Resize output display window.\n");
  fprintf(stderr,"  (W)input window value        Resize input window crop.\n");
  fprintf(stderr,"  (z)output window value       Smooth Resize until output display window.\n");
  fprintf(stderr,"  (Z)input window value        Smooth Resize until input window crop.\n");
  fprintf(stderr,"  (t)ransform buffer content : need 5 | 6 | 7 | 8 param : X Y W H mire_type <border_color/NOBORDER> <codebarvalue codebarnbbars DUAL/SINGLE> <inc> \n");
  fprintf(stderr,"                               note1: img_common_tool is needed as call by system commands\n");
  fprintf(stderr,"                               note2: if <codebarvalue> needed and if no border_color needed: it is mandatory to put NOBORDER \n");
  fprintf(stderr,"                               note3: if codebarvalue needed, it is mandatory to put all of the three parameters <codebarvalue codebarnbbars DUAL/SINGLE>\n");
  fprintf(stderr,"                               note4: <inc> if you want increment codebar in each buffer \n");
  fprintf(stderr,"                               mire_type:COLORBAR,COLORMIRE3X3,SOLIDBARWHITE/BLACK/GREEN/BLUE/YELLOW/CYAN/MAGENTA/GREY\n");
  fprintf(stderr,"  e(x)it                       Exit.\n");
}

static void usage(char *prgName)
{
  usage_begin_decode(prgName);
  usage_during_playback();
  exit(1);
}

void show_gam_colorformat(enum gamfile_type colorformat)
{
  switch (colorformat)
    {
    case GAMFILE_UNKNOWN:prog__printf("%s: colorformat %d is GAMFILE_UNKNOWN\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_RGB565:prog__printf("%s: colorformat %d is GAMFILE_RGB565\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_RGB888:prog__printf("%s: colorformat %d is GAMFILE_RGB888\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_xRGB8888:prog__printf("%s: colorformat %d is GAMFILE_xRGB8888\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_ARGB8565:prog__printf("%s: colorformat %d is GAMFILE_ARGB8565\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_ARGB8888:prog__printf("%s: colorformat %d is GAMFILE_ARGB8888\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_ARGB1555:prog__printf("%s: colorformat %d is GAMFILE_ARGB1555\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_ARGB4444:prog__printf("%s: colorformat %d is GAMFILE_ARGB4444\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_CLUT1:prog__printf("%s: colorformat %d is GAMFILE_CLUT1\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_CLUT2:prog__printf("%s: colorformat %d is GAMFILE_CLUT2\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_CLUT4:prog__printf("%s: colorformat %d is GAMFILE_CLUT4\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_CLUT8:prog__printf("%s: colorformat %d is GAMFILE_CLUT8\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_ACLUT44:prog__printf("%s: colorformat %d is GAMFILE_ACLUT44\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_ACLUT88:prog__printf("%s: colorformat %d is GAMFILE_ACLUT88\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_CLUT8_4444:prog__printf("%s: colorformat %d is GAMFILE_CLUT8_4444\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCBCR888:prog__printf("%s: colorformat %d is GAMFILE_YCBCR888\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCBCR101010:prog__printf("%s: colorformat %d is GAMFILE_YCBCR101010\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCBCR422R:prog__printf("%s: colorformat %d is GAMFILE_YCBCR422R\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCBCR420MB:prog__printf("%s: colorformat %d is GAMFILE_YCBCR420MB\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCBCR422MB:prog__printf("%s: colorformat %d is GAMFILE_YCBCR422MB\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_AYCBCR8888:prog__printf("%s: colorformat %d is GAMFILE_AYCBCR8888\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_A1:prog__printf("%s: colorformat %d is GAMFILE_A1\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_A8:prog__printf("%s: colorformat %d is GAMFILE_A8\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_I420:prog__printf("%s: colorformat %d is GAMFILE_I420\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCBCR420R2B:prog__printf("%s: colorformat %d is GAMFILE_YCBCR420R2B\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCBCR422R2B:prog__printf("%s: colorformat %d is GAMFILE_YCBCR422R2B\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_XY:prog__printf("%s: colorformat %d is GAMFILE_XY\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_XYL:prog__printf("%s: colorformat %d is GAMFILE_XYL\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_XYC:prog__printf("%s: colorformat %d is GAMFILE_XYC\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_XYLC:prog__printf("%s: colorformat %d is GAMFILE_XYLC\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_RGB101010:prog__printf("%s: colorformat %d is GAMFILE_RGB101010\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_CRYCBY422R:prog__printf("%s: colorformat %d is GAMFILE_CRYCBY422R\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCBYCR422R:prog__printf("%s: colorformat %d is GAMFILE_YCBYCR422R\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCRYCB422R:prog__printf("%s: colorformat %d is GAMFILE_YCRYCB422R\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_CBYCRA444R:prog__printf("%s: colorformat %d is GAMFILE_CBYCRA444R\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_ARGB8565_24:prog__printf("%s: colorformat %d is GAMFILE_ARGB8565_24\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_RGB888_24:prog__printf("%s: colorformat %d is GAMFILE_RGB888_24\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_RGBA8888:prog__printf("%s: colorformat %d is GAMFILE_RGBA8888\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_CBYCR444R:prog__printf("%s: colorformat %d is GAMFILE_CBYCR444R\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCBCR_10B_420R2B:prog__printf("%s: colorformat %d is GAMFILE_YCBCR_10B_420R2B\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCBCR_10B_422R2B:prog__printf("%s: colorformat %d is GAMFILE_YCBCR_10B_422R2B\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCRCB_10B_422R2B:prog__printf("%s: colorformat %d is GAMFILE_YCRCB_10B_422R2B\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_YCBCR_10B_422R:prog__printf("%s: colorformat %d is GAMFILE_YCBCR_10B_422R\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_CBYCR_10B_444R:prog__printf("%s: colorformat %d is GAMFILE_CBYCR_10B_444R\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_RGBX8888:prog__printf("%s: colorformat %d is GAMFILE_RGBX8888\n",PRG_TRACE_PREDIX, colorformat);break;
    case GAMFILE_CBYCRX444R:prog__printf("%s: colorformat %d is GAMFILE_CBYCRX444R\n",PRG_TRACE_PREDIX, colorformat);break;
    default :
      prog__printf("%s: fct %s colorformat is UNKNOWN : %d\n",PRG_TRACE_PREDIX, __FUNCTION__, colorformat);break;
    }
}


void show_v4l2_colorformat(int colorformat)
{
  switch (colorformat)
    {
      //from sources/linux/kernelspace/modules/st/hals/stlinuxtv/linux/include/linux/stmvout.h
    case V4L2_PIX_FMT_STM422MB:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_STM422MB\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_STM420MB:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_STM420MB\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_BGRA5551:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_BGRA5551\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_BGRA4444:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_BGRA4444\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_ARGB8565:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_ARGB8565\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_CLUT2:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_CLUT2\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_CLUT4:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_CLUT4\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_CLUT8:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_CLUT8\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_CLUTA8:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_CLUTA8\n",PRG_TRACE_PREDIX, colorformat);break;
      //from sources/linux/kernelspace/linux-kernel/kernel3/include/linux/videodev2.h
    case V4L2_PIX_FMT_RGB332:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_RGB332\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_RGB444:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_RGB444\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_RGB555:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_RGB555\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_RGB565:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_RGB565\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_RGB555X:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_RGB555X\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_RGB565X:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_RGB565X\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_BGR666:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_BGR666\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_BGR24:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_BGR24\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_RGB24:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_RGB24\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_BGR32:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_BGR32\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_RGB32:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_RGB32\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_GREY:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_GREY\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_Y4:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_Y4\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_Y6:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_Y6\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_Y10:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_Y10\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_Y12:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_Y12\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_Y16:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_Y16\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_Y10BPACK:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_Y10BPACK\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_PAL8:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_PAL8\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YVU410:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YVU410\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YVU420:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YVU420\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YUYV:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YUYV\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YYUV:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YYUV\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YVYU:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YVYU\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_UYVY:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_UYVY\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_VYUY :prog__printf("%s: colorformat %d is V4L2_PIX_FMT_VYUY\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YUV422P:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YUV422P\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YUV411P:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YUV411P\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_Y41P:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_Y41P\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YUV444:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YUV444\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YUV555:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YUV555\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YUV565:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YUV565\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YUV32:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YUV32\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YUV410:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YUV410\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YUV420:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YUV420\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_HI240:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_HI240\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_HM12:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_HM12\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_M420:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_M420\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_NV12:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_NV12\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_NV21:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_NV21\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_NV16:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_NV16\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_NV61:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_NV61\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_NV24:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_NV24\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_NV42:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_NV42\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_NV12M:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_NV12M\n",PRG_TRACE_PREDIX, colorformat);break;
      //    case V4L2_PIX_FMT_NV16M:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_NV16M\n",PRG_TRACE_PREDIX, colorformat);break;
      //    case V4L2_PIX_FMT_NV24M:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_NV24M\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_NV12MT:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_NV12MT\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_YUV420M:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_YUV420M\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SBGGR8:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SBGGR8\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SGBRG8:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SGBRG8\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SGRBG8:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SGRBG8\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SRGGB8:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SRGGB8\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SBGGR10:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SBGGR10\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SGBRG10:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SGBRG10\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SGRBG10:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SGRBG10\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SRGGB10:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SRGGB10\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SBGGR12:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SBGGR12\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SGBRG12:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SGBRG12\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SGRBG12:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SGRBG12\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SRGGB12:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SRGGB12\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SGRBG10DPCM8:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SGRBG10DPCM8\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SBGGR16:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SBGGR16\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_MJPEG:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_MJPEG\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_JPEG:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_JPEG\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_DV:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_DV\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_MPEG:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_MPEG\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_H264:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_H264\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_H264_NO_SC:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_H264_NO_SC\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_H263:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_H263\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_MPEG1:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_MPEG1\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_MPEG2:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_MPEG2\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_MPEG4:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_MPEG4\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_XVID:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_XVID\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_VC1_ANNEX_G:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_VC1_ANNEX_G\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_VC1_ANNEX_L:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_VC1_ANNEX_L\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_CPIA1:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_CPIA1\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_WNVA:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_WNVA\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SN9C10X:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SN9C10X\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SN9C20X_I420:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SN9C20X_I420\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_PWC1:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_PWC1\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_PWC2:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_PWC2\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_ET61X251 :prog__printf("%s: colorformat %d is V4L2_PIX_FMT_ET61X251\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SPCA501:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SPCA501\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SPCA505:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SPCA505\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SPCA508:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SPCA508\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SPCA561:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SPCA561\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_PAC207:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_PAC207\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_MR97310A:prog__printf("%s: colorformat %d isV4L2_PIX_FMT_MR97310A\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_JL2005BCD:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_JL2005BCD\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SN9C2028:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SN9C2028\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SQ905C:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SQ905C\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_PJPG:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_PJPG\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_OV511:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_OV511\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_OV518:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_OV518\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_STV0680:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_STV0680\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_TM6000:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_TM6000\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_CIT_YYVYUY:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_CIT_YYVYUY\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_KONICA420:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_KONICA420\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_JPGL:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_JPGL\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_SE401:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_SE401\n",PRG_TRACE_PREDIX, colorformat);break;

    case V4L2_PIX_FMT_STI_YUV444_10:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_STI_YUV444_10\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_STI_YUV422_10:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_STI_YUV422_10\n",PRG_TRACE_PREDIX, colorformat);break;
    case V4L2_PIX_FMT_STI_YUV420_10:prog__printf("%s: colorformat %d is V4L2_PIX_FMT_STI_YUV420_10\n",PRG_TRACE_PREDIX, colorformat);break;

    default :
      prog__printf("%s: colorformat is UNKNOWN : %d\n",PRG_TRACE_PREDIX, colorformat);break;
    }
}

void show_v4l2_colorspace(int colorspace)
{
  switch (colorspace)
    {
    case V4L2_COLORSPACE_SMPTE170M:
      prog__printf("%s: colorspace %d is V4L2_COLORSPACE_SMPTE170M : ITU-R 601 -- broadcast NTSC/PAL\n",PRG_TRACE_PREDIX, colorspace);break;
    case V4L2_COLORSPACE_SMPTE240M:
      prog__printf("%s: colorspace %d is V4L2_COLORSPACE_SMPTE240M: 1125-Line (US) HDTV\n",PRG_TRACE_PREDIX, colorspace);break;
    case V4L2_COLORSPACE_REC709:
      prog__printf("%s: colorspace %d is V4L2_COLORSPACE_REC709 : HD and modern captures\n",PRG_TRACE_PREDIX, colorspace);break;
    case V4L2_COLORSPACE_BT878:
      prog__printf("%s: colorspace %d is V4L2_COLORSPACE_BT878 : broken BT878 extents (601, luma range 16-253 instead of 16-235\n",PRG_TRACE_PREDIX, colorspace);break;
    case V4L2_COLORSPACE_470_SYSTEM_M:
      prog__printf("%s: colorspace %d is V4L2_COLORSPACE_470_SYSTEM_M : These should be useful.  Assume 601 extents\n",PRG_TRACE_PREDIX, colorspace);break;
    case V4L2_COLORSPACE_470_SYSTEM_BG:
      prog__printf("%s: colorspace %d is V4L2_COLORSPACE_470_SYSTEM_BG\n",PRG_TRACE_PREDIX, colorspace);break;
    case V4L2_COLORSPACE_JPEG:
      prog__printf("%s: colorspace %d is V4L2_COLORSPACE_JPEG : unspecified chromaticities and full 0-255 on each of the* Y'CbCr components\n",PRG_TRACE_PREDIX, colorspace);break;
    case V4L2_COLORSPACE_SRGB:
      prog__printf("%s: colorspace %d is V4L2_COLORSPACE_SRGB : For RGB colourspaces, this is probably a good start\n",PRG_TRACE_PREDIX, colorspace);break;
    default :
      prog__printf("%s: colorspace is UNKNOWN : %d\n",PRG_TRACE_PREDIX, colorspace);break;
    }
}

void show_v4l2_field(int filed)
{
  switch (filed)
    {

    case V4L2_FIELD_ANY: prog__printf("%s: filed %d is V4L2_FIELD_ANY , driver can choose from none, top, bottom, interlaced depending on whatever it thinks is approximate ... \n",PRG_TRACE_PREDIX, filed);break;
    case V4L2_FIELD_NONE: prog__printf("%s: filed %d is V4L2_FIELD_NONE , this device has no fields ... \n",PRG_TRACE_PREDIX, filed);break;
    case V4L2_FIELD_TOP: prog__printf("%s: filed %d is V4L2_FIELD_TOP , top field only \n",PRG_TRACE_PREDIX, filed);break;
    case V4L2_FIELD_BOTTOM : prog__printf("%s: filed %d is V4L2_FIELD_BOTTOM , bottom field only \n",PRG_TRACE_PREDIX, filed);break;
    case V4L2_FIELD_INTERLACED: prog__printf("%s: filed %d is V4L2_FIELD_INTERLACED , both fields interlaced \n",PRG_TRACE_PREDIX, filed);break;
    case V4L2_FIELD_SEQ_TB: prog__printf("%s: filed %d is V4L2_FIELD_SEQ_TB , both fields sequential into one buffer, top-bottom order \n",PRG_TRACE_PREDIX, filed);break;
    case V4L2_FIELD_SEQ_BT: prog__printf("%s: filed %d is V4L2_FIELD_SEQ_BT , same as above + bottom-top order \n",PRG_TRACE_PREDIX, filed);break;
    case V4L2_FIELD_ALTERNATE: prog__printf("%s: filed %d is V4L2_FIELD_ALTERNATE , both fields alternating into separate buffers \n",PRG_TRACE_PREDIX, filed);break;
    case V4L2_FIELD_INTERLACED_TB: prog__printf("%s: filed %d is V4L2_FIELD_INTERLACED_TB , both fields interlaced, top field first and the top field is transmitted first \n",PRG_TRACE_PREDIX, filed);break;
    case V4L2_FIELD_INTERLACED_BT: prog__printf("%s: filed %d is V4L2_FIELD_INTERLACED_BT , both fields interlaced, top field first and the bottom field is transmitted first \n",PRG_TRACE_PREDIX, filed);break;
    default :
      prog__printf("%s: filed is UNKNOWN : %d\n",PRG_TRACE_PREDIX, filed);break;
    }
}


int GetFormatNameForImgTool(char *format_for_imgtool, int pixfmt)
{
  int ret = 0;
  switch (pixfmt)
    {
    case V4L2_PIX_FMT_BGR32:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_BGR32\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"argb8888", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_BGR24:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_BGR24\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"rgb888", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_BGRA4444:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_BGRA4444\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"argb4444", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_RGB565:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_RGB565\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"rgb565", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_ARGB8565:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_ARGB8565\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"argb8565", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_BGRA5551:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_BGRA5551\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"argb1555", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_YUV32:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_YUV32\n",PRG_TRACE_PREDIX, __FUNCTION__);
      // not yet supported
      strncpy(format_for_imgtool,"UNSUPPORTED_V4L2_PIX_FMT_YUV32", FORMAT_STR_SIZE - 1);
      ret = -1;
      break;
    case V4L2_PIX_FMT_UYVY:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_UYVY\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"yuv422ilcbycry", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_VYUY:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_VYUY\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"yuv422ilcrycby", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_YUYV:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_YUYV\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"yuv422ilycbycr", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_YVYU:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_YVYU\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"yuv422ilycrycb", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_NV12:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_NV12\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"yuv420nv12semiplanar", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_STM420MB:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_STM420MB\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"yuv420mbpsp", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_STM422MB:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_STM422MB\n",PRG_TRACE_PREDIX, __FUNCTION__);
      // not yet supported
      strncpy(format_for_imgtool,"UNSUPPORTED_V4L2_PIX_FMT_STM422MB", FORMAT_STR_SIZE - 1);
      ret = -1;
      break;
    case V4L2_PIX_FMT_NV16:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_NV16\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"yuv422semiplanarycbcr", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_CLUT8:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_CLUT8\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"clut8", FORMAT_STR_SIZE - 1);
      break;

    case V4L2_PIX_FMT_STI_YUV444_10:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_STI_YUV444_10\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"yuv444bits10ilcbycr", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_STI_YUV422_10:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_STI_YUV422_10\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"yuv422bits10semiplanarycbcr", FORMAT_STR_SIZE - 1);
      break;
    case V4L2_PIX_FMT_STI_YUV420_10:
      prog__printf("%s: fct %s case V4L2_PIX_FMT_STI_YUV420_10\n",PRG_TRACE_PREDIX, __FUNCTION__);
      strncpy(format_for_imgtool,"yuv420bits10semiplanarycbcr", FORMAT_STR_SIZE - 1);
      break;

    default:
      strncpy(format_for_imgtool,"UNSUPPORTED", FORMAT_STR_SIZE - 1);
      printf( "%s: ERROR : fct %s, line %d : V4L2_PIX_FMT format not supported :  pixfmt = 0x%x\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__, pixfmt);
      ret = -1;
      break;
    }
  prog__printf("%s: fct %s, format_for_imgtool = %s\n",PRG_TRACE_PREDIX, __FUNCTION__, format_for_imgtool);
  return ret;
}

static FILE *read_gam_header(char *file_name, int *pixfmt_p, GamPictureHeader_t * GamPictureHeader_p, unsigned int *pitch_p, enum v4l2_colorspace *cspace_p, unsigned int *size_area_1, unsigned int *size_area_2)
{
  FILE *fp = (FILE*)NULL;
  int readBytes=0;
  unsigned int used_height=0;
  //  int real_size_of_data_gam=0;

  fp = fopen(file_name, "rb");
  if (fp == NULL)
    {
      printf( "%s: ERROR : File not found!\n",PRG_TRACE_PREDIX);
      return 0;
    }

  // Read Gam header
  readBytes = fread(GamPictureHeader_p, 1, 4, fp);
  fseek(fp, 0, SEEK_SET);
  readBytes = fread(GamPictureHeader_p, 1, GamPictureHeader_p->HeaderSize * 4, fp);
  if (readBytes < (6 * 4) )
    {
      printf( "%s: ERROR : while reading the GAM Header!\n",PRG_TRACE_PREDIX);
      prog__printf("%s: free (fct %s, line %d) : fclose(fp = 0x%x) \n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__, (int)fp);
      fclose(fp);
      fp = (FILE*)NULL;
      return 0;
    }

  switch (GamPictureHeader_p->Type)
    {
    case GAMFILE_CLUT8:
      // 0x8B = CLUT8
      *pixfmt_p = V4L2_PIX_FMT_CLUT8;
      *pitch_p = GamPictureHeader_p->PictureWidth;
      *cspace_p = V4L2_COLORSPACE_SRGB;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_CLUT8 = 0x%x => V4L2_PIX_FMT_CLUT8 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
     break;

    case GAMFILE_ARGB8888:
      // 0x85 = ARGB 8888
      *cspace_p = V4L2_COLORSPACE_SRGB;
      *pixfmt_p = V4L2_PIX_FMT_BGR32;
      *pitch_p = 4 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_ARGB8888 = 0x%x => V4L2_PIX_FMT_BGR32 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_RGB888:
      // 0x81 = RGB 8888
      *pixfmt_p = V4L2_PIX_FMT_BGR32;
      *cspace_p = V4L2_COLORSPACE_SRGB;
      *pitch_p = 4 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_RGB888 = 0x%x => V4L2_PIX_FMT_BGR32 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_RGB888_24:
      // 0xE6 = RGB888_24
      *pixfmt_p = V4L2_PIX_FMT_BGR24;
      *cspace_p = V4L2_COLORSPACE_SRGB;
      *pitch_p = 3 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_RGB888_24 = 0x%x => V4L2_PIX_FMT_BGR24 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_ARGB4444:
      // 0x87 = ARGB 4444
      *pixfmt_p = V4L2_PIX_FMT_BGRA4444;
      *cspace_p = V4L2_COLORSPACE_SRGB;
      *pitch_p = 2 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_ARGB4444 = 0x%x => V4L2_PIX_FMT_BGRA4444 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_RGB565:
      // 0x80 = RGB 565
      *pixfmt_p = V4L2_PIX_FMT_RGB565;
      *cspace_p = V4L2_COLORSPACE_SRGB;
      *pitch_p = 2 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_RGB565 = 0x%x => V4L2_PIX_FMT_RGB565 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_ARGB8565_24:
      // 0xE5 = ARGB8565_24
      *pixfmt_p = V4L2_PIX_FMT_ARGB8565;
      *cspace_p = V4L2_COLORSPACE_SRGB;
      *pitch_p = 3 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_ARGB8565_24 = 0x%x => V4L2_PIX_FMT_ARGB8565 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_ARGB1555:
      // 0x86 = ARGB 1555
      *pixfmt_p = V4L2_PIX_FMT_BGRA5551;
      *cspace_p = V4L2_COLORSPACE_SRGB;
      *pitch_p = 2 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_ARGB1555 = 0x%x => V4L2_PIX_FMT_BGRA5551 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_AYCBCR8888:
      // 0x96 = AYCBCR8888
      *pixfmt_p = V4L2_PIX_FMT_YUV32;
      *cspace_p = V4L2_COLORSPACE_SMPTE170M;
      *pitch_p = 4 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_AYCBCR8888 = 0x%x => V4L2_PIX_FMT_YUV32 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_YCBCR422R:
      // 0x92 = YcbCr 4:2:2 Raster
      *pixfmt_p = V4L2_PIX_FMT_UYVY;
      *cspace_p = V4L2_COLORSPACE_SMPTE170M;
      *pitch_p = 2 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_YCBCR422R = 0x%x => V4L2_PIX_FMT_UYVY = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_CRYCBY422R :
      // 0xE1
      // ColorFmt = YUV422_V8Y8U8Y8_R;
      *pixfmt_p = V4L2_PIX_FMT_VYUY;
      *cspace_p = V4L2_COLORSPACE_SMPTE170M;
      *pitch_p = 2 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_CRYCBY422R = 0x%x => V4L2_PIX_FMT_VYUY = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_YCBYCR422R  :
      // 0xE2
      // ColorFmt = YUV422_Y8U8Y8V8_R;
      *pixfmt_p = V4L2_PIX_FMT_YUYV;
      *cspace_p = V4L2_COLORSPACE_SMPTE170M;
      *pitch_p = 2 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_YCBYCR422R = 0x%x => V4L2_PIX_FMT_YUYV = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_YCRYCB422R  :
      // 0xE3
      // ColorFmt = YUV422_Y8V8Y8U8_R;
      *pixfmt_p = V4L2_PIX_FMT_YVYU;
      *cspace_p = V4L2_COLORSPACE_SMPTE170M;
      *pitch_p = 2 * GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = 0;
      prog__printf("%s: Pixel format is GAMFILE_YCRYCB422R = 0x%x => V4L2_PIX_FMT_YVYU = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      if ( GamPictureHeader_p->gh_w02 != 0)
      {
          prog__printf("%s: gam header problem with gh_w02 not equal to 0, but %d\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);
          goto header_problem;
      }
      break;

    case GAMFILE_YCBCR420R2B:
      // 0xB0 = YcbCr 4:2:0 Raster Double Buffer
      // ColorFmt = SURF_YCbCr420R2B;
      *pixfmt_p = V4L2_PIX_FMT_NV12;
      *cspace_p = V4L2_COLORSPACE_SMPTE170M;
      // With 4:2:0 R2B, PictureWidth is a OR between Pitch and Width (each one on 16 bits)
      *pitch_p = GamPictureHeader_p->PictureWidth >> 16;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = *pitch_p * GamPictureHeader_p->PictureHeight / 2;
      if (*pitch_p == 0)
      {
          goto header_problem;
      }
      GamPictureHeader_p->PictureWidth = GamPictureHeader_p->PictureWidth & 0xFFFF;

      prog__printf("%s: Pixel format is GAMFILE_YCBCR420R2B = 0x%x => V4L2_PIX_FMT_NV12 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      break;

    case GAMFILE_YCBCR420MB:
      // 0x94 = OMEGA2 4:2:0 (= Macroblock)
      // ColorFmt = SURF_YCBCR420MB;
      *pixfmt_p = V4L2_PIX_FMT_STM420MB;
      *cspace_p = V4L2_COLORSPACE_SMPTE170M;
      *pitch_p = GamPictureHeader_p->PictureWidth;
      prog__printf("%s: Gam pitch: %u \n",PRG_TRACE_PREDIX, *pitch_p);
      *pitch_p = ((*pitch_p + 15) / 16) * 16;
      prog__printf("%s: Gam pitch: %u is aligned to a multiple of 16\n",PRG_TRACE_PREDIX, *pitch_p);
      used_height = ((GamPictureHeader_p->PictureHeight + 15) / 16) * 16;
      prog__printf("%s: used_height = %d as PictureHeight = %d\n",PRG_TRACE_PREDIX, used_height, GamPictureHeader_p->PictureHeight);
      *size_area_1 = *pitch_p * used_height;
      *size_area_2 = *pitch_p * used_height / 2;
      prog__printf("%s: size_area_1 = %u\n",PRG_TRACE_PREDIX, *size_area_1);
      prog__printf("%s: size_area_2 = %u\n",PRG_TRACE_PREDIX, *size_area_2);
      prog__printf("%s: Pixel format is GAMFILE_YCBCR420MB = 0x%x => V4L2_PIX_FMT_STM420MB = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      break;

    case GAMFILE_YCBCR422MB:
      // 0x95 = OMEGA2 4:2:2 (= Macroblock)
      // ColorFmt = SURF_YCBCR422MB;
      *pixfmt_p = V4L2_PIX_FMT_STM422MB;
      *cspace_p = V4L2_COLORSPACE_SMPTE170M;
      *pitch_p = GamPictureHeader_p->PictureWidth;
      *pitch_p = ((*pitch_p + 15) / 16) * 16;
      prog__printf("%s: Gam pitch: %u is aligned to a multiple of 16\n",PRG_TRACE_PREDIX, *pitch_p);
      used_height = ((GamPictureHeader_p->PictureHeight + 15) / 16) * 16;
      prog__printf("%s: used_height = %d as PictureHeight = %d\n",PRG_TRACE_PREDIX, used_height, GamPictureHeader_p->PictureHeight);
      *size_area_1 = *pitch_p * used_height;
      *size_area_2 = *pitch_p * used_height / 1;
      prog__printf("%s: Pixel format is GAMFILE_YCBCR422MB = 0x%x => V4L2_PIX_FMT_STM422MB = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      break;

    case GAMFILE_YCBCR422R2B:
      // 0xB1 = YcbCr 4:2:2 Raster Double Buffer
      // ColorFmt = SURF_YCbCr422R2B;
      *pixfmt_p = V4L2_PIX_FMT_NV16;
      *cspace_p = V4L2_COLORSPACE_SMPTE170M;
      // With 4:2:2 R2B, PictureWidth is a OR between Pitch and Width (each one on 16 bits)
      *pitch_p = GamPictureHeader_p->PictureWidth;
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = *pitch_p * GamPictureHeader_p->PictureHeight / 1;
      prog__printf("%s: Pixel format is GAMFILE_YCBCR422R2B = 0x%x => V4L2_PIX_FMT_NV16 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      break;

    case GAMFILE_YCBCR_10B_420R2B:
      // 0xB2
      *pixfmt_p = V4L2_PIX_FMT_STI_YUV420_10;
      *cspace_p = V4L2_COLORSPACE_SMPTE170M;
      *pitch_p = ((GamPictureHeader_p->PictureWidth * 10 + 127) / 128) * 128 / 8; // 128 bits aligned
      prog__printf("%s: Gam pitch: %u is aligned to a multiple of 128 bits\n",PRG_TRACE_PREDIX, *pitch_p);
      *size_area_1 = *pitch_p * GamPictureHeader_p->PictureHeight;
      *size_area_2 = *pitch_p * GamPictureHeader_p->PictureHeight / 2;
      prog__printf("%s: Pixel format is GAMFILE_YCBCR_10B_420R2B = 0x%x => V4L2_PIX_FMT_STI_YUV420_10 = 0x%x\n",PRG_TRACE_PREDIX, GamPictureHeader_p->Type, *pixfmt_p);
      break;

//V4L2_PIX_FMT_STI_YUV444_10:
//V4L2_PIX_FMT_STI_YUV422_10:

    default:
      *size_area_1 = 0;
      *size_area_2 = 0;
      show_gam_colorformat(GamPictureHeader_p->Type);
      printf( "%s: ERROR : fct %s, line %d : GamPictureHeader_p->Type = 0x%x pixel format not taken into account\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__, GamPictureHeader_p->Type);
      prog__printf("%s: free (fct %s, line %d) : fclose(fp = 0x%x) \n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__, (int)fp);
      fclose(fp);
      fp = (FILE*)NULL;
      return 0;
    }

  prog__printf("%s: Gam width: %u\n",PRG_TRACE_PREDIX, GamPictureHeader_p->PictureWidth);
  prog__printf("%s: Gam height: %u\n",PRG_TRACE_PREDIX, GamPictureHeader_p->PictureHeight);
  prog__printf("%s: Gam pitch: %u\n",PRG_TRACE_PREDIX, *pitch_p);

  prog__printf("%s: Gam gh_w01: %u\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w01);
  prog__printf("%s: Gam gh_w02: %u\n",PRG_TRACE_PREDIX, GamPictureHeader_p->gh_w02);

/*
{
    unsigned int fileSize;
  // Obtain file size
  fseek(fp, 0, SEEK_END);
  fileSize = ftell(fp);
  fseek(fp, 0, SEEK_SET);
    if( fileSize != ( GamPictureHeader_p->HeaderSize * 4 + real_size_of_data_gam ) )
    {
        prog__printf("%s: Invalid data : real_size_of_data_gam = %d\n",PRG_TRACE_PREDIX, real_size_of_data_gam);
        goto header_problem;
    }
}
*/
  return fp;

header_problem :
prog__printf("%s: header_problem \n",PRG_TRACE_PREDIX);
prog__printf("%s: free (fct %s, line %d) : fclose(fp = 0x%x) \n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__, (int)fp);
fclose(fp);
fp = (FILE*)NULL;
return 0;

}

static int read_gam_buffer(FILE * fp, char *buffer, GamPictureHeader_t GamPictureHeader, unsigned int srcStrideInBytes, struct v4l2_pix_format pix, char** ptr_buf_color_palette, unsigned int size_area_1, unsigned int size_area_2)
{
  unsigned int dstWidth = pix.bytesperline;
  char *area_1_BufferAddr = buffer;
  char *area_2_BufferAddr = buffer + pix.priv;
  unsigned int area_1_BufferSize =  (pix.priv == 0 ? pix.sizeimage : pix.priv);
  unsigned int area_2_BufferSize = pix.sizeimage - area_1_BufferSize;
  unsigned readBytes = 0, copiedBytes = 0;
  unsigned int reading_offset=0;
  prog__printf("%s: fct %s, line %d : GamPictureHeader.Type = 0x%x\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__, GamPictureHeader.Type);
  prog__printf("%s: srcStrideInBytes = %d\n",PRG_TRACE_PREDIX, srcStrideInBytes);
  prog__printf("%s: pix.priv = %d, pix.sizeimage = %d, pix.bytesperline = %d\n",PRG_TRACE_PREDIX, pix.priv, pix.sizeimage, pix.bytesperline);

  if (fp == NULL)
    {
      printf("%s: ERROR : fct %s, line %d : Invalid file !\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
      return -1;
    }

  // initialize buffer to zero
  memset(area_1_BufferAddr, 0x00, pix.sizeimage);

  switch (GamPictureHeader.Type)
    {
    case GAMFILE_CLUT8:
      {
        char *colour_palette=(char*)NULL;
        prog__printf("%s: case GAMFILE_CLUT8\n",PRG_TRACE_PREDIX);

        colour_palette = malloc(4*256);
        if(colour_palette)
          {
            prog__printf("%s: alloc (fct %s, line %d) : colour_palette \n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
            fseek(fp, GamPictureHeader.HeaderSize * 4, SEEK_SET);
            prog__printf("%s: Reading for GAMFILE_CLUT8 : color_palette\n",PRG_TRACE_PREDIX);
            fread(colour_palette, 1, 256*4, fp);
            *ptr_buf_color_palette = colour_palette;
            //free(colour_palette); will be done later

            prog__printf("%s: Reading for GAMFILE_CLUT8 area_1_BufferSize = %d\n",PRG_TRACE_PREDIX, area_1_BufferSize);

            reading_offset=256*4;

            fseek(fp, GamPictureHeader.HeaderSize * 4 + 256*4, SEEK_SET);
            prog__printf("%s: fseek of header + palette = %d + 256*4 = %d\n",PRG_TRACE_PREDIX, GamPictureHeader.HeaderSize * 4 , GamPictureHeader.HeaderSize * 4 + 256*4);
            fread(area_1_BufferAddr, 1, area_1_BufferSize, fp);
          }
        else
          {
            printf( "%s: ERROR : fct %s, line %d : Error memory allocation for color_palette\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
          }
      }
      break;
      /*
        case GAMFILE_YCBCR420MB: // ftb_mb.gam created a problem with original line by line reading (asking to big size to fread made a segfault)
        {
        need_to_read_line_by_line = 0;
        prog__printf("%s: case GAMFILE_YCBCR420MB changing need_to_read_line_by_line to %d, as pix.bytesperline = %d and pix.width = %d, pix.height = %d, pix.sizeimage = %d\n",PRG_TRACE_PREDIX, need_to_read_line_by_line, pix.bytesperline, pix.width, pix.height, pix.sizeimage);
        }
        break;

        case GAMFILE_YCBCR422R: // width 720 => 1472 (v4l2 buffer has padding so feeding data requires to takes this padding into account)
        {
        need_to_read_line_by_line = 1;
        prog__printf("%s: case GAMFILE_YCBCR422R changing need_to_read_line_by_line to %d, as pix.bytesperline = %d and pix.width = %d, pix.height = %d, pix.sizeimage = %d\n",PRG_TRACE_PREDIX, need_to_read_line_by_line, pix.bytesperline, pix.width, pix.height, pix.sizeimage);
        }
        break;
        case GAMFILE_YCBCR420R2B: // width 720 => 736 (v4l2 buffer has padding so feeding data requires to takes this padding into account)
        {
        need_to_read_line_by_line = 1;
        prog__printf("%s: case GAMFILE_YCBCR420R2B changing need_to_read_line_by_line to %d, as pix.bytesperline = %d and pix.width = %d, pix.height = %d, pix.sizeimage = %d\n",PRG_TRACE_PREDIX, need_to_read_line_by_line, pix.bytesperline, pix.width, pix.height, pix.sizeimage);
        }
        break;
      */
    default:
      break;
    }


    prog__printf("%s: Reading data line by line\n",PRG_TRACE_PREDIX);
    // read data to buffer line by line
    if (0 != fseek(fp, (GamPictureHeader.HeaderSize * 4 + reading_offset), SEEK_SET))
    {
        printf("%s: ERROR : fct %s, line %d : problem fseek  \n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
        return -1;
    }

    do
    {
        readBytes += fread(area_1_BufferAddr, 1, srcStrideInBytes, fp);
        copiedBytes += dstWidth;
        area_1_BufferAddr += dstWidth;
    } while ((readBytes < size_area_1) && (copiedBytes < area_1_BufferSize));
    prog__printf("%s: GamPictureHeader.gh_w01 = %d\n",PRG_TRACE_PREDIX, GamPictureHeader.gh_w01);
    prog__printf("%s: size_area_1 = %d\n",PRG_TRACE_PREDIX, size_area_1);
    prog__printf("%s: area_1_BufferSize = %d\n",PRG_TRACE_PREDIX, area_1_BufferSize);
    prog__printf("%s: area_1_Buffer copiedBytes = %d\n",PRG_TRACE_PREDIX, copiedBytes);
    prog__printf("%s: area_1_Buffer readBytes = %d\n",PRG_TRACE_PREDIX, readBytes);
    prog__printf("%s: reading packets of srcStrideInBytes = %d, and shifting dstWidth = %d\n",PRG_TRACE_PREDIX, srcStrideInBytes, dstWidth);


    if (size_area_2)
    {
        // seek the read pointer of the beginning of the area_2_Buffer
        if( 0 != fseek(fp, (GamPictureHeader.HeaderSize * 4 + reading_offset + GamPictureHeader.gh_w01), SEEK_SET))
        {
            printf("%s: ERROR : fct %s, line %d : problem fseek area_1_Buffer GamPictureHeader.gh_w01 = %d\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__, GamPictureHeader.gh_w01);
            return -1;
        }

        copiedBytes = 0;
        readBytes = 0;
       // read area_2_Buffer data to buffer line by line
        do
        {
            readBytes += fread(area_2_BufferAddr, 1, srcStrideInBytes, fp);
            copiedBytes += dstWidth;
            area_2_BufferAddr += dstWidth;
        } while ((readBytes < size_area_2)  && (copiedBytes < area_2_BufferSize));
        prog__printf("%s: GamPictureHeader.gh_w02 = %d\n",PRG_TRACE_PREDIX, GamPictureHeader.gh_w02);
        prog__printf("%s: size_area_2 = %d\n",PRG_TRACE_PREDIX, size_area_2);
        prog__printf("%s: area_2_BufferSize = %d\n",PRG_TRACE_PREDIX, area_2_BufferSize);
        prog__printf("%s: area_2_Buffer copiedBytes = %d\n",PRG_TRACE_PREDIX, copiedBytes);
        prog__printf("%s: area_2_Buffer readBytes = %d\n",PRG_TRACE_PREDIX, readBytes);
        prog__printf("%s: reading packets of srcStrideInBytes = %d, and shifting dstWidth = %d\n",PRG_TRACE_PREDIX, srcStrideInBytes, dstWidth);
    }
    else
    {
        prog__printf("%s: no need to read area_2_Buffer\n",PRG_TRACE_PREDIX);
    }


  prog__printf("%s: Read completed\n",PRG_TRACE_PREDIX);
  prog__printf("%s: free (fct %s, line %d) : fclose(fp = 0x%x) \n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__, (int)fp);
  fclose(fp);
  return 0;
}


video_window_t calculatenextstepforzoom(int v4lfd,video_window_t currentpos, video_window_t finalpos,int nbofstep) {

int stepXoffset,stepYoffset;
int xrest,xreststep,yrest,yreststep;

          prog__printf("%s: currentpos: (%d,%d,%d,%d)\n",PRG_TRACE_PREDIX,currentpos.x, currentpos.y,currentpos.width,currentpos.height);
          stepXoffset =  (currentpos.x - finalpos.x) / nbofstep;
          xrest = (finalpos.x + finalpos.width) - (currentpos.x + currentpos.width);
          xreststep = xrest / nbofstep;
          yrest = (finalpos.y + finalpos.height) - (currentpos.y + currentpos.height);
          yreststep = yrest / nbofstep;
          stepYoffset =  (currentpos.y - finalpos.y) / nbofstep;
          prog__printf("%s: stepXoffset: %d\n",PRG_TRACE_PREDIX,stepXoffset);
          prog__printf("%s: xrest: %d\n",PRG_TRACE_PREDIX,xrest);
          prog__printf("%s: xreststep: %d\n",PRG_TRACE_PREDIX,xreststep);
          prog__printf("%s: stepYoffset: %d\n",PRG_TRACE_PREDIX,stepYoffset);
          prog__printf("%s: yrest: %d\n",PRG_TRACE_PREDIX,yrest);
          prog__printf("%s: yreststep: %d\n",PRG_TRACE_PREDIX,yreststep);

          currentpos.x = currentpos.x - stepXoffset;
          currentpos.y = currentpos.y - stepYoffset;
          currentpos.width = currentpos.width + xreststep + stepXoffset;
          currentpos.height = currentpos.height + yreststep + stepYoffset;
          prog__printf("%s: currentpos: (%d,%d,%d,%d)\n",PRG_TRACE_PREDIX,currentpos.x, currentpos.y,currentpos.width,currentpos.height);
          return(currentpos);
}


static bool CheckKey(int v4lfd, char *prgName)
{
  bool Running = true;
  char Key;
  char show_Key[2]={0,0};
  static unsigned int AspectRatioConversionMode = 0;
  static enum _V4L2_CID_STM_OUTPUT_DISPLAY_ASPECT_RATIO   OutputDisplayAspectRatio = VCSOUTPUT_DISPLAY_ASPECT_RATIO_16_9;
  struct v4l2_control Control;
  struct v4l2_crop crop;
  unsigned int Parameters[16];
  unsigned int ParameterCount = 0;
  char Parameters_5[32];
  char Parameters_6[32];
  char Parameters_7[32];
  char line[128];

  if (fgets(line, sizeof(line), stdin) != NULL)
    {
      prog__printf("%s: line = %s(end)\n",PRG_TRACE_PREDIX, line);
      ParameterCount = sscanf(line, "%c\n", &Key);
      ParameterCount--;
      show_Key[0]=Key;
      prog__printf("%s: show_Key = %s(end)\n",PRG_TRACE_PREDIX, show_Key);
    }
  else
    {
      prog__printf("%s: setting Key to y (due to fgets)\n",PRG_TRACE_PREDIX);
      Key = 'y';
    }

  switch (Key)
    {
    case 'c':
      ParameterCount = sscanf(line, "%c %d\n", &Key, &Parameters[0]);
      ParameterCount--;
      global_iterations = Parameters[0];
      if (global_iterations < 5) global_iterations =5;   // smaller give bad effect! don't know why!
      break;
    case 'a':
      ParameterCount = sscanf(line, "%c %d\n", &Key, &Parameters[0]);
      ParameterCount--;
      if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_OUTPUT_ALPHA, &Parameters[0])) < 0)
        {
          perror("VIDIOC_S_CTRL VIDIOC_S_OUTPUT_ALPHA failed\n");
          printf( "%s: ERROR ioctl VIDIOC_S_OUTPUT_ALPHA (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
          return(false);
        }
      else
        {
          prog__printf("%s: OK ioctl VIDIOC_S_OUTPUT_ALPHA : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
        }
      break;

    case 'e':
      ParameterCount = sscanf(line, "%c %d\n", &Key, &Parameters[0]);
      ParameterCount--;
      // Switch to next incremental mode
      AspectRatioConversionMode++;
      if (AspectRatioConversionMode > 3)
        {
          AspectRatioConversionMode = 0;
        }
      // If Parameter given apply the Parameter mode
      if (ParameterCount != 0)
        {
          AspectRatioConversionMode = Parameters[0];
          ParameterCount = 0; // Disable Parameter mode for next time
        }

      Control.id = V4L2_CID_STM_ASPECT_RATIO_CONV_MODE;
      Control.value = AspectRatioConversionMode;
      prog__printf("%s: VIDIOC_S_CTRL V4L2_CID_STM_ASPECT_RATIO_CONV_MODE=%d\n",PRG_TRACE_PREDIX, AspectRatioConversionMode);
      if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_CTRL, &Control)) < 0)
        {
          perror("VIDIOC_S_CTRL V4L2_CID_STM_ASPECT_RATIO_CONV_MODE failed\n");
          printf( "%s: ERROR ioctl VIDIOC_S_CTRL (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
          return (false);
        }
      else
        {
          prog__printf("%s: OK ioctl VIDIOC_S_CTRL  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
        }

      Control.id = V4L2_CID_STM_ASPECT_RATIO_CONV_MODE;
      if (LTV_CHECK(ioctl(v4lfd, VIDIOC_G_CTRL, &Control)) < 0)
        {
          perror("VIDIOC_G_CTRL V4L2_CID_STM_ASPECT_RATIO_CONV_MODE failed\n");
          printf( "%s: ERROR ioctl VIDIOC_G_CTRL (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
          return (false);
        }
      else
        {
          prog__printf("%s: OK ioctl VIDIOC_G_CTRL  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
        }

      if (AspectRatioConversionMode != Control.value)
        {
          perror ("Plane aspect ratio conversion mode set and get are different.\n");
        }
      break;

    case 'E':
      ParameterCount = sscanf(line, "%c %d\n", &Key, &Parameters[0]);
      ParameterCount--;
      // Switch to next output aspect ratio value.
      OutputDisplayAspectRatio++;
      if (OutputDisplayAspectRatio < VCSOUTPUT_DISPLAY_ASPECT_RATIO_16_9 || OutputDisplayAspectRatio > VCSOUTPUT_DISPLAY_ASPECT_RATIO_14_9)
        {
          OutputDisplayAspectRatio =  VCSOUTPUT_DISPLAY_ASPECT_RATIO_16_9;
        }
      // If Parameter given apply the Parameter mode
      if (ParameterCount != 0)
        {
          OutputDisplayAspectRatio = Parameters[0];
          ParameterCount = 0; // Disable Parameter mode for next time
        }

      Control.id = V4L2_CID_DV_STM_TX_ASPECT_RATIO;
      Control.value = OutputDisplayAspectRatio;
      prog__printf("%s: VIDIOC_S_CTRL V4L2_CID_DV_STM_TX_ASPECT_RATIO=%d\n",PRG_TRACE_PREDIX,  OutputDisplayAspectRatio);
      if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_CTRL, &Control)) < 0)
        {
          perror("VIDIOC_S_CTRL V4L2_CID_DV_STM_TX_ASPECT_RATIO failed\n");
          printf( "%s: ERROR ioctl VIDIOC_S_CTRL (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
          return (false);
        }
      else
        {
          prog__printf("%s: OK ioctl VIDIOC_S_CTRL  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
        }

      Control.id = V4L2_CID_DV_STM_TX_ASPECT_RATIO;
      if (LTV_CHECK(ioctl(v4lfd, VIDIOC_G_CTRL, &Control)) < 0)
        {
          perror("VIDIOC_G_CTRL V4L2_CID_DV_STM_TX_ASPECT_RATIO failed\n");
          printf( "%s: ERROR ioctl VIDIOC_G_CTRL (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
          return (false);
        }
      else
        {
          prog__printf("%s: OK ioctl VIDIOC_G_CTRL  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
        }

      if (OutputDisplayAspectRatio != Control.value)
        {
          perror("Output display aspect ratio set and get are different.\n");
        }
      break;

    case 'j':
      ParameterCount = sscanf(line, "%c %d %d\n", &Key, &Parameters[0], &Parameters[1]);
      ParameterCount--;
      Control.id = V4L2_CID_STM_OUTPUT_WINDOW_MODE;
      if (ParameterCount == 0)
        {
          if (LTV_CHECK(ioctl(v4lfd, VIDIOC_G_CTRL, &Control)) < 0)
            {
              perror("VIDIOC_G_CTRL V4L2_CID_STM_OUTPUT_WINDOW_MODE failed\n");
              printf( "%s: ERROR ioctl VIDIOC_G_CTRL (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
              return (false);
            }
          else
            {
              prog__printf("%s: OK ioctl VIDIOC_G_CTRL  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
            }

          // Switch mode
          Control.value = ((Control.value == VCSPLANE_AUTO_MODE) ? VCSPLANE_MANUAL_MODE : VCSPLANE_AUTO_MODE);
        }
      else if (ParameterCount == 1)
        {
          Control.value = Parameters[0];
        }

      if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_CTRL, &Control)) < 0)
        {
          perror("VIDIOC_S_CTRL V4L2_CID_STM_OUTPUT_WINDOW_MODE failed\n");
          printf( "%s: ERROR ioctl VIDIOC_S_CTRL (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
          return (false);
        }
      else
        {
          prog__printf("%s: OK ioctl VIDIOC_S_CTRL  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
        }

      if (Control.value == VCSPLANE_AUTO_MODE)
        {
          prog__printf("%s: Setting output window mode to AUTOMATIC\n",PRG_TRACE_PREDIX);
        }
      else
        {
          prog__printf("%s: Setting output window mode to MANUAL\n",PRG_TRACE_PREDIX);
        }
      break;

    case 'J':
      ParameterCount = sscanf(line, "%c %d %d\n", &Key, &Parameters[0], &Parameters[1]);
      ParameterCount--;
      Control.id = V4L2_CID_STM_INPUT_WINDOW_MODE;
      if (ParameterCount == 0)
        {
          if (LTV_CHECK(ioctl(v4lfd, VIDIOC_G_CTRL, &Control)) < 0)
            {
              perror("VIDIOC_G_CTRL V4L2_CID_STM_INPUT_WINDOW_MODE failed\n");
              printf( "%s: ERROR ioctl VIDIOC_G_CTRL (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
              return (false);
            }
          else
            {
              prog__printf("%s: OK ioctl VIDIOC_G_CTRL  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
            }

          // Switch mode
          Control.value = ((Control.value == VCSPLANE_AUTO_MODE) ? VCSPLANE_MANUAL_MODE : VCSPLANE_AUTO_MODE);
        }
      else if (ParameterCount == 1)
        {
          Control.value = Parameters[0];
        }

      if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_CTRL, &Control)) < 0)
        {
          perror("VIDIOC_S_CTRL V4L2_CID_STM_INPUT_WINDOW_MODE failed\n");
          printf( "%s: ERROR ioctl VIDIOC_S_CTRL (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
          return (false);
        }
      else
        {
          prog__printf("%s: OK ioctl VIDIOC_S_CTRL  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
        }

      if (Control.value == VCSPLANE_AUTO_MODE)
        {
          prog__printf("%s: Setting input window mode to AUTOMATIC\n",PRG_TRACE_PREDIX);
        }
      else
        {
          prog__printf("%s: Setting input window mode to MANUAL\n",PRG_TRACE_PREDIX);
        }

      break;

    case 'h':
      usage_during_playback();
      break;

    case 'i':
      //Status  = player_flip_display (Player);
      break;

    case 'w':
      ParameterCount = sscanf(line, "%c %d %d %d %d\n", &Key, &Parameters[0], &Parameters[1], &Parameters[2], &Parameters[3]);
      ParameterCount--;
      if (ParameterCount == 4)
        {
          memset(&crop, 0, sizeof(struct v4l2_crop));
          crop.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
          crop.c.left = Parameters[0];
          crop.c.top = Parameters[1];
          crop.c.width = Parameters[2];
          crop.c.height = Parameters[3];
          prog__printf("%s: Output Image Dimensions (%d,%d) (%d x %d)\n",PRG_TRACE_PREDIX, crop.c.left, crop.c.top, crop.c.width, crop.c.height);

          // set output crop
          if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_CROP, &crop)) < 0)
            {
              perror("VIDIOC_S_CROP failed\n");
              printf( "%s: ERROR ioctl VIDIOC_S_CROP (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
              return false;
            }
          else
            {
              prog__printf("%s: OK ioctl VIDIOC_S_CROP  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
            }
        }
      else
        {
          prog__printf("%s: bd nb param  : ParameterCount = %d\n",PRG_TRACE_PREDIX, ParameterCount);

        }
      break;

    case 'z':
      ParameterCount = sscanf(line, "%c %d %d %d %d %d\n", &Key, &Parameters[0], &Parameters[1], &Parameters[2], &Parameters[3], &Parameters[4]);
      ParameterCount--;
      if ((ParameterCount == 4) || (ParameterCount == 5))
        {

         video_window_t currentpos,finalpos,nextcrop;
         int nbofstep;

          if (ParameterCount == 4)
             nbofstep=50;
          else
             nbofstep=Parameters[4];

          finalpos.x = Parameters[0];
          finalpos.y = Parameters[1];
          finalpos.width = Parameters[2];
          finalpos.height = Parameters[3];

          while (nbofstep > 0) {
             memset(&crop, 0, sizeof(struct v4l2_crop));
             crop.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
             // Get output crop
             if (LTV_CHECK(ioctl(v4lfd, VIDIOC_G_CROP, &crop)) < 0)
               {
                 perror("VIDIOC_G_CROP failed\n");
                 printf( "%s: ERROR ioctl VIDIOC_G_CROP (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
                 return false;
               }
             else
               {
                 prog__printf("%s: OK ioctl VIDIOC_G_CROP  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
                 currentpos.x = crop.c.left;
                 currentpos.y = crop.c.top;
                 currentpos.width = crop.c.width;
                 currentpos.height = crop.c.height;
               }
             nextcrop = calculatenextstepforzoom(v4lfd,currentpos,finalpos,nbofstep);
             nbofstep--;
             crop.c.left = nextcrop.x;
             crop.c.top = nextcrop.y;
             crop.c.width = nextcrop.width;
             crop.c.height = nextcrop.height;
             prog__printf("%s: Output Image Dimensions (%d,%d) (%d x %d)\n",PRG_TRACE_PREDIX, crop.c.left, crop.c.top, crop.c.width, crop.c.height);

             // set output crop
             if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_CROP, &crop)) < 0)
                {
                perror("VIDIOC_S_CROP failed\n");
                printf( "%s: ERROR ioctl VIDIOC_S_CROP (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
                return false;
                }
             else
                {
                prog__printf("%s: OK ioctl VIDIOC_S_CROP  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
                }
             system("sleep 0.05");
             }
        }
      else
        {
          prog__printf("%s: bd nb param  : ParameterCount = %d\n",PRG_TRACE_PREDIX, ParameterCount);

        }
      break;

    case 'W':
      ParameterCount = sscanf(line, "%c %d %d %d %d\n", &Key, &Parameters[0], &Parameters[1], &Parameters[2], &Parameters[3]);
      ParameterCount--;
      if (ParameterCount == 4)
        {
          memset(&crop, 0, sizeof(struct v4l2_crop));
          crop.type = V4L2_BUF_TYPE_PRIVATE;
          crop.c.left = Parameters[0];
          crop.c.top = Parameters[1];
          crop.c.width = Parameters[2];
          crop.c.height = Parameters[3];

          prog__printf("%s: Source Image Dimensions (%d,%d) (%d x %d)\n",PRG_TRACE_PREDIX, crop.c.left, crop.c.top, crop.c.width, crop.c.height);

          // set source crop
          if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_CROP, &crop)) < 0)
            {
              perror("VIDIOC_S_CROP failed\n");
              printf( "%s: ERROR ioctl VIDIOC_S_CROP (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
              return false;
            }
          else
            {
              prog__printf("%s: OK ioctl VIDIOC_S_CROP  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
            }
        }
      else
        {
          prog__printf("%s: bd nb param  : ParameterCount = %d\n",PRG_TRACE_PREDIX, ParameterCount);

        }
      break;

    case 'Z':
      ParameterCount = sscanf(line, "%c %d %d %d %d %d\n", &Key, &Parameters[0], &Parameters[1], &Parameters[2], &Parameters[3], &Parameters[4]);
      ParameterCount--;
      if ((ParameterCount == 4) || (ParameterCount == 5))
        {
         video_window_t currentpos,finalpos,nextcrop;
         int nbofstep;

          if (ParameterCount == 4)
             nbofstep=50;
          else
             nbofstep=Parameters[4];
          finalpos.x = Parameters[0];
          finalpos.y = Parameters[1];
          finalpos.width = Parameters[2];
          finalpos.height = Parameters[3];

          while (nbofstep > 0) {
             memset(&crop, 0, sizeof(struct v4l2_crop));
             crop.type = V4L2_BUF_TYPE_PRIVATE;
             // Get output crop
             if (LTV_CHECK(ioctl(v4lfd, VIDIOC_G_CROP, &crop)) < 0) {
                 perror("VIDIOC_G_CROP failed\n");
                 printf( "%s: ERROR ioctl VIDIOC_G_CROP (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
                 return false;
               }
             else {
                 prog__printf("%s: OK ioctl VIDIOC_G_CROP  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
                 currentpos.x = crop.c.left;
                 currentpos.y = crop.c.top;
                 currentpos.width = crop.c.width;
                 currentpos.height = crop.c.height;
               }
             nextcrop = calculatenextstepforzoom(v4lfd,currentpos,finalpos,nbofstep);
             nbofstep--;
             crop.c.left = nextcrop.x;
             crop.c.top = nextcrop.y;
             crop.c.width = nextcrop.width;
             crop.c.height = nextcrop.height;
             prog__printf("%s: input Image Dimensions (%d,%d) (%d x %d)\n",PRG_TRACE_PREDIX, crop.c.left, crop.c.top, crop.c.width, crop.c.height);

             // set output crop
             if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_CROP, &crop)) < 0)
                {
                perror("VIDIOC_S_CROP failed\n");
                printf( "%s: ERROR ioctl VIDIOC_S_CROP (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
                return false;
                }
             else
                {
                prog__printf("%s: OK ioctl VIDIOC_S_CROP  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
                }
             system("sleep 0.05");
             }
        }
      else
        {
          prog__printf("%s: bd nb param  : ParameterCount = %d\n",PRG_TRACE_PREDIX, ParameterCount);

        }
      break;

    case 't':
      ParameterCount = sscanf(line, "%c %d %d %d %d %s %s %d %d %s %d\n", &Key, &Parameters[0], &Parameters[1], &Parameters[2], &Parameters[3], Parameters_5, Parameters_6, &Parameters[4],&Parameters[5],Parameters_7,&Parameters[6]);
      ParameterCount--;
      // X Y W H mire_type <border_color/NOBORDER> <codebarvalue codebarnbbars DUAL/SINGLE> <inc>
      if ( (ParameterCount == 5) || (ParameterCount == 6) || (ParameterCount == 7) || (ParameterCount == 8) || (ParameterCount == 9) || (ParameterCount == 10) )
        {
          // create a file corresponding to the desired mire with defined size and placed at offset
          struct v4l2_format fmt;
          unsigned int ImageOffsetX=Parameters[0];
          unsigned int ImageOffsetY=Parameters[1];
          unsigned int ImageSizeX=Parameters[2];
          unsigned int ImageSizeY=Parameters[3];
          char format_s[FORMAT_STR_SIZE];
          char mire_type[32];
          char option1_cmd[32];
          char option2_cmd[64];
          char border_color[32];
          char system_cmd[400];
          char complete_system_cmd[400];
          int resu=0;
          int i=0;
          unsigned int incr=0;
          unsigned int codebar;
          unsigned int codebarnb;
          bool codebar_option=false;

          //Get display buffer format
          memset(&fmt, 0, sizeof(fmt));
          fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
          // get buffer format
          if (LTV_CHECK(ioctl(v4lfd, VIDIOC_G_FMT, &fmt)) < 0)
            {
              printf( "%s: ERROR ioctl VIDIOC_G_FMT: unable to get format buffer -> impossible to build specific buffer\n",PRG_TRACE_PREDIX);
              break;
            }
          else
            {
              prog__printf("%s: OK ioctl VIDIOC_G_FMT : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
            }

          prog__printf("%s: after calling ioctl VIDIOC_G_FMT: pix.width        = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.width);
          prog__printf("%s: after calling ioctl VIDIOC_G_FMT: pix.height       = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.height);
          prog__printf("%s: after calling ioctl VIDIOC_G_FMT: pix.bytesperline = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.bytesperline);
          prog__printf("%s: after calling ioctl VIDIOC_G_FMT: pix.sizeimage    = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.sizeimage);
          prog__printf("%s: after calling ioctl VIDIOC_G_FMT: pix.priv (Chroma Offset) = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.priv);
          prog__printf("%s: after calling ioctl VIDIOC_G_FMT: pix.field        = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.field);
          show_v4l2_field(fmt.fmt.pix.field);
          prog__printf("%s: after calling ioctl VIDIOC_G_FMT: pix.pixelformat  = 0x%x\n",PRG_TRACE_PREDIX, fmt.fmt.pix.pixelformat);
          show_v4l2_colorformat(fmt.fmt.pix.pixelformat);
          prog__printf("%s: after calling ioctl VIDIOC_G_FMT: pix.colorspace   = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.colorspace);
          show_v4l2_colorspace(fmt.fmt.pix.colorspace);

          if(global_colorformat != fmt.fmt.pix.pixelformat)
            {
              printf( "%s: ERROR check colorformat : global_colorformat != fmt.fmt.pix.pixelformat\n",PRG_TRACE_PREDIX);
              printf( "%s: global_colorformat -> \n",PRG_TRACE_PREDIX);
              show_v4l2_colorformat(global_colorformat);
              printf( "%s: fmt.fmt.pix.pixelformat -> \n",PRG_TRACE_PREDIX);
              show_v4l2_colorformat(fmt.fmt.pix.pixelformat);
              break;
            }

          if (GetFormatNameForImgTool(format_s, fmt.fmt.pix.pixelformat) < 0 )
            {
              printf( "%s: ERROR v4l2 buffer Format not supported by imgtool -> impossible to build specific buffer\n",PRG_TRACE_PREDIX);
              break;
            }

          strncpy(mire_type, Parameters_5, sizeof(mire_type) - 1);
          prog__printf("%s: mire_type = %s\n",PRG_TRACE_PREDIX, mire_type);

          snprintf( system_cmd, sizeof(system_cmd) - 1, "progvalidimagingtool_armv7 -o /tmp/bufferfile.raw -format %s -t generateframe -width %d -height %d -stride %d -sliceheight %d -topleftx %d -toplefty %d -rectsizewidth %d -rectsizeheight %d -imagecontent %s -q", format_s, fmt.fmt.pix.width, fmt.fmt.pix.height, fmt.fmt.pix.bytesperline, fmt.fmt.pix.height, ImageOffsetX, ImageOffsetY, ImageSizeX, ImageSizeY, mire_type);
          if (ParameterCount >= 6)
            {
             strncpy(border_color, Parameters_6, sizeof(border_color) -1 );
             if (0 != strncmp(border_color,"NOBORDER",8))
                {
                 prog__printf("%s: border_color = %s\n",PRG_TRACE_PREDIX, border_color);
                 snprintf(option1_cmd, sizeof(option1_cmd) -1 , " -roiColor %s", border_color);
                }
             else
                {
                 snprintf(option1_cmd, sizeof(option1_cmd) - 1, " ");
                }
             if (ParameterCount >= 7)
                {
                 codebar=Parameters[4];
                 codebarnb=Parameters[5];
                 codebar_option=true;
                 snprintf(option2_cmd, sizeof(option2_cmd) - 1, " ");
                 prog__printf("%s: codebar = %d on %d bits\n",PRG_TRACE_PREDIX, codebar,codebarnb);
                 if (ParameterCount == 10)
                     {
                      incr=Parameters[6];
                     }
                 else
                     {
                      incr=0;
                     }
                }
             strncat(system_cmd, option1_cmd, sizeof(option1_cmd) - 1);
            }

          for (i=0;i<global_numberofallocatedbuffer;i++)
          {
             strncpy(complete_system_cmd, system_cmd, sizeof(complete_system_cmd) - 1);
             if (codebar_option==true)
                  {
                  if (0 == strncmp(Parameters_7,"DUAL",4))
                      snprintf(option2_cmd, sizeof(option2_cmd) - 1, " -codebarvalue %d -codebarnbbars %d",(((codebar + (i*incr))&0xff)<<8)  + ((codebar + (i*incr))&0xff),codebarnb);
                  else
                      snprintf(option2_cmd, sizeof(option2_cmd) - 1, " -codebarvalue %d -codebarnbbars %d",(codebar + (i*incr)),codebarnb);
                  strncat(complete_system_cmd, option2_cmd, sizeof(option2_cmd) - 1);
                  }
              prog__printf("%s: system_cmd = %s\n",PRG_TRACE_PREDIX,complete_system_cmd);
              resu=system(complete_system_cmd);
              prog__printf("%s: resu system is = 0x%x\n",PRG_TRACE_PREDIX,resu);
              if(resu == 0x100)// 0 -> 0x000, 1 -> 0x100, 2 -> 0x200 on platform (don't know why)
                {
                  prog__printf("%s: execution OK of: %s\n",PRG_TRACE_PREDIX,complete_system_cmd);
                  FILE *fp=(FILE*)NULL;
                  int readBytes=0;
                  unsigned int fileSize=0;
                  int offset_buf=0;
                  fp = fopen("/tmp/bufferfile.raw", "rb");

                  if (fp == NULL)
                    {
                      printf( "%s: ERROR! File not found!\n",PRG_TRACE_PREDIX);
                      return false;
                    }
                  // Obtain file size
                  fseek(fp, 0, SEEK_END);
                  fileSize = ftell(fp);
                  fseek(fp, 0, SEEK_SET);
                  if(fmt.fmt.pix.pixelformat == V4L2_PIX_FMT_CLUT8)
                    {
                      prog__printf("%s: case V4L2_PIX_FMT_CLUT8 -> offset of 256*4\n",PRG_TRACE_PREDIX);
                      offset_buf=256*4;
                      prog__printf("%s: updating fileSize = %d\n",PRG_TRACE_PREDIX, fileSize);
                      fileSize-=offset_buf;
                      prog__printf("%s: to fileSize = %d\n",PRG_TRACE_PREDIX, fileSize);
                      prog__printf("%s: fseek of offset_buf = %d\n",PRG_TRACE_PREDIX, offset_buf);
                      fseek(fp, offset_buf, SEEK_SET);
                    }
                  readBytes = fread(buffers[i].start, 1, fileSize, fp);
                  if (readBytes != fileSize)
                    {
                      printf( "%s: ERROR while reading the raw file %d %d\n",PRG_TRACE_PREDIX, readBytes, fileSize);
                      return false;
                    }
                  prog__printf("%s: free (fct %s, line %d) : fclose(fp = 0x%x) \n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__, (int)fp);
                  fclose(fp);
                }
              else
                {
                  prog__printf("%s: WARNING!!! execution NOK of: %s\n",PRG_TRACE_PREDIX,complete_system_cmd);
                }
           }
        }
      else
        {
          prog__printf("%s: WARNING!!! bad number of parameters as ParameterCount = %d\n",PRG_TRACE_PREDIX,ParameterCount);
        }
      system("touch /tmp/semafile_for_renderer_appli");
      prog__printf("%s: system_cmd = touch /tmp/semafile_for_renderer_appli\n",PRG_TRACE_PREDIX);
      break;

    case 'x':
      prog__printf("%s: Exiting from %s\n",PRG_TRACE_PREDIX, prgName);
      Running = false;
      break;
    }

  return Running;
}

int main(int argc, char **argv)
{
  bool Running = true;
  int v4lfd = 0;
  int viddevice = 1;// Default stmvout device is now video1
  char vidname[32];
  const char *plane_name = "Main-VID";
  int progressive = 1;
  int delay = 1;
  int pixfmt = V4L2_PIX_FMT_UYVY;
  int halfsize = 0, left_half = 0, right_half = 0, quartersize = 0;
  int adjusttime = 0;
  int bufferNb = 3;
  int i = 0;
  unsigned int image_width = 0, image_height = 0;
  video_window_t Window;
  int zoom = 0;
  unsigned long long frameduration = 0;
  struct v4l2_format fmt;
  struct v4l2_requestbuffers reqbuf;
  struct v4l2_crop crop;
  v4l2_std_id stdid = V4L2_STD_UNKNOWN;
  struct v4l2_standard standard;
  struct v4l2_cropcap cropcap;
  struct v4l2_buffer buffer[20];
  struct timespec currenttime;
  struct timeval ptime;
  char gamFileName[256];
  FILE *fp = (FILE*)NULL;
  unsigned int pitch = 0;
  GamPictureHeader_t GamPictureHeader;
  char *colour_palette=(char*)NULL;
  enum v4l2_colorspace cspace = V4L2_COLORSPACE_SMPTE170M;
  unsigned int size_area_1 = 0, size_area_2 = 0;
  unsigned int formatBefore = 0, formatAfter = 0;
  char *prgName = argv[0];
  unsigned int aplha = 255;

  printf( "------------------- v4l2gam ------------------------------------------\n" );
  fprintf(stderr,"%s: %s info-version : %s\n",PRG_TRACE_PREDIX, prgName, PRG_VERSION);

  argc--;
  argv++;

  memset(&reqbuf, 0, sizeof(reqbuf));

  Window.x = 0;
  Window.y = 0;
  global_iterations = 20;// Default value to get ~1s of display (20 iteration * 3 buffers * 1/60s in ntsc)

  // The next argument contains the name of the GAM file to load
  if (argv[0] != NULL)
    {
      strncpy(gamFileName, argv[0], sizeof(gamFileName));
      argc--;
      argv++;
    }

  if(argc == 0)
  {
      fprintf(stderr, "%s: argc = %d => call usage function\n",PRG_TRACE_PREDIX, argc);
      usage(prgName);
    }

  while (argc > 0)
    {
      switch (**argv)
        {
        case 's' :
          global_verbose = 0;
          break;

        case 'e':
          global_verbose = 1;
          break;

        case 'a':
          adjusttime = 1;
          break;

        case 'd':
          argc--;
          argv++;
          if (argc <= 0)
            {
              fprintf(stderr, "%s: Missing delay\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          delay = atoi(*argv);
          if (delay < 1)
            {
              fprintf(stderr,"%s: Require a minimum of 1sec delay\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          break;

        case 'i':
          argc--;
          argv++;
          if (argc <= 0)
            {
              fprintf(stderr, "%s: Missing iterations\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          global_iterations = atoi(*argv);
          if (global_iterations < 5)
            {
              fprintf(stderr,"%s: Require a minimum of 5 iterations\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          break;

        case 'I':
          progressive = 0;
          break;

        case 'h':
          if ((*argv)[1] == '1')
            {
              left_half = 1;
            }
          else if ((*argv)[1] == '2')
            {
              right_half = 1;
            }
          halfsize = 1;
          break;

        case 'q':
          quartersize = 1;
          break;

        case 'v':
          argc--;
          argv++;
          if (argc <= 0)
            {
              fprintf(stderr, "%s: Missing video device number\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          viddevice = atoi(*argv);
          if (viddevice < 0 || viddevice > 255)
            {
              fprintf(stderr,"%s: Video device number out of range\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          break;

        case 'w':
          argc--;
          argv++;
          if (argc <= 0)
            {
              fprintf(stderr, "%s: Missing plane name\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          plane_name = *argv;
          break;

        case 't':
          argc--;
          argv++;
          if (argc <= 0)
            {
              fprintf(stderr, "%s: Missing standard id\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          stdid = (v4l2_std_id) strtoull(*argv, NULL, 0);
          break;

        case 'b':
          argc--;
          argv++;
          if (argc <= 0)
            {
              fprintf(stderr, "%s: Missing number of buffers\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          bufferNb = atoi(*argv);
          if (bufferNb < 0 || bufferNb > 20)
            {
              fprintf(stderr,"%s: Number of buffers out of range\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          break;

        case 'x':
          argc--;
          argv++;
          if (argc <= 0)
            {
              fprintf(stderr, "%s: Missing pixel number\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          Window.x = atoi(*argv);
          break;

        case 'y':
          argc--;
          argv++;
          if (argc <= 0)
            {
              fprintf(stderr, "%s: Missing line number\n",PRG_TRACE_PREDIX);
              usage(prgName);
            }
          Window.y = atoi(*argv);
          break;

        case 'z':
          zoom = 1;
          break;

        default:
          fprintf(stderr, "%s: Unknown option '%s'\n",PRG_TRACE_PREDIX, *argv);
          usage(prgName);
          break;
        }

      argc--;
      argv++;
    }

  //Open the requested V4L2 device
  snprintf(vidname, sizeof(vidname), "/dev/video%d", viddevice);
  prog__printf("%s: v4l2Info : calling open %s\n",PRG_TRACE_PREDIX, vidname);
  if ((v4lfd = open(vidname, O_RDWR)) < 0)
    {
      perror("Unable to open video device");
      goto exit;
    }

  /*{
    struct v4l2_capability argp;
    if (LTV_CHECK( ioctl(v4lfd,VIDIOC_QUERYCAP, &argp)) < 0)
    {
    printf( "%s: ERROR ioctl VIDIOC_QUERYCAP (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
    goto exit;
    }
    else
    {
    prog__printf("%s: OK ioctl VIDIOC_QUERYCAP  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
    }
    prog__printf("%s: VIDIOC_QUERYCAP -> argp.driver       = %s \n",PRG_TRACE_PREDIX, argp.driver);
    prog__printf("%s: VIDIOC_QUERYCAP -> argp.card         = %s \n",PRG_TRACE_PREDIX, argp.card);
    prog__printf("%s: VIDIOC_QUERYCAP -> argp.bus_info     = %s \n",PRG_TRACE_PREDIX, argp.bus_info);
    prog__printf("%s: VIDIOC_QUERYCAP -> argp.capabilities = 0x%x \n",PRG_TRACE_PREDIX, argp.capabilities);

    if (argp.capabilities & V4L2_CAP_VIDEO_CAPTURE){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_VIDEO_CAPTURE The device supports the single-planar API through the Video Capture interface.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_VIDEO_CAPTURE_MPLANE){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_VIDEO_CAPTURE_MPLANE The device supports the multi-planar API through the Video Capture interface.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_VIDEO_OUTPUT){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_VIDEO_OUTPUT The device supports the single-planar API through the Video Output interface.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_VIDEO_OUTPUT_MPLANE){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_VIDEO_OUTPUT_MPLANE The device supports the multi-planar API through the Video Output interface.\n",PRG_TRACE_PREDIX);}
    //if (argp.capabilities & V4L2_CAP_VIDEO_M2M){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_VIDEO_M2M The device supports the single-planar API through the Video Memory-To-Memory interface.\n",PRG_TRACE_PREDIX);}
    //if (argp.capabilities & V4L2_CAP_VIDEO_M2M_MPLANE){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_VIDEO_M2M_MPLANE The device supports the multi-planar API through the Video Memory-To-Memory interface.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_VIDEO_OVERLAY){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_VIDEO_OVERLAY The device supports the Video Overlay interface. A video overlay device typically stores captured images directly in the video memory of a graphics card, with hardware clipping and scaling.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_VBI_CAPTURE){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_VBI_CAPTURE The device supports the Raw VBI Capture interface, providing Teletext and Closed Caption data.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_VBI_OUTPUT){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_VBI_OUTPUT The device supports the Raw VBI Output interface.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_SLICED_VBI_CAPTURE){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_SLICED_VBI_CAPTURE The device supports the Sliced VBI Capture interface.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_SLICED_VBI_OUTPUT){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_SLICED_VBI_OUTPUT The device supports the Sliced VBI Output interface.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_RDS_CAPTURE){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_RDS_CAPTURE The device supports the RDS capture interface.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_VIDEO_OUTPUT_OVERLAY){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_VIDEO_OUTPUT_OVERLAY The device supports the Video Output Overlay (OSD) interface. Unlike the Video Overlay interface, this is a secondary function of video output devices and overlays an image onto an outgoing video signal. When the driver sets this flag, it must clear the V4L2_CAP_VIDEO_OVERLAY flag and vice versa.[a]\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_HW_FREQ_SEEK){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_HW_FREQ_SEEK The device supports the VIDIOC_S_HW_FREQ_SEEK ioctl for hardware frequency seeking.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_RDS_OUTPUT){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_RDS_OUTPUT The device supports the RDS output interface.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_TUNER){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_TUNER The device has some sort of tuner to receive RF-modulated video signals. For more information about tuner programming see the section called “Tuners and Modulators”.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_AUDIO ){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_AUDIO The device has audio inputs or outputs. It may or may not support audio recording or playback, in PCM or compressed formats. PCM audio support must be implemented as ALSA or OSS interface. For more information on audio inputs and outputs see the section called “Audio Inputs and Outputs”.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_RADIO){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_RADIO This is a radio receiver.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_MODULATOR){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_MODULATOR The device has some sort of modulator to emit RF-modulated video/audio signals. For more information about modulator programming see the section called “Tuners and Modulators”.\n",PRG_TRACE_PREDIX);}
    //if (argp.capabilities & V4L2_CAP_SDR_CAPTURE){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_SDR_CAPTURE The device supports the SDR Capture interface.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_READWRITE){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_READWRITE The device supports the read() and/or write() I/O methods.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_ASYNCIO){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_ASYNCIO The device supports the asynchronous I/O methods.\n",PRG_TRACE_PREDIX);}
    if (argp.capabilities & V4L2_CAP_STREAMING){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_STREAMING The device supports the streaming I/O method.\n",PRG_TRACE_PREDIX);}
    //if (argp.capabilities & V4L2_CAP_DEVICE_CAPS){prog__printf("%s: VIDIOC_QUERYCAP V4L2_CAP_DEVICE_CAPS The driver fills the device_caps field. This capability can only appear in the capabilities field and never in the device_caps field.\n",PRG_TRACE_PREDIX);}
    }*/

  prog__printf("%s: v4l2Info : calling v4l2_list_outputs\n",PRG_TRACE_PREDIX);
  v4l2_list_outputs(v4lfd);
  if (!plane_name)
    {
      prog__printf("%s: v4l2Info : calling v4l2_set_output_by_index 0\n",PRG_TRACE_PREDIX);
      i = v4l2_set_output_by_index(v4lfd, 0);
    }
  else
    {
      prog__printf("%s: v4l2Info : calling v4l2_set_output_by_index %s\n",PRG_TRACE_PREDIX, plane_name);
      i = v4l2_set_output_by_name(v4lfd, plane_name);
    }

  if (i == -1)
    {
      perror("Unable to set video output");
      goto exit;
    }

  if (stdid == V4L2_STD_UNKNOWN)
    {
      // if no standard was requested...
      if (LTV_CHECK(ioctl(v4lfd, VIDIOC_G_OUTPUT_STD, &stdid)) < 0)
        {
          printf( "%s: ERROR ioctl VIDIOC_G_OUTPUT_STD (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);

          // Because Cannes2.5 4Kp60 mode (and a few others) is not supported by ioctl above, we are not always able to retrieve
          // the vtg mode currently used. This used to be a fatal error but we want to use v4l2gam with 4Kp60 mode on Cannes2.5
          // So we just ignore it.
          // Note that because of HDMI-phy bug on Cannes2.5-cut1, we jump after the ioctl "VIDIOC_S_OUTPUT_STD" as it would stop
          // the hdmi and it wouldn't be possible to restart it.
          goto ignore_display_mode_retrieval;
          // goto exit;
        }
      else
        {
          prog__printf("%s: OK ioctl VIDIOC_G_OUTPUT_STD  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
        }

      if (stdid == V4L2_STD_UNKNOWN)
        {
          // and no standard is currently set on the display pipeline, we just pick the first standard listed, whatever that might be.
          standard.index = 0;
          if (LTV_CHECK(ioctl(v4lfd, VIDIOC_ENUM_OUTPUT_STD, &standard)) < 0)
            {
              printf( "%s: ERROR ioctl VIDIOC_ENUM_OUTPUT_STD (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
              goto exit;
            }
          else
            {
              prog__printf("%s: OK ioctl VIDIOC_ENUM_OUTPUT_STD  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
            }
          stdid = standard.id;
        }
    }
  if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_OUTPUT_STD, &stdid)) < 0)
    {
      printf( "%s: ERROR ioctl VIDIOC_S_OUTPUT_STD (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
      goto exit;
    }
  else
    {
      prog__printf("%s: OK ioctl VIDIOC_S_OUTPUT_STD  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
    }

ignore_display_mode_retrieval:

    /* Set default alpha */
    if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_OUTPUT_ALPHA, &aplha)) < 0)
     {
      printf( "%s: ERROR ioctl VIDIOC_S_OUTPUT_ALPHA (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
       goto exit;
     }

  standard.index = 0;
  standard.id = V4L2_STD_UNKNOWN;
  do
    {
      if (LTV_CHECK(ioctl(v4lfd, VIDIOC_ENUM_OUTPUT_STD, &standard)) < 0)
        {
          printf( "%s: ERROR ioctl VIDIOC_ENUM_OUTPUT_STD (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
          goto exit;
        }
      else
        {
          prog__printf("%s: OK ioctl VIDIOC_ENUM_OUTPUT_STD  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
        }

      ++standard.index;
    } while ((standard.id & stdid) != stdid);

  prog__printf("%s: Current display standard is '%s'\n",PRG_TRACE_PREDIX, standard.name);

  memset(&cropcap, 0, sizeof(cropcap));
  cropcap.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

  if (LTV_CHECK(ioctl(v4lfd, VIDIOC_CROPCAP, &cropcap)) < 0)
    {
      printf( "%s: ERROR ioctl VIDIOC_CROPCAP (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
      goto exit;
    }
  else
    {
      prog__printf("%s: OK ioctl VIDIOC_CROPCAP  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
    }

  frameduration =  ((double)standard.frameperiod.numerator / (double)standard.frameperiod.denominator) * 1000000.0;

  prog__printf("%s: frameduration = %lluus\n",PRG_TRACE_PREDIX, frameduration);

  fp = read_gam_header(gamFileName, &pixfmt, &GamPictureHeader, &pitch, &cspace, &size_area_1, &size_area_2);
  if (fp == NULL)
    {
      perror("Unable to read gam header");
      goto exit;
    }
  image_width = GamPictureHeader.PictureWidth;
  image_height = GamPictureHeader.PictureHeight;

  //Set display sized buffer format
  memset(&fmt, 0, sizeof(fmt));
  fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  fmt.fmt.pix.pixelformat = pixfmt;
  fmt.fmt.pix.colorspace = cspace;
  fmt.fmt.pix.field = V4L2_FIELD_ANY;
  fmt.fmt.pix.width = image_width;
  fmt.fmt.pix.height = image_height;

  prog__printf("%s: before calling ioctl VIDIOC_S_FMT: pix.width        = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.width);
  prog__printf("%s: before calling ioctl VIDIOC_S_FMT: pix.height       = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.height);
  prog__printf("%s: before calling ioctl VIDIOC_S_FMT: pix.bytesperline = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.bytesperline);
  prog__printf("%s: before calling ioctl VIDIOC_S_FMT: pix.sizeimage    = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.sizeimage);
  prog__printf("%s: before calling ioctl VIDIOC_S_FMT: pix.priv (Chroma Offset) = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.priv);
  prog__printf("%s: before calling ioctl VIDIOC_S_FMT: pix.field        = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.field);
  show_v4l2_field(fmt.fmt.pix.field);
  prog__printf("%s: before calling ioctl VIDIOC_S_FMT: pix.pixelformat  = 0x%x\n",PRG_TRACE_PREDIX, fmt.fmt.pix.pixelformat);
  show_v4l2_colorformat(fmt.fmt.pix.pixelformat);
  prog__printf("%s: before calling ioctl VIDIOC_S_FMT: pix.colorspace   = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.colorspace);
  show_v4l2_colorspace(fmt.fmt.pix.colorspace);

  formatBefore = fmt.fmt.pix.pixelformat;
  global_colorformat = fmt.fmt.pix.pixelformat;

  // set buffer format
  if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_FMT, &fmt)) < 0)
    {
      printf( "%s: ERROR ioctl VIDIOC_S_FMT (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
      goto exit;
    }
  else
    {
      prog__printf("%s: OK ioctl VIDIOC_S_FMT  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
    }

  prog__printf("%s: Selected V4L2 pixel format:\n",PRG_TRACE_PREDIX);
  prog__printf("%s: after calling ioctl VIDIOC_S_FMT: pix.width                = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.width);
  prog__printf("%s: after calling ioctl VIDIOC_S_FMT: pix.height               = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.height);
  prog__printf("%s: after calling ioctl VIDIOC_S_FMT: pix.bytesperline         = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.bytesperline);
  prog__printf("%s: after calling ioctl VIDIOC_S_FMT: pix.sizeimage            = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.sizeimage);
  prog__printf("%s: after calling ioctl VIDIOC_S_FMT: pix.priv (Chroma Offset) = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.priv);
  prog__printf("%s: after calling ioctl VIDIOC_S_FMT: pix.field                = %d\n",PRG_TRACE_PREDIX, fmt.fmt.pix.field);
  show_v4l2_field(fmt.fmt.pix.field);
  prog__printf("%s: after calling ioctl VIDIOC_S_FMT: pix.pixelformat          = 0x%x\n",PRG_TRACE_PREDIX, fmt.fmt.pix.pixelformat);
  show_v4l2_colorformat(fmt.fmt.pix.pixelformat);
  prog__printf("%s: after calling ioctl VIDIOC_S_FMT: pix.colorspace           = %d\n\n",PRG_TRACE_PREDIX, fmt.fmt.pix.colorspace);
  show_v4l2_colorspace(fmt.fmt.pix.colorspace);

  formatAfter = fmt.fmt.pix.pixelformat;

  if (formatAfter == formatBefore)
    {
      printf( "%s: check colorformat OK : formatBefore = formatAfter\n",PRG_TRACE_PREDIX);
      show_v4l2_colorformat(formatBefore);
    }
  else
    {
      printf( "%s: ERROR check colorformat : formatBefore != formatAfter\n",PRG_TRACE_PREDIX);
      printf( "%s: formatBefore -> \n",PRG_TRACE_PREDIX);
      show_v4l2_colorformat(formatBefore);
      printf( "%s: formatAfter -> \n",PRG_TRACE_PREDIX);
      show_v4l2_colorformat(formatAfter);
      goto exit;
    }

  //Request buffers to be allocated on the V4L2 device
  reqbuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  reqbuf.memory = V4L2_MEMORY_MMAP;
  reqbuf.count = bufferNb;

  // request buffers
  if (LTV_CHECK(ioctl(v4lfd, VIDIOC_REQBUFS, &reqbuf)) < 0)
    {
      printf( "%s: ERROR ioctl VIDIOC_REQBUFS (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
      goto exit;
    }
  else
    {
      prog__printf("%s: OK ioctl VIDIOC_REQBUFS  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
      prog__printf("%s: alloc (fct %s, line %d) \n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
    }

  if (reqbuf.count < bufferNb)
    {
      perror("Unable to allocate all buffers\n");
      prog__printf("%s: only %d buffers allocated\n",PRG_TRACE_PREDIX,reqbuf.count);
      bufferNb = reqbuf.count;
      //goto exit;
    }
  global_numberofallocatedbuffer = reqbuf.count;
  buffers = calloc(reqbuf.count, sizeof(*buffers));
  if(NULL == buffers)
    {
      printf( "%s: ERROR allocation buffers struct (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
      goto exit;
    }

  for (i = 0; i < reqbuf.count; i++)
    {
      memset(&buffer[i], 0, sizeof(struct v4l2_buffer));
      buffer[i].index = i;
      buffer[i].type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

      // query buffer
      if (LTV_CHECK(ioctl(v4lfd, VIDIOC_QUERYBUF, &buffer[i])) < 0)
        {
          printf( "%s: ERROR ioctl VIDIOC_QUERYBUF (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
          goto exit;
        }
      else
        {
          prog__printf("%s: OK ioctl VIDIOC_QUERYBUF  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
        }

      buffer[i].field =  progressive ? V4L2_FIELD_NONE : V4L2_FIELD_INTERLACED;
      prog__printf("%s: mmap of size buffer[i].length = %d (remember for munmap())\n",PRG_TRACE_PREDIX, buffer[i].length);
      buffers[i].length = buffer[i].length; // remember for munmap()
      buffers[i].start = (char *)mmap(0, buffer[i].length, PROT_READ | PROT_WRITE, MAP_SHARED, v4lfd, buffer[i].m.offset);

      if (MAP_FAILED == buffers[i].start)
        {
          perror("mmap buffer failed\n");
          goto exit;
        }

      prog__printf("%s: creating buffer %d\n",PRG_TRACE_PREDIX, i);
      if (i == 0)
        {
          read_gam_buffer(fp, buffers[i].start, GamPictureHeader, pitch, fmt.fmt.pix, &colour_palette, size_area_1, size_area_2);
        }
      else
        {
          // For next buffers just copy content from first one
          prog__printf("%s: Copying buffer %d (fmt.fmt.pix.sizeimage = %d)\n",PRG_TRACE_PREDIX, i, fmt.fmt.pix.sizeimage);
          memcpy(buffers[i].start, buffers[0].start, fmt.fmt.pix.sizeimage);
        }
    }

  //Set the output size and position if downscale requested
  memset(&crop, 0, sizeof(struct v4l2_crop));
  crop.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  if (halfsize)
    {
      if (left_half)
        {
          crop.c.left = 0;
          crop.c.top = 0;
          crop.c.height = cropcap.bounds.height;
        }
      else if (right_half)
        {
          crop.c.left = cropcap.bounds.width / 2;
          crop.c.top = 0;
          crop.c.height = cropcap.bounds.height;
        }
      else
        {
          crop.c.left = cropcap.bounds.width / 4;
          crop.c.top = cropcap.bounds.height / 4;
          crop.c.height = cropcap.bounds.height / 2;
        }
      crop.c.width = cropcap.bounds.width / 2;
    }
  else if (quartersize)
    {
      crop.c.left = cropcap.bounds.width / 3;
      crop.c.top = cropcap.bounds.height / 3;
      crop.c.width = cropcap.bounds.width / 4;
      crop.c.height = cropcap.bounds.height / 4;
    }
  else
    {
      crop.c.left = 0;
      crop.c.top = 0;
      crop.c.width = cropcap.bounds.width;
      crop.c.height = cropcap.bounds.height;
    }
  prog__printf("%s: Output Image Dimensions (%d,%d) (%d x %d)\n",PRG_TRACE_PREDIX, crop.c.left, crop.c.top, crop.c.width, crop.c.height);

  // set output crop
  if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_CROP, &crop)) < 0)
    {
      printf( "%s: ERROR ioctl VIDIOC_S_CROP (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
      goto exit;
    }
  else
    {
      prog__printf("%s: OK ioctl VIDIOC_S_CROP  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
    }

  // Set the buffer crop to be the full image.//
  memset(&crop, 0, sizeof(struct v4l2_crop));
  crop.type = V4L2_BUF_TYPE_PRIVATE;
  crop.c.left = Window.x;
  crop.c.top = Window.y;
  crop.c.width = image_width - Window.x;
  crop.c.height = image_height - Window.y;

  prog__printf("%s: Source Image Dimensions (%d,%d) (%d x %d)\n",PRG_TRACE_PREDIX, Window.x, Window.y, image_width, image_height);

  // set source crop
  if (LTV_CHECK(ioctl(v4lfd, VIDIOC_S_CROP, &crop)) < 0)
    {
      printf( "%s: ERROR ioctl VIDIOC_S_CROP (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
      goto exit;
    }
  else
    {
      prog__printf("%s: OK ioctl VIDIOC_S_CROP  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
    }

  //Work out the presentation time of the first buffer to be queued.
  //Note the use of the posix monotomic clock NOT the adjusted wall clock.

  clock_gettime(CLOCK_MONOTONIC, &currenttime);

  prog__printf("%s: Current time = %ld.%ld\n",PRG_TRACE_PREDIX, currenttime.tv_sec, currenttime.tv_nsec / 1000);

  ptime.tv_sec = currenttime.tv_sec + delay;
  ptime.tv_usec = currenttime.tv_nsec / 1000;

  // Queue all the buffers up for display.
  for (i = 0; i < bufferNb; i++)
    {
      buffer[i].timestamp = ptime;

      if(colour_palette)
        {
          prog__printf("%s: v4l2Info : prepare info for colour_palette\n",PRG_TRACE_PREDIX);
          buffer[i].type         = V4L2_BUF_TYPE_VIDEO_OUTPUT;
          buffer[i].memory       = V4L2_MEMORY_USERPTR;
          buffer[i].m.userptr    = (unsigned long)buffers[i].start;
          buffer[i].field        = V4L2_FIELD_NONE;
          prog__printf("%s: image_width = %d, image_height = %d\n",PRG_TRACE_PREDIX, image_width, image_height);
          buffer[i].length       = image_width*image_height*1;//or*4?
          buffer[i].reserved     = (__u32)colour_palette;
          buffer[i].flags        = V4L2_BUF_FLAG_NON_PREMULTIPLIED_ALPHA;
        }

      prog__printf("%s:  VIDIOC_QBUF index %d \n",PRG_TRACE_PREDIX, i);
      //queue buffer
      if (LTV_CHECK(ioctl(v4lfd, VIDIOC_QBUF, &buffer[i])) < 0)
        {
          printf( "%s: ERROR ioctl VIDIOC_QBUF (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
          goto exit;
        }
      else
        {
          prog__printf("%s: OK ioctl VIDIOC_QBUF  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
        }

      ptime.tv_usec += frameduration;
      while (ptime.tv_usec >= 1000000L)
        {
          ptime.tv_sec++;
          ptime.tv_usec -= 1000000L;
        }
    }

  // start stream
  prog__printf("%s: Starting stream \n",PRG_TRACE_PREDIX);

  if (LTV_CHECK(ioctl(v4lfd, VIDIOC_STREAMON, &buffer[0].type)) < 0)
    {
      printf( "%s: ERROR ioctl VIDIOC_STREAMON (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
      goto exit;
    }
  else
    {
      prog__printf("%s: OK ioctl VIDIOC_STREAMON  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
    }

  while (Running)
    {
      for (i = 0; i < global_iterations; i++)
        {
          struct v4l2_buffer dqbuffer;
          long long time1=0;
          long long time2=0;
          long long timediff=0;

          //To Dequeue a streaming memory buffer you must set the buffer type
          //AND and the memory type correctly, otherwise the API returns an error.
          dqbuffer.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
          dqbuffer.memory = V4L2_MEMORY_MMAP;

          //We didn't open the V4L2 device non-blocking so dequeuing a buffer will sleep until a buffer becomes available.
          //prog__printf("%s: dequeuing buffer\n",PRG_TRACE_PREDIX);

          if (bufferNb < 3)
            {
              prog__printf("%s: Warning ! We use only %d buffer(s) so dequeuing will block forever\nPress CTRL+C to exit program !!!\n",PRG_TRACE_PREDIX, bufferNb);
            }

          if (LTV_CHECK(ioctl(v4lfd, VIDIOC_DQBUF, &dqbuffer)) < 0)
            {
              printf( "%s: ERROR ioctl VIDIOC_DQBUF (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
              goto streamoff;
            }
          else
            {
              prog__printf("%s: OK ioctl VIDIOC_DQBUF  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
            }

          time1 = (buffer[dqbuffer.index].timestamp.tv_sec * 1000000LL) + buffer[dqbuffer.index].timestamp.tv_usec;
          time2 = (dqbuffer.timestamp.tv_sec * 1000000LL) + dqbuffer.timestamp.tv_usec;

          timediff = time2 - time1;

          //prog__printf("%s: Buffer%d: wanted %ld.%06ld, actual %ld.%06ld, diff = %lld us\n",PRG_TRACE_PREDIX, dqbuffer.index, buffer[dqbuffer.index].timestamp.tv_sec, buffer[dqbuffer.index].timestamp.tv_usec, dqbuffer.timestamp.tv_sec, dqbuffer.timestamp.tv_usec, timediff);

          ptime.tv_usec += frameduration;
          if (unlikely(i == 0))
            {
              // after the first frame came back, we know when exactly the VSync is happening, so adjust the presentation time - only once, though!
              ptime.tv_usec += timediff;
            }
          if (adjusttime  && unlikely((dqbuffer.index == (bufferNb - 1)) && (i >= (bufferNb * 2) - 1)))
            {
              if (abs(timediff) > 500)
                {
                  prog__printf("%s: Adjusting presentation time by %lld us\n",PRG_TRACE_PREDIX, timediff / 2);
                  ptime.tv_usec += timediff / 2;
                }
            }
          while (ptime.tv_usec >= 1000000L)
            {
              ptime.tv_sec++;
              ptime.tv_usec -= 1000000L;
            }

          buffer[dqbuffer.index].timestamp = ptime;

          if (zoom)
            {
              memset(&crop, 0, sizeof(struct v4l2_crop));
              crop.type = V4L2_BUF_TYPE_PRIVATE;
              crop.c.left = i % (image_width / 4);
              crop.c.top = Window.y;
              crop.c.width = image_width - (crop.c.left * 2);
              crop.c.height = image_height - Window.y;

              // set source crop
              if (LTV_CHECK (ioctl(v4lfd, VIDIOC_S_CROP, &crop)) < 0)
                {
                  printf( "%s: ERROR ioctl VIDIOC_S_CROP (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
                  goto streamoff;
                }
              else
                {
                  prog__printf("%s: OK ioctl VIDIOC_S_CROP  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
                }
            }

          // queue buffer
          if (LTV_CHECK (ioctl(v4lfd, VIDIOC_QBUF, &buffer[dqbuffer.index])) < 0)
            {
              printf( "%s: ERROR ioctl VIDIOC_QBUF (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
              goto streamoff;
            }
          else
            {
              prog__printf("%s: OK ioctl VIDIOC_QBUF  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
            }
        }

      Running = CheckKey(v4lfd, prgName);
    }

 streamoff:

  prog__printf("%s: Stream off \n",PRG_TRACE_PREDIX);

  if (LTV_CHECK(ioctl(v4lfd, VIDIOC_STREAMOFF, &buffer[0].type)) < 0)
    {
      printf( "%s: ERROR ioctl VIDIOC_STREAMOFF (fct %s, line %d)\n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__);
      goto streamoff;
    }
  else
    {
      prog__printf("%s: OK ioctl VIDIOC_STREAMOFF  : %s:%d\n",PRG_TRACE_PREDIX,__FUNCTION__,__LINE__);
    }

 exit:
  prog__printf("%s: reqbuf.count = %d \n",PRG_TRACE_PREDIX, reqbuf.count);
  for (i = 0; i < reqbuf.count; i++)
    {
      prog__printf("%s: munmap buffer index %d \n",PRG_TRACE_PREDIX, i);
      if(NULL != buffers)
        {
          prog__printf("%s: case buffers not NULL\n",PRG_TRACE_PREDIX);
          if(buffers[i].length > 0)
            {
              prog__printf("%s: really call munmap\n",PRG_TRACE_PREDIX);
              munmap(buffers[i].start, buffers[i].length);
            }
          else
            {
              prog__printf("%s: don't call munmap as length is not > 0\n",PRG_TRACE_PREDIX);
            }
        }
      else
        {
          prog__printf("%s: case buffers NULL\n",PRG_TRACE_PREDIX);
        }
    }
  if(colour_palette)
    {
      prog__printf("%s: free colour_palette \n",PRG_TRACE_PREDIX);
      free(colour_palette);
    }

  prog__printf("%s: free (fct %s, line %d) : close(v4lfd = 0x%x) \n",PRG_TRACE_PREDIX, __FUNCTION__, __LINE__, v4lfd);
  close(v4lfd);
  return 0;
}
