/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with STLinuxTV; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Keyboard thread to accept runtime options
************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <stdbool.h>
#include <limits.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <poll.h>
#include <sys/time.h>

#include <linux/dvb/audio.h>
#include <linux/dvb/video.h>
#include <linux/dvb/stm_audio.h>
#include <linux/dvb/stm_video.h>

#include "dvb-play.h"
#include "stm_utils.h"

#define COMMAND_LINE_SIZE	128
enum {
	CUR_DEMUX_FLUSH,
	SE_SWITCH,
	FE_LOCK,
	SELECTOR_SWITCH,
	TIMESTAMP_END,
};

struct timeval timestamps[TIMESTAMP_END];
enum commands {
	PROGRAM_UP,
	PROGRAM_DOWN,
};

/**
 * keyboard_help() - keyboard help
 */
void keyboard_help()
{
	printf("  dvb-play: Options to control the playback\n\n");
	printf("  +\n");
	printf("   Zap to program UP in the list. Only valid for fcc\n\n");
	printf("  -\n");
	printf("   Zap to program DOWN in the list. Only valid for fcc\n\n");
}

/**
 * find_command() - finds out if the command is supported
 */
static int find_command(char *command_line)
{
	int command = -1;

	if (!strncmp(command_line, "+", strlen("+")))
		command = PROGRAM_UP;
	if (!strncmp(command_line, "-", strlen("-")))
		command = PROGRAM_DOWN;

	return command;
}

/**
 * setup_fcc_video_decoder() - setup video decoder context for fcc
 */
static int setup_fcc_video_decoder(int debug_level, int viddec_fd,
					 struct video_decoder_context *viddec_ctx)
{
	int ret;
	bool decoder_stopped;

	struct video_command video_cmd [] = {
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_LIVE_PLAY,
		 .option.value = DVB_OPTION_VALUE_DISABLE,
		},
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_VIDEO_START_IMMEDIATE,
		 .option.value = DVB_OPTION_VALUE_ENABLE,
		},
	};

	/*
	 * Start or stop decoder depending on the video encoding value
	 */
	ret = video_decoder_start_stop(viddec_fd, viddec_ctx->encoding, &decoder_stopped);
	if (ret || decoder_stopped)
		goto skip_video_setup;

	/*
	 * Setup video decoder with new policies
	 */
	viddec_ctx->video_cmd = video_cmd;
	viddec_ctx->num_commands = sizeof(video_cmd)/sizeof(struct video_command);
	ret = dvb_video_setup_play(viddec_fd, viddec_ctx);
	if (ret)
		printf("%s(): failed to setup video for play\n", __func__);

skip_video_setup:
	if (!ret && (debug_level >= 2))
		printf("Video configuration complete\n");

	return ret;
}

/**
 * setup_fcc_audio_decoder() - setup audio decoder for fcc
 */
static int setup_fcc_audio_decoder(int debug_level, int auddec_fd,
					struct audio_decoder_context *auddec_ctx)
{
	int ret;
	bool decoder_stopped;

	struct audio_command audio_cmd[ ] = {
		{
		.cmd = AUDIO_CMD_SET_OPTION,
		.u.option.option = DVB_OPTION_LIVE_PLAY,
		.u.option.value = DVB_OPTION_VALUE_DISABLE,
		},
	};

	/*
	 * Start or stop decoder depending on the audio encoding value
	 */
	ret = audio_decoder_start_stop(auddec_fd, auddec_ctx->encoding, &decoder_stopped);
	if (ret || decoder_stopped)
		goto skip_audio_setup;

	/*
	 * Setup audio for play
	 */
	auddec_ctx->audio_cmd = audio_cmd;
	auddec_ctx->num_commands = sizeof(audio_cmd)/sizeof(struct audio_command);
	ret = dvb_audio_setup_play(auddec_fd, auddec_ctx);
	if (ret)
		printf("%s(): failed to setup audio for program change\n", __func__);

skip_audio_setup:
	if (!ret && (debug_level >= 2))
		printf("Audio configuration complete\n");

	return ret;
}

/**
 * process_command() - process command
 */
static void process_command(struct playback_context *playback_ctx,
				enum commands command,
				char *command_line,
				int size)
{
	int ret;
	bool retuned = false;
	struct program_context swap_prog_ctx;
	struct program_context *cur_prog_ctx, *next_prog_ctx;
	struct demuxer_context *cur_demux_ctx, *next_demux_ctx;
	struct decoder_context *dec_ctx = &playback_ctx->dec_ctx;

	if (!playback_ctx->fcc)
		goto done;

	/*
	 * Start processing command
	 */
	ret = gettimeofday(&playback_ctx->start_time, NULL);
	if (ret) {
		printf("%s(): failed to get command start time\n", __func__);
		goto done;
	}

	/*
	 * Prepare for the next program context
	 */
	switch (command) {
	case PROGRAM_UP:
		cur_prog_ctx = &playback_ctx->play_prog_ctx;
		next_prog_ctx = &playback_ctx->cached_prog_ctx[MAX_CACHED_PROGRAMS/2];
		break;

	case PROGRAM_DOWN:
		cur_prog_ctx = &playback_ctx->play_prog_ctx;
		next_prog_ctx = &playback_ctx->cached_prog_ctx[MAX_CACHED_PROGRAMS/2 - 1];
		break;
	}

	/*
	 * Process for the commands
	 */
	switch (command) {
	case PROGRAM_UP:
	case PROGRAM_DOWN:
	{
		int i;
		cur_demux_ctx = &cur_prog_ctx->demux_ctx;
		next_demux_ctx = &next_prog_ctx->demux_ctx;

		printf("Switching to program %s\n", next_prog_ctx->name);

		/*
		 * Put demuxer in cached mode, so, that, no new data goes to SE
		 */
		if (cur_prog_ctx->fe_data) {
			ret = dvb_demux_cached(cur_demux_ctx, cur_demux_ctx->start_code);
			if (ret) {
				printf("%s(): failed to put current demuxer in cached mode\n", __func__);
				break;
			}
		}

		if (playback_ctx->debug_level > 0) {
			memset(&timestamps[CUR_DEMUX_FLUSH], 0, sizeof(timestamps[CUR_DEMUX_FLUSH]));
			gettimeofday(&timestamps[CUR_DEMUX_FLUSH], NULL);
		}
		/*
		 * Set encoding anyhow, so, that first frame on display event is received.
		 * We may need to update the playback policies
		 */
		dec_ctx->viddec_ctx.encoding = next_demux_ctx->pid_info.video_enc;
		dec_ctx->auddec_ctx.encoding = next_demux_ctx->pid_info.audio_enc;
		ret = setup_fcc_video_decoder(playback_ctx->debug_level,
						dec_ctx->viddec_fd, &dec_ctx->viddec_ctx);
		if (ret) {
			printf("%s(): failed to setup video decoder for next program\n", __func__);
			break;
		}
		ret = setup_fcc_audio_decoder(playback_ctx->debug_level,
						dec_ctx->auddec_fd, &dec_ctx->auddec_ctx);
		if (ret) {
			printf("%s(): failed to setup audio decoder for next program\n", __func__);
			break;
		}

		if (playback_ctx->debug_level > 0) {
			memset(&timestamps[SE_SWITCH], 0, sizeof(timestamps[SE_SWITCH]));
			gettimeofday(&timestamps[SE_SWITCH], NULL);
		}
		/*
		 * Tune for next program
		 */
		ret = play_setup_frontend(next_prog_ctx, &retuned);
		if (ret)
			util_err("Cannot tune %s\n", next_prog_ctx->name);

		if (playback_ctx->debug_level > 0) {
			memset(&timestamps[FE_LOCK], 0, sizeof(timestamps[FE_LOCK]));
			gettimeofday(&timestamps[FE_LOCK], NULL);
		}
		/*
		 * See above, the ret value is going to be zero'ed out here,
		 * as we don't want to miss a chance to setup cached context.
		 */
		ret = play_setup_main_demux(next_demux_ctx,
				next_prog_ctx->fe_data, DMX_OUT_SELECTOR, retuned);
		if (ret) {
			util_err("Failed to setup main demux\n");
			break;
		}

		if (playback_ctx->debug_level > 0) {
			memset(&timestamps[SELECTOR_SWITCH], 0, sizeof(timestamps[SELECTOR_SWITCH]));
			gettimeofday(&timestamps[SELECTOR_SWITCH], NULL);
		}
		/*
		 * Update the play_prog_ctx with the new ctx
		 */
		memcpy(&swap_prog_ctx, cur_prog_ctx, sizeof(swap_prog_ctx));
		memcpy(cur_prog_ctx, next_prog_ctx, sizeof(*cur_prog_ctx));
		memcpy(next_prog_ctx, &swap_prog_ctx, sizeof(swap_prog_ctx));

		/*
		 * Update cached program contexts now
		 */
		ret = play_setup_cached_program_ctx(playback_ctx, cur_prog_ctx->name);
		if (ret) {
			printf("%s(): failed to setup cached program context\n", __func__);
			break;
		}

		/*
		 * We are going to reassign the frontend information for cached channels
		 */
		for (i = 0; i < MAX_CACHED_PROGRAMS; i++)
			playback_ctx->cached_prog_ctx[i].fe_data = NULL;

		/*
		 * Update the cached tuner information
		 */
		ret = play_setup_cached_frontend(playback_ctx);
		if (ret) {
			printf("%s(): failed to setup up cached programs tuner context\n", __func__);
			break;
		}

		/*
		 * Update demuxer filters for cached programs
		 */
		ret = play_setup_cached_demuxers(playback_ctx);
		if (ret)
			util_err("Failed to set up cached demuxers\n");

		break;
	}

	}

done:
	return;
}

/**
 * zaptime_monitor() - measures zap time
 */
static void *zaptime_monitor(void *arg)
{
	int ret;
	struct pollfd pollfd;
	struct playback_context *playback_ctx = arg;
	struct decoder_context *dec_ctx = &playback_ctx->dec_ctx;
	unsigned long long time_in_us, key_time_us, prev_event_time_in_us;
	unsigned long long times_in_us[4];
	struct timeval timeval;
	char time_break_up[200];

	ret = gettimeofday(&timeval, NULL);
	if (ret) {
		printf("%s(): failed to get time of day\n", __func__);
	}
	time_in_us = timeval.tv_sec;
	time_in_us *= 1000000;
	time_in_us += timeval.tv_usec;

	while (!playback_ctx->exit) {

		memset(&pollfd, 0, sizeof(pollfd));
		pollfd.fd = dec_ctx->viddec_fd;
		pollfd.events = POLLPRI;
		while (poll(&pollfd, 1, -1) > 0) {

			struct video_event video_event;

			if (!(pollfd.revents & POLLPRI))
				continue;

			ret = ioctl(dec_ctx->viddec_fd, VIDEO_GET_EVENT, &video_event);
			if (ret && (errno != -EOVERFLOW))
				continue;

			switch (video_event.type) {
			case VIDEO_EVENT_FIRST_FRAME_ON_DISPLAY:
			{
				int i, str_len;
				int len;

				memset(&timeval, 0, sizeof(timeval));
				if (memcmp(&playback_ctx->start_time, &timeval, sizeof(struct timeval))) {

					ret = gettimeofday(&timeval, NULL);
					if (ret) {
						printf("%s(): failed to get time of day\n", __func__);
						continue;
					}
					prev_event_time_in_us = time_in_us;
					time_in_us = timeval.tv_sec;
					time_in_us *= 1000000;
					time_in_us += timeval.tv_usec;

					key_time_us = playback_ctx->start_time.tv_sec;
					key_time_us *= 1000000;
					key_time_us += playback_ctx->start_time.tv_usec;

					printf("%sZapping time (msec: %.2f) - %s%s\n", BLUE,
							(double)(time_in_us - key_time_us)/1000,
							playback_ctx->play_prog_ctx.name,
							NONE);
					if (!playback_ctx->debug_level)
						break;
					for (i = 0; i < TIMESTAMP_END; i++) {
						times_in_us[i] = timestamps[i].tv_sec;
						times_in_us[i] *= 1000000;
						times_in_us[i] += timestamps[i].tv_usec;
					}
					str_len = sizeof(time_break_up);
					memset(time_break_up, 0, str_len);;
					snprintf(time_break_up, str_len - 1, "Time Break Up (msec): Last Playback %.2f | ", (double)(key_time_us - prev_event_time_in_us)/1000);
					len = strlen(time_break_up);
					snprintf(&time_break_up[len], str_len - len - 1, "Cur Demux Flush: %.2f | ", (double)(times_in_us[CUR_DEMUX_FLUSH] - key_time_us)/1000);
					len = strlen(time_break_up);
					snprintf(&time_break_up[len], str_len - len - 1, "SE Switch: %.2f | ", (double)(times_in_us[SE_SWITCH] - times_in_us[CUR_DEMUX_FLUSH])/1000);
					len = strlen(time_break_up);
					snprintf(&time_break_up[len], str_len - len - 1, "FE Lock: %.2f | ", (double)(times_in_us[FE_LOCK] - times_in_us[SE_SWITCH])/1000);
					len = strlen(time_break_up);
					snprintf(&time_break_up[len], str_len - len - 1, "Selector switch: %.2f | ", (double)(times_in_us[SELECTOR_SWITCH] - times_in_us[FE_LOCK])/1000);
					len = strlen(time_break_up);
					snprintf(&time_break_up[len], str_len - len - 1, "SE decode: %.2f", (double)(time_in_us - times_in_us[SELECTOR_SWITCH])/1000);
					printf("%s%s%s\n", YELLOW, time_break_up, NONE);
				}

				break;
			}
			default:
				break;
			}
		}
	}

	return NULL;
}

/**
 * keyboard_monitor() - keyboard monitor thread
 */
void *keyboard_monitor(void *arg)
{
	int ret, command;
	char command_line[COMMAND_LINE_SIZE];
	struct playback_context *playback_ctx = arg;

	/*
	 * Create a zap time measurement thread
	 */
	ret = pthread_create(&playback_ctx->zaptime_thread,
					NULL, zaptime_monitor, playback_ctx);
	if (ret) {
		printf("%s(): failed to create zap time measurement thread\n", __func__);
		goto exit;
	}

	/*
	 * Read the command line and parse it for the options
	 */
	while (fgets(command_line, sizeof(command_line), stdin)) {

		command = find_command(command_line);
		if (command < 0) {
			keyboard_help();
			continue;
		}

		process_command(playback_ctx, command, command_line, sizeof(command_line));
	}

exit:
	return NULL;
}
