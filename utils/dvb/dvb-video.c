/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * ldvb video decoder start and helper functions
************************************************************************/
#include <stdio.h>
#include <sys/ioctl.h>
#include "dvb-video.h"
#include "linux/dvb/video.h"
#include "linux/dvb/stm_dvb.h"
#include "linux/dvb/stm_video.h"

/**
 * dvb_video_set_source() - setup video decoder source
 */
int dvb_video_set_source(int fd, __u32 source)
{
	int ret;

	/*
	 * Setup video source
	 */
	ret = ioctl(fd, VIDEO_SELECT_SOURCE, (void *)source);
	if (ret)
		printf("%s(): failed to source to %d\n", __func__, source);

	return ret;
}

/**
 * dvb_video_setup_play() - setup decoder for play
 */
int dvb_video_setup_play(int fd, struct video_decoder_context *viddec_ctx)
{
	int i, ret;

	/*
	 * Set all the policies
	 */
	for (i = 0; i < viddec_ctx->num_commands; i++) {
		ret = ioctl(fd, VIDEO_COMMAND, (void *)&viddec_ctx->video_cmd[i]);
		if (ret) {
			printf("%s(): failed to set command at index %d\n", __func__, i);
			goto set_policy_failed;
		}
	}


	/*
	 * Set the encoding
	 */
	ret = ioctl(fd, VIDEO_SET_ENCODING, (void *)viddec_ctx->encoding);
	if (ret)
		printf("%s(): failed to set video encoding\n", __func__);

set_policy_failed:
	return ret;
}

/*
 * dvb_video_decoder_start() - start video decoder
 */
int dvb_video_decoder_start(int fd)
{
	int ret;

	ret = ioctl(fd, VIDEO_PLAY, NULL);
	if (ret)
		printf("%s(): failed to start video decoder\n", __func__);

	return ret;
}

/**
 * dvb_video_decoder_stop() - stop video decoder
 */
int dvb_video_decoder_stop(int fd, enum vid_dec_blank_value blank_value)
{
	int ret;

	ret = ioctl(fd, VIDEO_STOP, blank_value);
	if (ret)
		printf("%s(): failed to stop video decoder\n", __func__);

	return ret;
}

/*
 * stm_dvb_video_set_speed() - set speed of playback
 */
int stm_dvb_video_set_speed(int fd, int speed)
{
	int ret;

	ret = ioctl(fd, VIDEO_SET_SPEED, speed);
	if (ret)
		printf("%s(): failed to set playback speed\n", __func__);

	return ret;
}
