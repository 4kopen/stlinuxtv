/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Tuner tune and lock header
************************************************************************/
#ifndef __DVB_TUNE_H__
#define __DVB_TUNE_H__

#include <linux/dvb/frontend.h>
#include "dvb-demux.h"

enum band_type {
	BAND_US,
	BAND_EU_LOW,
	BAND_EU_HIGH,
	BAND_EU_TER,
	BAND_CABLE,
};

struct dvb_fe_tune_params {
	fe_delivery_system_t delivery_system;
	fe_modulation_t	modulation;
	fe_code_rate_t fec;
	fe_spectral_inversion_t inversion;
	fe_rolloff_t rolloff;
	fe_pilot_t pilot;
	fe_sec_voltage_t voltage;
	fe_sec_tone_mode_t tone;
	__u32 frequency;
	__u32 symbol_rate;
	__u32 satellite;
	enum band_type band;
	char polarization[32];
};

int dvb_fe_tune(int debug_level, int fd, char *conf_file, char *channel_name);
int dvb_get_pids(int debug_level, char *conf_file,
			char *channel_file, struct pid_info *pid_info);
int dvb_fe_get_params(int debug_level, char *conf_file, char *channel_name,
					struct dvb_fe_tune_params *dvb_tune_params);

#endif
