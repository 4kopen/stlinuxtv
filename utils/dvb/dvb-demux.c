/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * DVB demuxer filter creation and starting demux
************************************************************************/
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/dvb/dmx.h>
#include "linux/dvb/stm_dmx.h"
#include "dvb-demux.h"

#define VIDEO_BUF_SIZE (5 * 1024 * 1024 + 512 * 1024)
#define AUDIO_BUF_SIZE (1024 * 1024)

/**
 * dvb_demux_bypass() - start a selective demuxer
 */
int dvb_demux_bypass(struct demuxer_context *demux_ctx, bool forced)
{
	int ret = 0;
	struct dmx_ctrl ctrl;

	/*
	 * Put demuxer in pass through mode
	 */
	memset(&ctrl, 0, sizeof(ctrl));
	ctrl.id = DMX_CTRL_PES_SELECTOR_MODE;
	ctrl.value = DMX_CTRL_VALUE_PES_PASS_THROUGH_MODE;
	ctrl.output = DMX_OUT_SELECTOR;

	if (forced)
		ctrl.value = DMX_CTRL_VALUE_PES_FORCE_PASS_THROUGH_MODE;
	/*
	 * Zero PID value means we have an invalid PID value.
	 * So we do skip setting up filters with invalid PID.
	 *
	 * Put pcr in bypass mode
	 */
	if (demux_ctx->pcr_pid_fd > 0 && demux_ctx->pid_info.pcr_pid > 0) {
		ctrl.pes_type = demux_ctx->pcr_pes_type,
		ret = ioctl(demux_ctx->pcr_pid_fd, DMX_SET_CTRL, &ctrl);
		if (ret) {
			printf("%s(): unable to set pcr on demux%d in bypass mode\n", __func__, demux_ctx->id);
			goto set_ctrl_failed;
		}
	}

	/*
	 * Put video in bypass mode
	 */
	if (demux_ctx->video_pid_fd > 0&& demux_ctx->pid_info.video_pid > 0) {
		ctrl.pes_type = demux_ctx->video_pes_type;
		ret = ioctl(demux_ctx->video_pid_fd, DMX_SET_CTRL, &ctrl);
		if (ret) {
			printf("%s(): unable to set video on demux%d in bypass mode\n", __func__, demux_ctx->id);
			goto set_ctrl_failed;
		}
	}

	/*
	 * Put audio in bypass mode
	 */
	if (demux_ctx->audio_pid_fd > 0 && demux_ctx->pid_info.audio_pid > 0) {
		ctrl.pes_type = demux_ctx->audio_pes_type;
		ret = ioctl(demux_ctx->audio_pid_fd, DMX_SET_CTRL, &ctrl);
		if (ret) {
			printf("%s(): unable to set audio on demux%d in bypass mode\n", __func__, demux_ctx->id);
			goto set_ctrl_failed;
		}
	}

	return ret;

set_ctrl_failed:
	errno = 0;
	return ret;
}

/**
 * dvb_demux_cached() - put the demuxer in cached mode
 */
int dvb_demux_cached(struct demuxer_context *demux_ctx, char start_code[6])
{
	int ret = 0;
	struct dmx_ctrl ctrl;

	/*
	 * Put demuxer in pass through mode
	 */
	memset(&ctrl, 0, sizeof(ctrl));
	ctrl.id = DMX_CTRL_PES_SELECTOR_MODE;
	ctrl.output = DMX_OUT_SELECTOR;

	/*
	 * Put pcr in bypass mode
	 */
	if (demux_ctx->pcr_pid_fd > 0 && demux_ctx->pid_info.pcr_pid > 0) {
		ctrl.value = DMX_CTRL_VALUE_PES_CACHED_MODE;
		ctrl.pes_type = demux_ctx->pcr_pes_type;
		ret = ioctl(demux_ctx->pcr_pid_fd, DMX_SET_CTRL, &ctrl);
		if (ret) {
			printf("%s(): failed to set pcr on demux%d in cached mode\n", __func__, demux_ctx->id);
			goto set_ctrl_failed;
		}
	}

	/*
	 * Put video in cached mode
	 */
	if (demux_ctx->video_pid_fd > 0 && demux_ctx->pid_info.video_pid > 0) {
		ctrl.start_code[0] = start_code[0];
		ctrl.start_code[1] = start_code[1];
		ctrl.start_code[2] = start_code[2];
		ctrl.start_code[3] = start_code[3];
		ctrl.start_code[4] = start_code[4];
		ctrl.start_code[5] = start_code[5];
		ctrl.pes_type = demux_ctx->video_pes_type;

		ret = ioctl(demux_ctx->video_pid_fd, DMX_SET_CTRL, &ctrl);
		if (ret) {
			printf("%s(): failed to set video on demux%d in cached mode\n", __func__, demux_ctx->id);
			goto set_ctrl_failed;
		}
	}

	/*
	 * Put audio in bypass mode
	 */
	if (demux_ctx->audio_pid_fd > 0 && demux_ctx->pid_info.audio_pid > 0) {
		ctrl.value = DMX_CTRL_VALUE_PES_CACHED_MODE;
		ctrl.pes_type = demux_ctx->audio_pes_type;
		ret = ioctl(demux_ctx->audio_pid_fd, DMX_SET_CTRL, &ctrl);
		if (ret) {
			printf("%s(): failed to set audio on demux%d in cached mode\n", __func__, demux_ctx->id);
			goto set_ctrl_failed;
		}
	}

set_ctrl_failed:
	return ret;
}

/**
 * stm_dvb_demux_set_source() - change the source of demux
 */
int stm_dvb_demux_set_source(int fd, __u32 source)
{
	int ret;

	ret = ioctl(fd, DMX_SET_SOURCE, &source);
	if (ret)
		printf("%s(): failed to set demux src to frontend%d\n", __func__, source);

	return ret;
}

/**
 * stm_dvb_demux_setup_filters() - setup all filters
 */
int stm_dvb_demux_setup_filters(int debug_level,
		struct demuxer_context *demux_ctx, __u32 filter_out)
{
	int ret = 0;
	struct dmx_pes_filter_params pes_params;

	memset(&pes_params, 0, sizeof(pes_params));

	if (debug_level >= 2)
		printf("Setting filters on demux%d with source set to frontend\n", demux_ctx->id);

	/*
	 * Set up pcr filter
	 */
	if (demux_ctx->pcr_pid_fd > 0) {
		pes_params.pid = demux_ctx->pid_info.pcr_pid;
		pes_params.input = DMX_IN_FRONTEND;
		pes_params.output = filter_out;
		pes_params.pes_type = demux_ctx->pcr_pes_type;
		ret = ioctl(demux_ctx->pcr_pid_fd, DMX_SET_PES_FILTER, &pes_params);
		if (ret) {
			printf("%s(): failed to set pcr pes filter\n", __func__);
			goto set_pes_filter_failed;
		}
		if (debug_level >=2 )
			printf("Applied pcr filter with pid: %d on demux%d\n", pes_params.pid, demux_ctx->id);
	}

	/*
	 * Setup video pes filter
	 */
	if (demux_ctx->video_pid_fd > 0) {
		pes_params.pid = demux_ctx->pid_info.video_pid;
		pes_params.input = DMX_IN_FRONTEND;
		pes_params.output = filter_out;
		pes_params.pes_type = demux_ctx->video_pes_type;
		ret = ioctl(demux_ctx->video_pid_fd, DMX_SET_PES_FILTER, &pes_params);
		if (ret) {
			printf("%s(): failed to set video pes filter\n", __func__);
			goto set_pes_filter_failed;
		}

		ret = ioctl(demux_ctx->video_pid_fd, DMX_SET_BUFFER_SIZE, VIDEO_BUF_SIZE);
		if (ret) {
			printf("%s(): failed to set video pes buffer size\n", __func__);
			goto set_pes_filter_failed;
		}

		if (debug_level >= 2)
			printf("Applied video filter with pid: %d on demux%d\n", pes_params.pid, demux_ctx->id);
	}

	/*
	 * Setup audio pes filter
	 */

	if (demux_ctx->audio_pid_fd > 0) {
		pes_params.pid = demux_ctx->pid_info.audio_pid;
		pes_params.input = DMX_IN_FRONTEND;
		pes_params.output = filter_out;
		pes_params.pes_type = demux_ctx->audio_pes_type;
		ret = ioctl(demux_ctx->audio_pid_fd, DMX_SET_PES_FILTER, &pes_params);
		if (ret) {
			printf("%s(): failed to set audio pes filter\n", __func__);
			goto set_pes_filter_failed;
		}

		ret = ioctl(demux_ctx->audio_pid_fd, DMX_SET_BUFFER_SIZE, AUDIO_BUF_SIZE);
		if (ret) {
			printf("%s(): failed to set audio pes buffer size\n", __func__);
		}

		if (debug_level >= 2)
			printf("Applied audio filter with pid: %d on demux%d\n", pes_params.pid, demux_ctx->id);
	}

set_pes_filter_failed:
	return ret;;
}

/**
 * stm_dvb_demux_open() - open the demuxer per pid
 */
int stm_dvb_demux_open(struct demuxer_context *demux_ctx, __u32 filter_out)
{
	int i, ret = 0;
	char name[32];

	/*
	 * Find out the demuxer which can be opened in O_EXCL mode.
	 * We are going to setup a tunneled decode, and each decode
	 * can be supported from an exclusive demuxer
	 */
	for (i = 0; ; i++) {
		snprintf(name, sizeof(name), "/dev/dvb/adapter0/demux%d", i);
		name[sizeof(name) - 1] = '\0';
		ret = open(name, O_RDWR | O_EXCL);
		if ((ret < 0) && (errno == EBUSY))
			continue;
		else if (ret > 0) {
			close(ret);
			break;
		} else {
			printf("%s(): No free demuxers found, reached the count of %d\n", __func__, i);
			goto open_failed;
		}
	}
	demux_ctx->id = i;

	/*
	 * Open for video filter
	 */
	if (demux_ctx->pid_info.video_pid >= 0) {
		demux_ctx->video_pid_fd = open(name, O_RDWR);
		if (demux_ctx->video_pid_fd < 0) {
			printf("%s(): failed to open demux for video filter\n", __func__);
			ret = errno;
			goto open_failed;
		}
	}

	/*
	 * Open for audio filter
	 */
	if (demux_ctx->pid_info.audio_pid >= 0) {
		demux_ctx->audio_pid_fd = open(name, O_RDWR);
		if (demux_ctx->audio_pid_fd < 0) {
			printf("%s(): failed to open demux for audio filter\n", __func__);
			ret = errno;
			goto audio_open_failed;
		}
	}

	/*
	 * Open for pcr filter
	 */
	if (demux_ctx->pid_info.pcr_pid >= 0) {
		demux_ctx->pcr_pid_fd = open(name, O_RDWR);
		if (demux_ctx->pcr_pid_fd < 0) {
			printf("%s(): failed to open demux for pcr filter\n", __func__);
			ret = errno;
			goto pcr_open_failed;
		}
	}

	/*
	 * Open for subt filter
	 */
	if (demux_ctx->pid_info.subt_pid >= 0) {
		demux_ctx->subt_pid_fd = open(name, O_RDWR);
		if (demux_ctx->subt_pid_fd < 0) {
			printf("%s(): failed to open demux for subt filter\n", __func__);
			ret = errno;
			goto subt_open_failed;
		}
	}

	/*
	 * Open for ttx filter
	 */
	if (demux_ctx->pid_info.ttx_pid >= 0) {
		demux_ctx->ttx_pid_fd = open(name, O_RDWR);
		if (demux_ctx->ttx_pid_fd < 0) {
			printf("%s(): failed to open demux for ttx filter\n", __func__);
			ret = errno;
			goto ttx_open_failed;
		}
	}

	return 0;

ttx_open_failed:
	close(demux_ctx->subt_pid_fd);
subt_open_failed:
	close(demux_ctx->pcr_pid_fd);
pcr_open_failed:
	close(demux_ctx->audio_pid_fd);
audio_open_failed:
	close(demux_ctx->video_pid_fd);
open_failed:
	return ret;
}

int dvb_demux_start_filters(struct demuxer_context *demux_ctx)
{
	int ret=0;

	if (demux_ctx->pcr_pid_fd > 0 && demux_ctx->pid_info.pcr_pid > 0) {
		ret = ioctl(demux_ctx->pcr_pid_fd, DMX_START);
		if (ret) {
			printf("%s(): failed to start pcr pes\n", __func__);
			goto exit;
		}
	}

	if (demux_ctx->video_pid_fd > 0 && demux_ctx->pid_info.video_pid > 0) {
		ret = ioctl(demux_ctx->video_pid_fd, DMX_START);
		if (ret) {
			printf("%s(): failed to start video pes\n", __func__);
			goto exit;
		}
	}

	if (demux_ctx->audio_pid_fd > 0 && demux_ctx->pid_info.audio_pid > 0) {
		ret = ioctl(demux_ctx->audio_pid_fd, DMX_START);
		if (ret) {
			printf("%s(): failed to start audio pes\n", __func__);
			goto exit;
		}
	}

exit:
	return ret;
}

/**
 * dvb_demux_stop_inactive_filters() - stop the filters
 * Stops the filter which are not required after zapping.
 */
int dvb_demux_stop_inactive_filters(struct demuxer_context *demux_ctx)
{
	int ret=0;

	if (demux_ctx->pcr_pid_fd > 0 && demux_ctx->pid_info.pcr_pid <= 0) {
		ret = ioctl(demux_ctx->pcr_pid_fd, DMX_STOP);
		if (ret) {
			printf("%s(): failed to start pcr pes\n", __func__);
			goto exit;
		}
	}

	if (demux_ctx->video_pid_fd > 0 && demux_ctx->pid_info.video_pid <= 0) {
		ret = ioctl(demux_ctx->video_pid_fd, DMX_STOP);
		if (ret) {
			printf("%s(): failed to start video pes\n", __func__);
			goto exit;
		}
	}

	if (demux_ctx->audio_pid_fd > 0 && demux_ctx->pid_info.audio_pid <= 0) {
		ret = ioctl(demux_ctx->audio_pid_fd, DMX_STOP);
		if (ret) {
			printf("%s(): failed to start audio pes\n", __func__);
			goto exit;
		}
	}

exit:
	return ret;
}
