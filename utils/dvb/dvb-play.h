/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Fast Channel change definitions and declarations
************************************************************************/
#ifndef __DVB_PLAY_H__
#define __DVB_PLAY_H__

#include <pthread.h>
#include <semaphore.h>

#include "dvb-video.h"
#include "dvb-audio.h"
#include "dvb-demux.h"

/*
 * This represents one program instance. Since, frontend
 * and demuxers are per program context, so, it broadly
 * represents them
 */
#define PROG_NAME_LEN		64

struct decoder_context {
	int dec_id;
	int auddec_fd, viddec_fd;

	struct video_decoder_context viddec_ctx;
	struct audio_decoder_context auddec_ctx;
};

/*
 * Frontend data represented by the this structure
 * @id   : physical id of the frontend to be used
 * @fd   : fd of this physical frontend
 * @tuned: tuned status
 */
struct frontend_data {
	int id;
	int fd;
	bool tuned;
};

/*
 * This is per program context structure. Each program requires one
 * individual demux, so, program context encapsulates the demuxer instance.
 * @name     : name of the program
 * @fe_data  : pointer to common pool of frontends
 * @demux_ctx: each program requires one demuxer (tunneled data flow)
 */
struct program_context {
	char name[PROG_NAME_LEN];

	struct frontend_data *fe_data;
	struct demuxer_context demux_ctx;
};

/*
 * Total number of programs are listed by this structure
 * @fpos: file position of the particular program in conf file
 * @name: program name
 */
struct program_list {
	int fpos;
	char name[PROG_NAME_LEN];
};

#define MAX_CACHED_PROGRAMS	2
/*
 * Overall context representing complete playback
 * @fcc             : is the playback fcc or not
 * @exit            : global exit status
 * @zaptime_thread  : measurement of zap time
 * @start_time      : Time when zap requested
 * @debug_level     : global debug level to control logs
 * @prog_count      : Total programs in the conf file
 * @prog_list       : Program info (items = prog_count)
 * @fe_count        : Number of frontends available (passed from command_line)
 * @fe_data         : frontend info (items = fe_count)
 * @dec_ctx         : One decoder for normal/fcc
 * @play_prog_ctx   : Currently playing program context
 * @cached_prog_ctx : Cached program context
 */
struct playback_context {
	bool fcc, exit;
	int debug_level;

	pthread_t zaptime_thread;
	struct timeval start_time;

	int prog_count;
	struct program_list *prog_list;

	int fe_count;
	struct frontend_data *fe_data;

	struct decoder_context dec_ctx;
	struct program_context play_prog_ctx, cached_prog_ctx[MAX_CACHED_PROGRAMS];
};

void *keyboard_monitor(void *arg);
inline int play_get_valid_demux_fd(struct demuxer_context *demux_ctx);
int play_setup_frontend(struct program_context *play_prog_ctx, bool *retuned);
int play_setup_fcc_dvb_demux(struct playback_context *playback_ctx);
int play_setup_main_demux(struct demuxer_context *demux_ctx,
		struct frontend_data *fe_data, __u32 filter_out, bool retuned);
int play_setup_cached_frontend(struct playback_context *playback_ctx);
int play_setup_cached_demuxers(struct playback_context *playback_ctx);
int play_setup_cached_program_ctx(struct playback_context *playback_ctx, char *play_prog_name);
int video_decoder_start_stop(int viddec_fd, unsigned int encoding, bool *decoder_stopped);
int audio_decoder_start_stop(int auddec_fd, unsigned int encoding, bool *decoder_stopped);
#endif
