/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Live playback, and fast channel change
************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <poll.h>
#include <signal.h>
#include <limits.h>
#include <ctype.h>

#include "linux/dvb/dmx.h"
#include "linux/dvb/audio.h"
#include "linux/dvb/video.h"
#include "linux/dvb/stm_dmx.h"
#include "linux/dvb/stm_audio.h"
#include "linux/dvb/stm_video.h"

#include "dvb-play.h"
#include "dvb-tune.h"
#include "dvb-audio.h"
#include "dvb-video.h"
#include "dvb-demux.h"
#include "stm_utils.h"

#define CHANNEL_CONF		"stlinuxtv-dvb-channels.conf"

static char usr_fe[32], main_prog[PROG_NAME_LEN];
static int fe_count = 1;
static pthread_t keyboard_thread;
static struct playback_context playback_ctx;

/**
 * play_get_valid_demux_fd() - return a valid fd of demux
 */
inline int play_get_valid_demux_fd(struct demuxer_context *demux_ctx)
{
	int fd;

	if (demux_ctx->video_pid_fd > 0)
		fd = demux_ctx->video_pid_fd;
	else if (demux_ctx->audio_pid_fd > 0)
		fd = demux_ctx->audio_pid_fd;
	else if (demux_ctx->pcr_pid_fd > 0)
		fd = demux_ctx->pcr_pid_fd;
	else
		fd = -1;

	return fd;
}

/**
 * display_prog_info() - display per program info
 */
static void display_prog_info(struct program_context *play_prog_ctx,
				struct program_context *cached_prog_ctx)
{
#define PRINT_LINE(len)			\
	printf(" ");			\
	for (i = 0; i < len - 3; i++)	\
		 printf("-");		\
	printf("\n")

#define MAIN_PROG	0
#define CACHED_PROG	1

#define PRINT_PROG_CTX(idx, cached, prog_ctx)	\
	printf("| %-6d| %-24s| %-9d| %-9d| %-9s| %-9d| %-10d| %-10d| %-8d|\n",	\
		cached ? idx + 1 : idx, prog_ctx[idx].name,				\
		prog_ctx[idx].fe_data ? prog_ctx[idx].fe_data->id : -1,		\
		prog_ctx[idx].fe_data ? prog_ctx[idx].fe_data->fd: -1,		\
		prog_ctx[idx].fe_data ?						\
		(prog_ctx[idx].fe_data->tuned ? "true" : "false") : "unknown",	\
		prog_ctx[idx].demux_ctx.id,						\
		prog_ctx[idx].demux_ctx.pid_info.video_pid,				\
		prog_ctx[idx].demux_ctx.pid_info.audio_pid,				\
		prog_ctx[idx].demux_ctx.pid_info.pcr_pid)

	int i, len;
	char str[256];

	if (playback_ctx.debug_level < 1)
		goto print_done;

	len = snprintf(str, sizeof(str), "| %-6s| %-24s| %-9s| %-9s| %-9s| %-9s| %-10s| %-10s| %-8s|\n",
			"S.No.", "Program Name", "demod-id", "demod-fd", "tuned",
			"demux-id", "video-pid", "audio-pid", "pcr-pid");

	PRINT_LINE(len);

	printf("%s", str);

	PRINT_LINE(len);

	PRINT_PROG_CTX(0, MAIN_PROG, play_prog_ctx);

	for (i = 0; i < MAX_CACHED_PROGRAMS; i++)
		PRINT_PROG_CTX(i, CACHED_PROG, cached_prog_ctx);

	PRINT_LINE(len);

print_done:
	return;

#undef PRINT_PROG_CTX
#undef CACHED_PROG
#undef MAIN_PROG
#undef PRINT_LINE
}

/**
 * setup_fe_tune() - scan over all available fe's and tune
 */
static int setup_fe_tune(struct program_context *prog_ctx, bool force)
{
	int i, ret;
	struct frontend_data *fe_data = playback_ctx.fe_data;

	/*
	 * Try to tune by scanning all available frontends
	 */
	for (i = 0; i < fe_count; i++) {

		/*
		 * Cached programs has a lower priority, so, don't
		 * try to reclaim any other program's frontend.
		 * force = false for cached tuner setup
		 */
		if (fe_data[i].tuned && !force)
			continue;

		ret = dvb_fe_tune(playback_ctx.debug_level, fe_data[i].fd,
						CHANNEL_CONF, prog_ctx->name);
		if (ret) {
			util_err("** failed to tune fe%d for program %s **\n", fe_data[i].id, prog_ctx->name);
			continue;
		}
		fe_data[i].tuned = true;
		prog_ctx->fe_data = &fe_data[i];
		break;
	}

	if (i == fe_count)
		ret = -EINVAL;

	return ret;
}

/**
 * play_setup_cached_frontend() - setup the frontend for cache'd programs
 */
int play_setup_cached_frontend(struct playback_context *playback_ctx)
{
	int i, j, ret = 0;
	struct program_context *play_prog_ctx = &playback_ctx->play_prog_ctx;
	struct program_context *cached_prog_ctx = playback_ctx->cached_prog_ctx;
	struct dvb_fe_tune_params play_tune_params;
	struct dvb_fe_tune_params cached_tune_params[MAX_CACHED_PROGRAMS];

	memset(&play_tune_params, 0, sizeof(play_tune_params));
	memset(&cached_tune_params, 0, sizeof(cached_tune_params));

	/*
	 * Get the first program frontend parameters to see if we require a new
	 * frontend or not
	 */
	ret = dvb_fe_get_params(playback_ctx->debug_level, CHANNEL_CONF,
			play_prog_ctx->name, &play_tune_params);
	if (ret) {
		printf("%s(): failed to get fe params for %s\n", __func__, play_prog_ctx->name);
		goto setup_done;
	}

	/*
	 * Fill up the cached program context
	 */
	for (i = 0; i < MAX_CACHED_PROGRAMS; i++) {

		ret = dvb_fe_get_params(playback_ctx->debug_level, CHANNEL_CONF,
				cached_prog_ctx[i].name, &cached_tune_params[i]);
		if (ret) {
			printf("%s(): failed to get fe params for %s\n", __func__, cached_prog_ctx[i].name);
			goto setup_done;
		}

		/*
		 * If the cached program params matches with the one which is
		 * the main program, reassign the same info to it. If the main
		 * program here is untuneable, then this will be too, so, need
		 * of an attempt to retune. If the main program is already tuned,
		 * then it's good to reuse that info and save time for another tune.
		 */
		if (!memcmp(&cached_tune_params[i], &play_tune_params,
					sizeof(struct dvb_fe_tune_params))) {
			if (play_prog_ctx->fe_data) {
				cached_prog_ctx[i].fe_data = play_prog_ctx->fe_data;
				util_log("fe%d reused for locking cached program %s\n",
						cached_prog_ctx[i].fe_data->id, cached_prog_ctx[i].name);
			}
			continue;
		}

		/*
		 * Find out if the current cached program params matches with any
		 * of the previous cached program params. If it matches and have
		 * valid frontend information, use it, else look out for a new
		 * frontend. If a valid information is found, it means that the
		 * program is now tuneable, so, no attempt to be made to do it again.
		 */
		for (j = i - 1; j >= 0; j--) {
			if (!memcmp(&cached_tune_params[i],
					&cached_tune_params[j], sizeof(struct dvb_fe_tune_params)) &&
					(cached_prog_ctx[j].fe_data)) {
				cached_prog_ctx[i].fe_data = cached_prog_ctx[j].fe_data;
				util_log("fe%d reused for locking cached program %s\n",
						cached_prog_ctx[i].fe_data->id, cached_prog_ctx[i].name);
				break;
			}
		}

		/*
		 * If we cannot reuse any frontend info from earlier contexts,
		 * try to find a new one, which can be used
		 */
		if (j < 0 ) {

			if (setup_fe_tune(&cached_prog_ctx[i], false))
				util_info("Failed to tune %s\n", cached_prog_ctx[i].name);
			else
				util_log("fe%d locked for cached program %s\n",
						cached_prog_ctx[i].fe_data->id, cached_prog_ctx[i].name);
		}
	}

	/*
	 * Print debug information about cached channels
	 */
	display_prog_info(play_prog_ctx, cached_prog_ctx);

setup_done:
	return ret;
}

/**
 * play_setup_frontend() - tune the frontend for specific programs
 */
int play_setup_frontend(struct program_context *play_prog_ctx, bool *retuned)
{
	int i, ret = 0;
	struct frontend_data *fe_data = play_prog_ctx->fe_data;

	/*
	 * If the play_prog_ctx has no fe data, then tune it. Do not
	 * reassign any invalid data for untuneable program. fe_data
	 * to remain NULL for untuneable program
	 */
	*retuned = false;
	if (!fe_data) {

		/*
		 * Try to tune for this program. If it's not possible to tune, this
		 * app does not exit, so, the return value is genenrally ignored
		 * and may be used for setting up demuxer.
		 */
		ret = setup_fe_tune(play_prog_ctx, true);
		if (!ret)
			*retuned = true;
	}

	/*
	 * Reset info for all other frontends because, they are
	 * going to be reassinged as the cached program context
	 * is going to change.
	 */
	for (i = 0; i < fe_count; i++) {
		if (&(playback_ctx.fe_data[i]) == play_prog_ctx->fe_data)
			continue;

		playback_ctx.fe_data[i].tuned = false;
	}

	if (ret)
		goto exit;

	if (*retuned)
		util_log("fe%d locked for main program %s\n",
				play_prog_ctx->fe_data->id, play_prog_ctx->name);
	else
		util_log("fe%d was already locked for main program %s\n",
				play_prog_ctx->fe_data->id, play_prog_ctx->name);

exit:
	return ret;
}

/**
 * setup_program_ctx() - setup the program context
 * @prog_ctx: program context to be setup
 * @name    : name of the program
 * Setup the program context with pid info
 */
static int setup_program_ctx(struct program_context *prog_ctx, char *name)
{
	int ret;
	struct demuxer_context *demux_ctx = &prog_ctx->demux_ctx;

	strncpy(prog_ctx->name, name, PROG_NAME_LEN);
	prog_ctx->name[PROG_NAME_LEN - 1] = '\0';

	/*
	 * Get pid info from conf file
	 */

	ret = dvb_get_pids(playback_ctx.debug_level, CHANNEL_CONF,
				prog_ctx->name, &demux_ctx->pid_info);
	if (ret) {
		printf("%s(): failed to parse pid info\n", __func__);
		goto get_pid_failed;
	}

	/*
	 * Fill in the start code info
	 */
	memset(demux_ctx->start_code, 0, sizeof(demux_ctx->start_code));
	if ((demux_ctx->pid_info.video_enc == VIDEO_ENCODING_MPEG1) ||
		(demux_ctx->pid_info.video_enc == VIDEO_ENCODING_MPEG2)) {

		demux_ctx->start_code[0] = 0xb3;

	} else if ((demux_ctx->pid_info.video_enc == VIDEO_ENCODING_H264)) {
		demux_ctx->start_code[0] = 0x67;
		demux_ctx->start_code[1] = 0x27;
		demux_ctx->start_code[2] = 0x47;
	} else if (demux_ctx->pid_info.video_enc == VIDEO_ENCODING_HEVC) {
		demux_ctx->start_code[0] = 0x20;
		demux_ctx->start_code[1] = 0x22;
		demux_ctx->start_code[2] = 0x24;
		demux_ctx->start_code[3] = 0x26;
		demux_ctx->start_code[4] = 0x28;
		demux_ctx->start_code[5] = 0x2A;
	}

get_pid_failed:
	return ret;
}

/**
 * setup_audio_decoder() - setup audio decoder before play
 */
static int setup_audio_decoder(int debug_level, int auddec_fd,
					struct audio_decoder_context *auddec_ctx)
{
	int ret;
	bool decoder_stopped;

	struct audio_command audio_cmd[ ] = {
		{
		 .cmd =AUDIO_CMD_SET_OPTION,
		 .u.option.option = DVB_OPTION_AV_SYNC,
		 .u.option.value = DVB_OPTION_VALUE_ENABLE,
		},
		{
		 .cmd = AUDIO_CMD_SET_OPTION,
		 .u.option.option = DVB_OPTION_DISCARD_LATE_FRAMES,
		 .u.option.value = DVB_OPTION_VALUE_DISCARD_LATE_FRAMES_AFTER_SYNCHRONIZE,
		},
		{
		 .cmd = AUDIO_CMD_SET_OPTION,
		 .u.option.option = DVB_OPTION_MASTER_CLOCK,
		 .u.option.value = DVB_OPTION_VALUE_SYSTEM_CLOCK_MASTER,
		},
		{
		 .cmd = AUDIO_CMD_SET_OPTION,
		 .u.option.option = DVB_OPTION_LIVE_PLAY,
		 .u.option.value = DVB_OPTION_VALUE_ENABLE,
		},
	};

	/*
	 * Start or stop decoder depending on the audio encoding value
	 */
	ret = audio_decoder_start_stop(auddec_fd, auddec_ctx->encoding, &decoder_stopped);
	if (ret || decoder_stopped)
		goto skip_audio_setup;


	/*
	 * Setup audio decoder context
	 */
	auddec_ctx->audio_cmd = audio_cmd;
	auddec_ctx->num_commands = sizeof(audio_cmd)/sizeof(struct audio_command);

	/*
	 * Setup audio for play
	 */
	ret = dvb_audio_setup_play(auddec_fd, auddec_ctx);
	if (ret)
		printf("%s(): failed to setup audio for play\n", __func__);

skip_audio_setup:
	if (!ret && debug_level >= 2)
		printf("Audio configuration complete\n");

	return ret;
}

/*
 * audio_decoder_start_stop - Stops audio decoder if audio encoding is not set
 */
int audio_decoder_start_stop(int auddec_fd, unsigned int encoding, bool *decoder_stopped)
{
	int ret = 0;

	/*
	 * Stop audio encoder in case encoding format is not set
	 */
	if (encoding == 0) {
		dvb_audio_decoder_stop(auddec_fd);
		*decoder_stopped = true;
	} else {
		dvb_audio_decoder_start(auddec_fd);
		*decoder_stopped = false;
	}

	return ret;
}

/*
 * video_decoder_start_stop - Stops video decoder if video encoding is not set
 */
int video_decoder_start_stop(int viddec_fd, unsigned int encoding, bool *decoder_stopped)
{
	int ret = 0;

	/*
	 * Stop video encoder in case encoding format is not set
	 */
	if (encoding == 0) {
		*decoder_stopped = true;
		dvb_video_decoder_stop(viddec_fd, VIDEO_DECODER_BLANK_ENABLE);
	} else {
		dvb_video_decoder_start(viddec_fd);
		*decoder_stopped = false;
	}

	return ret;
}

static int setup_video_decoder_live(int debug_level, int viddec_fd,
					struct video_decoder_context *viddec_ctx)
{
	int ret;
	bool decoder_stopped;
	struct video_command video_cmd [] = {
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_LIVE_PLAY,
		 .option.value = DVB_OPTION_VALUE_ENABLE,
		},
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_VIDEO_START_IMMEDIATE,
		 .option.value = DVB_OPTION_VALUE_DISABLE,
		},
	};

	/*
	 * Start or stop decoder depending on the video encoding value
	 */
	ret = video_decoder_start_stop(viddec_fd, viddec_ctx->encoding, &decoder_stopped);
	if (ret || decoder_stopped)
		goto skip_video_setup;

	viddec_ctx->video_cmd = video_cmd;
	viddec_ctx->num_commands = sizeof(video_cmd)/sizeof(struct video_command);

	/*
	 * Setup video for play
	 */
	ret = dvb_video_setup_play(viddec_fd, viddec_ctx);
	if (ret)
		printf("%s(): failed to setup video for live play\n", __func__);

skip_video_setup:
	if (!ret && debug_level > 1)
		printf("Video live configuration complete\n");

	return ret;

}
/**
 * setup_video_decoder() - setup video decoder for fcc
 */
static int setup_video_decoder(int debug_level, int viddec_fd,
					struct video_decoder_context *viddec_ctx)
{
	int ret;
	bool decoder_stopped;
	struct video_command video_cmd [] = {
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_CTRL_VIDEO_MEMORY_PROFILE,
		 .option.value = DVB_OPTION_CTRL_VALUE_AUTO_PROFILE,
		},
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_AV_SYNC,
		 .option.value = DVB_OPTION_VALUE_ENABLE,
		},
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_DISPLAY_FIRST_FRAME_EARLY,
		 .option.value = DVB_OPTION_VALUE_DISABLE,
		},
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_DISCARD_LATE_FRAMES,
		 .option.value = DVB_OPTION_VALUE_DISCARD_LATE_FRAMES_AFTER_SYNCHRONIZE,
		},
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_MASTER_CLOCK,
		 .option.value = DVB_OPTION_VALUE_SYSTEM_CLOCK_MASTER,
		},
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_REBASE_ON_DATA_DELIVERY_LATE,
		 .option.value = DVB_OPTION_VALUE_DISABLE,
		},
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_REBASE_ON_FRAME_DECODE_LATE,
		 .option.value = DVB_OPTION_VALUE_DISABLE,
		},
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_LIVE_PLAY,
		 .option.value = DVB_OPTION_VALUE_ENABLE,
		},
		{
		 .cmd = VIDEO_CMD_SET_OPTION,
		 .option.option = DVB_OPTION_VIDEO_START_IMMEDIATE,
		 .option.value = DVB_OPTION_VALUE_DISABLE,
		},
	};

	/*
	 * Start or stop decoder depending on the video encoding value
	 */
	ret = video_decoder_start_stop(viddec_fd, viddec_ctx->encoding, &decoder_stopped);
	if (ret || decoder_stopped)
		goto skip_video_setup;

	/*
	 * Setup video decoder context
	 */
	viddec_ctx->video_cmd = video_cmd;
	viddec_ctx->num_commands = sizeof(video_cmd)/sizeof(struct video_command);

	/*
	 * Setup video for play
	 */
	ret = dvb_video_setup_play(viddec_fd, viddec_ctx);
	if (ret)
		printf("%s(): failed to setup video for play\n", __func__);

skip_video_setup:
	if (!ret && (debug_level >= 2))
		printf("Video configuration complete\n");

	return ret;
}

/**
 * setup_dvb_decoder() - open dvb decoders and set their source
 */
static int setup_dvb_decoder()
{
	char name[32];
	int ret, id = playback_ctx.dec_ctx.dec_id;
	unsigned int audio_src = AUDIO_SOURCE_DEMUX;
	unsigned int video_src = VIDEO_SOURCE_DEMUX;

	/*
	 * Open decoders before setting up demuxer,
	 * as it's the mandatory condition for tunneled filters
	 */
	snprintf(name, sizeof(name), "/dev/dvb/adapter0/audio%d", id);
	name[sizeof(name) - 1] = '\0';
	playback_ctx.dec_ctx.auddec_fd = open(name, O_RDWR);
	if (playback_ctx.dec_ctx.auddec_fd < 0) {
		printf("%s(): failed to open %s\n", __func__, name);
		ret = errno;
		goto open_audio_failed;
	}

	snprintf(name, sizeof(name), "/dev/dvb/adapter0/video%d", id);
	name[sizeof(name) - 1] = '\0';
	playback_ctx.dec_ctx.viddec_fd = open(name, O_RDWR);
	if (playback_ctx.dec_ctx.viddec_fd < 0) {
		printf("%s(): failed to open %s\n", __func__, name);
		ret = errno;
		goto open_video_failed;
	}

	/*
	 * Set the decoder source properly before setting up demux
	 */
	if (playback_ctx.fcc) {
		audio_src = AUDIO_SOURCE_SELECTOR;
		video_src = VIDEO_SOURCE_SELECTOR;
	}

	ret = dvb_audio_set_source(playback_ctx.dec_ctx.auddec_fd, audio_src);
	if (ret) {
		printf("%s(): failed to setup audio decoder source\n", __func__);
		goto set_source_failed;
	}

	ret = dvb_video_set_source(playback_ctx.dec_ctx.viddec_fd, video_src);
	if (ret) {
		printf("%s(): failed to setup video decoder source\n", __func__);
		goto set_source_failed;
	}

	return 0;

set_source_failed:
	close(playback_ctx.dec_ctx.viddec_fd);
open_video_failed:
	close(playback_ctx.dec_ctx.auddec_fd);
open_audio_failed:
	return ret;
}

/**
 * play_setup_cached_demuxers() - setup cached demuxers
 */
int play_setup_cached_demuxers(struct playback_context *playback_ctx)
{
	int ret = 0, i, fd;
	struct demuxer_context *demux_ctx;
	struct program_context *prog_ctx = playback_ctx->cached_prog_ctx;

	/*
	 * Setup the demux for cached channels
	 */
	for (i = 0; i < MAX_CACHED_PROGRAMS; i++) {

		demux_ctx = &prog_ctx[i].demux_ctx;
		demux_ctx->video_pes_type = video_pes_type_map[playback_ctx->dec_ctx.dec_id];
		demux_ctx->audio_pes_type = audio_pes_type_map[playback_ctx->dec_ctx.dec_id];
		demux_ctx->pcr_pes_type = pcr_pes_type_map[playback_ctx->dec_ctx.dec_id];

		/*
		 * Do not modify the above sequence, this is mandatory, in case,
		 * the cached programs are not tuneable
		 */
		if (!prog_ctx[i].fe_data)
			continue;

		fd = play_get_valid_demux_fd(demux_ctx);
		if (fd < 0) {
			printf("%s(): demux not opened for any filter\n", __func__);
			ret = -ENODEV;
			break;
		}

		ret = stm_dvb_demux_set_source(fd, prog_ctx[i].fe_data->id);
		if (ret) {
			printf("%s(): failed to set demux%d source to frontend%d\n",
					__func__, demux_ctx->id, prog_ctx[i].fe_data->id);
			break;
		}

		/*
		 * Since demux filters are always open, we need to stop any filter
		 * with invalid PID
		 */
		ret = dvb_demux_stop_inactive_filters(demux_ctx);
		if (ret) {
			printf("%s(): failed to stop invalid pes filters\n", __func__);
			break;
		}

		ret = stm_dvb_demux_setup_filters(playback_ctx->debug_level,
						demux_ctx, DMX_OUT_SELECTOR);
		if (ret) {
			printf("%s(): failed to setup demuxer for cached programs\n", __func__);
			break;
		}

		ret = dvb_demux_cached(demux_ctx, demux_ctx->start_code);
		if (ret) {
			printf("%s(): failed to put fcc demuxer in cached mode\n", __func__);
			break;
		}

		ret = dvb_demux_start_filters(demux_ctx);
		if (ret){
			printf("%s(): failed to start filter on demux%d during zap\n", __func__, demux_ctx->id);
			break;
		}
	}

	return ret;
}

/**
 * setup_fcc_dvb_demux() - setup the demux for fcc
 */
static int setup_fcc_dvb_demux()
{
	int i, ret = 0;
	struct demuxer_context *demux_ctx;
	struct program_context *prog_ctx = playback_ctx.cached_prog_ctx;

	if (!playback_ctx.fcc)
		goto setup_done;

	/*
	 * Open all the demuxers for cached programs
	 */
	for (i = 0; i < MAX_CACHED_PROGRAMS; i++) {

		demux_ctx = &prog_ctx[i].demux_ctx;

		ret = stm_dvb_demux_open(demux_ctx, DMX_OUT_SELECTOR);
		if (ret) {
			printf("%s(): failed to open demuxer for cached programs\n", __func__);
			goto setup_done;
		}
	}

	/*
	 * Setup all the cached demuxers
	 */
	ret = play_setup_cached_demuxers(&playback_ctx);
	if (ret)
		util_err("Failed to setup cached demuxers\n");

setup_done:
	return ret;
}

int demux_bypass_forced(struct demuxer_context *demux_ctx, int debug_level)
{
	int ret = 0;
	struct video_decoder_context *viddec_ctx;

	viddec_ctx = &playback_ctx.dec_ctx.viddec_ctx;

	ret = dvb_demux_bypass(demux_ctx, false);
	if (ret) {
		if (debug_level > 0)
			printf("%s(): Unable to FCC. Fallback to normal ZAP\n", __func__);

		ret = setup_video_decoder_live(playback_ctx.debug_level,
				playback_ctx.dec_ctx.viddec_fd, viddec_ctx);
		if (ret && debug_level > 0) {
			printf("%s(): Failed to put video decoder to live mode\n", __func__);
			goto exit;
		}
		ret = dvb_demux_bypass(demux_ctx, true);
		if (ret) {
			printf("%s(): Failed to put demux in forced bypass\n", __func__);
			goto exit;
		}
	}

exit:
	return ret;
}

/**
 * play_setup_main_demux() - setup main demux for bypass
 */
int play_setup_main_demux(struct demuxer_context *demux_ctx,
		struct frontend_data *fe_data, __u32 filter_out, bool retuned)
{
	int ret = 0, fd;

	/*
	 * Get the valid demux fd
	 */
	fd = play_get_valid_demux_fd(demux_ctx);
	if (fd < 0) {
		printf("%s(): demux not opened for any filter\n", __func__);
		ret = ENODEV;
		goto exit;
	}

	demux_ctx->video_pes_type = video_pes_type_map[playback_ctx.dec_ctx.dec_id];
	demux_ctx->audio_pes_type = audio_pes_type_map[playback_ctx.dec_ctx.dec_id];
	demux_ctx->pcr_pes_type = pcr_pes_type_map[playback_ctx.dec_ctx.dec_id];

	/*
	 * If the current program is not tuneable, there's nothing left to do here.
	 */
	if (!fe_data)
		goto exit;

	/*
	 * If the program was already being cached, just make it pass-through
	 */
	if (!retuned) {
		ret = demux_bypass_forced(demux_ctx, playback_ctx.debug_level);
		if (ret)
			printf("%s(): failed to setup demuxer to be the new source of decoder\n", __func__);
		goto exit;
	}

	/*
	 * If the tuner has been retuned for getting this program on board, then
	 * we need to setup the stuff, and so obviously this is non-FCC zap
	 */
	ret = stm_dvb_demux_set_source(fd, fe_data->id);
	if (ret) {
		printf("%s(): failed to set demux%d source to frontend%d\n",
						__func__, demux_ctx->id, fe_data->id);
		goto exit;
	}

	/*
	 * Since demux filters are always open, we need to stop any filter
	 * with invalid PID
	 */
	ret = dvb_demux_stop_inactive_filters(demux_ctx);
	if (ret) {
		printf("%s(): failed to stop invalid pes filters\n", __func__);
		goto exit;
	}

	/*
	 * Setup the tunneled playback filters
	 */
	ret = stm_dvb_demux_setup_filters(playback_ctx.debug_level, demux_ctx, filter_out);
	if (ret) {
		printf("%s(): failed to setup demuxer for play program\n", __func__);
		goto exit;
	}

	ret = dvb_demux_cached(demux_ctx, demux_ctx->start_code);
	if (ret) {
		printf("%s(): failed to setup demuxer in cached mode\n", __func__);
		goto exit;
	}

	ret = demux_bypass_forced(demux_ctx, playback_ctx.debug_level);
	if (ret) {
		printf("%s(): failed to setup demuxer to be the new source of decoder\n", __func__);
		goto exit;
	}

	ret = dvb_demux_start_filters(demux_ctx);
	if (ret)
		printf("%s(): failed to start filter on demux%d during zap\n", __func__, demux_ctx->id);

exit:
	return ret;
}

/**
 * setup_dvb_demux() - setup demux for filtering
 */
static int setup_dvb_demux()
{
	int ret;
	__u32 filter_out = playback_ctx.fcc ? DMX_OUT_SELECTOR : DMX_OUT_DECODER;
	struct program_context *prog_ctx = &playback_ctx.play_prog_ctx;
	struct demuxer_context *demux_ctx = &prog_ctx->demux_ctx;

	/*
	 * Setup the demuxer for playback
	 */
	ret = stm_dvb_demux_open(demux_ctx, filter_out);
	if (ret) {
		printf("%s(): failed to open demuxer\n", __func__);
		goto exit;
	}

	/*
	 * Setup the main demux for playing the program
	 */
	ret = play_setup_main_demux(demux_ctx, prog_ctx->fe_data, filter_out, true);
	if (ret) {
		util_err("Failed to setup the primary demux\n");
		goto exit;
	}

	/*
	 * If it's FCC, then open up other demuxers as well
	 */
	ret = setup_fcc_dvb_demux();
	if (ret) {
		printf("%s(): failed to setup fcc dvb demux\n", __func__);
		goto exit;
	}
exit:
	return ret;
}

/**
 * play_program() - start the program
 */
int play_program(struct playback_context *playback_ctx, struct program_context *playing_ctx)
{
	int ret;
	struct demuxer_context *demux_ctx;
	struct audio_decoder_context *auddec_ctx;
	struct video_decoder_context *viddec_ctx;

	/*
	 * We want to play program and so, the context is play_prog
	 */
	demux_ctx = &playing_ctx->demux_ctx;
	auddec_ctx = &playback_ctx->dec_ctx.auddec_ctx;
	viddec_ctx = &playback_ctx->dec_ctx.viddec_ctx;

	auddec_ctx->encoding = demux_ctx->pid_info.audio_enc;
	viddec_ctx->encoding = demux_ctx->pid_info.video_enc;

	/*
	 * Setup audio decoder for normal playback
	 */
	ret = setup_audio_decoder(playback_ctx->debug_level,
				playback_ctx->dec_ctx.auddec_fd, auddec_ctx);
	if (ret) {
		printf("%s(): failed to setup audio decoder for fcc\n", __func__);
		goto exit;
	}

	/*
	 * Setup video decoder for normal playback
	 */
	ret = setup_video_decoder(playback_ctx->debug_level,
				playback_ctx->dec_ctx.viddec_fd, viddec_ctx);
	if (ret) {
		printf("%s(): failed to setup audio decoder for fcc\n", __func__);
		goto exit;
	}

	/*
	 * Start audio decoder
	 */
	ret = dvb_audio_decoder_start(playback_ctx->dec_ctx.auddec_fd);
	if (ret) {
		printf("%s(): failed to start audio decoder\n", __func__);
		goto exit;
	}

	/*
	 * Start video decoder
	 */
	ret = dvb_video_decoder_start(playback_ctx->dec_ctx.viddec_fd);
	if (ret) {
		printf("%s(): failed to start video decoder\n", __func__);
		goto exit;
	}

exit:
	return ret;
}

/**
 * setup_program_database() - build the program index of all programs
 */
static int setup_program_database(char *conf_file)
{
	FILE *fp;
	char conf_line[256];
	int i, ret = 0, prog_count = 0;
	struct program_list *prog_list;

	/*
	 * Open conf file
	 */
	fp = fopen(conf_file, "r");
	if (!fp) {
		printf("%s(): failed to open conf file: %s\n", __func__, conf_file);
		ret = errno;
		goto build_index_failed;
	}

	/*
	 * Count the number of programs possible
	 */
	while (fgets(conf_line, sizeof(conf_line), fp)) {
		if (strchr(conf_line, '#') || (*conf_line == '\n'))
			continue;
		prog_count++;
	}

	/*
	 * Build program info
	 */
	playback_ctx.prog_count = prog_count;
	playback_ctx.prog_list = malloc(sizeof(*prog_list) * prog_count);
	if (!playback_ctx.prog_list) {
		printf("%s(): alloc failed for program list\n", __func__);
		ret = -ENOMEM;
		goto alloc_index_failed;
	}

	i = 0;
	prog_list = playback_ctx.prog_list;
	fseek(fp, 0, SEEK_SET);
	while (fgets(conf_line, sizeof(conf_line), fp)) {

		int size;
		char *name_start, *name_end;

		if (strchr(conf_line, '#') || (*conf_line == '\n'))
			continue;

		prog_list[i].fpos = ftell(fp) - strnlen(conf_line, sizeof(conf_line) - 1);
		name_start = strchr(conf_line, ':');
		name_end = strchr(name_start + 1, ':');
		*name_end = '\0';
		size = sizeof(prog_list[i].name);
		strncpy(prog_list[i].name, name_start + 1, size);
		prog_list[i].name[size - 1] = '\0';
		i++;
	}

	if (playback_ctx.debug_level >= 2) {
		printf("Total program count: %d\n", prog_count);
		printf("S.No\t\t Program Name\n");
		for (i = 0; i < prog_count; i++) {
			fseek(fp, playback_ctx.prog_list[i].fpos, SEEK_SET);
			fgets(conf_line, sizeof(conf_line), fp);
			printf("%d\t\t %s\n", i + 1, playback_ctx.prog_list[i].name);
		}
	}

alloc_index_failed:
	fclose(fp);
build_index_failed:
	return ret;
}

/**
 * play_setup_cached_program_ctx() - setup the program context for cached channels
 */
int play_setup_cached_program_ctx(struct playback_context *playback_ctx, char *play_name)
{
	int i, j, play_idx, cached_idx, prog_cnt;

	/*
	 * Find out the index of the program which is being played
	 */
	for (play_idx = 0; play_idx < playback_ctx->prog_count; play_idx++) {
		if (!strncmp(playback_ctx->prog_list[play_idx].name,
						play_name, strnlen(play_name,
						sizeof(playback_ctx->prog_list[play_idx].name) - 1)))
			break;
	}

	/*
	 * Find out the programs to be cached. The logic is based on the fact
	 * that each broadcaster is transmitting a bouquet of channels, which is
	 * actually the case too.
	 */
	prog_cnt = playback_ctx->prog_count - 1;
	if ((play_idx + MAX_CACHED_PROGRAMS/2) > prog_cnt) {

		short int overflow_idx = 0;;

		cached_idx = play_idx;
		for (i = 0; i < (MAX_CACHED_PROGRAMS / 2); i++)
			setup_program_ctx(&playback_ctx->cached_prog_ctx[i],
						playback_ctx->prog_list[--cached_idx].name);

		cached_idx = play_idx;
		for (j = i; j < i + (MAX_CACHED_PROGRAMS / 2); j++) {

			if (++cached_idx <= prog_cnt)
				setup_program_ctx(&playback_ctx->cached_prog_ctx[j],
						playback_ctx->prog_list[cached_idx].name);
			else
				setup_program_ctx(&playback_ctx->cached_prog_ctx[j],
						playback_ctx->prog_list[overflow_idx++].name);
		}

	} else if ((play_idx - (MAX_CACHED_PROGRAMS / 2)) < 0) {

		short int underflow_idx = prog_cnt;

		cached_idx = play_idx;
		for (i = 0; i < (MAX_CACHED_PROGRAMS / 2); i++) {

			if (--cached_idx >= 0)
				setup_program_ctx(&playback_ctx->cached_prog_ctx[i],
						playback_ctx->prog_list[cached_idx].name);
			else
				setup_program_ctx(&playback_ctx->cached_prog_ctx[i],
						playback_ctx->prog_list[underflow_idx--].name);
		}

		cached_idx = play_idx;
		for (j = i; j <  i + (MAX_CACHED_PROGRAMS / 2); j++)
			setup_program_ctx(&playback_ctx->cached_prog_ctx[j],
						 playback_ctx->prog_list[++cached_idx].name);

	} else {

		cached_idx = play_idx;
		for (i = 0; i < (MAX_CACHED_PROGRAMS / 2); i++)
			setup_program_ctx(&playback_ctx->cached_prog_ctx[i],
						playback_ctx->prog_list[--cached_idx].name);

		cached_idx = play_idx;
		for (j = i; j <  i + (MAX_CACHED_PROGRAMS / 2); j++)
			setup_program_ctx(&playback_ctx->cached_prog_ctx[j],
						 playback_ctx->prog_list[++cached_idx].name);
	}

	printf("------------------------\n");
	printf("Caching programs:\n");
	printf("------------------------\n");
	for (i = 0; i < MAX_CACHED_PROGRAMS; i++)
		printf("%s\n", playback_ctx->cached_prog_ctx[i].name);
	printf("------------------------\n");

	return 0;
}

/**
 * get_optarg_int_value() - convert optarg in integer value
 */
static int get_optarg_int_value(char *optarg, int *value)
{
	int val, ret = 0;

	if (!isdigit(*optarg)) {
		ret = -EINVAL;
		goto get_val_failed;
	}

	val = strtol(optarg, NULL, 10);
	if ((errno == ERANGE && (val == LONG_MAX ||
			val == LONG_MIN)) || (errno != 0 && val == 0)) {
		ret = errno;
		goto get_val_failed;
	}

	*value = val;

get_val_failed:
	return ret;
}

/**
 * count_args() - count the number of arguments
 */
static int count_args(char *fe_str)
{
	char *ptr;
	int ret, count;

	ret = get_optarg_int_value(fe_str, &count);
	if (ret) {
		printf("%s(): empty argument list\n", __func__);
		count = -1;
		goto exit;
	}
	count = 1;

	/*
	 * Find out the number of channels and fill up the contexts
	 */
	ptr = fe_str;
	for (; ; count++) {

		ptr = strchr(ptr, ',');
		if (!ptr)
			break;
		ptr += 1;
		if (!strlen(ptr))
			break;
	}

exit:
	return count;
}

/**
 * setup_program_context() - setup the program context's at startup
 */
static int setup_program_context()
{
	int i, ret;

	/*
	 * Setup the program which is to be played
	 */
	playback_ctx.play_prog_ctx.demux_ctx.id = -1;
	ret = setup_program_ctx(&playback_ctx.play_prog_ctx, main_prog);
	if (ret) {
		printf("%s(): failed to setup the program information\n", __func__);
		goto exit;
	}

	/*
	 * If it's fcc, then setup the cached program context.
	 * The logic is to setup the equal number of programs
	 * before and after the played program as per the list
	 */
	if (playback_ctx.fcc) {
		for (i = 0; i < MAX_CACHED_PROGRAMS; i++)
			playback_ctx.cached_prog_ctx[i].demux_ctx.id = -1;

		ret = play_setup_cached_program_ctx(&playback_ctx, main_prog);
		if (ret) {
			printf("%s(): failed to setup cached program context\n", __func__);
			goto exit;
		}
	}

exit:
	return ret;
}

/**
 * fill_fe_info() - fill in the frontend info based on command line
 */
static int fill_fe_info(struct frontend_data *fe_data, char *str)
{
	char name[48];
	int val, ret = 0;

	/*
	 * Get the frontend id
	 */
	val = strtol(str, NULL, 10);
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
			|| (errno != 0 && val == 0)) {
		ret = errno;
		goto filled;
	}
	fe_data->id = val;

	/*
	 * Try to open these frontends
	 */
	snprintf(name, sizeof(name), "/dev/dvb/adapter0/frontend%d", val);
	fe_data->fd = open(name, O_WRONLY);
	if (fe_data->fd < 0) {
		printf("%s(): failed to open frontend at index %d\n", __func__, val);
		ret = errno;
		goto filled;
	}

filled:
	return ret;
}

/**
 * setup_frontend_context() - setup the frontends at startup
 */
static int setup_frontend_context()
{
	int i, ret = 0;
	char *temp = usr_fe, *ptr;

	/*
	 * By default, frontend count is taken to be 1, in case user doesn't
	 * want to pass any frontend arguments. It is assumed that instance 0
	 * is atleast connected to feed and is tuneable
	 */
	playback_ctx.fe_data = calloc(1, sizeof(*playback_ctx.fe_data) * fe_count);
	if (!playback_ctx.fe_data) {
		printf("%s(): out of memory for frontend info\n", __func__);
		ret = -ENOMEM;
		goto alloc_failed;
	}
	playback_ctx.fe_count = fe_count;

	for (i = 0; i < fe_count && strlen(usr_fe); i++) {

		ptr = strchr(temp, ',');
		if (!ptr) {
			ret = fill_fe_info(&playback_ctx.fe_data[i], temp);
			if (ret) {
				printf("%s()failed to setup frontend\n", __func__);
				goto fill_info_failed;
			}
			break;
		}

		*ptr = '\0';
		ret = fill_fe_info(&playback_ctx.fe_data[i], temp);
		if (ret) {
			printf("%s()failed to setup frontend\n", __func__);
			goto fill_info_failed;
		}
		temp = ptr + 1;
	}

	/*
	 * If user didn't specified any frontend fill in the with 0th index
	 */
	if (!strlen(usr_fe)) {
		ret = fill_fe_info(&playback_ctx.fe_data[0], "0");
		if (ret) {
			printf("%s()failed to setup frontend 0\n", __func__);
			goto fill_info_failed;
		}
	}

	return 0;

fill_info_failed:
	free(playback_ctx.fe_data);
alloc_failed:
	return ret;
}

/**
 * play_sighandler() - signal handler to manage clean exit
 */
static void play_sighandler(int signum, siginfo_t *siginfo, void *data)
{
	int i;

	switch (signum) {
	case SIGINT:
		/*
		 * Closing the decoder first is mandatory for a clean exit
		 */
		pthread_cancel(playback_ctx.zaptime_thread);
		close(playback_ctx.dec_ctx.auddec_fd);
		close(playback_ctx.dec_ctx.viddec_fd);
		close(playback_ctx.play_prog_ctx.demux_ctx.video_pid_fd);
		close(playback_ctx.play_prog_ctx.demux_ctx.audio_pid_fd);
		close(playback_ctx.play_prog_ctx.demux_ctx.pcr_pid_fd);
		close(playback_ctx.play_prog_ctx.demux_ctx.subt_pid_fd);
		close(playback_ctx.play_prog_ctx.demux_ctx.ttx_pid_fd);

		for (i = 0; i < MAX_CACHED_PROGRAMS; i++) {
			if (!playback_ctx.cached_prog_ctx)
				break;
			close(playback_ctx.cached_prog_ctx[i].demux_ctx.video_pid_fd);
			close(playback_ctx.cached_prog_ctx[i].demux_ctx.audio_pid_fd);
			close(playback_ctx.cached_prog_ctx[i].demux_ctx.pcr_pid_fd);
			close(playback_ctx.cached_prog_ctx[i].demux_ctx.subt_pid_fd);
			close(playback_ctx.cached_prog_ctx[i].demux_ctx.ttx_pid_fd);
		}

		pthread_cancel(keyboard_thread);
		printf("\n");
		break;

	default:
		printf("Unhandled signal number: %d\n", signum);
	}
}

/**
 * usage() - help to tbe displayed
 */
static void usage()
{
	printf("dvb-play version-1.0\n");
	printf("Last modified (23 Nov 2014)\n");
	printf("Compile info:- DATE(%s), TIME(%s)\n", __DATE__, __TIME__);
	printf("Usage: dvb-play [OPTIONS]\n");
	printf("Start playback from the desired program and optionally configure for fast channel change\n");
	printf("\n");
	printf("  --prog=<name>\n");
	printf("     <name> specifies program to be played. **Mandatory**\n");
	printf("     NOTE: non-fcc zapping is currently not supported\n\n");
	printf("  --decoder=<id>\n");
	printf("     Specify the decoder <id> to be used, common for both audio/video **Optional**\n");
	printf("     A single decoder is always used for zapping. By default, application tries to use id=0\n\n");
	printf("  --front=<0,1,2>\n");
	printf("     A comma separated list of frontends which are available for tuning.\n");
	printf("     NOTE: non-fcc zapping is currently not supported\n\n");
	printf("  --fcc=<val>\n");
	printf("    <val>=0 or 1, Configure for fcc or non fcc playback,\n\n");
	printf("  --debug_level\n");
	printf("    Starts printing debug_level information about the state of the application.\n");
	printf("    It currently supports level=1,2,3 Level=1 prints basic info, level=2 prints a bit more and so on\n\n");
	printf("  -h, --help\n");
	printf("    Prints this information\n\n");
	printf(" Command to use: dvb-play --fcc=1 --prog=<name> --front=0,1\n\n");
	printf(" Command to use: dvb-play --debug=1 --fcc=1 --prog=<name> --front=0,1\n\n");
}

/*
 * main() - main entry point
 */
int main(int argc, char *argv[])
{
	bool retuned;
	int val, ret, option;
	struct sigaction play_sigaction;

	/*
	 * Construct the arguments to be parsed by this application
	 */
	enum {
		DEBUG_INFO,
		FCC,
		PROGRAM_NAME,
		FRONTEND_ID,
		DECODER_ID,
	};

	struct option longopt[] = {
		{"debug", 1, NULL, DEBUG_INFO},
		{"fcc", 1, NULL, FCC},
		{"prog", 1, NULL, PROGRAM_NAME},
		{"front", 1, NULL, FRONTEND_ID},
		{"decoder", 1, NULL, DECODER_ID},
		{"help", 0, NULL, 'h'},
		{0, 0, 0, 0}
	};

	if (argc == 1) {
		usage();
		goto exit;
	}

	memset(&play_sigaction, 0, sizeof(play_sigaction));
	play_sigaction.sa_sigaction = play_sighandler;
	play_sigaction.sa_flags = SA_SIGINFO;
	sigaction(SIGINT, &play_sigaction, NULL);

	/*
	 * Parse the arguments
	 */
	while ((option = getopt_long(argc, argv, "h", longopt, NULL)) != -1) {

		switch(option) {
		case DEBUG_INFO:

			ret = get_optarg_int_value(optarg, &val);
			if (ret) {
				printf("Invalid value for --debug\n");
				goto exit;
			}

			if ((val < 0) || (val > 3)) {
				printf("Debug level supported from 0 - 3 inclusive\n");
				goto exit;
			}

			playback_ctx.debug_level = val;

			break;

		case FCC:

			ret = get_optarg_int_value(optarg, &val);
			if (ret) {
				printf("Invalid value for --fcc\n");
				goto exit;
			}

			if ((val != 0) && (val != 1)) {
				printf("Either it's fcc or not, so 0 or 1\n");
				goto exit;
			}

			playback_ctx.fcc = val;

			break;

		case PROGRAM_NAME:

			strncpy(main_prog, optarg, PROG_NAME_LEN);
			main_prog[PROG_NAME_LEN - 1] = '\0';
			break;

		case FRONTEND_ID:
			/*
			 * Count the number of frontend arguments
			 */
			fe_count = count_args(optarg);
			if (fe_count < 0) {
				printf("Invalid frontend number passed\n");
				usage();
				goto exit;
			}
			strncpy(usr_fe, optarg, sizeof(usr_fe));
			usr_fe[sizeof(usr_fe) - 1] = '\0';

			break;

		case DECODER_ID:

			ret = get_optarg_int_value(optarg, &val);
			if (ret) {
				printf("Invalid value for --decoder\n");
				goto exit;
			}
			playback_ctx.dec_ctx.dec_id = val;
			break;

		case 'h':
		default:
			usage();
			goto exit;
		}
	}

	/*
	 * Validate the parameters
	 */
	if (!strlen(main_prog)) {
		printf("No program name specified to be played\n");
		goto exit;
	}

	/*
	 * Build program indexing for program zap.
	 */
	ret = setup_program_database("stlinuxtv-dvb-channels.conf");
	if (ret) {
		printf("Failed to build program indexes\n");
		goto exit;
	}

	/*
	 * Setup program context
	 */
	ret = setup_program_context();
	if (ret) {
		printf("Failed to setup program context\n");
		goto exit;
	}

	/*
	 * Setup frontend context
	 */
	ret = setup_frontend_context();
	if (ret) {
		printf("Failed to get the frontend ready for tuning\n");
		goto exit;
	}

	/*
	 * Tune the frontend for playing the requested program
	 */
	play_setup_frontend(&playback_ctx.play_prog_ctx, &retuned);

	/*
	 * Tune the frontend for cached programs
	 */
	ret = play_setup_cached_frontend(&playback_ctx);
	if (ret) {
		printf("%s(): Failed to setup the cached programs\n", __func__);
		goto exit;
	}

	/*
	 * Set up decoder
	 */
	ret = setup_dvb_decoder();
	if (ret) {
		printf("%s(): failed to open dvb decoders\n", __func__);
		goto exit;
	}

	/*
	 * Setup demuxer per program
	 */
	ret = setup_dvb_demux();
	if (ret) {
		printf("%s(): failed to open demuxer\n", __func__);
		goto exit;
	}

	/*
	 * Start the program specified
	 */
	ret = play_program(&playback_ctx, &playback_ctx.play_prog_ctx);
	if (ret) {
		printf("%s(): failed to play program\n", __func__);
		goto exit;
	}

	/*
	 * Create the keyboard thread to process the runtime options
	 */
	ret = pthread_create(&keyboard_thread, NULL, keyboard_monitor, &playback_ctx);
	if (ret) {
		printf("%s(): failed to create keyboard thread\n", __func__);
		goto exit;
	}

	pthread_join(keyboard_thread, NULL);

exit:
	return ret;
}
