/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Reading events from dvb audio/video decoders
************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <poll.h>
#include <signal.h>
#include <mediactl/v4l2subdev.h>
#include <linux/v4l2-subdev.h>

#include "linux/stm_v4l2_export.h"
#include "linux/stm_v4l2_decoder_export.h"
#include "stm_v4l2_helper.h"

struct dvbdec_context {
	char vid_name[32], aud_name[32];

	struct media_device *media;
	struct media_entity *viddec_entity, *auddec_entity;
} dvbdec_ctx;

static pthread_t audio_event_thread, video_event_thread;

static void *video_event_monitor(void *arg)
{
	int ret = 0;
	struct v4l2_event event;
	struct v4l2_sti_video_size *size;
	struct pollfd viddec_pollfd;
	struct v4l2_event_subscription subs;

	/*
	 * Open video decoder subdev
	 */
	ret = stm_v4l2_subdev_open(dvbdec_ctx.media, dvbdec_ctx.vid_name,
					&dvbdec_ctx.viddec_entity);
	if (ret) {
		printf("%s(): failed to open %s\n", __func__, dvbdec_ctx.vid_name);
		goto viddec_open_failed;
	}

	/*
	 * Subscribe to the requisite events
	 */
	memset(&subs, 0, sizeof(subs));
	subs.type = V4L2_EVENT_STI_PARAMETERS_CHANGED;
	subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
	ret = ioctl(dvbdec_ctx.viddec_entity->fd,
				VIDIOC_SUBSCRIBE_EVENT, &subs);
	if (ret) {
		printf("%s(): failed to subscribe to size change event\n", __func__);
		goto subs_failed;
	}

	memset(&subs, 0, sizeof(subs));
	subs.type = V4L2_EVENT_STI_FRAME_RATE_CHANGED;
	subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
	ret = ioctl(dvbdec_ctx.viddec_entity->fd,
				VIDIOC_SUBSCRIBE_EVENT, &subs);
	if (ret) {
		printf("%s(): failed to subscribe to framerate change event\n", __func__);
		goto subs_failed;
	}

	memset(&subs, 0, sizeof(subs));
	subs.type = V4L2_EVENT_VSYNC;
	ret = ioctl(dvbdec_ctx.viddec_entity->fd,
				VIDIOC_SUBSCRIBE_EVENT, &subs);
	if (ret) {
		printf("%s(): failed to subscribe to field change event\n", __func__);
		goto subs_failed;
	}

	memset(&subs, 0, sizeof(subs));
	subs.type = V4L2_EVENT_STI_PIXEL_ASPECT_RATIO_CHANGED;
	ret = ioctl(dvbdec_ctx.viddec_entity->fd,
				VIDIOC_SUBSCRIBE_EVENT, &subs);
	if (ret) {
		printf("%s(): failed to subscribe to pixel aspect ratio change event\n", __func__);
		goto subs_failed;
	}

	/*
	 * Start reading events
	 */
	while (1) {

		memset(&viddec_pollfd, 0, sizeof(viddec_pollfd));
		viddec_pollfd.fd = dvbdec_ctx.viddec_entity->fd;
		viddec_pollfd.events = POLLIN | POLLPRI;

		if (poll(&viddec_pollfd, 1, 100)) {

			if (viddec_pollfd.revents & POLLERR) {
				goto subs_failed;
			}
		} else
			continue;

		memset(&event, 0, sizeof(event));
		ret = ioctl(dvbdec_ctx.viddec_entity->fd, VIDIOC_DQEVENT, &event);
		if (ret) {
			printf("%s(): failed to dq video decoder event\n", __func__);
			goto subs_failed;
		}

		if (event.type == V4L2_EVENT_STI_PARAMETERS_CHANGED) {
			size = (struct v4l2_sti_video_size *)event.u.data;
			printf("Event: V4L2_EVENT_STI_SIZE_CHANGED\n");
			printf("Width      : %d\n", size->width);
			printf("Height     : %d\n", size->height);
			printf("AspectRatio: %s\n", (size->aspect_ratio == 0) ? "4:3" :
						((size->aspect_ratio == 1) ? "16:9" : "221:1"));
			continue;
		}

		if (event.type == V4L2_EVENT_STI_FRAME_RATE_CHANGED) {
			printf("Event: V4L2_EVENT_STI_FRAME_RATE_CHANGED\n");
			printf("Framerate %d\n", *(int *)event.u.data);
			continue;
		}

		if (event.type == V4L2_EVENT_VSYNC) {
			printf("Event: V4L2_EVENT_VSYNC\n");
			printf("Field type: %d\n", event.u.vsync.field);
			continue;
		}

		if (event.type == V4L2_EVENT_STI_PIXEL_ASPECT_RATIO_CHANGED) {
			struct v4l2_sti_ratio *ratio = (struct v4l2_sti_ratio *)&event.u.data;
			printf("Event: V4L2_EVENT_STI_PIXEL_ASPECT_RATIO_CHANGED\n");
			printf("Pixel Aspect Ratio: Numerator: %d, Denominator: %d\n",
							ratio->numerator, ratio->denominator);
		}
	}


subs_failed:
	stm_v4l2_subdev_close(dvbdec_ctx.viddec_entity);
	dvbdec_ctx.viddec_entity = NULL;
viddec_open_failed:
	return NULL;
}

static void *audio_event_monitor(void *arg)
{
	int ret = 0;
	struct v4l2_event event;
	struct v4l2_sti_audio_params *audio_params;
	struct pollfd auddec_pollfd;
	struct v4l2_event_subscription subs;

	/*
	 * Open audio decoder subdev
	 */
	ret = stm_v4l2_subdev_open(dvbdec_ctx.media, dvbdec_ctx.aud_name,
					&dvbdec_ctx.auddec_entity);
	if (ret) {
		printf("%s(): failed to open %s\n", __func__, dvbdec_ctx.aud_name);
		goto viddec_open_failed;
	}

	/*
	 * Subscribe to the requisite events
	 */
	memset(&subs, 0, sizeof(subs));
	subs.type = V4L2_EVENT_STI_PARAMETERS_CHANGED;
	subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
	ret = ioctl(dvbdec_ctx.auddec_entity->fd,
				VIDIOC_SUBSCRIBE_EVENT, &subs);
	if (ret) {
		printf("%s(): failed to subscribe to size change event\n", __func__);
		goto subs_failed;
	}

	/*
	 * Start reading events
	 */
	while (1) {

		memset(&auddec_pollfd, 0, sizeof(auddec_pollfd));
		auddec_pollfd.fd = dvbdec_ctx.auddec_entity->fd;
		auddec_pollfd.events = POLLIN | POLLPRI;

		if (poll(&auddec_pollfd, 1, 100)) {

			if (auddec_pollfd.revents & POLLERR) {
				goto subs_failed;
			}
		} else
			continue;

		memset(&event, 0, sizeof(event));
		ret = ioctl(dvbdec_ctx.auddec_entity->fd, VIDIOC_DQEVENT, &event);
		if (ret) {
			printf("%s(): failed to dq video decoder event\n", __func__);
			goto subs_failed;
		}

		if (event.type == V4L2_EVENT_STI_PARAMETERS_CHANGED) {
			audio_params = (struct v4l2_sti_audio_params *)event.u.data;
			char *placement = (char *)&audio_params->channel_placement;
			printf("Event: V4L2_EVENT_STI_PARAMETERS_CHANGED\n");
			printf("Encoding                  : %d\n", audio_params->encoding);
			printf("Channel Placment.pair0    : 0x%x\n", placement[0] & 0xFC);
			printf("Channel Placment.pair1    : 0x%x\n", (placement[0] & 0x03) | (placement[1] & 0xF0) );
			printf("Channel Placment.pair2    : 0x%x\n", (placement[1] & 0x0F) | (placement[2] & 0xC0));
			printf("Channel Placment.pair3    : 0x%x\n", placement[2] & 0x3F);
			printf("Channel Placment.pair4    : 0x%x\n", placement[3] & 0xFC);
			printf("Channel Placment.reserved : %d\n", placement[3] & 0x02);
			printf("Channel Placment.malleable: %d\n", placement[3] & 0x01);
			printf("Bitrate                   : %d\n", audio_params->bitrate);
			printf("Sampling Frequency        : %d\n", audio_params->sampling_freq);
			printf("Channels                  : %d\n", audio_params->channels);
			printf("Dual Mono                 : %d\n", audio_params->dual_mono);
		}
	}

subs_failed:
	stm_v4l2_subdev_close(dvbdec_ctx.auddec_entity);
	dvbdec_ctx.auddec_entity = NULL;
viddec_open_failed:
	return NULL;
}

/**
 * dvbdec_sighandler() - signal handler to manage clean exit
 */
static void dvbdec_sighandler(int signum, siginfo_t *siginfo, void *data)
{
	switch (signum) {
	case SIGINT:
		if (audio_event_thread)
			pthread_cancel(audio_event_thread);
		if (video_event_thread)
			pthread_cancel(video_event_thread);
		printf("\n");
		break;

	default:
		printf("Unhandled signal number: %d\n", signum);
	}
}
/**
 * usage() - help to be displayed
 */
static void usage()
{
	printf("dvb-decoder version-1.1\n");
	printf("Last modified (7th April 2015)\n");
	printf("Compile info:- DATE(%s), TIME(%s)\n", __DATE__, __TIME__);
	printf("Usage: dvb-decoder [OPTIONS]\n");
	printf("Read the dvb events from the audio/video dvb decoders\n");
	printf("\n");
	printf("  -d, --device\n");
	printf("     Which audio and video device to use for fetching events\n\n");
	printf("  --audio-events\n");
	printf("     DVB audio decoder events will be read, if the playback is already started\n\n");
	printf("  --video-events\n");
	printf("     DVB video decoder events will be read, if the playback is already started\n\n");
}

/*
 * main() - main entry point
 */
int main(int argc, char *argv[])
{
	int ret = -EINVAL, option, device_id;
	struct sigaction dvbdec_sigaction;

	/*
	 * Construct the arguments to be parsed by this application
	 */
	enum {
		AUDIO_EVENT,
		VIDEO_EVENT,
	};

	struct option longopt[] = {
		{"device", 1, NULL, 'd'},
		{"audio-events", 0, NULL, AUDIO_EVENT},
		{"video-events", 0, NULL, VIDEO_EVENT},
		{0, 0, 0, 0}
	};

	memset(&dvbdec_sigaction, 0, sizeof(dvbdec_sigaction));
	dvbdec_sigaction.sa_sigaction = dvbdec_sighandler;
	dvbdec_sigaction.sa_flags = SA_SIGINFO;
	sigaction(SIGINT, &dvbdec_sigaction, NULL);

	if (argc <=1) {
		usage();
		goto exit;
	}

	/*
	 * Opens the media device
	 */
	ret = stm_v4l2_media_open(0, &dvbdec_ctx.media);
	if (ret) {
		printf("%s(): failed to open media device\n", __func__);
		goto exit;
	}

	/*
	 * Parse the arguments
	 */
	while ((option = getopt_long(argc, argv, "d:", longopt, NULL)) != -1) {

		switch(option) {
		case 'd':
			device_id = atoi(optarg);
			snprintf(dvbdec_ctx.aud_name, sizeof(dvbdec_ctx.aud_name) - 1, "dvb0.audio%d", device_id);
			snprintf(dvbdec_ctx.vid_name, sizeof(dvbdec_ctx.vid_name) - 1, "dvb0.video%d", device_id);
			break;

		case AUDIO_EVENT:
			if (!strlen(dvbdec_ctx.aud_name)) {
				usage();
				goto media_exit;
			}

			ret = pthread_create(&audio_event_thread, NULL, audio_event_monitor, NULL);
			if (ret) {
				printf("%s(): failed to create audio event monitor thread\n", __func__);
				goto media_exit;
			}
			break;

		case VIDEO_EVENT:
			if (!strlen(dvbdec_ctx.vid_name)) {
				usage();
				goto media_exit;
			}

			ret = pthread_create(&video_event_thread, NULL, video_event_monitor, NULL);
			if (ret) {
				printf("%s(): failed to create video event monitor thread\n", __func__);
				goto media_exit;
			}
			break;

		default:
			usage();
			goto media_exit;
		}
	}

	if (audio_event_thread)
		pthread_join(audio_event_thread, NULL);
	if (video_event_thread)
		pthread_join(video_event_thread, NULL);

media_exit:
	stm_v4l2_media_close(dvbdec_ctx.media);
exit:
	return ret;
}
