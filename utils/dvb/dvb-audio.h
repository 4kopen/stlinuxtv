/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * ldvb audio decoder definitions
************************************************************************/
#ifndef __DVB_AUDIO_H__
#define __DVB_AUDIO_H__

struct audio_decoder_context {
	__u32 encoding;
	__u32 num_commands;
	struct audio_command *audio_cmd;
};

int dvb_audio_set_source(int fd, __u32 source);
int dvb_audio_setup_play(int fd, struct audio_decoder_context *auddec_ctx);
int dvb_audio_decoder_start(int fd);
int dvb_audio_decoder_stop(int fd);
int stm_dvb_audio_set_speed(int fd, int speed);

#endif
