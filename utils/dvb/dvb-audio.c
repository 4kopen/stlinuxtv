/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * ldvb audio start and helpers
************************************************************************/
#include <stdio.h>
#include <linux/types.h>
#include <sys/ioctl.h>
#include "dvb-audio.h"
#include "linux/dvb/audio.h"
#include "linux/dvb/stm_audio.h"

/**
 * dvb_audio_set_source() - set audio decoder source
 * @fd: audio decoder fd (opened in WR mode)
 * @source: audio_stream_source_t/audio_stream_source_sti
 */
int dvb_audio_set_source(int fd, __u32 source)
{
	int ret;

	/*
	 * Setup audio source
	 */
	ret = ioctl(fd, AUDIO_SELECT_SOURCE, (void *)source);
	if (ret)
		printf("%s(): failed to source to %d\n", __func__, source);


	return ret;
}

/**
 * dvb_audio_setup_play() - setup decoder for play
 */
int dvb_audio_setup_play(int fd, struct audio_decoder_context *auddec_ctx)
{
	int i, ret;

	/*
	 * Set audio encoding
	 */
	ret = ioctl(fd, AUDIO_SET_ENCODING, (void *)auddec_ctx->encoding);
	if (ret) {
		printf("%s(): failed to set audio encoding\n", __func__);
		goto set_src_failed;
	}

	/*
	 * Set all the policies
	 */
	for (i = 0; i < auddec_ctx->num_commands; i++) {
		ret = ioctl(fd, AUDIO_COMMAND, (void *)&auddec_ctx->audio_cmd[i]);
		if (ret) {
			printf("%s(): failed to set command at index %d\n", __func__, i);
			goto set_src_failed;
		}
	}

set_src_failed:
	return ret;
}

/**
 * dvb_audio_decoder_start() - start audio decoder
 */
int dvb_audio_decoder_start(int fd)
{
	int ret;

	ret = ioctl(fd, AUDIO_PLAY, NULL);
	if (ret)
		printf("%s(): failed to start audio decoder\n", __func__);

	return ret;
}

/**
 * dvb_audio_decoder_stop() - stop audio decoder
 */
int dvb_audio_decoder_stop(int fd)
{
	int ret;

	ret = ioctl(fd, AUDIO_STOP, NULL);
	if (ret)
		printf("%s(): failed to stop audio decoder\n", __func__);

	return ret;
}

/*
 * stm_dvb_audio_set_speed() - set speed of playback
 */
int stm_dvb_audio_set_speed(int fd, int speed)
{
	int ret;

	ret = ioctl(fd, AUDIO_SET_SPEED, speed);
	if (ret)
		printf("%s(): failed to set playback speed\n", __func__);

	return ret;
}
