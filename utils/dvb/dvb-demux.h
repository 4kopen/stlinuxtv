/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Definitions for demuxer
************************************************************************/
#ifndef __DVB_DEMUX_H__
#define __DVB_DEMUX_H__

#include <linux/dvb/dmx.h>
#include <linux/dvb/stm_dmx.h>
#include <stdbool.h>

static const int video_pes_type_map[] = {
	DMX_PES_VIDEO0 , DMX_PES_VIDEO1 , DMX_PES_VIDEO2 , DMX_PES_VIDEO3,
	DMX_PES_VIDEO4 , DMX_PES_VIDEO5 , DMX_PES_VIDEO6 , DMX_PES_VIDEO7,
	DMX_PES_VIDEO8 , DMX_PES_VIDEO9 , DMX_PES_VIDEO10, DMX_PES_VIDEO11,
	DMX_PES_VIDEO12, DMX_PES_VIDEO13, DMX_PES_VIDEO14, DMX_PES_VIDEO15,
	DMX_PES_VIDEO16, DMX_PES_VIDEO17, DMX_PES_VIDEO18, DMX_PES_VIDEO19,
	DMX_PES_VIDEO20, DMX_PES_VIDEO21, DMX_PES_VIDEO22, DMX_PES_VIDEO23,
	DMX_PES_VIDEO24, DMX_PES_VIDEO25, DMX_PES_VIDEO26, DMX_PES_VIDEO27,
	DMX_PES_VIDEO28, DMX_PES_VIDEO29, DMX_PES_VIDEO30, DMX_PES_VIDEO31,
};

static const int audio_pes_type_map[] = {
	DMX_PES_AUDIO0 , DMX_PES_AUDIO1 , DMX_PES_AUDIO2 , DMX_PES_AUDIO3,
	DMX_PES_AUDIO4 , DMX_PES_AUDIO5 , DMX_PES_AUDIO6 , DMX_PES_AUDIO7,
	DMX_PES_AUDIO8 , DMX_PES_AUDIO9 , DMX_PES_AUDIO10, DMX_PES_AUDIO11,
	DMX_PES_AUDIO12, DMX_PES_AUDIO13, DMX_PES_AUDIO14, DMX_PES_AUDIO15,
	DMX_PES_AUDIO16, DMX_PES_AUDIO17, DMX_PES_AUDIO18, DMX_PES_AUDIO19,
	DMX_PES_AUDIO20, DMX_PES_AUDIO21, DMX_PES_AUDIO22, DMX_PES_AUDIO23,
	DMX_PES_AUDIO24, DMX_PES_AUDIO25, DMX_PES_AUDIO26, DMX_PES_AUDIO27,
	DMX_PES_AUDIO28, DMX_PES_AUDIO29, DMX_PES_AUDIO30, DMX_PES_AUDIO31,
};

static const int pcr_pes_type_map[] = {
	DMX_PES_PCR0 , DMX_PES_PCR1 , DMX_PES_PCR2 , DMX_PES_PCR3,
	DMX_PES_PCR4 , DMX_PES_PCR5 , DMX_PES_PCR6 , DMX_PES_PCR7,
	DMX_PES_PCR8 , DMX_PES_PCR9 , DMX_PES_PCR10, DMX_PES_PCR11,
	DMX_PES_PCR12, DMX_PES_PCR13, DMX_PES_PCR14, DMX_PES_PCR15,
	DMX_PES_PCR16, DMX_PES_PCR17, DMX_PES_PCR18, DMX_PES_PCR19,
	DMX_PES_PCR20, DMX_PES_PCR21, DMX_PES_PCR22, DMX_PES_PCR23,
	DMX_PES_PCR24, DMX_PES_PCR25, DMX_PES_PCR26, DMX_PES_PCR27,
	DMX_PES_PCR28, DMX_PES_PCR29, DMX_PES_PCR30, DMX_PES_PCR31,
};

/*
 * This structure gets the pid information from conf file
 * @video_pid: video pid to be set on demux for filtering
 * @audio_pid: audio pid to be set on demux for filtering
 * @pcr_pid  : pcr pid to be set on demux for filtering
 * @subt_pid : subt pid to be set on demux for filtering (no support for the moment)
 * @ttx_pid  : ttx pid to be set on demux for filtering (no support for the moment)
 * @video_enc: video encoding to be set on decoder for decoding
 * @audio_enc: audio encoding to be set on decoder for decoding
 */
struct pid_info {
	int video_pid, audio_pid;
	int pcr_pid, subt_pid, ttx_pid;
	unsigned int video_enc, audio_enc;
};

struct demuxer_context {
	int id;
	int video_pid_fd, video_pes_type;
	int audio_pid_fd, audio_pes_type;
	int pcr_pid_fd, pcr_pes_type;
	int subt_pid_fd, subt_pes_type;
	int ttx_pid_fd, ttx_pes_type;
	struct pid_info pid_info;
	char start_code[MAX_START_CODE];
};

int stm_dvb_demux_open(struct demuxer_context *demux_ctx, __u32 filter_out);
int stm_dvb_demux_setup_filters(int debug_level, struct demuxer_context *demux_ctx, __u32 filter_out);
int dvb_demux_start_filters(struct demuxer_context *demux_ctx);
int dvb_demux_bypass(struct demuxer_context *demux_ctx, bool forced);
int dvb_demux_cached(struct demuxer_context *demux_ctx, char start_code[6]);
int dvb_demux_stop_inactive_filters(struct demuxer_context *demux_ctx);
int stm_dvb_demux_set_source(int fd, __u32 source);

#endif
