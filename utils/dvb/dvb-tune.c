/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Tuner tune and lock
************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <poll.h>
#include <signal.h>
#include <limits.h>
#include <unistd.h>
#include "dvb-tune.h"

#define FE_BAND_EU

struct tuner_band {
	unsigned int start;
	unsigned int end;
	unsigned local_oscillator;
} tuner_band[] = {
	[BAND_US]      = {12200000 , 12700000 , 11250000},
	[BAND_EU_LOW]   = {10600000 , 11700000 , 9750000},
	[BAND_EU_HIGH] = {11700000 , 12750000 , 10600000},
	[BAND_EU_TER]  = {474000   , 858000   , 0},
	[BAND_CABLE]   = {40000000 , 900000000, 0},
	};

enum param_index {
	DELSYS_IDX,
	CHANNEL_NAME_IDX,
	FREQUENCY_IDX,
	POLARIZATION_IDX,
	SATELLITE_IDX,
	SYMBOL_RATE_IDX,
	FEC_IDX,
	MODULATION_IDX,
	ROLLOFF_IDX,
	PILOT_IDX,
	VIDEO_PID_IDX,
	AUDIO_PID_IDX,
	PCR_PID_IDX,
	VIDEO_ENC_IDX,
	AUDIO_ENC_IDX,
	ANCI_PID_IDX,
	ANCI_ENC_IDX,
};
#define PARAM_NUM		(ANCI_ENC_IDX + 1)
#define PARAM_LEN		32

struct fe_tune_params {
	char param_list[PARAM_NUM][PARAM_LEN];
	struct dvb_fe_tune_params dvb_tune_params;
};

/**
 * fe_parse_conf_file() - parse channel confiugration
 */
static int fe_parse_conf_file(char *conf_file, char *channel_name,
				struct fe_tune_params *fe_tune_params)
{
	int i, ret = 0;
	FILE *fp;
	char conf_line[256], *name, *ptr, *line;

	if (!strlen(channel_name)) {
		printf("%s(): no valid name presented for channel name\n", __func__);
		goto open_failed;
	}

	/*
	 * Open the conf file
	 */
	fp = fopen(conf_file, "r");
	if (!fp) {
		printf("%s(): failed to open channel conf file\n", __func__);
		ret = -EINVAL;
		goto open_failed;
	}

	/*
	 * See if channel exists
	 */
	while ((name = fgets(conf_line, sizeof(conf_line), fp))) {

		if (strchr(conf_line, '#') || (*conf_line == '\n'))
			continue;

		name = strchr(conf_line, ':');
		if (!strncmp(name + 1, channel_name, strlen(channel_name)))
			break;
		name = NULL;
	}
	if (!name) {
		printf("%s(): no such channel exists in the conf file\n", __func__);
		ret = -EINVAL;
		goto find_failed;
	}

	/*
	 * Channel found, get all the parameters
	 */
	line = conf_line;
	for (i = 0; i < PARAM_NUM; i++) {

		ptr = strchr(line, ':');
		if (!ptr) {
			strncpy(fe_tune_params->param_list[i], line, strnlen(line,
							sizeof(fe_tune_params->param_list[i]) - 1));
			fe_tune_params->param_list[i][PARAM_LEN - 1] = '\0';
			break;
		}

		*ptr = '\0';
		strncpy(fe_tune_params->param_list[i], line, strnlen(line,
					sizeof(fe_tune_params->param_list[i]) - 1));
		fe_tune_params->param_list[i][PARAM_LEN - 1] = '\0';
		line = ptr + 1;
	}

find_failed:
	fclose(fp);
open_failed:
	return ret;
}

/**
 * fe_parse_delivery_system() - get the delivery system from channel params
 */
static fe_delivery_system_t fe_parse_delivery_system(char *delivery_system)
{
	fe_delivery_system_t delsys;

	if (!strncasecmp(delivery_system, "S1", strlen("S1")))
		delsys = SYS_DVBS;
	else if (!strncasecmp(delivery_system, "S2", strlen("S2")))
		delsys = SYS_DVBS2;
	else
		delsys = -EINVAL;

	return delsys;
}

/**
 * fe_parse_modulation() - parse modulation from channel param
 */
static fe_modulation_t fe_parse_modulation(char *modulation)
{
	fe_modulation_t mod;

	if (!strncasecmp(modulation, "QPSK", strlen("QPSK")))
		mod = QPSK;
	else if (!strncasecmp(modulation, "PSK_8", strlen("PSK_8")))
		mod = PSK_8;
	else
		mod = -EINVAL;

	return mod;
}

/**
 * fe_parse_fec() - parse fec from channel param
 */
fe_code_rate_t fe_parse_fec(char *fec)
{
	fe_code_rate_t code;

	if (!strncasecmp(fec, "FEC_NONE", strlen("FEC_NONE")))
		code = FEC_NONE;
	else if (!strncasecmp(fec, "FEC_1_2", strlen("FEC_1_2")))
		code = FEC_1_2;
	else if (!strncasecmp(fec, "FEC_2_3", strlen("FEC_2_3")))
		code = FEC_2_3;
	else if (!strncasecmp(fec, "FEC_3_4", strlen("FEC_3_4")))
		code = FEC_3_4;
	else if (!strncasecmp(fec, "FEC_4_5", strlen("FEC_4_5")))
		code = FEC_4_5;
	else if (!strncasecmp(fec, "FEC_5_6", strlen("FEC_5_6")))
		code = FEC_5_6;
	else if (!strncasecmp(fec, "FEC_6_7", strlen("FEC_6_7")))
		code = FEC_6_7;
	else if (!strncasecmp(fec, "FEC_7_8", strlen("FEC_7_8")))
		code = FEC_7_8;
	else if (!strncasecmp(fec, "FEC_8_9", strlen("FEC_8_9")))
		code = FEC_8_9;
	else if (!strncasecmp(fec, "FEC_AUTO", strlen("FEC_AUTO")))
		code = FEC_AUTO;
	else if (!strncasecmp(fec, "FEC_3_5", strlen("FEC_3_5")))
		code = FEC_3_5;
	else if (!strncasecmp(fec, "FEC_9_10", strlen("FEC_9_10")))
		code = FEC_9_10;
	else if (!strncasecmp(fec, "FEC_2_5", strlen("FEC_2_5")))
		code = FEC_2_5;
	else
		code = -EINVAL;

	return code;
}

/**
 * fe_parse_rolloff() - parse roll off from channel conf
 */
static fe_rolloff_t fe_parse_rolloff(char *param)
{
	fe_rolloff_t rolloff;

	if (!strncasecmp(param, "ROLLOFF_35", strlen("ROLLOFF_35")))
		rolloff = ROLLOFF_35;
	else if (!strncasecmp(param, "ROLLOFF_20", strlen("ROLLOFF_20")))
		rolloff = ROLLOFF_20;
	else if (!strncasecmp(param, "ROLLOFF_25", strlen("ROLLOFF_25")))
		rolloff = ROLLOFF_25;
	else if (!strncasecmp(param, "ROLLOFF_AUTO", strlen("ROLLOFF_AUTO")))
		rolloff = ROLLOFF_AUTO;
	else
		rolloff = -EINVAL;

	return rolloff;
}

/**
 * fe_parse_pilot() - get pilot info from channel conf
 */
static fe_pilot_t fe_parse_pilot(char *param)
{
	fe_pilot_t pilot;

	if (!strncasecmp(param, "PILOT_ON", strlen("PILOT_ON")))
		pilot = PILOT_ON;
	else if (!strncasecmp(param, "PILOT_OFF", strlen("PILOT_OFF")))
		pilot = PILOT_OFF;
	else if (!strncasecmp(param, "PILOT_AUTO", strlen("PILOT_AUTO")))
		pilot = PILOT_AUTO;
	else
		pilot = -EINVAL;

	return pilot;
}

/**
 * fe_parse_program_params() - get the tuning params from channel params
 */
static int fe_parse_program_params(int debug_level, struct fe_tune_params *fe_tune_params)
{
	__u32 val;
	int ret = 0;
	char *param;
	struct dvb_fe_tune_params *dvb_tune_params = &fe_tune_params->dvb_tune_params;

	/*
	 * Parse delivery system
	 */
	param = fe_tune_params->param_list[DELSYS_IDX];
	ret = fe_parse_delivery_system(param);
	if (ret < 0) {
		printf("%s(): invalid delivery system specified\n", __func__);
		goto parse_param_failed;
	}
	dvb_tune_params->delivery_system = ret;

	/*
	 * Parse modulation type
	 */
	param = fe_tune_params->param_list[MODULATION_IDX];
	ret = fe_parse_modulation(param);
	if (ret < 0) {
		printf("%s(): invalid modulation type specified\n", __func__);
		goto parse_param_failed;
	}
	dvb_tune_params->modulation = ret;

	/*
	 * Set the band information
	 */
#ifdef FE_BAND_EU
	if (dvb_tune_params->frequency >= tuner_band[BAND_EU_HIGH].start)
		dvb_tune_params->band = BAND_EU_HIGH;
	else
		dvb_tune_params->band = BAND_EU_LOW;
#else
		dvb_tune_params.band = BAND_US;
#endif

	/*
	 * Get the channel frequency
	 */
	val = strtoul(fe_tune_params->param_list[FREQUENCY_IDX], NULL, 10);
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
						|| (errno != 0 && val == 0)) {
		ret = errno;
		printf("%s(): invalid frequency in the conf\n", __func__);
		goto parse_param_failed;
	}
	dvb_tune_params->frequency = val * 1000;
	if (dvb_tune_params->frequency >= 2150)
		dvb_tune_params->frequency -= tuner_band[dvb_tune_params->band].local_oscillator;

	/*
	 * Get the channel symbol rate
	 */
	val = strtoul(fe_tune_params->param_list[SYMBOL_RATE_IDX], NULL, 10);
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
						|| (errno != 0 && val == 0)) {
		ret = errno;
		printf("%s(): invalid symbol rate in the conf\n", __func__);
		goto parse_param_failed;
	}
	dvb_tune_params->symbol_rate = val * 1000;

	/*
	 * Parse FEC
	 */
	param = fe_tune_params->param_list[FEC_IDX];
	ret = fe_parse_fec(param);
	if (ret < 0) {
		printf("%s(): invalid fec specified\n", __func__);
		goto parse_param_failed;
	}
	dvb_tune_params->fec = ret;

	/*
	 * Set the inversion to AUTO
	 */
	dvb_tune_params->inversion = INVERSION_AUTO;

	/*
	 * Set the rolloff
	 */
	param = fe_tune_params->param_list[ROLLOFF_IDX];
	ret = fe_parse_rolloff(param);
	if (ret < 0) {
		printf("%s(): invalid roll off specified\n", __func__);
		goto parse_param_failed;
	}
	dvb_tune_params->rolloff = ret;

	/*
	 * Set the pilot
	 */
	param = fe_tune_params->param_list[PILOT_IDX];
	ret = fe_parse_pilot(param);
	if (ret < 0) {
		printf("%s(): invalid pilot specified\n", __func__);
		goto parse_param_failed;
	}
	dvb_tune_params->pilot = ret;

	/*
	 * Set the polarization
	 */
	param = fe_tune_params->param_list[POLARIZATION_IDX];
	if (!strncasecmp(param, "v", strlen("v"))) {
		snprintf(dvb_tune_params->polarization,
			sizeof(dvb_tune_params->polarization), "vertical");
		dvb_tune_params->voltage = SEC_VOLTAGE_13;
	} else if (!strncasecmp(param, "h", strlen("h"))) {
		snprintf(dvb_tune_params->polarization,
			sizeof(dvb_tune_params->polarization), "horizontal");
		dvb_tune_params->voltage = SEC_VOLTAGE_18;
	} else {
		printf("%s(): invalid polarization specified\n", __func__);
		ret = -EINVAL;
		goto parse_param_failed;
	}

	/*
	 * Set the satellite information
	 */
	val = strtoul(fe_tune_params->param_list[SATELLITE_IDX], NULL, 10);
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
						|| (errno != 0 && val == 0)) {
		ret = errno;
		printf("%s(): invalid satellite info in conf errno: %d, strerror(errno): %s, %d\n", __func__, errno, strerror(errno), val);
		goto parse_param_failed;
	}
	dvb_tune_params->satellite = val;

	/*
	 * Set the tone information
	 */
	dvb_tune_params->tone = dvb_tune_params->band == BAND_EU_HIGH ? SEC_TONE_ON : SEC_TONE_OFF;

	/*
	 * Print debug information
	 */
	if (debug_level >= 2) {
		printf("---------- Channel Parameters ------------------\n");
		printf("DVB-S: Delivery System: %d\n", dvb_tune_params->delivery_system);
		printf("DVB-S: Modulation: %d\n", dvb_tune_params->modulation);
		printf("DVB-S: Frequency: %d\n",dvb_tune_params->frequency);
		printf("DVB-S: Symbol Rate: %d\n",dvb_tune_params->symbol_rate);
		printf("DVB-S: FEC: %d\n", dvb_tune_params->fec);
		printf("DVB-S: Inversion: %d\n", dvb_tune_params->inversion);
		printf("DVB-S: Rolloff: %d\n", dvb_tune_params->rolloff);
		printf("DVB-S: Pilot: %d\n", dvb_tune_params->pilot);
		printf("DVB-S: Polarization: %s\n", dvb_tune_params->polarization);
		printf("------------------------------------------------\n");
	}
	ret = 0;

parse_param_failed:
	return ret;
}

/**
 * fe_send_diseqc_command() - send diseqc commands
 */
static int fe_send_diseqc_command(int fd, struct dvb_fe_tune_params *dvb_tune_params)
{
	int ret = 0;
	struct dvb_diseqc_master_cmd diseqc_cmd = {{0xe0, 0x10, 0x38, 0x00, 0x00, 0x00}, 4};
	bool vertical;

	vertical = !strncasecmp(dvb_tune_params->polarization, "vertical", strlen("vertical")) ? 1 : 0;
	diseqc_cmd.msg[3]  = 0xf0 | (dvb_tune_params->band == BAND_EU_HIGH ? 1 : 0);
	diseqc_cmd.msg[3] |= vertical ? 0 : 2;
	diseqc_cmd.msg[3] |= (dvb_tune_params->satellite << 2) & 0x0F;

	ret = ioctl(fd, FE_DISEQC_SEND_MASTER_CMD, &diseqc_cmd);
	if (ret) {
		printf("%s(): failed to send diseqc command\n", __func__);
		goto diseqc_cmd_failed;
	}

	ret = ioctl(fd, FE_DISEQC_SEND_BURST,
			(dvb_tune_params->satellite % 2) ?
			(void *)SEC_MINI_B : (void *)SEC_MINI_A);
	if (ret) {
		printf("%s(): failed to send diseqc burst command\n", __func__);
		goto diseqc_cmd_failed;
	}

diseqc_cmd_failed:
	return ret;
}

/**
 * fe_tune() - do the tune
 */
static int fe_tune(int fd, int debug_level, struct dvb_fe_tune_params *dvb_tune_params)
{
	int i, ret = 0, retry = 5;
	struct dtv_properties dtv_props;
	struct dtv_property props;
	fe_status_t fe_status = 0;

	/*
	 * Get frontend info
	 */
	memset(&dtv_props, 0, sizeof(dtv_props));
	memset(&props, 0, sizeof(props));
	dtv_props.num = 1;
	props.cmd = DTV_ENUM_DELSYS;
	dtv_props.props = &props;
	ret = ioctl(fd, FE_GET_PROPERTY, &dtv_props);
	if (ret) {
		printf("%s(): faild to enumerate delivery system\n", __func__);
		goto get_failed;
	}
	if (debug_level >= 2) {
		printf("------------------ Delivery system supported ----------------\n");
		for (i = 0; i < props.u.buffer.len; i++)
			printf("%d.: %d\n", i + 1, props.u.buffer.data[i]);
		printf("-------------------------------------------------------------\n");
	}

	for (i = 0; i < props.u.buffer.len; i++) {
		if (props.u.buffer.data[i] == dvb_tune_params->delivery_system)
			break;
	}
	if (i == props.u.buffer.len) {
		printf("%s(): This tuner does not support requested delivery system\n", __func__);
		goto get_failed;
	}

	/*
	 * Prepare to send diseqc commands
	 */
	struct dtv_property dtv_property[] = {
		{.cmd = DTV_VOLTAGE, .u.data = dvb_tune_params->voltage},
		{.cmd = DTV_TONE, .u.data = SEC_TONE_OFF},
	};
	dtv_props.num = 2;
	dtv_props.props = dtv_property;
	ret = ioctl(fd, FE_SET_PROPERTY, &dtv_props);
	if (ret) {
		printf("errno: %d, strerror(errno): %s\n", errno, strerror(errno));
		printf("%s(): failed to configure for diseqc commands\n", __func__);
		goto get_failed;
	}

	/*
	 * Send Diseqc commands
	 */
	ret = fe_send_diseqc_command(fd, dvb_tune_params);
	if (ret) {
		printf("%s(): failed to send diseqc commands\n", __func__);
		goto get_failed;
	}

	/*
	 * Send the commands to tune
	 */
	struct dtv_property dtv_tune_property[] = {
		{.cmd = DTV_TONE, .u.data = dvb_tune_params->tone},
		{.cmd = DTV_DELIVERY_SYSTEM, .u.data = dvb_tune_params->delivery_system},
		{.cmd = DTV_FREQUENCY, .u.data = dvb_tune_params->frequency},
		{.cmd = DTV_MODULATION, .u.data = dvb_tune_params->modulation},
		{.cmd = DTV_SYMBOL_RATE, .u.data = dvb_tune_params->symbol_rate},
		{.cmd = DTV_INNER_FEC, .u.data = dvb_tune_params->fec},
		{.cmd = DTV_INVERSION, .u.data = dvb_tune_params->inversion},
		{.cmd = DTV_ROLLOFF, .u.data = dvb_tune_params->rolloff},
		{.cmd = DTV_PILOT, .u.data = dvb_tune_params->pilot},
		{.cmd = DTV_TUNE}
	};
	dtv_props.num = 10;
	dtv_props.props = dtv_tune_property;
	ret = ioctl(fd, FE_SET_PROPERTY, &dtv_props);
	if (ret) {
		printf("%s(): failed to tune\n", __func__);
		goto get_failed;
	}

	/*
	 * Wait for tune
	 */
	while (!(fe_status & FE_HAS_LOCK) && retry--) {

		usleep(50000);

		ret = ioctl(fd, FE_READ_STATUS, &fe_status);
		if (ret) {
			printf("%s(): failed to read status\n", __func__);
			goto get_failed;
		}
	}

	if (!(fe_status & FE_HAS_LOCK))
		ret = -ECHRNG;

#if 0
	if (debug) {
		struct dtv_property dtv_get_property[] = {
			{.cmd = DTV_STAT_SIGNAL_STRENGTH},
			{.cmd = DTV_STAT_CNR},
		};
		dtv_props.num = 2;
		dtv_props.props = dtv_get_property;
		ret = ioctl(fd, FE_GET_PROPERTY, &dtv_props);
		if (ret) {
			printf("%s(): failed to get debug information\n", __func__);
			ret = 0;
			goto get_failed;
		}
	}
#endif

get_failed:
	return ret;
}

/**
 * dvb_fe_tune() - tune for the particular channel
 * @conf_file    : information about the channel
 * @channel_name : channel to find from
 */
int dvb_fe_tune(int debug_level, int fd, char *conf_file, char *channel_name)
{
	int ret;
	struct fe_tune_params fe_tune_params;

	/*
	 * Get all the parameters from the conf file
	 */
	memset(&fe_tune_params, 0, sizeof(fe_tune_params));
	ret = fe_parse_conf_file(conf_file, channel_name, &fe_tune_params);
	if (ret) {
		printf("%s(): failed to parse conf file\n", __func__);
		goto read_failed;
	}

	/*
	 * Set the tuning parameters
	 */
	ret = fe_parse_program_params(debug_level, &fe_tune_params);
	if (ret) {
		printf("%s(): failed to parse channel params\n", __func__);
		goto read_failed;
	}

	/*
	 * Do the actual tune
	 */
	ret = fe_tune(fd, debug_level, &fe_tune_params.dvb_tune_params);
	if (ret)
		printf("%s(): failed to tune for program %s\n", __func__, channel_name);


read_failed:
	return ret;
}

/**
 * dvb_get_pids() - get pids information from channel conf
 */
int dvb_get_pids(int debug_level, char *conf_file,
			char *channel_name, struct pid_info *pid_info)
{
	int ret, val;
	struct fe_tune_params fe_tune_params;

	/*
	 * Get all the parameters from the conf file
	 */
	memset(&fe_tune_params, 0, sizeof(fe_tune_params));
	ret = fe_parse_conf_file(conf_file, channel_name, &fe_tune_params);
	if (ret) {
		printf("%s(): failed to parse conf file\n", __func__);
		goto read_failed;
	}

	/*
	 * Parse for video pid
	 */
	val = strtol(fe_tune_params.param_list[VIDEO_PID_IDX], 0, 10);
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
						|| (errno != 0 && val == 0)) {
		ret = errno;
		goto read_failed;
	}
	pid_info->video_pid = val;

	/*
	 * Parse for audio pid
	 */
	val = strtol(fe_tune_params.param_list[AUDIO_PID_IDX], 0, 10);
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
						|| (errno != 0 && val == 0)) {
		ret = errno;
		goto read_failed;
	}
	pid_info->audio_pid = val;

	/*
	 * Parse for pcr pid
	 */
	val = strtol(fe_tune_params.param_list[PCR_PID_IDX], 0, 10);
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
						|| (errno != 0 && val == 0)) {
		ret = errno;
		goto read_failed;
	}
	pid_info->pcr_pid = val;

	/*
	 * Parse for video encoding
	 */
	val = strtol(fe_tune_params.param_list[VIDEO_ENC_IDX], 0, 10);
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
						|| (errno != 0 && val == 0)) {
		ret = errno;
		goto read_failed;
	}
	pid_info->video_enc = val;

	/*
	 * Parse for video pid
	 */
	val = strtol(fe_tune_params.param_list[AUDIO_ENC_IDX], 0, 10);
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
						|| (errno != 0 && val == 0)) {
		ret = errno;
		goto read_failed;
	}
	pid_info->audio_enc = val;

	pid_info->subt_pid = -1;
	pid_info->ttx_pid = -1;

	if (debug_level >= 2) {
		printf("-----------------------------------\n");
		printf("Program: %s\n", channel_name);
		printf("--------- PID Info ----------------\n");
		printf("Video PID: %d\n", pid_info->video_pid);
		printf("Audio PID: %d\n", pid_info->audio_pid);
		printf("PCR PID  : %d\n", pid_info->pcr_pid);
		printf("Video Encoding: %d\n", pid_info->video_enc);
		printf("Audio Encoding: %d\n", pid_info->audio_enc);
		printf("-----------------------------------\n");
	}

read_failed:
	return ret;
}

/**
 * dvb_fe_get_params() - Get the tune params from fe
 */
int dvb_fe_get_params(int debug_level, char *conf_file, char *channel_name,
					struct dvb_fe_tune_params *dvb_tune_params)
{
	int ret;
	struct fe_tune_params fe_tune_params;

	/*
	 * Get all the parameters from the conf file
	 */
	memset(&fe_tune_params, 0, sizeof(fe_tune_params));
	ret = fe_parse_conf_file(conf_file, channel_name, &fe_tune_params);
	if (ret) {
		printf("%s(): failed to parse conf file %s\n", __func__, conf_file);
		goto read_failed;
	}

	/*
	 * Set the tuning parameters
	 */
	if (debug_level >= 2) {
		printf("---------------------------------\n");
		printf("Program name: %s\n", channel_name);
		printf("---------------------------------\n");
	}
	ret = fe_parse_program_params(debug_level, &fe_tune_params);
	if (ret) {
		printf("%s(): failed to parse channel params\n", __func__);
		goto read_failed;
	}

	memcpy(dvb_tune_params, &fe_tune_params.dvb_tune_params, sizeof(*dvb_tune_params));

read_failed:
	return ret;
}
