/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __CGMSHD_H
#define __CGMSHD_H


/******************************************************************************/
/* Modules includes                                                           */
/******************************************************************************/


#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************/
/* Public constants                                                           */
/******************************************************************************/

/******************************************************************************/
/* Public types                                                               */
/******************************************************************************/

typedef enum cgms_type_e
{
    CGMS_TYPE_A
  , CGMS_TYPE_B
} cgms_type_t;

typedef enum cgms_standard_e
{
    CGMS_HD_480P60000
  , CGMS_HD_720P60000
  , CGMS_HD_1080I60000
  , CGMS_TYPE_B_HD_480P60000
  , CGMS_TYPE_B_HD_720P60000
  , CGMS_TYPE_B_HD_1080I60000
} cgms_standard_t;


/******************************************************************************/
/* Public macros                                                              */
/******************************************************************************/

/******************************************************************************/
/* Public functions prototypes                                                */
/******************************************************************************/

void CGMSHD_DrawWaveform( cgms_standard_t Standard
                        , unsigned char  *Data_p
                        , int BytesPerLine
                        , unsigned long  *BufferData_p
                        );

#ifdef __cplusplus
}
#endif

#endif /* #ifndef __CGMSHD_H */
