/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Keyboard thread to accept runtime options
************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/ioctl.h>

#include "compo-capture.h"

#define COMMAND_LINE_SIZE	128

enum commands {
	QUIT
};

/**
 * keyboard_help() - keyboard help
 */
void keyboard_help()
{
	printf("  q\n");
	printf("   Quit the application\n\n");
}

/**
 * find_command() - finds out if the command is supported
 */
static int find_command(char *command_line)
{
	int command = -1;

	if (!strncmp(command_line, "q", strlen("q")))
		command = QUIT;

	return command;
}

/**
 * process_command() - process command
 */
static void process_command(struct compo_capture_data *data,
				enum commands command,
				char *command_line,
				int size)
{
	switch (command) {
	case QUIT:
		data->exit = true;
		break;
	}
}

/**
 * keyboard_monitor() - keyboard monitor thread
 */
void *keyboard_monitor(void *arg)
{
	int command;
	char command_line[COMMAND_LINE_SIZE];
	struct compo_capture_data *data = arg;

	/*
	 * Read the command line and parse it for the options
	 */
	while (!data->exit && fgets(command_line, sizeof(command_line), stdin)) {

		if (data->exit)
			goto exit;

		command = find_command(command_line);
		if (command < 0) {
			keyboard_help();
			continue;
		}

		process_command(data, command, command_line, sizeof(command_line));
	}

exit:
	return NULL;
}
