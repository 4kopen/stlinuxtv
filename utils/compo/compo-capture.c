/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Compo-capture and display mirroring in tunneled mode
************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <poll.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include <limits.h>
#include <mediactl/mediactl.h>
#include <mediactl/v4l2subdev.h>
#include <linux/v4l2-subdev.h>

#include "linux/dvb/dvb_v4l2_export.h"
#include "linux/stm_v4l2_decoder_export.h"
#include "stm_v4l2_helper.h"
#include "linux/stm_v4l2_export.h"
#include "linux/stmvout.h"
#include "compo-capture.h"

#define PAL_HEIGHT	576
#define PAL_WIDTH	720
#define NTSC_HEIGHT	480
#define NTSC_WIDTH	PAL_WIDTH

enum aux_mixer_std {
	NTSC,
	PAL,
};

struct capture_context {
	__u32 underscan;

	int latency_profile;
	enum aux_mixer_std disp_std;
	int capture_height, capture_width;

	bool tunnel, debug_info;
	struct media_device *media;

	struct compo_capture_data data;

	char compo_name[32], decoder_name[32];
	struct media_entity *compo_entity, *decoder_entity;
	struct media_entity *plane_entity, *hdout_entity;
}cap_ctx;

static pthread_t compo_thread, compo_user_grab_thread, keyboard_thread;

/**
 * compo_print_pixel_codes() - print supported pixel codes
 */
static void compo_print_pixel_codes()
{
	int ret, i;
	struct v4l2_subdev_mbus_code_enum code;
	struct v4l2_subdev_frame_size_enum fse;

	if (!cap_ctx.debug_info)
		goto get_pixel_code_done;

	ret = stm_v4l2_subdev_open(cap_ctx.media,
			cap_ctx.compo_name, &cap_ctx.compo_entity);
	if (ret) {
		printf("%s(): failed to open compo subdev\n", __func__);
		goto get_pixel_code_done;
	}


	printf("====================================\n");
	printf("=====Supported Capture formats======\n");
	for (i = 0; ;i++) {
		memset(&code, 0, sizeof(code));
		code.pad = 1;
		code.index = i;
		ret = ioctl(cap_ctx.compo_entity->fd,
				VIDIOC_SUBDEV_ENUM_MBUS_CODE, &code);
		if (ret)
			break;

		memset(&fse, 0, sizeof(fse));
		fse.index=i;
		fse.pad = 1;
		ret = ioctl(cap_ctx.compo_entity->fd,
				VIDIOC_SUBDEV_ENUM_FRAME_SIZE,
				&fse);
		if (ret) {
			printf("%s(): failed to get the frame size info\n", __func__);
			break;
		}
		printf("\tPixel code: 0x%x\n", code.code);
		printf("\t\t--Resizing caps--\n");
		printf("\t\tmin_width : %d\n", fse.min_width);
		printf("\t\tmax_width : %d\n", fse.max_width);
		printf("\t\tmin_height: %d\n", fse.min_height);
		printf("\t\tmax_height: %d\n", fse.max_height);
	}
	printf("====================================\n");

get_pixel_code_done:
	return;
}

/**
 * set_default_height() - set default height
 */
static inline void set_default_height()
{
	if (cap_ctx.disp_std == PAL)
		cap_ctx.capture_height = PAL_HEIGHT;
	else
		cap_ctx.capture_height = NTSC_HEIGHT;
}

/**
 * set_default_width() - set default width
 */
static inline void set_default_width()
{
	if (cap_ctx.disp_std == PAL)
		cap_ctx.capture_width = PAL_WIDTH;
	else
		cap_ctx.capture_width = NTSC_WIDTH;
}

/**
 * get_capture_param() - get the capture height/width from the line
 */
static int get_capture_param(char *line)
{
	int ret;
	char *ptr;
	bool height = false, width = false;

	if (strstr(line, "capture_width"))
		width = true;
	else if (strstr(line, "capture_height"))
		height = true;
	else {
		ret = -EINVAL;
		goto g_param_failed;
	}

	ptr = strrchr(line, ' ');
	if (!ptr || !(ptr + 1)) {
		printf("%s(): parameter value missing\n", __func__);
		ret = -EINVAL;
		goto g_param_failed;
	}

	if (width) {
		if (!strncmp(ptr + 1, "plane_width", strlen("plane_width"))) {

			set_default_width();

		} else if (isdigit(*(ptr + 1))) {

			cap_ctx.capture_width = strtoul(ptr + 1, NULL, 10);
			if (cap_ctx.capture_width < 0) {
				printf("%s(): invalid capture width\n", __func__);
				ret = errno;
				goto g_param_failed;
			}

		} else {
			printf("%s(): invalid line syntax\n", __func__);
			ret = -EINVAL;
			goto g_param_failed;
		}
	} else if (height) {

		if (!strncmp(ptr + 1, "plane_height", strlen("plane_height"))) {

			set_default_height();

		} else if (isdigit(*(ptr + 1))) {

			cap_ctx.capture_height = strtoul(ptr + 1, NULL, 10);
			if (cap_ctx.capture_height < 0) {
				printf("%s(): invalid capture width\n", __func__);
				ret = errno;
				goto g_param_failed;
			}

		} else {
			printf("%s(): invalid line syntax\n", __func__);
			ret = -EINVAL;
			goto g_param_failed;
		}
	}

g_param_failed:
	return ret;
}

/**
 * get_capture_params() - parse config file for resize params
 */
static void get_capture_params(char *config_file, char *plane_mode)
{
	FILE *fp;
	int ret, framerate;
	__u32 htotal, vtotal;
	char line[256], res_string[16];
	char *plane_tag, *disp_mode_tag;
	struct v4l2_dv_timings timings;
	struct v4l2_bt_timings *bt = &timings.bt;

	disp_mode_tag = (cap_ctx.disp_std == PAL) ? "PAL" : "NTSC";
	plane_tag = !strncmp(plane_mode, "lite", strlen("lite")) ? "[LITE]" : "[FULL]";

	/*
	 * Get the main mixer dv timings to determine the resize operation
	 */
	memset(&timings, 0, sizeof(timings));
	ret = ioctl(cap_ctx.hdout_entity->fd,
			VIDIOC_SUBDEV_G_DV_TIMINGS, &timings);
	if (ret) {
		printf("%s(): failed to get main mixer dv timings\n", __func__);
		goto g_timings_failed;
	}
	htotal = bt->width + bt->hsync + bt->hfrontporch + bt->hbackporch;
	vtotal = bt->height + bt->vsync + bt->vfrontporch + bt->vbackporch;
	framerate = bt->pixelclock / (__u64)(htotal * vtotal);

	snprintf(res_string, sizeof(res_string), "%d%s%d", bt->height,
				bt->interlaced > V4L2_FIELD_NONE ? "i" : "p", framerate);
	printf("Main mixer is transmitting %s\n", res_string);

	/*
	 * Open conf file
	 */
	fp = fopen(config_file, "r");
	if (!fp) {
		printf("%s(): failed to open %s\n", __func__, config_file);
		ret = -ENODEV;
		goto g_timings_failed;
	}

	/*
	 * Start parsing the conf file
	 */
	while (fgets(line, sizeof(line), fp)) {

		if (strchr(line, '#'))
			continue;

		if (!strstr(line, plane_tag))
			continue;

		/*
		 * Found the plane tag, now look for resize info
		 */
		while (fgets(line, sizeof(line), fp)) {

			int i;

			if (strchr(line, '#'))
				continue;

			if (!strstr(line, res_string))
				continue;

			if (!strstr(line, disp_mode_tag))
				continue;

			/*
			 * Now, a tag line with main mixer resolution and aux mixer
			 * resolution is found, let's proceed with finding out the
			 * capture width and height. Just iterate over 2 lines to
			 * find out if we succeed in getting the height/width, else
			 * error out.
			 */
			for (i = 0; i < 2; i++) {

				if (!fgets(line, sizeof(line), fp)) {
					printf("%s(): EOF reached without any resize info\n", __func__);
					ret = -EINVAL;
					goto g_timings_failed;
				}

				ret = get_capture_param(line);
				if (ret) {
					printf("%s(): failed to get height/width from conf file\n", __func__);
					goto g_timings_failed;
				}
			}

			printf("Capture height: %d, width: %d\n", cap_ctx.capture_height, cap_ctx.capture_width);

			/*
			 * We got everything, let's get out
			 */
			goto g_timings_failed;
		}
	}

g_timings_failed:
	if (feof(fp) || ret) {
		printf("== No entry found for resize, so, defauting to Aux resolution\n");
		set_default_width();
		set_default_height();
	}
}

/**
 * config_full_plane_capture() - configuration for full plane capture
 */
static int config_full_plane_capture()
{
	int ret = 0;
	int height, width;
	struct v4l2_control ctrl;
	struct v4l2_rect rect;
	struct v4l2_mbus_framefmt mbus_fmt;

	/*
	 * Get capture height/width
	 */
	get_capture_params("compo-capture.conf", "full");

	/*
	 * Set the format on compo for the particular output.
	 */
	memset(&mbus_fmt, 0, sizeof(mbus_fmt));
	ret = v4l2_subdev_get_format(cap_ctx.compo_entity,
				&mbus_fmt, 1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret) {
		printf("%s(): failed to get compo format\n", __func__);
		goto get_params_failed;
	}

	mbus_fmt.width = cap_ctx.capture_width;
	mbus_fmt.height = cap_ctx.capture_height;
	mbus_fmt.colorspace = V4L2_COLORSPACE_SMPTE170M;
	ret = v4l2_subdev_set_format(cap_ctx.compo_entity,
				&mbus_fmt, 1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret) {
		printf("%s(): failed to set compo format\n", __func__);
		goto get_params_failed;
	}

	compo_print_pixel_codes();

	/*
	 * Configure input window of full plane to receive capture frame
	 */
	memset(&ctrl, 0, sizeof(ctrl));
	ctrl.id = V4L2_CID_STM_INPUT_WINDOW_MODE;
	ctrl.value = VCSPLANE_AUTO_MODE;
	ret = ioctl(cap_ctx.plane_entity->fd, VIDIOC_S_CTRL, &ctrl);
	if (ret) {
		printf("%s(): failed to set %s input to AUTO mode\n", __func__, cap_ctx.data.plane);
		goto get_params_failed;
	}

	/*
	 * Final resize on full plane
	 */
	memset(&ctrl, 0, sizeof(ctrl));
	ctrl.id = V4L2_CID_STM_OUTPUT_WINDOW_MODE;
	ctrl.value = VCSPLANE_MANUAL_MODE;
	ret = ioctl(cap_ctx.plane_entity->fd, VIDIOC_S_CTRL, &ctrl);
	if (ret) {
		printf("%s(): failed to set %s output to AUTO mode\n", __func__, cap_ctx.data.plane);
		goto get_params_failed;
	}
	memset(&rect, 0, sizeof(rect));
	width = cap_ctx.disp_std == PAL ? PAL_WIDTH : NTSC_WIDTH;
	height = cap_ctx.disp_std == PAL ? PAL_HEIGHT: NTSC_HEIGHT;
	rect.left = (width * cap_ctx.underscan) / 100;
	rect.top = (height * cap_ctx.underscan) / 100;
	rect.width = width - ((width * 2 * cap_ctx.underscan) / 100);
	rect.height =  height - ((height * 2 * cap_ctx.underscan) / 100);
	ret = v4l2_subdev_set_crop(cap_ctx.plane_entity, &rect,
					1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret)
		printf("%s(): failed to set crop on output window of %s\n", __func__, cap_ctx.data.plane);

get_params_failed:
	return ret;
}

/**
 * config_lite_plane_capture() - compo configuration for aux planes
 */
static int config_lite_plane_capture()
{
	int ret = 0;
	int height, width;
	struct v4l2_rect rect;
	struct v4l2_mbus_framefmt mbus_fmt;

	/*
	 * Get the capture height/width
	 */
	get_capture_params("compo-capture.conf", "lite");

	/*
	 * Set resize on compo-capture
	 */
	memset(&mbus_fmt, 0, sizeof(mbus_fmt));
	ret = v4l2_subdev_get_format(cap_ctx.compo_entity,
				&mbus_fmt, 1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret) {
		printf("%s(): failed to get compo format\n", __func__);
		goto get_params_failed;
	}
	mbus_fmt.width = cap_ctx.capture_width -
		((cap_ctx.capture_width * 2 * cap_ctx.underscan) / 100);
	mbus_fmt.height = cap_ctx.capture_height -
		((cap_ctx.capture_height * 2 * cap_ctx.underscan) / 100);;
	mbus_fmt.colorspace = V4L2_COLORSPACE_SMPTE170M;
	ret = v4l2_subdev_set_format(cap_ctx.compo_entity,
				&mbus_fmt, 1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret) {
		printf("%s(): failed to set compo format\n", __func__);
		goto get_params_failed;
	}

	compo_print_pixel_codes();

	/*
	 * Set the input window dimensions of aux plane. Take into account
	 * the underscan parameter if given. The idea is to resize from all
	 * sides with the same percentage.
	 */
	memset(&rect, 0, sizeof(rect));
	rect.width = mbus_fmt.width;
	rect.height = mbus_fmt.height;
	ret = v4l2_subdev_set_crop(cap_ctx.plane_entity, &rect,
					0, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret) {
		printf("%s(): failed to set crop on %s\n", __func__, cap_ctx.data.plane);
		goto get_params_failed;
	}

	/*
	 * Set the output window dimensions of aux plane
	 */
	width = cap_ctx.disp_std == PAL ? PAL_WIDTH : NTSC_WIDTH;
	height = cap_ctx.disp_std == PAL ? PAL_HEIGHT: NTSC_HEIGHT;
	rect.left =  ((width * cap_ctx.underscan) / 100);
	rect.top = ((height * cap_ctx.underscan) / 100);
	rect.width = width;
	rect.height = height;
	ret = v4l2_subdev_set_crop(cap_ctx.plane_entity, &rect,
					1, V4L2_SUBDEV_FORMAT_ACTIVE);
	if (ret) {
		printf("%s(): failed to set crop on %s\n", __func__, cap_ctx.data.plane);
		goto get_params_failed;
	}

get_params_failed:
	return ret;
}

/**
 * compo_cap_configure_resize() - resize for aux dis
 */
static int compo_cap_configure_resize()
{
	int ret;
	struct v4l2_queryctrl query;
	struct v4l2_ext_control ctrl;
	struct v4l2_ext_controls ext_ctrls;

	/*
	 * Query control to see the parameters required for  getting it's value
	 */
	memset(&query, 0, sizeof(query));
	query.id = V4L2_CID_STI_PLANE_FEATURE_WINDOW_MODE;
	ret = ioctl(cap_ctx.plane_entity->fd, VIDIOC_QUERYCTRL, &query);
	if (ret) {
		printf("%s(): failed to query plane feature mode\n", __func__);
		goto query_failed;
	}

	/*
	 * Check whether the plane is lite or full
	 */
	memset(&ctrl, 0, sizeof(ctrl));
	ctrl.id = V4L2_CID_STI_PLANE_FEATURE_WINDOW_MODE;
	ctrl.size = query.maximum + 1;
	ctrl.string = malloc(ctrl.size);
	if (!ctrl.string) {
		ret = -ENOMEM;
		printf("failed to allocate memory for ctrl value\n");
		goto query_failed;
	}

	memset(&ext_ctrls, 0, sizeof(ext_ctrls));
	ext_ctrls.ctrl_class = V4L2_CTRL_CLASS_IMAGE_PROC;
	ext_ctrls.count = 1;
	ext_ctrls.controls = &ctrl;
	ret = ioctl(cap_ctx.plane_entity->fd, VIDIOC_G_EXT_CTRLS, &ext_ctrls);
	if (ret) {
		printf("%s(): Failed to get plane feature mode\n", __func__);
		goto g_ctrl_failed;
	}

	if (!strncmp(ctrl.string, "auto", strlen("auto"))) {
		printf("Display plane is a full plane\n");
		ret = config_full_plane_capture();
	} else {
		printf("Display plane is lite plane\n");
		ret = config_lite_plane_capture();
	}

g_ctrl_failed:
	free(ctrl.string);
query_failed:
	return ret;
}

/**
 * set_aux_aspect_ratio() - set the aspect ratio on aux plane
 */
static int set_aux_aspect_ratio()
{
	FILE *fp;
	char line[256];
	int ret, aspect_ratio;
	struct v4l2_control ctrl;

	fp = fopen("compo-capture.conf", "r");
	if (!fp) {
		printf("%s(): failed to open configuration file\n", __func__);
		ret = -ENODEV;
		goto open_failed;
	}

	while (fgets(line, sizeof(line), fp)) {

		if (strchr(line, '#'))
			continue;

		if (!strstr(line, "[CONF]"))
			continue;

		while (fgets(line, sizeof(line), fp)) {

			if (strchr(line, '#'))
				continue;

			if (!strstr(line, "aspect_ratio"))
				continue;

			/*
			 * Found the value, then convert it to
			 * display aspect ratio.
			 */

			if (!strncmp(strrchr(line, ' ') + 1, "16:3", strlen("16:3")))
				aspect_ratio = VCSOUTPUT_DISPLAY_ASPECT_RATIO_16_9;
			else if (!strncmp(strrchr(line, ' ') + 1, "4:3", strlen("4:3")))
				aspect_ratio = VCSOUTPUT_DISPLAY_ASPECT_RATIO_4_3;
			else if (!strncmp(strrchr(line, ' ') + 1, "14:9", strlen("14:9")))
				aspect_ratio = VCSOUTPUT_DISPLAY_ASPECT_RATIO_14_9;
			else
				aspect_ratio = VCSOUTPUT_DISPLAY_ASPECT_RATIO_16_9;
			break;
		}
		break;
	}

	if (feof(fp))
		aspect_ratio = VCSOUTPUT_DISPLAY_ASPECT_RATIO_16_9;

	/*
	 * Setting the aspect ratio now
	 */
	memset(&ctrl, 0, sizeof(ctrl));
	ctrl.id = V4L2_CID_DV_STM_TX_ASPECT_RATIO;
	ctrl.value = aspect_ratio;
	ret = ioctl(cap_ctx.plane_entity->fd, VIDIOC_S_CTRL, &ctrl);
	if (ret)
		printf("%s(): failed to set aspect ratio on aux window\n", __func__);

open_failed:
	return ret;
}

/**
 * compo_capture_start() - configure and start compo capture
 */
static void *compo_capture_start(void *arg)
{
	int ret;
	struct pollfd pollfd;
	struct v4l2_event event;
	struct v4l2_control ctrl;

	/*
	 * Open all the required sub-devices here
	 */
	ret = stm_v4l2_subdev_open(cap_ctx.media,
			cap_ctx.data.plane, &cap_ctx.plane_entity);
	if (ret) {
		printf("%s(): failed to open plane entity\n", __func__);
		goto plane_open_failed;
	}

	ret = stm_v4l2_subdev_open(cap_ctx.media,
			cap_ctx.compo_name, &cap_ctx.compo_entity);
	if (ret) {
		printf("%s(): failed to open compo entity\n", __func__);
		goto compo_open_failed;
	}

	ret = stm_v4l2_subdev_open(cap_ctx.media,
			cap_ctx.decoder_name, &cap_ctx.decoder_entity);
	if (ret) {
		printf("%s(): failed to open decoder entity\n", __func__);
		goto decoder_open_failed;
	}

	ret = stm_v4l2_subdev_open(cap_ctx.media, "analog_hdout0", &cap_ctx.hdout_entity);
	if (ret) {
		printf("%s(): Failed to configure the capture & resize\n", __func__);
		goto hdout_open_failed;
	}

	/*
	 * Default to the configuration file specified aspect ratio
	 */
	ret = set_aux_aspect_ratio();
	if (ret) {
		printf("%s(): failed to set aspect ratio\n", __func__);
		goto set_aspect_ratio_failed;
	}

	while (!cap_ctx.data.exit) {

		memset(&pollfd, 0, sizeof(pollfd));
		pollfd.fd = cap_ctx.hdout_entity->fd;
		pollfd.events = POLLPRI;

		if (poll(&pollfd, 1, 100)) {
			if (pollfd.revents & POLLERR)
				goto set_aspect_ratio_failed;
		} else
				continue;

		memset(&event, 0, sizeof(event));
		ret = ioctl(cap_ctx.hdout_entity->fd, VIDIOC_DQEVENT, &event);
		if (ret) {
			printf("%s(): failed to dequeue output event\n", __func__);
			continue;
		}

		if ((event.id == 1) &&
			(event.type == V4L2_EVENT_SOURCE_CHANGE) &&
			(event.u.src_change.changes == V4L2_EVENT_SRC_CH_RESOLUTION)) {
			printf("Main compositor started with new timings ~~~~\n");

			/*
			 * The planes are categorized as full and lite planes. Full planes
			 * can do a resize whereas lite planes cannot do so. So, there's
			 * a need to configure capture IP and plane to get a good Aux output
			 */
			ret = compo_cap_configure_resize();
			if (ret) {
				printf("%s(): failed to configure plane\n", __func__);
				goto set_aspect_ratio_failed;
			}

			/*
			 * Set the latency profile before starting the tunneled display
			 * with streaming engine
			 */
			memset(&ctrl, 0, sizeof(ctrl));
			ctrl.id = V4L2_CID_MPEG_STI_CAPTURE_PROFILE;
			ctrl.value = cap_ctx.latency_profile;
			ret = ioctl(cap_ctx.decoder_entity->fd, VIDIOC_S_CTRL, &ctrl);
			if (ret) {
				printf("%s(): failed to set latency profile\n", __func__);
				goto set_aspect_ratio_failed;
			}

			ret = ioctl(cap_ctx.decoder_entity->fd,
					VIDIOC_SUBDEV_STI_STREAMON, NULL);
			if (ret) {
				printf("%s(): failed to streamon decoder \n", __func__);
				goto set_aspect_ratio_failed;
			}

			memset(&ctrl, 0, sizeof(ctrl));
			ctrl.id = V4L2_CID_MPEG_STI_VIDEO_LATENCY;
			ret = ioctl (cap_ctx.decoder_entity->fd, VIDIOC_G_CTRL, &ctrl);
			if (ret) {
				printf("%s(): failed to get capture to render latency\n", __func__);
				continue;
			}

			printf("Capture to render latency: %d ms\n", ctrl.value );

			continue;
		}

		if ((event.id == 1) &&
			(event.type == V4L2_EVENT_SOURCE_CHANGE) &&
			(event.u.src_change.changes == !V4L2_EVENT_SRC_CH_RESOLUTION))
			printf("Main compositor stopped xxxx\n");
	}

set_aspect_ratio_failed:
	stm_v4l2_subdev_close(cap_ctx.hdout_entity);
hdout_open_failed:
	stm_v4l2_subdev_close(cap_ctx.decoder_entity);
decoder_open_failed:
	stm_v4l2_subdev_close(cap_ctx.compo_entity);
compo_open_failed:
	stm_v4l2_subdev_close(cap_ctx.plane_entity);
plane_open_failed:
	cap_ctx.data.exit = true;
	return NULL;
}

/**
 * compo_capture_setup_tunneled() - setup the devices for tunneled data flow
 */
static int compo_capture_setup_tunneled()
{
	int ret = 0;
	struct v4l2_event_subscription subs;

	if (!strlen(cap_ctx.decoder_name) || !strlen(cap_ctx.data.plane)) {
		printf("%s(): either plane or decoder not specified, cannot start\n", __func__);
		ret = -EINVAL;
		goto setup_failed;
	}

	/*
	 * Open output subdev and subscribe to events
	 */
	ret = stm_v4l2_subdev_open(cap_ctx.media,
			"analog_hdout0", &cap_ctx.hdout_entity);
	if (ret) {
		printf("%s(): failed to open output entity\n", __func__);
		goto setup_failed;
	}

	memset(&subs, 0, sizeof(subs));
	subs.id = 1;
	subs.type = V4L2_EVENT_SOURCE_CHANGE;
	subs.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;
	ret = ioctl(cap_ctx.hdout_entity->fd, VIDIOC_SUBSCRIBE_EVENT, &subs);
	if (ret) {
		printf("%s(): failed to subscribe to output property event change\n", __func__);
		goto subs_failed;
	}

	/*
	 * Start compo monitor
	 */
	ret = pthread_create(&compo_thread, NULL, compo_capture_start, NULL);
	if (ret) {
		printf("%s(): failed to create compo thread\n", __func__);
		goto subs_failed;
	}

	return 0;

subs_failed:
	stm_v4l2_subdev_close(cap_ctx.hdout_entity);
setup_failed:
	return ret;
}

/**
 * compo_sighandler() - signal handler to manage clean exit
 */
static void compo_sighandler(int signum, siginfo_t *siginfo, void *data)
{
	switch (signum) {
	case SIGINT:
		cap_ctx.data.exit = true;
		if (cap_ctx.tunnel)
			stm_v4l2_subdev_close(cap_ctx.decoder_entity);
		else
			pthread_join(compo_user_grab_thread, NULL);

		pthread_cancel(keyboard_thread);

		printf("\n");
		break;

	default:
		printf("Unhandled signal number: %d\n", signum);
	}
}

/*
 * usage() - help message
 */
static void usage()
{
	printf("compo-capture version-2.0\n");
	printf("Last modified (28 Nov 2014)\n");
	printf("Compile info:- DATE(%s), TIME(%s)\n", __DATE__, __TIME__);
	printf("Usage: compo-capture [OPTIONS]\n");
	printf("Configure the compo capture.\n");
	printf("\n");
	printf("  --debug\n");
	printf("    Starts printing more debug information such as supported pixel formats by compo for an input\n\n");
	printf("  --dec-id, [0 ... ]\n");
	printf("     Select the decoder device to be used for capture. [TUNNEL]\n\n");
	printf("  -p, --plane=[Main-VID, Aux-GDP2 ... ]\n");
	printf("     Select the plane which will be used to display data captured from compositor\n\n");
	printf("  --underscan\n");
	printf("     Select the percentage by which the image will appear underscanned from horizontal and vertical axis [TUNNEL]\n\n");
	printf("  -t, --tunnel [0, 1]\n");
	printf("     Select the way compo-capture is done. value of <0> allows user-space grab and display on selected plane.\n");
	printf("     By default, tunneled compo-capture is enabled (value <1> )\n\n");
	printf("  -s, --std [pal, ntsc]\n");
	printf("     Configures the output of capture based on Aux-Mixer configuration which can be ntsc or pal.\n\n");
	printf("  --compo-start\n");
	printf("     Maintained for backward compatibility of test scripts. It serves no purpose for the moment.\n\n");
	printf("  -l, --latency-profile [5, 6]\n");
	printf("     Specifies the latency profile. By default value is 5 (Video FRC enabled). 6 can be enabled if no FRC required\n\n");
	printf("  --help, -h\n");
	printf("     Displays this information\n\n");
	printf("    Example Usage:\n");
	printf("     compo-capture --dec-id=1 -p Aux-GDP2 (for tunneled capture and display, dec-id=1 because the decoder instance is 1)\n");
	printf("     compo-capture -t 0 -p Aux-GDP2 (for non-tunneled capture and display)\n");
}

/*
 * main() - main entry point
 */
int main(int argc, char *argv[])
{
	int ret = 0, option, decoder_id = 0;
	struct sigaction compo_sigaction;

	/*
	 * Construct the arguments to be parsed by this application
	 */
	enum {
		DEBUG_INFO,
		DECODER_ID,
		COMPO_START,
		UNDERSCAN,
	};

	struct option longopt[] = {
		{"debug", 0, NULL, DEBUG_INFO},
		{"dec-id", 1, NULL, DECODER_ID},
		{"plane", 1, NULL, 'p'},
		{"underscan", 1, NULL, UNDERSCAN},
		{"tunnel", 1, NULL, 't'},
		{"compo-start", 0, NULL, COMPO_START},
		{"std", 1, NULL, 's'},
		{"latency-profile", 1, NULL, 'l'},
		{"help", 0, NULL, 'h'},
		{0, 0, 0, 0}
	};

	memset(&compo_sigaction, 0, sizeof(compo_sigaction));
	compo_sigaction.sa_sigaction = compo_sighandler;
	compo_sigaction.sa_flags = SA_SIGINFO;
	sigaction(SIGINT, &compo_sigaction, NULL);

	/*
	 * Set the default properties of compo capture context
	 */
	cap_ctx.disp_std = PAL;
	cap_ctx.tunnel = true;
	cap_ctx.latency_profile = V4L2_CID_MPEG_STI_CAPTURE_PROFILE_COMPO_NO_AUD_VID_FRC;

	/*
	 * Open the media device
	 */
	ret = stm_v4l2_media_open(0, &cap_ctx.media);
	if (ret) {
		printf("%s(): failed to open media device 0\n", __func__);
		goto exit;
	}

	while ((option = getopt_long(argc, argv, "t:p:hs:", longopt, NULL)) != -1) {

		switch(option) {
		case DEBUG_INFO:
			cap_ctx.debug_info = true;
			break;

		case DECODER_ID:
			decoder_id = atoi(optarg);
			snprintf(cap_ctx.decoder_name, sizeof(cap_ctx.decoder_name) - 1, "v4l2.video%d", decoder_id);
			snprintf(cap_ctx.compo_name, sizeof(cap_ctx.compo_name) - 1, "compo-capture0");
			break;

		case 'p':
			strncpy(cap_ctx.data.plane, optarg, sizeof(cap_ctx.data.plane) - 1);
			cap_ctx.data.plane[strlen(cap_ctx.data.plane)] = '\0';
			break;

		case UNDERSCAN:
			cap_ctx.underscan = atoi(optarg);
			break;

		case 'l':
			cap_ctx.latency_profile = atoi(optarg);
			if ((cap_ctx.latency_profile < 5) || (cap_ctx.latency_profile > 6)) {
				printf("%s(): only latency profile 5 and 6 are valid\n", __func__);
				goto media_exit;
			}
			break;

		case 't':
		{
			int val = atoi(optarg);

			if (val == 1)
				break;

			if (val < 0 || val > 1)
				goto media_exit;

			if (strlen(cap_ctx.decoder_name)) {
				printf("%s(): options not supported with non-tunneled grab\n", __func__);
				goto media_exit;
			}

			cap_ctx.tunnel = false;

			break;
		}

		case COMPO_START:
			break;

		case 's':
			if (!strncasecmp(optarg, "pal", strlen("pal")))
				cap_ctx.disp_std = PAL;
			else if (!strncasecmp(optarg, "ntsc", strlen("ntsc")))
				cap_ctx.disp_std = NTSC;
			else {
				ret = -EINVAL;
				goto media_exit;
			}
			break;

		case 'h':
		default:
			usage();
			goto media_exit;
		}
	}

	/*
	 * Start the compo-capture
	 */
	if (cap_ctx.tunnel) {

		ret = compo_capture_setup_tunneled();
		if (ret) {
			printf("%s(): failed to setup devices for tunneled data flow\n", __func__);
			goto media_exit;
		}

	} else {
		if (!strlen(cap_ctx.data.plane)) {
			printf("%s(): --plane, -p missing\n", __func__);
			usage();
			goto exit;
		}

		ret = pthread_create(&compo_user_grab_thread, NULL, compo_user_grab_start, &cap_ctx.data);
		if (ret) {
			printf("%s(): failed to start compo capture in non-tunneled mode\n", __func__);
			goto media_exit;
		}
	}

	/*
	 * Start keyboard thread for runtime options
	 */
	ret = pthread_create(&keyboard_thread, NULL, keyboard_monitor, &cap_ctx.data);
	if (ret) {
		printf("%s(): failed to create keyboard thread\n", __func__);
		goto media_exit;
	}

	if (compo_thread)
		pthread_join(compo_thread, NULL);
	if (compo_user_grab_thread)
		pthread_join(compo_user_grab_thread, NULL);

	pthread_cancel(keyboard_thread);
	pthread_join(keyboard_thread, NULL);

	printf("\n");

media_exit:
	stm_v4l2_media_close(cap_ctx.media);
exit:
	return ret;
}
