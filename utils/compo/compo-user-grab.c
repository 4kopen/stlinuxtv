/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the STLinuxTV Library.

STLinuxTV is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

STLinuxTV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The STLinuxTV Library may alternatively be licensed under a proprietary
license from ST.
 * Compo-capture from user-space
************************************************************************/
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdbool.h>

#include "stm_v4l2_capture.h"
#include "stm_v4l2_output.h"
#include "compo-capture.h"

#define COMPO_CAPTURE_BUF_COUNT		1
#define COMPO_CAPTURE_WIDTH		600
#define COMPO_CAPTURE_HEIGHT		800
#define COMPO_CAPTURE_PIXFMT		V4L2_PIX_FMT_BGR24
#define COMPO_CAPTURE_COLORSPACE	V4L2_COLORSPACE_SRGB
#define COMPO_CAPTURE_FIELD		V4L2_FIELD_NONE
#define COMPO_CAPTURE_PIXEL_SIZE	3 /* V4L2_PIX_FMT_BGR24 */

static int compo_buf_num = 1;
static int display_buf_num = 2;

struct buf_metadata {
	void *ptr;
	int length;
};

int setup_mmap_buffers(int fd, int count, struct buf_metadata *meta)
{
	int i, ret;
	struct v4l2_buffer buf;

	for (i = 0; i < count; i++) {
		memset(&buf, 0, sizeof(buf));
		buf.index = i;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
		ret = ioctl(fd, VIDIOC_QUERYBUF, &buf);
		if (ret) {
			printf("%s(): failed to querybuf display at index %d\n", __func__, i);
			goto setup_done;
		}
		meta[i].ptr = mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buf.m.offset);
		if (meta[i].ptr == MAP_FAILED) {
			printf("%s(): failed to mmap region for compo capture\n", __func__);
			ret = errno;
			goto mmap_failed;
		}
		meta[i].length = buf.length;
	}

	return 0;

mmap_failed:
	for (i = i - 1; i > 0;)
		munmap(meta[i].ptr, meta[i].length);
setup_done:
	return ret;
}

/**
 * compo_user_grab_start() - compo capture thread
 */
void *compo_user_grab_start(void *arg)
{
	struct v4l2_format fmt;
	struct buf_metadata *meta;
	struct v4l2_buffer compo_buf;
	struct compo_capture_data *data = arg;
	int i, count, ret, compo_fd, display_fd;

	count = compo_buf_num > display_buf_num ? compo_buf_num : display_buf_num;

	/*
	 * Allocate all the buffers/buffer context
	 */
	meta = malloc(sizeof(*meta) * count);
	if (!meta) {
		printf("%s(): alloc failed for metadata\n", __func__);
		goto alloc_meta_failed;
	}

	/*
	 * Setup the compo-capture for grabbing format
	 */
	compo_fd = stm_v4l2_capture_set_input("Compo", "stm_input_mix1");
	if (compo_fd < 0) {
		printf("%s(): failed to find correct compo instance for grab\n", __func__);
		ret = errno;
		goto grab_setup_failed;
	}

	memset(&fmt, 0, sizeof(fmt));
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width = COMPO_CAPTURE_WIDTH;
	fmt.fmt.pix.height = COMPO_CAPTURE_HEIGHT;
	fmt.fmt.pix.pixelformat = COMPO_CAPTURE_PIXFMT;
	fmt.fmt.pix.colorspace = COMPO_CAPTURE_COLORSPACE;
	fmt.fmt.pix.field = COMPO_CAPTURE_FIELD;
	fmt.fmt.pix.bytesperline = COMPO_CAPTURE_WIDTH * COMPO_CAPTURE_PIXEL_SIZE;
	ret = stm_v4l2_capture_streamon(compo_fd, compo_buf_num, V4L2_MEMORY_USERPTR, &fmt);
	if (ret) {
		printf("%s(): failed to setup compo fmt and streamon\n", __func__);
		goto grab_streamon_failed;
	}

	/*
	 * Setup the stmvout driver for displaying the captured data
	 */
	display_fd = stm_v4l2_setup_output("Planes", data->plane);
	if (display_fd < 0) {
		printf("%s(): failed to find plane for displaying data\n", __func__);
		goto setup_output_failed;
	}

	memset(&fmt, 0, sizeof(fmt));
	fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	fmt.fmt.pix.width = COMPO_CAPTURE_WIDTH;
	fmt.fmt.pix.height = COMPO_CAPTURE_HEIGHT;
	fmt.fmt.pix.pixelformat = COMPO_CAPTURE_PIXFMT;
	fmt.fmt.pix.colorspace = COMPO_CAPTURE_COLORSPACE;
	fmt.fmt.pix.field = COMPO_CAPTURE_FIELD;
	fmt.fmt.pix.bytesperline = COMPO_CAPTURE_WIDTH * COMPO_CAPTURE_PIXEL_SIZE;
	ret = stm_v4l2_output_streamon(display_fd, display_buf_num, V4L2_MEMORY_MMAP, &fmt);
	if (ret) {
		printf("%s(): failed to setup and streamon the plane\n", __func__);
		goto display_streamon_failed;
	}

	/*
	 * mmap the address from display driver to be passed to compo-capture
	 */
	ret = setup_mmap_buffers(display_fd, display_buf_num, meta);
	if (ret) {
		printf("%s(): failed to setup buffers for compo capture\n", __func__);
		goto mmap_failed;
	}

	memset(&compo_buf, 0, sizeof(compo_buf));
	compo_buf.index = 0;
	compo_buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	compo_buf.field = COMPO_CAPTURE_FIELD;
	compo_buf.memory = V4L2_MEMORY_MMAP;
	compo_buf.length = meta[0].length;

	/*
 	 * Queue one buffer to display, so, that the first captured fram is displayed
	 */
	ret = ioctl(display_fd, VIDIOC_QBUF, &compo_buf);
	if (ret) {
		printf("%s(): failed to start display pipe\n", __func__);
		goto qbuf_failed;
	}

	/*
	 * Let's rock and roll
	 */
	i = 1;
	while (!data->exit) {
		/*
		 * Start capturing data
		 */
		compo_buf.index = 0;
		compo_buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		compo_buf.field = COMPO_CAPTURE_FIELD;
		compo_buf.memory = V4L2_MEMORY_USERPTR;
		compo_buf.m.userptr = (unsigned long)meta[i].ptr;
		compo_buf.length = meta[i].length;
		ret = ioctl(compo_fd, VIDIOC_QBUF, &compo_buf);
		if (ret) {
			printf("%s(): failed to qbuf to compo\n", __func__);
			goto qbuf_failed;
		}
		ret = ioctl(compo_fd, VIDIOC_DQBUF, &compo_buf);
		if (ret) {
			printf("%s(): failed to dqbuf from compo\n", __func__);
			goto qbuf_failed;
		}

		/*
		 * Start displaying data
		 */
		compo_buf.index = i;
		compo_buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
		compo_buf.memory = V4L2_MEMORY_MMAP;
		memset(&compo_buf.m, 0, sizeof(compo_buf.m));
		memset(&compo_buf.timestamp, 0, sizeof(compo_buf.timestamp));
		ret = ioctl(display_fd, VIDIOC_QBUF, &compo_buf);
		if (ret) {
			printf("%s(): failed to queue to display\n", __func__);
			goto qbuf_failed;
		}
		ret = ioctl(display_fd, VIDIOC_DQBUF, &compo_buf);
		if (ret) {
			printf("%s(): failed to dqbuf from display\n", __func__);
			goto qbuf_failed;
		}
		i = compo_buf.index;
	}

qbuf_failed:
	for (i = 0; i < count; i++)
		munmap(meta[i].ptr, meta[i].length);
mmap_failed:
	ioctl(display_fd, VIDIOC_STREAMOFF, &fmt.type);
display_streamon_failed:
	close(display_fd);
setup_output_failed:
	ioctl(compo_fd, VIDIOC_STREAMOFF, &fmt.type);
grab_streamon_failed:
	close(compo_fd);
grab_setup_failed:
	free(meta);
alloc_meta_failed:
	return NULL;
}
