##########################################################################
# Copyright (C) 2015 STMicroelectronics. All Rights Reserved.
#
# This file is part of the STLinuxTV Library.
#
# STLinuxTV is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License version 2 as published by the
# Free Software Foundation.
#
# STLinuxTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with player2; see the file COPYING.  If not, write to the Free Software
# Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# The STLinuxTV Library may alternatively be licensed under a proprietary
# license from ST.
# * resize information of compo-capture.
##########################################################################
# Comments are inserted in this file using # only
# Syntax:
# [TAG] : This is the main headline under which some recognized params
#         values can be passed
#
# Recognized params: capture_width, capture height.
#                    Special names: plane_width and plane_height indicates
#                    to application that deduce this from PAL or NTSC conf.
# <variable><space>=<space><value>

# Resize info for Lite planes
[LITE]
[4K: NTSC: PAL]
capture_width = -1
capture_height = -1

# Every line is space sensitive. The syntax is:
# resolution,<space>resolution:<space>NTSC:<space>PAL
# Both NTSC and PAL are there, as it is common resize
# information
[1080p50, 1080p60, 720p50, 720p60, 1080i50: NTSC: PAL]
capture_width = plane_width
capture_height = plane_height

[1080i60: NTSC]
capture_width = plane_width
capture_height = plane_height

[1080i60: PAL]
capture_width = -1
capture_height = -1

[480i: NTSC: PAL]
capture_width = plane_width
capture_height = plane_height

[576i: NTSC: PAL]
capture_width = plane_width
capture_height = plane_height

# Rest all full planes
[FULL]
[4K: NTSC: PAL]
capture_width = 1440
capture_height = 720

# Every line is space sensitive. The syntax is:
# resolution,<space>resolution:<space>NTSC:<space>PAL
# Both NTSC and PAL are there, as it is common resize
# information
[1080p50, 1080p60: NTSC: PAL]
capture_width = 960
capture_height = 864

[1080i50, 1080i60: NTSC: PAL]
capture_width = 960
capture_height = 540

[720p50, 720p60: NTSC: PAL]
capture_width = 960
capture_height = 720

[480i: NTSC: PAL]
capture_width = plane_width
capture_height = plane_height

[576i: NTSC: PAL]
capture_width = plane_width
capture_height = plane_height

[CONF]
# aspect ratio can be 16:9, 4:3, 14:9
aspect_ratio = 16:9
