/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include <getopt.h>
#include <stdlib.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <linux/fb.h>
#include <linux/videodev2.h>

#include "linux/include/linux/stmvout.h"
#include "utils/v4l2_helper.h"
 
#define V4L2_DISPLAY_DRIVER_NAME	"Planes"
#define V4L2_DISPLAY_CARD_NAME		"STMicroelectronics"

/* This is a simple program to test V4L2 zorder ioctls */
static void
usage (const char * const progname)
{
  fprintf (stderr,
           "%s: outputname [z value]:\n",
           progname);
}


static __u32
zorder_get (int videofd)
{
  struct v4l2_control cntrl;

  cntrl.id = V4L2_CID_STM_Z_ORDER;

  LTV_CHECK (ioctl (videofd, VIDIOC_G_CTRL, &cntrl));
  return cntrl.value;
}

static int
zorder_set (int videofd, int value)
{
  struct v4l2_control cntrl;
  int ret;

  cntrl.id    = V4L2_CID_STM_Z_ORDER;
  cntrl.value = value;

  ret = LTV_CHECK (ioctl (videofd, VIDIOC_S_CTRL, &cntrl));

  return ret;
}


int
main (int    argc,
      char **argv)
{
  int   val_z;
  int   videofd;
  char *endptr;

  if(argc<2 || argc >3)
  {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  videofd = LTV_CHECK (v4l2_open_by_name (V4L2_DISPLAY_DRIVER_NAME, V4L2_DISPLAY_CARD_NAME, O_RDWR));
  v4l2_list_outputs (videofd);

  v4l2_set_output_by_name (videofd, argv[1]);

  printf ("zorder for %s: %u\n",
          argv[1],
          zorder_get (videofd));

  if(argc == 3)
  {
    val_z = strtol (argv[2], &endptr, 10);
    if ((errno == ERANGE && (val_z == LONG_MAX || val_z == LONG_MIN))
        || (errno != 0 && val_z == 0)) {
      perror ("strtol");
      exit (EXIT_FAILURE);
    }

    if (zorder_set (videofd,  val_z) < 0)
	exit (EXIT_FAILURE);

  }

  exit (EXIT_SUCCESS);
}
