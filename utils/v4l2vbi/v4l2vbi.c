/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/poll.h>

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <getopt.h>

#include <linux/fb.h>
#include <linux/videodev2.h>

#include "linux/include/linux/stmvout.h"
#include "utils/v4l2_helper.h"

#include "ttx_data.h"

/* TRACE and DEBUG macros */
#define print_msg(fmt,arg...) printf(fmt,##arg)
#ifdef DEBUG
#define debug_msg(fmt,arg...) printf(fmt,##arg)
#else
#define debug_msg(fmt,arg...)
#endif

/* STM VBI constants */
#define STM_VBI_DEVICE_FLAGS      (O_RDWR)
#define STM_VBI_MAX_FIELDS               2
#define STM_VBI_TTX_MAX_LINES_PER_FIELD 18
#define STM_VBI_TTX_FIRST_LINE           6
#define STM_VBI_TTX_LAST_LINE           22

typedef struct ttx_context_s
{
  /* v4l2 device */
  int                         v4lfd;
  /* data buffers */
  struct v4l2_format          fmt;
  struct v4l2_buffer         *buffer;
  struct v4l2_sliced_vbi_data bufferdata[(STM_VBI_MAX_FIELDS*STM_VBI_TTX_MAX_LINES_PER_FIELD)];
  /* CC data */
  unsigned char              *ttx_data;
  unsigned long               ttx_size;
  /* CC lines and field */
  unsigned int                ttx_linemask;
  unsigned int                ttx_field;
  /* Timings */
  struct timeval              ptime;
  unsigned long long          frameduration;
} ttx_context_t;

static struct option long_options[] = {
        { "delay"              , 1, 0, 'd' },
        { "help"               , 0, 0, 'h' },
        { "iterations"         , 1, 0, 'i' },
        { "vbidev"             , 1, 0, 'v' },
        { "field"              , 1, 0, 'f' },
        { "output"             , 1, 0, 'o' },
        { 0, 0, 0, 0 }
};

/*
 * This is a simple program to test V4L2 sliced VBI output of EBU teletext data.
 *
 * The code is deliberately written in a linear, straight forward, way to show
 * the step by step usage of V4L2 to display EBU teletext.
 */
static void usage(void)
{
    fprintf(stderr,"Usage: v4l2vbi [options]\n");
    fprintf(stderr,"\t-d,--delay num       - Delay of first buffer in seconds, default is 0\n");
    fprintf(stderr,"\t-h,--help            - Display this message\n");
    fprintf(stderr,"\t-i,--iterations num  - Do num interations, default is 5\n");
    fprintf(stderr,"\t-v,--vbidev num      - Open V4L2 device /dev/vbiNUM, default is 0\n");
    fprintf(stderr,"\t-f,--field num       - TTX Field : 0,1 or 2 for both, default is 0\n");
    fprintf(stderr,"\t-o,--output num      - Output id, default is 1 (Aux)\n");
    fprintf(stderr,"\t-m,--linemask        - TTX Line Mask\n");
    fprintf(stderr,"\t-p,--pattern         - Use a TTX pattern data\n");
    exit(1);
}

#ifdef DEBUG
static void print_services(int services)
{
  if(services == 0)
    printf("None");

  if(services & V4L2_SLICED_TELETEXT_B)
    printf("TeletextB ");
  if(services & V4L2_SLICED_CAPTION_525)
    printf("CC ");
  if(services & V4L2_SLICED_VPS)
    printf("VPS ");
  if(services & V4L2_SLICED_WSS_625)
    printf("WSS625 ");

  printf("\n");
}
#endif

static int stm_ttx_adjust_timestamp(ttx_context_t *ttx_context, int udelay)
{
  struct timespec currenttime;

  if(udelay < 0) {
    ttx_context->ptime.tv_sec   = 0;
    ttx_context->ptime.tv_usec  = 0;
  }
  else if(STM_VBI_DEVICE_FLAGS & O_NONBLOCK) {
      ttx_context->ptime.tv_sec   = 0;
      ttx_context->ptime.tv_usec  = 0;
      usleep(udelay);
  }
  else {
    /*
     * Work out the presentation time of the first buffer to be queued.
     * Note the use of the posix monotomic clock NOT the adjusted wall clock.
     */
    clock_gettime(CLOCK_MONOTONIC, &currenttime);

    debug_msg("%s: Current time = %ld.%06ld\n", __func__, currenttime.tv_sec,
             currenttime.tv_nsec / 1000);

    ttx_context->ptime.tv_sec   = currenttime.tv_sec;
    ttx_context->ptime.tv_usec  = (currenttime.tv_nsec / 1000) + udelay;
    while (ttx_context->ptime.tv_usec >= 1000000L) {
      ttx_context->ptime.tv_sec++;
      ttx_context->ptime.tv_usec -= 1000000L;
    }
  }

  if(ttx_context->buffer) {
    ttx_context->buffer->timestamp = ttx_context->ptime;
  }

  return 0;
}

static int stm_ttx_push_data_to_display(ttx_context_t *ttx_context, unsigned char *ttx_data, unsigned long ttx_size)
{
  int                         read_pos = 0;
  int                         field;
  int                         first_field;
  int                         last_field;

  print_msg("%s: in  - ctx = %p, time = %ld.%06ld\n",__func__, ttx_context,
        ttx_context->ptime.tv_sec,
        ttx_context->ptime.tv_usec);

  ttx_context->ttx_data       = ttx_data;
  ttx_context->ttx_size       = ttx_size;

#ifdef DEBUG_CC_CONTEXT
  fprintf(stdout, "%s: v4lfd=%d, fmt=%p, buffer=%p\n",__func__
                , ttx_context->v4lfd, &ttx_context->fmt, ttx_context->buffer);
  fprintf(stdout, "%s: data=%p, size=%lu\n",__func__
                , ttx_context->ttx_data, ttx_context->ttx_size);
  fprintf(stdout, "%s: linemask=%x, fields=%d\n",__func__
                , ttx_context->ttx_linemask, ttx_context->ttx_field);
#endif


  if(ttx_context->ttx_field == STM_VBI_MAX_FIELDS) {
    first_field = 0;
    last_field = 2;
  }
  else {
    first_field = ttx_context->ttx_field;
    last_field = ttx_context->ttx_field+1;
  }

  while(read_pos<ttx_context->ttx_size)
  {
    struct v4l2_buffer  dqbuffer;
    int data_offset = 0;
    int ttx_line;

    memset(ttx_context->bufferdata, 0, (STM_VBI_TTX_MAX_LINES_PER_FIELD * sizeof(struct v4l2_sliced_vbi_data)));

    /* STEP is 48 : 43 + 5 do we need a padding for FDMA TTX ??*/
    for(ttx_line=STM_VBI_TTX_FIRST_LINE;ttx_line<=STM_VBI_TTX_LAST_LINE;ttx_line++) {
      if(ttx_context->ttx_linemask & (1L << ttx_line)) {
        int step = 0;
        for(field=first_field; field<last_field; field++) {
        /* Set field data */
          if(ttx_context->fmt.fmt.sliced.service_lines[field][ttx_line] == V4L2_SLICED_TELETEXT_B) {
            debug_msg("Setting bufferdata[%02d] for field %d line %02d, off=%d\n", data_offset, field, ttx_line, read_pos);
            ttx_context->bufferdata[data_offset].id      = V4L2_SLICED_TELETEXT_B;
            ttx_context->bufferdata[data_offset].line    = ttx_line;
            ttx_context->bufferdata[data_offset].field   = field;
            memcpy(ttx_context->bufferdata[data_offset].data, &ttx_context->ttx_data[read_pos], 48); /* Framing code + 42bytes of data + Padiing data */
            data_offset++;
            step++;
          }
        }
        if(step == 0) { /* No enabled fields for this line ?? */
          fprintf(stderr, "No enabled fields for this line %d ??\n",ttx_line);
          return -EINVAL;
        }
      }
    }

    /* queue buffer */
    if(LTV_CHECK (ioctl (ttx_context->v4lfd, VIDIOC_QBUF, ttx_context->buffer)) < 0)
      return -errno;

    debug_msg("Queued buffer %d\n",(read_pos/2));

    /*
     * To dequeue a streaming memory buffer you must set the buffer type
     * AND and the memory type correctly, otherwise the API returns an error.
     */
    dqbuffer.type   = V4L2_BUF_TYPE_SLICED_VBI_OUTPUT;
    dqbuffer.memory = V4L2_MEMORY_USERPTR;

    /*
     * We didn't open the V4L2 device non-blocking so dequeueing a buffer will
     * sleep until a buffer becomes available.
     */
    if(STM_VBI_DEVICE_FLAGS & O_NONBLOCK) {
      while(ioctl (ttx_context->v4lfd, VIDIOC_DQBUF, &dqbuffer) == -1) {
        usleep (2 * ttx_context->frameduration);
      }
    }
    else {
      if(LTV_CHECK (ioctl (ttx_context->v4lfd, VIDIOC_DQBUF, &dqbuffer)) < 0)
        return -errno;
    }

    debug_msg("Dequeued buffer %d\n",(read_pos/2));

    read_pos+=48;
  }

  debug_msg("%s: out - ctx = %p, time = %ld.%06ld\n",__func__, ttx_context,
              ttx_context->ptime.tv_sec,
              ttx_context->ptime.tv_usec);

  return 0;
}

static int stm_ttx_set_fmt(ttx_context_t *ttx_context)
{
  int                         field, ttx_line;
  int                         first_field;
  int                         last_field;
  struct v4l2_format          fmt;
  struct v4l2_sliced_vbi_cap  vbicap;

  debug_msg("%s: in  - ctx = %p\n",__func__, ttx_context);
#ifdef DEBUG_CC_CONTEXT
    fprintf(stdout, "%s: v4lfd=%d, linemask=%x, fields=%d\n",__func__
                  , ttx_context->v4lfd, ttx_context->ttx_linemask, ttx_context->ttx_field);
#endif

  vbicap.type = V4L2_BUF_TYPE_SLICED_VBI_OUTPUT;
  if(LTV_CHECK (ioctl(ttx_context->v4lfd,VIDIOC_G_SLICED_VBI_CAP,&vbicap))<0)
    return -errno;

#ifdef DEBUG
    printf("VBI Capabilities\n");
    printf("----------------\n");
    printf("VBI services:"); print_services(vbicap.service_set);
    for(i=0;i<24;i++)
    {
      printf("Field0 Line%d:",i); print_services(vbicap.service_lines[0][i]);
    }
    for(i=0;i<24;i++)
    {
      printf("Field1 Line%d:",i); print_services(vbicap.service_lines[1][i]);
    }
#endif

  if(ttx_context->ttx_field == STM_VBI_MAX_FIELDS) {
    first_field = 0;
    last_field = 2;
  }
  else {
    first_field = ttx_context->ttx_field;
    last_field = ttx_context->ttx_field+1;
  }

  memset(&fmt, 0, sizeof(struct v4l2_format));

  fmt.type                   = V4L2_BUF_TYPE_SLICED_VBI_OUTPUT;
  fmt.fmt.sliced.service_set = 0; /* V4L2_SLICED_TELETEXT_B */;

  for(field=first_field; field<last_field; field++) {
    for(ttx_line=STM_VBI_TTX_FIRST_LINE;ttx_line<=STM_VBI_TTX_LAST_LINE;ttx_line++) {
      if(ttx_context->ttx_linemask & (1L << ttx_line)) {
        fmt.fmt.sliced.service_lines[field][ttx_line]  = V4L2_SLICED_TELETEXT_B;
      }
    }
  }

  /* set buffer format */
  if(LTV_CHECK (ioctl(ttx_context->v4lfd,VIDIOC_S_FMT,&fmt))<0)
    return -errno;

  /* get the buffer format */
  if(LTV_CHECK (ioctl(ttx_context->v4lfd,VIDIOC_G_FMT,&fmt))<0)
    return -errno;

#ifdef DEBUG
    printf("VBI Format\n");
    printf("----------\n");
    printf("VBI services:"); print_services(fmt.fmt.sliced.service_set);
    for(i=0;i<24;i++)
    {
      printf("Field0 Line%d:",i); print_services(fmt.fmt.sliced.service_lines[0][i]);
    }
    for(i=0;i<24;i++)
    {
      printf("Field1 Line%d:",i); print_services(fmt.fmt.sliced.service_lines[1][i]);
    }
#endif

  ttx_context->fmt = fmt;

  debug_msg("%s: out - ctx = %p\n",__func__, ttx_context);

  return 0;
}

static int stm_ttx_initialize_ttx_context(ttx_context_t *ttx_context, struct v4l2_buffer *buffer, unsigned int buf_count)
{
  int                         i;
  struct v4l2_buffer          dqbuffer;
  struct v4l2_requestbuffers  reqbuf;
  unsigned int                field,ttx_line;
  unsigned int                first_field;
  unsigned int                last_field;
  unsigned int                data_offset;

  debug_msg("%s: in  - ctx = %p\n",__func__, ttx_context);
#ifdef DEBUG_CC_CONTEXT
    fprintf(stdout, "%s: v4lfd=%d, linemask=%08x, fields=%d, count=%d\n",__func__
                  , ttx_context->v4lfd, ttx_context->ttx_linemask, ttx_context->ttx_field, buf_count);
#endif

  memset(buffer, 0, (buf_count * sizeof(struct v4l2_buffer)));

  /*
   * Request buffers to be allocated on the V4L2 device
   */
  memset(&reqbuf, 0, sizeof(reqbuf));
  reqbuf.type   = V4L2_BUF_TYPE_SLICED_VBI_OUTPUT;
  reqbuf.memory = V4L2_MEMORY_USERPTR;
  reqbuf.count  = buf_count;

  /* request buffers */
  if(LTV_CHECK (ioctl (ttx_context->v4lfd, VIDIOC_REQBUFS, &reqbuf))<0)
    return -errno;

  if(ttx_context->ttx_field == STM_VBI_MAX_FIELDS) {
    first_field = 0;
    last_field = 2;
  }
  else {
    first_field = ttx_context->ttx_field;
    last_field = ttx_context->ttx_field+1;
  }

  stm_ttx_adjust_timestamp(ttx_context, -1);

  for(i=0; i<buf_count; i++) {
    buffer[i].type      = V4L2_BUF_TYPE_SLICED_VBI_OUTPUT;
    buffer[i].memory    = V4L2_MEMORY_USERPTR;
    buffer[i].m.userptr = (unsigned long)ttx_context->bufferdata;
    memset(ttx_context->bufferdata, 0, (2 * sizeof(struct v4l2_sliced_vbi_data)));
    buffer[i].timestamp.tv_sec   = 0;
    buffer[i].timestamp.tv_usec  = 0;

    data_offset = 0;
    for(field=first_field; field<last_field; field++) {
      for(ttx_line=STM_VBI_TTX_FIRST_LINE;ttx_line<=STM_VBI_TTX_LAST_LINE;ttx_line++) {
        if(ttx_context->ttx_linemask & (1L << ttx_line)) {
          /* Set Field data */
          if(ttx_context->fmt.fmt.sliced.service_lines[field][ttx_line] == V4L2_SLICED_TELETEXT_B) {
            debug_msg("Clearing bufferdata[%02d] for field %d line %02d\n", data_offset, field, ttx_line);
            ttx_context->bufferdata[data_offset].id      = V4L2_SLICED_TELETEXT_B;
            ttx_context->bufferdata[data_offset].line    = ttx_line;
            ttx_context->bufferdata[data_offset].field   = field;
            ttx_context->bufferdata[data_offset].data[0] = 0xE4;          /* Framing code */
            memset(&ttx_context->bufferdata[data_offset].data[1], 0, 47); /* 42bytes of data + 5 padding bytes */
            data_offset++;
          }
        }
      }
    }

    debug_msg("%s: Queuing Buffer %d - ctx = %p\n",__func__, i, ttx_context);

    if(LTV_CHECK (ioctl (ttx_context->v4lfd, VIDIOC_QBUF, &buffer[i])) < 0)
      return -errno;
  }

  debug_msg("%s: Streaming On  - ctx = %p\n",__func__, ttx_context);

  /* start stream */
  if(LTV_CHECK (ioctl (ttx_context->v4lfd, VIDIOC_STREAMON, &buffer[0].type)) < 0)
    return -errno;

  for(i=0; i<buf_count; i++) {
    /*
     * To dequeue a streaming memory buffer you must set the buffer type
     * AND and the memory type correctly, otherwise the API returns an error.
     */
    dqbuffer.type   = V4L2_BUF_TYPE_SLICED_VBI_OUTPUT;
    dqbuffer.memory = V4L2_MEMORY_USERPTR;

    /*
     * We didn't open the V4L2 device non-blocking so dequeueing a buffer will
     * sleep until a buffer becomes available.
     */
    if(STM_VBI_DEVICE_FLAGS & O_NONBLOCK) {
      while(ioctl (ttx_context->v4lfd, VIDIOC_DQBUF, &dqbuffer) == -1) {
        usleep (2 * ttx_context->frameduration);
      }
    }
    else {
      if(LTV_CHECK (ioctl (ttx_context->v4lfd, VIDIOC_DQBUF, &dqbuffer)) < 0)
        return -errno;
    }

    debug_msg("Dequeued buffer %d\n",i);
  }

  ttx_context->buffer = buffer;

  debug_msg("%s: out - ctx = %p\n",__func__, ttx_context);

  return 0;
}

int main(int argc, char **argv)
{
  int           v4lfd;
  int           vbidevice;
  char          vbiname[32];
  int           outputid;
  int           iterations;
  int           delay;
  int           i;
  int           option;

  int           ttx_field = STM_VBI_MAX_FIELDS; /* All fields */
  int           linemask;
  int           ttx_linemask = (0x1FFFF<<STM_VBI_TTX_FIRST_LINE); /* Full lines */
  int           use_pattern = 0;
  unsigned int  pattern = 0xA5, ttx_test_size = 0;
  unsigned char *ttx_test_data;

  struct v4l2_buffer          buffer[1];

  ttx_context_t ttx_context;

  delay      = 0;
  vbidevice  = 0;
  outputid   = 1; /* Default output is Aux */
  iterations = 10;

  memset(&ttx_context, 0, sizeof(ttx_context_t));

  while((option = getopt_long (argc, argv, "d:i:v:f:o:m:p:h", long_options, NULL)) != -1)
  {
    switch(option)
    {
      case 'd':
        delay = atoi(optarg);
        if(delay<0)
          usage();

        break;
      case 'h':
        usage();

      case 'i':
        iterations = atoi(optarg);
        if(iterations<10)
        {
          fprintf(stderr,"Require a minimum of 5 iterations\n");
          usage();
        }

        break;
      case 'v':
        vbidevice = atoi(optarg);
        if(vbidevice<0 || vbidevice>255)
        {
          fprintf(stderr,"Vbi device number out of range\n");
          usage();
        }

        break;
       case 'f':
         ttx_field = atoi(optarg);
         if(ttx_field<0 || ttx_field>STM_VBI_MAX_FIELDS)
         {
           fprintf(stderr,"Field value out of range\n");
           usage();
         }

         break;
      case 'o':
        outputid = atoi(optarg);
        if(outputid<0 || outputid>255)
        {
          fprintf(stderr,"output id number out of range\n");
          usage();
        }

        break;
      case 'm':
        if(sscanf(optarg, "%x", &linemask)>0) {
          ttx_linemask = ((linemask & 0x1FFFF)<<STM_VBI_TTX_FIRST_LINE);
        }

        break;
      case 'p':
        if(sscanf(optarg, "%x", &pattern)>0) {
          use_pattern = 1;
        }

        break;
      default:
        fprintf(stderr,"Unknown option '%s'\n",*argv);
        usage();
    }
  }

  /*
   * Open the requested V4L2 device
   */
  snprintf(vbiname, sizeof(vbiname), "/dev/vbi%d", vbidevice);
  if((v4lfd = open(vbiname, STM_VBI_DEVICE_FLAGS)) < 0)
  {
    perror("Unable to open video device");
    goto exit;
  }

  fprintf(stdout,"\nTeletext test is started :\n * linemask is '0x%08X'\n * field is %d\n", ttx_linemask, ttx_field);

#ifdef DEBUG
    v4l2_list_outputs(v4lfd);
#endif

  /* Select the mixer output */
  if ((v4l2_set_output_by_index (v4lfd, outputid)) == -1)
    goto exit;

  ttx_context.v4lfd         = v4lfd;
  ttx_context.ttx_linemask  = ttx_linemask;
  ttx_context.ttx_field     = ttx_field;
  ttx_context.frameduration = 16666;

  if(stm_ttx_set_fmt(&ttx_context)<0)
    goto exit;

  /* Initialize the context with new buffer */
  if(stm_ttx_initialize_ttx_context(&ttx_context, buffer, (sizeof(buffer)/sizeof(struct v4l2_buffer))) < 0) {
    goto streamoff;
  }

  if(delay) {
    stm_ttx_adjust_timestamp(&ttx_context, (delay * 1000000L));
  }

  if(use_pattern) {
    unsigned char pattern_data[100*48];
    ttx_test_size = sizeof(pattern_data);
    for(i=0; i< ttx_test_size; i++)
      pattern_data[i] = pattern;
    for(i=0; i< ttx_test_size; i+=48)
      pattern_data[i] = 0xE4;
    ttx_test_data = pattern_data;
  }
  else {
    ttx_test_size = sizeof(ttx_data);
    ttx_test_data = ttx_data;
  }

  for(i=0;i<iterations;i++)
  {
    if(stm_ttx_push_data_to_display(&ttx_context, ttx_test_data, ttx_test_size) < 0)
      goto streamoff;
  }

streamoff:
  if(ttx_context.buffer) {
    LTV_CHECK (ioctl(v4lfd, VIDIOC_STREAMOFF, &ttx_context.buffer[0].type));
  }

exit:
  return 0;
}
