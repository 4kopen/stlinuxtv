#! /usr/bin/env bash

stv_version=1.0

################################################################################
# Trace help (when run from Main script)
function usage_stv()
{
    echo -e "  STLinuxTV Debug Script $stv_version "
    echo -e "\n $BLUE Usage: <file> --group=<val> --level=<level_num> --module=<stv> $NC\n"
    echo "  Set STLinuxTV Trace Groups (specified with --type) with Level (specified with --level) for Debugging"
    echo "  Switch On/Off STLinuxTV Traces for Debugging"
    echo -e " $BLUE  --type=<val> $NC"
    echo "    Specify the type value to be used. **Mandatory**"
    echo "    Possible <val>:"
    echo "    (a)pi  		: API traces"
    echo "    (t)e   		: Transport Engine related traces"
    echo "    (s)e   		: Streaming Engine related traces"
    echo "    t(r)anscode	: Transcode related traces"
    echo "    (h)dmirx		: HDMIRX related traces"
    echo " "
    echo -e " $BLUE  --level=<level_val>: $NC "
    echo "    Starts printing level_num information about the state of the application. **Optional**"
    echo "    If no level provided, Auto value will be used (RECOMMENDED when not sure)"
    echo "    (D)ISABLE"
    echo "    (L)IGHT"
    echo "    (A)UTO"
    echo "    (V)ERBOSE"
    echo "    (E)XTENSIVE"
    echo " "
    echo -e " $BLUE  --help/-h/-? $NC"
    echo "    Prints this information"
}

################################################################################
# Validate input parameters (currently only prompts when no argument passed)
function validate_stv_trace_inputs ()
{
    # if No arument is passed, then it's issue
    if [ $# == 0 ]
    then
        echo -e "$RED process_stv_trace : Invalid input : no option passed $NC">&2
        process_stv_help
	exit
        return -1
    fi
}

################################################################################
# Enable API traces
function stv_enable_api_traces ()
{
    echo "Enabling API Traces"
    echo 'func snd_card_pseudo_pcm_trigger +p' > $DBFS/dynamic_debug/control
    echo 'func snd_card_pseudo_pcm_prepare +p' > $DBFS/dynamic_debug/control
    echo 'func snd_card_pseudo_hw_params +p' > $DBFS/dynamic_debug/control
    echo 'func snd_card_pseudo_hw_free +p' > $DBFS/dynamic_debug/control
    echo 'func snd_card_pseudo_pcm_mmap_data_fault +p' > $DBFS/dynamic_debug/control
    echo 'func snd_card_pseudo_pcm_mmap +p' > $DBFS/dynamic_debug/control
    echo 'func snd_card_pseudo_playback_open +p' > $DBFS/dynamic_debug/control
    echo 'func snd_card_pseudo_playback_close +p' > $DBFS/dynamic_debug/control
    echo 'func snd_card_pseudo_pcm_pointer +p' > $DBFS/dynamic_debug/control
    echo 'func stm_dvb_ca_ioctl +p' > $DBFS/dynamic_debug/control
    echo 'func stm_dvb_ca_release +p' > $DBFS/dynamic_debug/control
    echo 'func stmcec_open +p' > $DBFS/dynamic_debug/control
    echo 'func stmcec_release +p' > $DBFS/dynamic_debug/control
    echo 'func stmcec_ioctl +p' > $DBFS/dynamic_debug/control
    echo 'func ipfe_release +p' > $DBFS/dynamic_debug/control
    echo 'func dvb_ipfe_do_ioctl +p' > $DBFS/dynamic_debug/control
    echo 'func AudioOpen +p' > $DBFS/dynamic_debug/control
    echo 'func AudioRelease +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_ioctl +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_mmap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_open +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_close +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_enum_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_g_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_g_fmt_vid_overlay +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_s_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_s_fmt_vid_overlay +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_try_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_vid_reqbufs +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_querybuf +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_vid_streamon +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_streamoff +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_enum_input +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_g_input +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_s_input +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_enumaudio +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_s_crop +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_g_audio +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_s_audio +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_g_crop +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_cropcap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_g_parm +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_s_dv_timings +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_g_dv_timings +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_g_fbuf_vid_overlay +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_s_fbuf_vid_overlay +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_subscribe_events +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_unsubscribe_events +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_capture_expbuf +p' > $DBFS/dynamic_debug/control
    echo 'func VideoOpen +p' > $DBFS/dynamic_debug/control
    echo 'func VideoRelease +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_enum_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_g_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_s_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_try_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_reqbufs +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_querybuf +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_streamon +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_streamoff +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_enum_input +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_g_input +p' > $DBFS/dynamic_debug/control
    echo 'func dvp_v4l2_s_input +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_unlocked_ioctl +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_mmap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_open +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_pixel_cap_release +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_enum_fmt_vid_out +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_g_fmt_vid_out +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_s_fmt_vid_out +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_try_fmt_vid_out +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_cropcap +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_g_crop +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_s_crop +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_g_parm +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_reqbufs +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_querybuf +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_streamon +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_streamoff +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_g_output +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_s_output +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_enum_output +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_expbuf +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_default_ioctl +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_unlocked_ioctl +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_mmap +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_open +p' > $DBFS/dynamic_debug/control
    echo 'func sti_v4l2_plane_release +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_enum_output +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_enum_input +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_enum_audout +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_enum_audio +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_g_output +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_s_output +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_g_input +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_s_input +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_s_audout +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_s_audio +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_enum_fmt +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_try_fmt +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_g_fmt +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_s_fmt +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_s_parm +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_reqbufs +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_querybuf +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_streamon +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_streamoff +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_open +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_close +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_encoder_mmap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_hdmi_g_ext_ctrls +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_hdmi_s_ext_ctrls +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_hdmi_subscribe_event +p' > $DBFS/dynamic_debug/control
    echo 'func v4l2_event_unsubscribe +p' > $DBFS/dynamic_debug/control
    echo 'func stm_v4l2_hdmi_default +p' > $DBFS/dynamic_debug/control
    echo 'func v4l2_fh_open +p' > $DBFS/dynamic_debug/control
    echo 'func v4l2_fh_release +p' > $DBFS/dynamic_debug/control
    echo 'func stm-c8jpg +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_enum_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_enum_fmt_vid_out +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_g_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_g_fmt_vid_out +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_s_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_s_fmt_vid_out +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_try_fmt_vid_cap +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_try_fmt_vid_out +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_reqbufs +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_querybufs +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_streamon +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_streamoff +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_g_priority +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_s_priority +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_open +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_release +p' > $DBFS/dynamic_debug/control
    echo 'func stm_c8jpg_mmap +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_enum_fmt +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_try_fmt +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_s_fmt_output +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_s_fmt_capture +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_g_fmt +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_queryctrl +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_g_index +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_reqbufs +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_querybuf +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_streamon +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_streamoff +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_open +p' > $DBFS/dynamic_debug/control
    echo 'func tsmux_close +p' > $DBFS/dynamic_debug/control
    echo 'func stmvbi_open +p' > $DBFS/dynamic_debug/control
    echo 'func stmvbi_close +p' > $DBFS/dynamic_debug/control
    echo 'func vidioc_enum_output +p' > $DBFS/dynamic_debug/control
    echo 'func vidioc_s_output +p' > $DBFS/dynamic_debug/control
    echo 'func vidioc_g_output +p' > $DBFS/dynamic_debug/control
    echo 'func vidioc_g_fmt_sliced_vbi_out +p' > $DBFS/dynamic_debug/control
    echo 'func vidioc_s_fmt_sliced_vbi_out +p' > $DBFS/dynamic_debug/control
    echo 'func vidioc_try_fmt_sliced_vbi_out +p' > $DBFS/dynamic_debug/control
    echo 'func vidioc_reqbufs +p' > $DBFS/dynamic_debug/control
    echo 'func vidioc_querybuf +p' > $DBFS/dynamic_debug/control
    echo 'func vidioc_streamon +p' > $DBFS/dynamic_debug/control
    echo 'func vidioc_streamoff +p' > $DBFS/dynamic_debug/control
    echo 'func vidioc_g_sliced_vbi_cap +p' > $DBFS/dynamic_debug/control
}

################################################################################
# Enable Streaming Engine traces
function stv_enable_se_traces ()
{
    echo "Enabling SE Traces\n"
    echo 'file dvb_audio.c +p' > $DBFS/dynamic_debug/control
    echo 'file dvb_video.c +p' > $DBFS/dynamic_debug/control
    echo 'file backend.c +p' > $DBFS/dynamic_debug/control
    echo 'file display.c +p' > $DBFS/dynamic_debug/control
}

################################################################################
# Enable Transport Engine traces
function stv_enable_te_traces ()
{
    echo "Enabling TE Traces\n"
    echo 'file stm-demux-device.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm-demux-ioctl.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm-demux-utils.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm-dvb-demux-ctrl.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm-dvb-demux-device.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm-dvb-demux-ioctl.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm-dvb-psdemux-device.c +p' > $DBFS/dynamic_debug/control
}


################################################################################
# Enable Transcode traces
function stv_enable_transcode_traces ()
{
    echo "Enabling Transcode Traces\n"
    echo 'file stm_v4l2_tsmux.c +p' > $DBFS/dynamic_debug/control
}

################################################################################
# Enable HDMIRX traces
function stv_enable_hdmirx_traces ()
{
    echo "Enabling HDMIRx Traces\n"
    echo 'file stm_extin_hdmirx.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm_v4l2_dvp_subdev.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm_v4l2_dvp_capture.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm_v4l2_audio_ctrls.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm_v4l2_audio_decoder.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm_v4l2_video_ctrls.c +p' > $DBFS/dynamic_debug/control
    echo 'file stm_v4l2_video_decoder.c +p' > $DBFS/dynamic_debug/control
}

################################################################################
# Disable traces
function stv_disable_traces ()
{
    echo "Disabling Traces"
    echo 'module stmvout -p' > $DBFS/dynamic_debug/control
    echo 'module pseudocard -p' > $DBFS/dynamic_debug/control
    echo 'module stm_v4l2_hdmirx -p' > $DBFS/dynamic_debug/control
    echo 'module stm-c8jpg -p' > $DBFS/dynamic_debug/control
    echo 'module stlinuxtv -p' > $DBFS/dynamic_debug/control
    echo 'module stm_v4l2_encoder -p' > $DBFS/dynamic_debug/control
    echo 'module stm_v4l2_capture -p' > $DBFS/dynamic_debug/control
    echo 'module stm_v4l2_decoder -p' > $DBFS/dynamic_debug/control
    echo 'module stm_v4l2_tsmuxer -p' > $DBFS/dynamic_debug/control
    echo 'module stmvbi -p' > $DBFS/dynamic_debug/control
}

################################################################################
# Internal function to set traces
function enable_stv_trace ()
{
    stv_group=$1
    stv_level=$2

    ################################################################################
    # trace activation root path
    # Find mount point, if not found, mount and set root
    ################################################################################
    DBFS=$(mount | grep debugfs | head -n1 | cut -f3 -d' ')
    if [ ! $DBFS ]
    then
        mount -t debugfs none /sys/kernel/debug
        DBFS=/sys/kernel/debug
    fi

    case $stv_group in
        api|a)
	    stv_enable_api_traces
            ;;
        se|s)
	    stv_enable_se_traces
            ;;
        te|t)
	    stv_enable_te_traces
            ;;
        transcode|r)
	    stv_enable_transcode_traces
            ;;
        hdmirx|h)
	    stv_enable_hdmirx_traces
            ;;
        *)
            echo "process_stv_trace : group not supported\n"
	    usage_stv
	    return -1
            ;;
        esac

    stv_level_val=2

    # If no level is passed, then use Auto level
    if [ $# == 1 ]
    then
        stv_level_val=2
        echo "process_stv_trace : No level passed, using Auto value : $stv_level\n"
    else

        case $stv_level in
            DISABLE|D)
                stv_disable_traces
	        stv_level_val=0
	        return 0
                ;;
            LIGHT|L)
	        stv_level_val=1
                ;;
            AUTO|A)
	        stv_level_val=2
                ;;
            VERBOSE|V)
	        stv_level_val=3
                ;;
            EXTENSIVE|E)
	        stv_level_val=4
                ;;
            *)
	        usage_stv
                return -1
                ;;
        esac
    fi

}

################################################################################
# Function used by master script to get information on List of available STL traces
function process_stv_help ()
{
    usage_stv
}

################################################################################
# Function used by master script to enable / disable STL traces
# Arg 1 : Enable/Disbale Traces
function process_stv_trace()
{
    validate_stv_trace_inputs $@
    enable_stv_trace $@
}

